﻿using anketa.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace anketa.Models.Application
{
    public class SecuredMessage
    {
        /// <summary>
        /// Зашифрованное сообщение
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Имя структуры
        /// </summary>
        public string StructureName { get; set; }

        /// <summary>
        /// Зашифрованный симметричный ключ, которым зашифровано сообщение
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Возвращает зашифрованное сообщение как параметр URL
        /// </summary>
        /// <returns></returns>
        public string GetAsQueryParameter()
        {
            var data = Newtonsoft.Json.JsonConvert.SerializeObject(this);
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(data));
        }

        /// <summary>
        /// Возвращает зашифрованное сообщение как текст
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Создает защищенное сообщение из объекта
        /// </summary>
        /// <param name="originalMessage">Объект, который нужно передать</param>
        /// <returns>Возвращает защищенное сообщение</returns>
        public static SecuredMessage Create(object originalMessage, EncryptionKey key)
        {
            var data = Newtonsoft.Json.JsonConvert.SerializeObject(originalMessage);

            // Генерируем симметричный ключ и шифруем им сообщение 
            // Затем шифруем симметричный ключ асимметричным ключом и отдаем как зашифрованное сообщение
            var symmetricKey = DataSecurity.GeneratePassword(64);
            var msg = new SecuredMessage
            {
                Message = DataSecurity.EncryptMessage(data, symmetricKey),
                StructureName = originalMessage.GetType().Name,
                Key = DataSecurity.EncryptMessage(symmetricKey, key)
            };
            return msg;
        }

        /// <summary>
        /// Проверяет, является ли защищенное сообщение валидным
        /// </summary>
        /// <returns></returns>
        internal bool IsValid()
        {
            return Message != null && Key != null && StructureName != null;
        }

        /// <summary>
        /// Возвращает объект из зашифрованного сообщения
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object GetObject(EncryptionKey key)
        {
            var json = DataSecurity.DecryptMessage(Message, key);
            Type type = Type.GetType(StructureName);
            object obj = Newtonsoft.Json.JsonConvert.DeserializeObject(json, type);
            return obj;
        }
    }
}
