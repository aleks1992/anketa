﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddDeliveryShippingAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ShippingAddress",
                table: "DeliveryOrders",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DeliveryOrderId",
                table: "Orders",
                column: "DeliveryOrderId");

            migrationBuilder.UpdateData("Orders", "DeliveryOrderId", 0, "DeliveryOrderId", 1);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_DeliveryOrders_DeliveryOrderId",
                table: "Orders",
                column: "DeliveryOrderId",
                principalTable: "DeliveryOrders",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_DeliveryOrders_DeliveryOrderId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_DeliveryOrderId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ShippingAddress",
                table: "DeliveryOrders");
        }
    }
}
