﻿using anketa.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Response
{
    public class UserInfo
    {
        public bool IsAuthorized { get; set; }

        public string Username { get; set; }

        public CartWishlistStorage CartWishlistStorage { get; set; }
    }
}
