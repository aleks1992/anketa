﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class addPriceType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ProductPrices");

            migrationBuilder.AddColumn<string>(
                name: "PriceTypeId",
                table: "UserWishlistItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SerialNumber",
                table: "UserWishlistItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StockId",
                table: "UserWishlistItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PriceTypeId",
                table: "UserCartItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SerialNumber",
                table: "UserCartItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StockId",
                table: "UserCartItems",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PriceTypes",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    PriceTypeName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PriceTypes", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserWishlistItems_PriceTypeId",
                table: "UserWishlistItems",
                column: "PriceTypeId");

            migrationBuilder.CreateIndex(
                name: "IX_UserCartItems_PriceTypeId",
                table: "UserCartItems",
                column: "PriceTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserCartItems_PriceTypes_PriceTypeId",
                table: "UserCartItems",
                column: "PriceTypeId",
                principalTable: "PriceTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserWishlistItems_PriceTypes_PriceTypeId",
                table: "UserWishlistItems",
                column: "PriceTypeId",
                principalTable: "PriceTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserCartItems_PriceTypes_PriceTypeId",
                table: "UserCartItems");

            migrationBuilder.DropForeignKey(
                name: "FK_UserWishlistItems_PriceTypes_PriceTypeId",
                table: "UserWishlistItems");

            migrationBuilder.DropTable(
                name: "PriceTypes");

            migrationBuilder.DropIndex(
                name: "IX_UserWishlistItems_PriceTypeId",
                table: "UserWishlistItems");

            migrationBuilder.DropIndex(
                name: "IX_UserCartItems_PriceTypeId",
                table: "UserCartItems");

            migrationBuilder.DropColumn(
                name: "PriceTypeId",
                table: "UserWishlistItems");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "UserWishlistItems");

            migrationBuilder.DropColumn(
                name: "StockId",
                table: "UserWishlistItems");

            migrationBuilder.DropColumn(
                name: "PriceTypeId",
                table: "UserCartItems");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "UserCartItems");

            migrationBuilder.DropColumn(
                name: "StockId",
                table: "UserCartItems");

            migrationBuilder.CreateTable(
                name: "ProductPrices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    Price = table.Column<decimal>(nullable: false),
                    ProductId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProductPrices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ProductPrices_Products_ProductId",
                        column: x => x.ProductId,
                        principalTable: "Products",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ProductPrices_ProductId",
                table: "ProductPrices",
                column: "ProductId");
        }
    }
}
