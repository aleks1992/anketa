﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Собщения отправляемые пользователю
    /// </summary>
    public class UserEmailMessage
    {
        /// <summary>
        /// ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Тема сообщения
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Сообщения
        /// </summary>
        public string Meassage { get; set; }

        /// <summary>
        /// Дата добавления
        /// </summary>
        public DateTime DateCreated { get; set; }

        /// <summary>
        /// Дата отправки
        /// </summary>
        public DateTime DateSending { get; set; }

        /// <summary>
        /// Отметка об отправлений
        /// </summary>
        public bool IsSend { get; set; }



    }
}
