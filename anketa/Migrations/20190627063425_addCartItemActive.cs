﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class addCartItemActive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductPrice",
                table: "UserCartItems");

            migrationBuilder.DropColumn(
                name: "ProductUrl",
                table: "UserCartItems");

            migrationBuilder.DropColumn(
                name: "ProductUrl",
                table: "OrderProductItems");

            migrationBuilder.AddColumn<bool>(
                name: "ItemActive",
                table: "UserCartItems",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ItemActive",
                table: "UserCartItems");

            migrationBuilder.AddColumn<decimal>(
                name: "ProductPrice",
                table: "UserCartItems",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<string>(
                name: "ProductUrl",
                table: "UserCartItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProductUrl",
                table: "OrderProductItems",
                nullable: true);
        }
    }
}
