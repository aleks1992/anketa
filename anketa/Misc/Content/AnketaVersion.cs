﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.Content
{
    public class AnketaVersion
    {
        private string BuildVersion { get; set; }

        private string RunnedAt { get; set; }

        public AnketaVersion()
        {
            RunnedAt = DateTime.Now.ToString();

            const string versionFile = "Version.txt";

            if (File.Exists(versionFile)) BuildVersion = File.ReadAllText(versionFile);
            else BuildVersion = "unknown";
        }

        public override string ToString()
        {
            return $"Version: {BuildVersion}, runned at: {RunnedAt}";
        }
    }

    public static class AnketaVersionHelper
    {
        public static HtmlString RenderAnketaVersion(this IHtmlHelper helper)
        {
            var service = helper.ViewContext.HttpContext.RequestServices.GetService(typeof(AnketaVersion));
            if(service is AnketaVersion)
            {
                return new HtmlString(((AnketaVersion)service).ToString());
            }
            return null;
        }
    }        
}
