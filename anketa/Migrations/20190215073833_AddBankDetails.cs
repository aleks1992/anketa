﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddBankDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BankDetailsId",
                table: "UserIndividualFaces",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "BankDetailsId",
                table: "UserEntityFaces",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BankDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CheckingAccount = table.Column<string>(nullable: true),
                    BankName = table.Column<string>(nullable: true),
                    CorrespondentAccount = table.Column<string>(nullable: true),
                    BIC = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BankDetails", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserIndividualFaces_BankDetailsId",
                table: "UserIndividualFaces",
                column: "BankDetailsId");

            migrationBuilder.CreateIndex(
                name: "IX_UserEntityFaces_BankDetailsId",
                table: "UserEntityFaces",
                column: "BankDetailsId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserEntityFaces_BankDetails_BankDetailsId",
                table: "UserEntityFaces",
                column: "BankDetailsId",
                principalTable: "BankDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserIndividualFaces_BankDetails_BankDetailsId",
                table: "UserIndividualFaces",
                column: "BankDetailsId",
                principalTable: "BankDetails",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserEntityFaces_BankDetails_BankDetailsId",
                table: "UserEntityFaces");

            migrationBuilder.DropForeignKey(
                name: "FK_UserIndividualFaces_BankDetails_BankDetailsId",
                table: "UserIndividualFaces");

            migrationBuilder.DropTable(
                name: "BankDetails");

            migrationBuilder.DropIndex(
                name: "IX_UserIndividualFaces_BankDetailsId",
                table: "UserIndividualFaces");

            migrationBuilder.DropIndex(
                name: "IX_UserEntityFaces_BankDetailsId",
                table: "UserEntityFaces");

            migrationBuilder.DropColumn(
                name: "BankDetailsId",
                table: "UserIndividualFaces");

            migrationBuilder.DropColumn(
                name: "BankDetailsId",
                table: "UserEntityFaces");
        }
    }
}
