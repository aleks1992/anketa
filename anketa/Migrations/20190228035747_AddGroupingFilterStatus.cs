﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddGroupingFilterStatus : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FilterStatus",
                table: "OrderStatuses",
                nullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "OrderData",
                table: "Orders",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.UpdateData("OrderStatuses", "Name", "ReadyToSync", "FilterStatus", "opened");
            migrationBuilder.UpdateData("OrderStatuses", "Name", "Sended", "FilterStatus", "opened");
            migrationBuilder.UpdateData("OrderStatuses", "Name", "Working", "FilterStatus", "opened");
            migrationBuilder.UpdateData("OrderStatuses", "Name", "Cancelled", "FilterStatus", "cancelled");
            migrationBuilder.UpdateData("OrderStatuses", "Name", "Done", "FilterStatus", "closed");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FilterStatus",
                table: "OrderStatuses");

            migrationBuilder.AlterColumn<string>(
                name: "OrderData",
                table: "Orders",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
