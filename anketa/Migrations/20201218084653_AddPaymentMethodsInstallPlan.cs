﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace anketa.Migrations
{
    public partial class AddPaymentMethodsInstallPlan : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData("OrderPaymentMethods", "Id", 10, "ForIndividualFace", false);

            // Добавляем новые способы оплаты для физ. лиц
            migrationBuilder.InsertData("OrderPaymentMethods", new string[] { "Id", "Guid", "Name", "ForIndividualFace" },
                new object[,]
                {
                    { 11, Guid.NewGuid(), "Покупка в рассрочку (комиссия не менее 3%)", true }
                });

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData("OrderPaymentMethods", "Id", 10, "ForIndividualFace", true);
            migrationBuilder.DeleteData("OrderPaymentMethods", "Id", 11);

        }
    }
}
