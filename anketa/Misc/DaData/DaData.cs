﻿using anketa.Misc.Yandex;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static anketa.Misc.SiteClient;


namespace anketa.Misc.DaData
{
    public class DaData
    {

        /// <summary>
        /// Токен доступа
        /// </summary>
        private const string KEY = "ca97758aa618f0a02b0d6c74df8b17872fd9a9c3";

        /// <summary>
        /// Url сайта
        /// </summary>
        private const string URL = "https://suggestions.dadata.ru/suggestions/api/4_1/rs/";

        /// <summary>
        /// Метод Получения городов
        /// </summary>
        private const string METHOD_CITY = "suggest/address";

        /// <summary>
        /// Метод получения города по IP
        /// </summary>
        private const string METHOD_IP = "iplocate/address";

        /// <summary>
        /// Данные для одного результат
        /// </summary>
        [JsonProperty("data")]
        public DaDataResponse Data { get; set; }

        /// <summary>
        /// Результат обработки
        /// </summary>
        [JsonProperty("suggestions")]
        public List<DaDataResponse> Suggestions { get; set; }

        /// <summary>
        /// Получения Заголовка с токеном доступом
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, string> GetHeader()
        {
            var headers = new Dictionary<string, string>();
            headers.Add("Authorization", "Token " + KEY);

            return headers;
        }


        /// <summary>
        /// Получения город по IP
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static DaData GetCityIP(string ip)
        {
            string url = URL + METHOD_IP;
            object data = new
            {
                ip = ip
            };

            string query = GetQueryString(data);

            var response = HttpClient(data, url + query, "GET", inAnswerResponse: false, headers: GetHeader());

            return JsonConvert.DeserializeObject<DaData>(response.Data.ToString());

        }


        /// <summary>
        /// Получения модели адрессов 
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public static DaData GetCity(string query, int count = 10, bool isCountryAll = true)
        {
            var fromBound = new Dictionary<string, string>();
            fromBound.Add("value", "city");

            var toBound = new Dictionary<string, string>();
            toBound.Add("value", "settlement");

            var locations = new Dictionary<string, string>();
            locations.Add("value", "*");

            object data = new
            {
                count = count,
                query = query,
                from_bound = fromBound,
                to_bound = toBound,
                restrict_value = false,
                locations = locations
            };

            string url = URL + METHOD_CITY;

            var response = HttpClient(data, url, "POST", inAnswerResponse: false, headers: GetHeader());


            return JsonConvert.DeserializeObject<DaData>(response.Data.ToString());

        }

        public List<YandexCity> GetCityList()
        {
            var cityList = new List<YandexCity>();

            foreach(var item in Suggestions)
            {
                if (string.IsNullOrEmpty(item.Data.Lat))
                    item.Data.Lat = "0";
                if (string.IsNullOrEmpty(item.Data.Lon))
                    item.Data.Lon = "0";

                cityList.Add(new YandexCity
                {
                    City = item.Data.City,
                    CodeIsoCountry = item.Data.CodeIsoCountry,
                    Country = item.Data.Country,
                    Fias = item.Data.Fias,
                    Lat = Convert.ToDouble(item.Data.Lat.Replace(".", ",")),
                    Lon = Convert.ToDouble(item.Data.Lon.Replace(".", ",")),
                    Label = item.Data.Label
                });

            }

            return cityList;
        }

    }


}
