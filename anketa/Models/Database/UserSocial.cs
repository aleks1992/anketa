﻿using anketa.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models
{
    /// <summary>
    /// Авторизация через социальные сети
    /// </summary>
    public class UserSocial
    {
        /// <summary>
        /// Ид элимента
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        ///  Пользователь 
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Ид пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Социальная сеть
        /// </summary>
        public string Social { get; set; }
    }
}
