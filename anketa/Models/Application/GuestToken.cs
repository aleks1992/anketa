﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application
{
    /// <summary>
    /// Представляет собой токен гостя
    /// </summary>
    public class GuestToken
    {
        public string GuestId { get; set; }
    }
}
