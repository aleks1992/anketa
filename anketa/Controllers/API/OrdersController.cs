﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using anketa.Misc;
using anketa.Misc.Content;
using anketa.Models;
using anketa.Models.API;
using anketa.Models.API.Response;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace anketa.Controllers.API
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "API-Orders")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        DatabaseContext DbContext;
        public OrdersController(DatabaseContext dbContext)
        {
            DbContext = dbContext;
        }

        /// <summary>
        /// Возвращает список заказов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            DateTime edgeModifyTime = DateTime.Now.AddMinutes(-5);
            var orders = DbContext.Orders
                .Include(p => p.Status)
                .Include(p => p.OrderPaymentMethod)
                .Where(p => !p.Uploaded && p.ModifyDate < edgeModifyTime).ToList();

            foreach(var o in orders)
            {
                // Подгружаем связанные данные
                await LoadFaceReferences(o, p => p.OrdererFace);
                await LoadFaceReferences(o, p => p.PayerFace);
                await LoadFaceReferences(o, p => p.DocumentsReceiver);
                await LoadFaceReferences(o, p => p.ProductReceiver);

                //await DbContext.Entry(o).Reference(p => p.DocumentsReceiverAddress).LoadAsync();
                //await DbContext.Entry(o).Reference(p => p.ProductReceiverAddress).LoadAsync();

                // Добавляем товары из заказов
                o.Products = DbContext.OrderProductItems
                    .Include(p => p.Product)
                    .Include(p => p.Project)
                    .Where(p => p.OrderId == o.Id).ToList();

                // Добавляем информацию о доставке
                o.DeliveryOrder = await DbContext.DeliveryOrders.FirstOrDefaultAsync(p => p.Id == o.DeliveryOrderId);

                foreach(var p in o.Products) p.Order = null;
            }

            return Ok(orders);
        }

        /// <summary>
        /// Загружает ссылки связанных объектов
        /// </summary>
        /// <param name="order">Заказ</param>
        /// <param name="expression">Параметр</param>
        /// <returns></returns>
        private async Task LoadFaceReferences(Order order, Expression<Func<Order, UserFace>> expression)
        {
            Func<Order, UserFace> func = expression.Compile();
            await DbContext.Entry(order).Reference(expression).LoadAsync();
            await DbContext.Entry(func(order)).Reference(p => p.EntityFace).LoadAsync();
            if(func(order).EntityFace != null)
            {
                await DbContext.Entry(func(order).EntityFace).Reference(p => p.ActOnBase).LoadAsync();
                await DbContext.Entry(func(order).EntityFace).Reference(p => p.LegalForm).LoadAsync();
                await DbContext.Entry(func(order).EntityFace).Reference(p => p.Head).LoadAsync();
                await DbContext.Entry(func(order).EntityFace).Reference(p => p.ActFromOrgFace).LoadAsync();
                await DbContext.Entry(func(order).EntityFace).Reference(p => p.BankDetails).LoadAsync();
                if (func(order).EntityFace.Head != null)
                {
                    await DbContext.Entry(func(order).EntityFace.Head).Reference(p => p.BankDetails).LoadAsync();
                }
            }

            await DbContext.Entry(func(order)).Reference(p => p.IndividualFace).LoadAsync();
            if(func(order).IndividualFace != null)
            {
                await DbContext.Entry(func(order).IndividualFace).Reference(p => p.BankDetails).LoadAsync();
            }
        }

        /// <summary>
        /// Обновляет информацию о заказах
        /// </summary>
        /// <param name="ordersData"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]List<OrderDataApi> ordersData, [FromServices]ApplicationCache appCache)
        {
            var response = new OrderUpdateApi
            {
                OrdersUpdated = false
            };

            foreach(var orderData in ordersData)
            {
                var order = await DbContext.Orders.Include(p => p.Status).FirstOrDefaultAsync(p => p.Id == orderData.Id);
                if (order == null) continue;

                order.Uploaded = orderData.Uploaded;

                if(orderData.Status != null)
                {
                    var status = await DbContext.OrderStatuses.FirstOrDefaultAsync(p => p.Name == orderData.Status.Name);
                    order.Status = status;
                }

                order.StatusComment = orderData.StatusComment;

                DbContext.Orders.Update(order);
                response.OrdersUpdated = true;

                // Очищаем кэш
                await appCache.RemoveAsync(Controllers.OrdersController.USER_ORDERS_DATA + order.UserId.ToString());
            }

            await DbContext.SaveChangesAsync();

            return Ok(response);
        }
    }
}