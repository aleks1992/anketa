﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.Yandex
{
    public class YandexData
    {
        /// <summary>
        /// 
        /// </summary>
        public ResponseYnadex Response { get; set; }

        /// <summary>
        /// Получиния списка городов
        /// </summary>
        /// <param name="isIgnoreRu"></param>
        /// <returns></returns>
        public List<YandexCity> GetCityList(bool isIgnoreRu = true)
        {
            var listYandexCity = new List<YandexCity>();
            foreach (var item in Response.GeoObjectCollection.FeatureMember)
            {
                var addressDatails = item.GeoObject.MetaDataProperty.GeocoderMetaData.AddressDetails;
                var kind = item.GeoObject.MetaDataProperty.GeocoderMetaData.Kind;
                var country = addressDatails.Country.CountryName;
                var countryCode = addressDatails.Country.CountryNameCode;
                var addressLine = addressDatails.Country.AddressLine;

                if ((isIgnoreRu && countryCode == "RU")
                    || string.IsNullOrEmpty(addressLine)
                    || (kind != "locality" && isIgnoreRu))
                    continue;
                var pos = item.GeoObject.Point.Get();
                listYandexCity.Add(new YandexCity
                {
                    Country = country,
                    City = item.GeoObject.MetaDataProperty.GeocoderMetaData.Address.Components[1].Name,
                    CodeIsoCountry = countryCode,
                    Fias = "",
                    Label = addressLine,
                    Lat = pos.Lat,
                    Lon = pos.Lon
                });
            }

            return listYandexCity;
        }
    }
}
