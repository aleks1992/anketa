﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.MessageServices
{
    public interface IEmailService
    {
        /// <summary>
        /// Отправляет письмо на электронную почту
        /// </summary>
        /// <param name="receiver">Получатель</param>
        /// <param name="subject">Тема письма</param>
        /// <param name="body">Тело письма</param>
        Task<bool> SendMessage(string receiver, string subject, string body, bool isSave = true);
    }
}
