﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// способы оплаты
    /// </summary>
    [Serializable]
    public class OrderPaymentMethods
    {
        /// <summary>
        /// Ид способа оплаты
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Guid способа оплаты
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Способ оплаты
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Это способ для физического лица
        /// </summary>
        public bool ForIndividualFace { get; set; }

        /// <summary>
        /// Это способ для юоидического лица
        /// </summary>
        public bool ForEntityFace { get; set; }
    }
}
