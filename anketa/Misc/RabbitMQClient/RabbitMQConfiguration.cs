﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc
{
    /// <summary>
    /// RabbitMQ конфигурации
    /// </summary>
    public class RabbitMQConfiguration
    {
        /// <summary>
        /// Логин
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Host
        /// </summary>
        public string VirtualHost { get; set; }

        /// <summary>
        /// Url
        /// </summary>
        public string HostName { get; set; }

        /// <summary>
        /// Порт
        /// </summary>
        public int Port { get; set; }
    }
}
