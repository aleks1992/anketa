﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace anketa.Misc
{
    public class ModelEngine
    {
        private static Dictionary<Type, ClassInformation> ClassesInfo = null;
        private List<Type> registeredModels = new List<Type>();

        public static void UpdateClassesInfo()
        {
            ClassesInfo = new Dictionary<Type, ClassInformation>();
            var q = from t in Assembly.GetExecutingAssembly().GetTypes()
                    where t.IsClass && t.ReflectedType == null && t.Namespace != null && t.Namespace.StartsWith("anketa.Models.")
                    select t;

            var list = q.ToList();

            foreach (var t in q.ToList())
            {
                string name = t.FullName;

                var classInfo = new ClassInformation(t);
                if (ClassesInfo.ContainsKey(t)) ClassesInfo.Remove(t);
                ClassesInfo.Add(t, classInfo);
            }
        }

        public ModelEngine()
        {

        }

        public void RegisterModels(params Type[] types)
        {
            registeredModels.Clear();
            registeredModels.AddRange(types);
        }

        public object PutModels()
        {
            if(registeredModels.Count > 0)
            {
                var output = new List<string>();

                foreach(var k in registeredModels)
                {
                    if(ClassesInfo.ContainsKey(k))
                    {
                        output.Add(string.Format("window.appData.models.{0} = {1};", k.Name.Substring(0, 1).ToLower() + k.Name.Substring(1), ClassesInfo[k].GetClassData()));
                    }
                }

                return string.Join('\n', output);
            }
            return null;
        }

        public bool HasModels
        {
            get
            {
                return registeredModels.Count > 0;
            }
        }

        public class ClassInformation
        {
            private string classData = null;

            public ClassInformation(Type type)
            {
                var info = new Dictionary<string, object>();
                foreach(var property in type.GetProperties())
                {
                    var data = new Dictionary<string, object>();
                    data.Add("DefaultValue", GetDefaultValue(property.PropertyType));

                    foreach(var customAttribute in property.CustomAttributes)
                    {
                        if(customAttribute.AttributeType == typeof(System.ComponentModel.DataAnnotations.DisplayAttribute))
                        { 
                            foreach(var namedArgument in customAttribute.NamedArguments)
                            {
                                if (namedArgument.TypedValue.Value is string argumentValue) data.Add(namedArgument.MemberName, argumentValue);
                            }
                        }
                        else if(customAttribute.AttributeType == typeof(System.ComponentModel.DefaultValueAttribute) && customAttribute.ConstructorArguments.Count > 0)
                        {
                            data["DefaultValue"] = customAttribute.ConstructorArguments[0].Value;
                        }
                    }

                    info.Add(property.Name, data);
                }

                classData = JsonConvert.SerializeObject(info);
            }

            public string GetClassData()
            {
                return classData;
            }

            private object GetDefaultValue(Type t)
            {
                if (t.IsValueType) return Activator.CreateInstance(t);
                return null;
            }
        }
    }
}
