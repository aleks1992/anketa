﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using anketa.Misc.Validators;
using anketa.Models.Application.Forms;
using Newtonsoft.Json;

namespace anketa.Models.Database
{
    [Serializable]
    public class UserEntityFace
    {
        /// <summary>
        /// ID юридического лица
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// ID пользователя, создавшего юридическое лицо
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Пользователь, создавший юридическое лицо
        /// </summary>
        [JsonIgnore]
        public User User { get; set; }

        [Display(Name = "Организацинно-правовая форма")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Выберите организационно-правовую форму.")]
        public LegalForm LegalForm { get; set; }

        /// <summary>
        /// ID ОПФ
        /// </summary>
        [Display(Name = "Организацинно-правовая форма")]
        [JsonIgnore]
        public int LegalFormId { get; set; }

        /// <summary>
        /// Полное наименование
        /// </summary>
        [Display(Name = "Полное наименование")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите название организации.")]
        public string FullName { get; set; }

        /// <summary>
        /// Сокращенное наименование
        /// </summary>
        [Display(Name = "Сокращенное наименование")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите сокращенное название организации.")]
        public string ShortName { get; set; }

        /// <summary>
        /// Руководитель
        /// </summary>
        [Display(Name = "Руководитель")]
        public UserIndividualFace Head { get; set; }

        /// <summary>
        /// ID руководителя
        /// </summary>
        [JsonIgnore]
        public int HeadId { get; set; }

        /// <summary>
        /// Действует в лице
        /// </summary>
        [Display(Name = "Действует в лице")]
        public bool ActFromOrganization { get; set; }

        /// <summary>
        /// Действует в лице
        /// </summary>
        [Display(Name = "Действует в лице")]
        public UserIndividualFace ActFromOrgFace { get; set; }

        /// <summary>
        /// Ид действующего от организации
        /// </summary>
        [JsonIgnore]
        public int? ActFromOrgFaceId { get; set; }

        /// <summary>
        /// Действует на основании
        /// </summary>
        [Display(Name = "Действует на основании")]
        public ActOnBase ActOnBase { get; set; }

        /// <summary>
        /// ID основания
        /// </summary>
        [JsonIgnore]
        public int? ActOnBaseId { get; set; }

        /// <summary>
        /// Серия, номер документа
        /// </summary>
        [Display(Name = "Серия, номер документа")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Заполните серию и номер документа.")]
        public string ActOnBaseDocumentSerialNumber { get; set; }

        /// <summary>
        /// Дата документа
        /// </summary>
        [Display(Name = "Дата документа")]
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Заполните дату выдачи документа.")]
        [DateRangeValidator(minDate: "1900-01-01")]
        public DateTime ActOnBaseDocumentDate { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        [Display(Name = "ИНН")]
        [ITINValidator(0, ErrorMessage = "ИНН заполнен некорректно.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите ИНН\\уникальный код.")]
        public string ITIN { get; set; }

        /// <summary>
        /// ОГРН
        /// </summary>
        [Display(Name = "ОГРН")]
        [PSRNValidator(ErrorMessage = "ОГРН заполнен некорректно.")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите ОГРН.")]
        public string PSRN { get; set; }

        /// <summary>
        /// Дополнительная информация
        /// </summary>
        [Display(Name = "Дополнительная информация")]
        [DataType(DataType.MultilineText)]
        public string AdditionalInfo { get; set; }

        /// <summary>
        /// Контактный номер
        /// </summary>
        [Display(Name = "Контактные номера")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите хотя бы один контактный номер")]
        public string Phones { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите электронную почту.")]
        public string Email { get; set; }

        /// <summary>
        /// Показывает, является ли контрагент иностранным лицом
        /// </summary>
        [Display(Name = "Иностранное лицо")]
        public bool IsForeignFace { get; set; }
        
        /// <summary>
        /// Банковские реквизиты
        /// </summary>
        public BankDetails BankDetails { get; set; }

        /// <summary>
        /// Ид банковских реквизитов
        /// </summary>
        [JsonIgnore]
        public int? BankDetailsId { get; set; }

        [Display(Name = "Выберите страну")]
        [DefaultValue("RU")]
        public string Country { get; set; }

        /// <summary>
        /// Это основной контрагент
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Устанавливает пустую строку non-nullable атрибутам
        /// </summary>
        internal void Disnull()
        {
            foreach(var prop in this.GetType().GetProperties())
            {
                if (prop.CustomAttributes.Count(p => p.AttributeType == typeof(RequiredAttribute)) > 0)
                {
                    if (prop.PropertyType == typeof(string) && (string)prop.GetValue(this) == null)
                        prop.SetValue(this, "");
                }
            }
        }

        /// <summary>
        /// Возвращает имя лица
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return FullName;
        }
    }
}
