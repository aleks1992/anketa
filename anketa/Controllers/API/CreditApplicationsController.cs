﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Models;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace anketa.Controllers.API
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "API-Orders")]
    //[AllowAnonymous]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CreditApplicationsController : ControllerBase
    {
        private readonly DatabaseContext DbContext;

        public CreditApplicationsController(DatabaseContext dbContext)
        {
            DbContext = dbContext;
        }

        /// <summary>
        /// Возвращает список кредитных заявок
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var applications = DbContext.CreditApplications
                .Where(p => !p.Uploaded)
                .ToList();

            foreach(var application in applications)
            {
                var q = from p in DbContext.CreditApplicationProducts
                        where p.Id == application.Id
                        select p.Name;

                application.ProductsViews = q.ToList();
            }

            return Ok(applications);
        }
        
        /// <summary>
        /// Изменяет статус заявок
        /// </summary>
        /// <param name="creditApplications"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]List<CreditApplication> creditApplications)
        {
            bool updated = false;

            if (creditApplications.Count > 0)
            {
                foreach (var credit in creditApplications)
                {
                    var entity = DbContext.CreditApplications.FirstOrDefault(p => p.Id == credit.Id);
                    if (entity != null)
                    {
                        entity.Uploaded = credit.Uploaded;
                        DbContext.CreditApplications.Update(entity);
                        if (!updated) updated = true;
                    }
                }
                await DbContext.SaveChangesAsync();
            }

            return Ok(new
            {
                CreditApplicationsUpdated = updated
            });            
        }
    }
}