﻿import Vue from 'vue/dist/vue.esm.js'

import Wishlist from './wishlist.vue'

new Vue({
    el: '#app',
    render: h => h(Wishlist),
});