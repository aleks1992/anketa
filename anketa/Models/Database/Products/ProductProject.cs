﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database.Products
{
    [Serializable]
    public class ProductProject
    {
        /// <summary>
        /// ID проекта
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// GUID проекта
        /// </summary>
        public Guid Guid { get; set; }

        /// <summary>
        /// Номер проекта
        /// </summary>
        public string ProjectNumber { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [JsonIgnore]
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        [JsonIgnore]
        public string Phone { get; set; }

        /// <summary>
        /// Артикул проекта
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Наименование проекта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Цена проекта
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Показывает, что проект в корзине
        /// </summary>
        [NotMapped]
        public bool InCart { get; set; }

        /// <summary>
        /// Список почтовых ящиков
        /// </summary>
        [NotMapped]
        public List<string> Emails { get; set; }

        /// <summary>
        /// Номера телефонов
        /// </summary>
        [NotMapped]
        public List<string> Phones { get; set; }
    }
}
