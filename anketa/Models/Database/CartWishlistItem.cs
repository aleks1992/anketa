﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    // TODO: описать классы

    public class CartWishlistStorage
    {
        public CartWishlistStorage()
        {
            Cart = new List<CartWishlistItem>();
            Wishlist = new List<CartWishlistItem>();
        }

        public List<CartWishlistItem> Cart { get; set; }
        public List<CartWishlistItem> Wishlist { get; set; }

        public int CartCount
        {
            get
            {
                return Cart.Sum(p => p.Count);
            }
        }

        public int WishlistCount
        {
            get
            {
                return Wishlist.Sum(p => p.Count);
            }
        }
    }

    public class CartWishlistItem
    {
        public int Id { get; set; }

        public string Guid { get; set; }

        public int Count { get; set; }

        public bool? ByOrder { get; set; }

        public decimal Price { get; set; }

        public string PriceTypeId { get; set; }

        public string PriceTypeName { get; set; }

        public string StockId { get; set; }

        public string SerialNumber { get; set; }

        public bool? ContainsInOthers { get; set; }

        public ItemType Type { get; set; } = ItemType.Product;

        public string Name { get; set; }

        public int CategoryID { get; set; }

        public string Prefix { get; set; }

        /// <summary>
        /// Показывает, разрешена ли продажа индивидуальным лицам
        /// </summary>
        public int AllowBuyByIndividualFace { get; set; }

        public enum ItemType
        {
            // Не менять порядок элементов, новые вставлять в конец
            Product, // by default
            Project
        }
    }
}
