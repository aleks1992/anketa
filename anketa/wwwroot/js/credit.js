﻿(function () {
    var creditPercent = 0.11; // процентная ставка

    var config = app.credit;

    if (!config.isCredit) {
        document.getElementById('credit-period-container').classList.add('hidden');
    }

    var getSum = function () {
        return +document.getElementById("Sum").value.replace(/[^\d]/g, '');
    };

    var calc = function () {
        var sum = getSum();

        var firstPayElem = document.getElementById("FirstPay");
        var periodElem = document.getElementById("CreditPeriod");

        var firstPay = sum * (+firstPayElem.value / 100);
        if (firstPay === 0) firstPay = 1;
        else firstPay = firstPay.toFixed(0);

        var payment = ((sum - firstPay) * (1 + creditPercent)) / +periodElem.value;
        payment = payment.toFixed(0);

        document.getElementById("FirstPaySum").value = firstPay + ' руб.';
        document.getElementById("AverageMonthlyPayment").value = payment + ' руб.';
    };

    var bindSlider = function (containerId, inputId, start, range, onUpdate) {
        var elem = document.getElementById(containerId);
        var inputElem = document.getElementById(inputId);
        if (elem === null || inputElem === null) return;

        noUiSlider.create(elem, {
            start: start,
            step: 1,
            connect: true,
            range: range
        });

        elem.noUiSlider.on('update', function (values, handle) {
            inputElem.value = +values[handle];
            if (onUpdate !== null) onUpdate();
        });

        inputElem.addEventListener('change', function () {
            elem.noUiSlider.set(this.value);
        });
    };

    bindSlider('first-pay', 'FirstPay', 10, {
        min: 0,
        max: 90
    }, function () {
        calc();
    });

    bindSlider('credit-period', 'CreditPeriod', config.period, {
        min: 3,
        max: 36
    }, function () {
        calc();
    });

    document.addEventListener('DOMContentLoaded', function () {
        calc();
    });
})();