﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Forms
{
    public class RestorePasswordForm
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите ваш email.")]
        [Display(Name = "Email", Description = "Ваш email")]
        public string Email { get; set; }
    }
}
