﻿using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace anketa.Misc.MessageServices
{
    public class BatutMasterSMSService : ISmsService
    {
        public bool SendMessage(string receiver, string content)
        {
            try
            {
                var result = false;
                var smsData = new SmsData
                {
                    Type = "sms",
                    Message = content,
                    Receivers = new string[] { receiver },
                };

                var requestBody = JsonConvert.SerializeObject(smsData);
                using (var rabbitClient = new RabbitMQClient()) result = rabbitClient.PublishMessage("bm.messages", Encoding.UTF8.GetBytes(requestBody));
                return result;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return false;
        }
    }
}
