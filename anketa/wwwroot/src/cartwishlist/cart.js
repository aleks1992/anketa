﻿import Vue from 'vue/dist/vue.esm.js'

import fetch from 'isomorphic-fetch'
import Cart from './cart.vue'

new Vue({
    el: '#app',
    render: h => h(Cart),
});