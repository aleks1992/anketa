﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddNameAndTypeToOrderItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "OrderProductItems",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "OrderProductItems",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "OrderProductItems");

            migrationBuilder.DropColumn(
                name: "Type",
                table: "OrderProductItems");
        }
    }
}
