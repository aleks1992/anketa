﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddPaymentMethodToOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "OrderProductItems");

            migrationBuilder.AddColumn<int>(
                name: "OrderPaymentMethodId",
                table: "Orders",
                nullable: false,
                defaultValue: 1);

            migrationBuilder.CreateTable(
                name: "OrderPaymentMethods",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Guid = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderPaymentMethods", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OrderPaymentMethodId",
                table: "Orders",
                column: "OrderPaymentMethodId");

            migrationBuilder.InsertData("OrderPaymentMethods", new string[] { "Id", "Guid", "Name" },
                new object[,] {
                    { 1, Guid.NewGuid(), "Не указан" },
                    { 2, Guid.NewGuid(), "Собственные средства" },
                    { 3, Guid.NewGuid(), "Кредитные средства" },
                    { 4, Guid.NewGuid(), "Кредитные и собственные средства" }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_OrderPaymentMethods_OrderPaymentMethodId",
                table: "Orders",
                column: "OrderPaymentMethodId",
                principalTable: "OrderPaymentMethods",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_OrderPaymentMethods_OrderPaymentMethodId",
                table: "Orders");

            migrationBuilder.DropTable(
                name: "OrderPaymentMethods");

            migrationBuilder.DropIndex(
                name: "IX_Orders_OrderPaymentMethodId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "OrderPaymentMethodId",
                table: "Orders");

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "OrderProductItems",
                nullable: true);
        }
    }
}
