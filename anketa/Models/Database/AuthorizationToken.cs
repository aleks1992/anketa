﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Токен авторизации связанных сервисов
    /// </summary>
    public class AuthorizationToken
    {
        /// <summary>
        /// ID токена
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Токен доступа
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Токен обновления
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// Индификатор пользователя
        /// </summary>
        public string Identifier { get; set;}

        /// <summary>
        /// Пользователь, которому принадлежит токен
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// ID пользователя, которому принадлежит токен
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Хост, которому назначен токен
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Дата отключения токена
        /// </summary>
        public DateTime Expires { get; set; }
    }
}
