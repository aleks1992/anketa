﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application
{
    public class AnketaFormRequest
    {
        /// <summary>
        /// Имя формы
        /// </summary>
        public string FormName { get; set; }
    }
}
