﻿using anketa.Controllers;
using anketa.Models;
using anketa.Models.API;
using anketa.Models.Application;
using anketa.Models.Application.Request;
using anketa.Models.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Server.HttpSys;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace anketa.Misc.Content
{
    public class ClientConfirmedContacts
    {
        #region Константы

        const string SESSION_CONFIRMED_CONTACTS = "CONFIRMED_CONTACTS";

        const string SESSION_VERIFICATION_CODES = "VERIFICATION_CODES";

        #endregion

        #region Внешние зависимости

        private readonly HttpContext _httpContent;

        private readonly DatabaseContext _dbContext;

        private readonly Limits _limits;

        #endregion

        /// <summary>
        /// Подтвержденные контакты
        /// </summary>
        private List<UserConfirmedContact> confirmedContacts;

        /// <summary>
        /// Запросы кодов подтверждения
        /// </summary>
        private List<ConfirmationCodeRequest> confirmationCodeRequests;

        private bool isDataLoaded = false;

        public ClientConfirmedContacts(IHttpContextAccessor httpContextAccessor, DatabaseContext dbContext, Limits limits)
        {
            _httpContent = httpContextAccessor.HttpContext;
            _dbContext = dbContext;
            _limits = limits;
        }

        #region Загрузка и сохранение данных

        /// <summary>
        /// Загружает данные из сессии
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        private T LoadDataFromSession<T>(string key)
        {
            var data = _httpContent.Session.GetString(key);
            return data != null ? JsonConvert.DeserializeObject<T>(data) : Activator.CreateInstance<T>();
        }

        /// <summary>
        /// Загружает данные из сессии
        /// </summary>
        public async Task LoadDataAsync()
        {
            // Загружаем подтвержденные контакты пользователя
            confirmedContacts = LoadDataFromSession<List<UserConfirmedContact>>(SESSION_CONFIRMED_CONTACTS);
            if(_httpContent.User.Identity.IsAuthenticated)
            {
                // Пользователь авторизован, получаем дополнительно контакты из БД
                var user = await _dbContext.GetUser(_httpContent.User);

                if (user != null)
                {
                    try
                    {
                        // Получаем номера телефонов
                        confirmedContacts.AddRange(_dbContext.UserConfirmedPhones.Where(p => p.UserId == user.ID).Select(p => new UserConfirmedContact
                        {
                            Contact = p.Phone,
                            Type = UserConfirmedContact.ContactType.Phone,
                            UserId = user.ID,
                        }));
                    } 
                    catch(Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }

                    try
                    {
                        // Получаем электронные почты
                        confirmedContacts.AddRange(_dbContext.UserConfirmedEmails.Where(p => p.UserId == user.ID).Select(p => new UserConfirmedContact
                        {
                            Contact = p.Email,
                            Type = UserConfirmedContact.ContactType.Email,
                            UserId = user.ID,
                        }));
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }

            // Загружаем запросы кодов клиента
            confirmationCodeRequests = LoadDataFromSession<List<ConfirmationCodeRequest>>(SESSION_VERIFICATION_CODES);
        }

        public async Task CheckData()
        {
            if (!isDataLoaded) await LoadDataAsync();
        }

        /// <summary>
        /// Сохраняет данные в сессию
        /// </summary>
        public void SaveContactsToSession()
        {
            _httpContent.Session.SetString(SESSION_CONFIRMED_CONTACTS, JsonConvert.SerializeObject(confirmedContacts));
            _httpContent.Session.SetString(SESSION_VERIFICATION_CODES, JsonConvert.SerializeObject(confirmationCodeRequests));
        }

        #endregion

        #region Проверка и добавление подтвержденных контактов

        /// <summary>
        /// Проверяет, подтвержден ли контакт
        /// </summary>
        /// <param name="codeRequest"></param>
        /// <returns></returns>
        public async Task<bool> ContactIsConfirmed(ConfirmationCodeRequest codeRequest)
        {
            if (codeRequest.IsPhone()) return await PhoneIsConfirmed(codeRequest.Phone);
            else if (codeRequest.IsEmail()) return await EmailIsConfirmed(codeRequest.Email);
            return false;
        }

        /// <summary>
        /// Проверяет, подтвержден ли номер телефона
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public async Task<bool> PhoneIsConfirmed(string phone)
        {
            await CheckData();

            phone = DataManager.PreparePhoneNumber(phone, true);
            if (string.IsNullOrWhiteSpace(phone)) return false;
            return confirmedContacts.FirstOrDefault(p => p.Type == UserConfirmedContact.ContactType.Phone && p.Contact.TrimStart('+') == phone) != null;
        }

        /// <summary>
        /// Проверяет, подтверждена ли электронная почта
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<bool> EmailIsConfirmed(string email)
        {
            await CheckData();

            if (string.IsNullOrWhiteSpace(email)) return false;
            return confirmedContacts.FirstOrDefault(p => p.Type == UserConfirmedContact.ContactType.Email && p.Contact.ToLower().Trim() == email.ToLower().Trim()) != null;
        }

        /// <summary>
        /// Подтверждает контакт клиента
        /// </summary>
        /// <param name="codeRequest"></param>
        public async Task ConfirmContact(ConfirmationCodeRequest codeRequest)
        {
            await CheckData();

            confirmedContacts.Add(codeRequest.GetConfirmedContact());
            SaveContactsToSession();
        }

        /// <summary>
        /// Подтверждает номер телефона
        /// </summary>
        /// <param name="phone"></param>
        public async Task<bool> ConfirmPhoneContact(string phone)
        {
            await CheckData();

            phone = DataManager.PreparePhoneNumber(phone);
            if (!string.IsNullOrEmpty(phone) && phone != "+")
            {
                confirmedContacts.Add(new UserConfirmedContact
                {
                    Contact = DataManager.PreparePhoneNumber(phone),
                    Type = UserConfirmedContact.ContactType.Phone
                });

                SaveContactsToSession();

                return true;
            }

            return false;
        }

        public async Task<bool> ConfirmEmailContact(string email)
        {
            await CheckData();

            if (!string.IsNullOrWhiteSpace(email))
            {
                confirmedContacts.Add(new UserConfirmedContact
                {
                    Contact = email.ToLower().Trim(),
                    Type = UserConfirmedContact.ContactType.Email
                });

                SaveContactsToSession();

                return true;
            }

            return false;
        }

        #endregion

        #region Подтверждение контактов

        public bool RequestConfirmationCode(ConfirmationCodeRequest request, out string message)
        {
            message = null;

            request.Normalize();
            request.VerificationCode = "";

            var success = false;

            if (request.IsPhone()) success = RequestConfirmationCodeToPhone(request, out message);
            else if (request.IsEmail()) success = RequestConfirmationCodeToEmail(request, out message);

            if(success)
            {
                ConfirmationCodeRequestSetDefaultData(request);
                confirmationCodeRequests.Add(request);
                SaveContactsToSession();
            }

            return success;
        }

        /// <summary>
        /// Регистрация запроса кода подтверждения для номера телефона
        /// </summary>
        /// <param name="request"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool RequestConfirmationCodeToPhone(ConfirmationCodeRequest request, out string message)
        {
            message = null;

            // Проверяем номер телефона, отправить можно только на номера РФ
            var russianNumber = new System.Text.RegularExpressions.Regex(@"^\+?79\d{9}$");
            if(!russianNumber.IsMatch(request.Phone))
            {
                message = "Подтвердить можно только мобильные номера Российской Федерации,<br />записанные в международном формате +7-9ХХ-ХХХ-ХХ-ХХ.<br />" +
                          "Если у Вас номер стационарной связи или Вы нерезидент РФ,<br />то можете оставить Ваш номер неподтверждённым и подтвердить только электронную почту.";
                return false;
            }

            // Проверяем, что за последние 3 минуты не было отправлено сообщений
            const int resendTimeout = 3;

            var time = DateTime.Now.AddMinutes(-resendTimeout);
            var requestInLastThreeMinutes = confirmationCodeRequests.OrderByDescending(p => p.DateGenerated).FirstOrDefault(p => p.IsPhone() && p.DateGenerated >= time);
            if(requestInLastThreeMinutes != null)
            {
                // Было отправлено сообщение за последние три минуты
                var timeout = requestInLastThreeMinutes.DateGenerated.AddMinutes(resendTimeout) - DateTime.Now;
                message = "Повторно отправить код можно через " + timeout.ToString(@"mm\:ss") + ".";
                return false;
            }


            // Проверяем, можно ли отправить сообщение

            // Проверяем, количество отправленных за последний час
            time = DateTime.Now.AddHours(-1);
            var requestsInLastHour = confirmationCodeRequests.Where(p => p.IsPhone() && p.DateGenerated >= time).ToList();
            if (requestsInLastHour.Count >= _limits.ConfirmationCodeInHourFromBrowser)
            {
                message = _limits.GetMessage(p => p.ConfirmationCodeInHourFromBrowser);
                return false;
            }

            // Можно отправить сообщение

            // Генерация кода подтверждения
            time = DateTime.Now.AddHours(-3);
            var lastRequest = confirmationCodeRequests.FirstOrDefault(p => p.IsPhone() && p.Phone == request.Phone && p.DateGenerated >= time);
            if (lastRequest != null) request.VerificationCode = lastRequest.VerificationCode;
            else
            {
                // Предыдущий код не найден, создаем новый
                request.VerificationCode = DataSecurity.GeneratePassword(5, DataSecurity.PasswordGeneratorMode.Numbers);
            }

            return true;

        }

        /// <summary>
        /// Регистрация запроса кода подтверждения для почты
        /// </summary>
        /// <param name="request"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool RequestConfirmationCodeToEmail(ConfirmationCodeRequest request, out string message)
        {
            message = null;

            // Проверяем, можно ли отправить сообщение

            // Проверяем, количество отправленных за последний час
            var time = DateTime.Now.AddHours(-1);
            var requestsInLastHour = confirmationCodeRequests.Where(p => p.IsEmail() && p.DateGenerated >= time).ToList();
            if (requestsInLastHour.Count >= _limits.ConfirmationCodeInHourFromBrowser)
            {
                message = _limits.GetMessage(p => p.ConfirmationCodeInHourFromBrowser);
                return false;
            }

            // Можно отправить сообщение

            // Генерация кода подтверждения
            time = DateTime.Now.AddHours(-3);
            var lastRequest = confirmationCodeRequests.FirstOrDefault(p => p.IsEmail() && p.Email == request.Email && p.DateGenerated >= time);
            if (lastRequest != null) request.VerificationCode = lastRequest.VerificationCode;
            else
            {
                // Предыдущий код не найден, создаем новый
                request.VerificationCode = DataSecurity.GeneratePassword(5, DataSecurity.PasswordGeneratorMode.Numbers);
            }

            return true;
        }

        /// <summary>
        /// Заполняет обязательные данные запроса кода подтверждения
        /// </summary>
        /// <param name="request"></param>
        private void ConfirmationCodeRequestSetDefaultData(ConfirmationCodeRequest request)
        {
            request.DateGenerated = DateTime.Now;
            request.Expires = TimeSpan.FromHours(3);
        }

        /// <summary>
        /// Проверяет код подтверждения
        /// </summary>
        /// <param name="codeRequest"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public bool CheckConfirmationCode(ConfirmationCodeRequest codeRequest, out string message)
        {
            Task.Run(() => CheckData()).Wait();
            
            message = null;
            codeRequest.Normalize();

            var success = false;
            var registeredRequest = confirmationCodeRequests.FirstOrDefault(p => p.Hash == codeRequest.Hash);
            if (registeredRequest != null)
            {
                var expirationDate = registeredRequest.DateGenerated.Add(registeredRequest.Expires);
                if (expirationDate > DateTime.Now)
                {
                    // добавляем контакт в подтвержденные
                    Task.Run(() => ConfirmContact(registeredRequest)).Wait();
                    
                    // Удаляем другие коды подтверждения
                    if (registeredRequest.IsPhone()) confirmationCodeRequests = confirmationCodeRequests.Where(p => !p.IsPhone()).ToList();
                    else if (registeredRequest.IsEmail()) confirmationCodeRequests = confirmationCodeRequests.Where(p => !p.IsEmail()).ToList();

                    success = true;
                }
                else message = "Код подтверждения уже истёк.";
            }
            else message = "Неправильный код подтверждения.";

            if (success) SaveContactsToSession();

            return success;
        }


        #endregion
    }
}
