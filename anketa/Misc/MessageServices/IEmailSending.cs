﻿using anketa.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace anketa.Misc.MessageServices
{
    public class IEmailSending : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public IEmailSending(IServiceScopeFactory serviceScopeFactory)
        {
            _serviceScopeFactory = serviceScopeFactory;
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Устанавливаем задержку
            var delayInterval = TimeSpan.FromHours(1);
            while (!stoppingToken.IsCancellationRequested)

            {
                using (var scope = _serviceScopeFactory.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<DatabaseContext>();
                    var iEmailSending = scope.ServiceProvider.GetRequiredService<IEmailService>();

                    var minuteAgo = DateTime.Now.AddHours(-48);
                    var listEmail = dbContext.UserEmailMessages.Where(p => p.IsSend == false && p.DateCreated >= minuteAgo).ToList();
                    foreach (var item in listEmail)
                    {
                        item.IsSend = await iEmailSending.SendMessage(item.Email, item.Subject, item.Meassage, false);
                        if (item.IsSend)
                            dbContext.UserEmailMessages.Update(item);
                    }
                    await dbContext.SaveChangesAsync();
                }

                await Task.Delay(delayInterval, stoppingToken);
            }
        }

    }
}
