﻿using Microsoft.AspNetCore.Html;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using reCAPTCHA.AspNetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.Captcha
{
    public class RecaptchaImplementation : ICaptchaService
    {
        private RecaptchaService recaptchaService;

        private RecaptchaSettings recaptchaSettings;

        /// <summary>
        /// Конфигурирует капчу
        /// </summary>
        /// <param name="services"></param>
        /// <param name="configuration"></param>
        public void SetConfiguration(IServiceCollection services, object configuration)
        {
            recaptchaSettings = ((IConfiguration)configuration).Get<RecaptchaSettings>();
            var options = Options.Create(recaptchaSettings);
            recaptchaService = new RecaptchaService(options);
        }

        /// <summary>
        /// Отрисовывает капчу
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <returns></returns>
        public HtmlString RenderCaptcha(Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper htmlHelper)
        {
            return RecaptchaHelper.Recaptcha<reCAPTCHA.AspNetCore.Versions.RecaptchaV2Checkbox>(htmlHelper, recaptchaSettings);
        }

        /// <summary>
        /// Проверяет правильность капчи
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<bool> IsValid(string token)
        {
            var response = await recaptchaService.Validate(token);
            return response.success;
        }

        /// <summary>
        /// Возвращает конфигурацию капчи
        /// </summary>
        /// <returns></returns>
        public object GetConfiguration()
        {
            var serializedConf = JsonConvert.SerializeObject(recaptchaSettings);
            var conf = JsonConvert.DeserializeObject<RecaptchaSettings>(serializedConf);
            conf.SecretKey = ""; // Прячем секретный ключ
            return conf;
        }
    }
}
