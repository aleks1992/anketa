﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace anketa.Controllers.API
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "API")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly DatabaseContext DbContext;

        public UsersController(DatabaseContext dbContext)
        {
            DbContext = dbContext;
        }
    }
}