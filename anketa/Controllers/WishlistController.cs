﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using anketa.Misc;
using anketa.Misc.SiteAPI;
using anketa.Models;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static anketa.Misc.SiteClient;

namespace anketa.Controllers
{
    [AllowAnonymous]
    public class WishlistController : Controller
    {
        private readonly CartWishlistManager cartWishlistManager;

        public WishlistController(DatabaseContext dbContext, CartWishlistManager _cartWishlistManager)
        {
            cartWishlistManager = _cartWishlistManager;
        }

        /// <summary>
        /// Отрисовывает список избранных товаров
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            if (User.IsInRole("UnconfirmedUser"))
                return RedirectToAction("PersonalData", "Cabinet");
            ViewBag.Status = cartWishlistManager.Status;
            return View("List", cartWishlistManager.Storage.Wishlist);
        }

        #region API


        /// <summary>
        /// Удаляет товар из избранного
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Delete([FromBody]CartWishlistItem item)
        {
            var response = new ServerResponseItemData();
            cartWishlistManager.Storage.Wishlist.RemoveAll(p => p.Guid == item.Guid);
            if (cartWishlistManager.Commit())
            {
                response.Success = true;
                response.Data = cartWishlistManager.Storage.Wishlist;
            }
            return Json(response);
        }

        // <summary>
        /// Добавления товара в корзину
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Insert([FromBody]CartWishlistItem item)
        {
            var response = new ServerResponseItemData();

            var itemInCart = cartWishlistManager.Storage.Wishlist.FirstOrDefault(p => p.Guid == item.Guid);

            if (itemInCart == null)
            {
                cartWishlistManager.Storage.Wishlist.Add(item);
                if (cartWishlistManager.Commit())
                {
                    response.Message = "Товар добавлен в избранное";
                    response.Success = true;
                }
                else
                {
                    response.Message = "Ошибка добавления товара в избранное";
                }
            }

            return Json(response);

        }

        #endregion

    }
}