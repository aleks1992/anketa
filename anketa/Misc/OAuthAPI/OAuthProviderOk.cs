﻿
using anketa.Models.Database;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static anketa.Misc.SiteClient;
using static System.Net.WebRequestMethods;

namespace anketa.Misc.OAuthAPI
{
    /// <summary>
    /// Аторизация через Ok.ru
    /// </summary>
    public class OAuthProviderOk : OAuthProvider
    {
        /// <summary>
        /// Получения типа запроса метода
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        protected override string GetHttpMethod(OAuthMethod method)
        {
            if (method == OAuthMethod.GetToken)
            {
                return Http.Post;
            }
            return Http.Get;
        }

        /// <summary>
        /// Получения Uri
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public override string GetUri(OAuthMethod method)
        {
            if (method == OAuthMethod.GetCode)
            {
                var dataCode = new RequestOkCode
                {
                    client_id = Config.ClientId,
                    response_type = "code",
                    scope = "GET_EMAIL;VALUABLE_ACCESS;LONG_ACCESS_TOKEN;",
                    redirect_uri = RedirectUri,
                    state = State,
                };
                string getParameter = SiteClient.GetQueryString(dataCode);
                return "https://connect.ok.ru/oauth/authorize?" + getParameter;
            }
            else if (method == OAuthMethod.GetToken) return "https://api.ok.ru/oauth/token.do";
            else if (method == OAuthMethod.GetUser) return "https://api.ok.ru/fb.do";
            return null;
        }

        /// <summary>
        /// Получения подписи для запроса
        /// </summary>
        /// <returns></returns>
        protected override string GetSignature(string[] exceptFields)
        {
            var data = SiteClient.GetQueryString(Request, "", exceptFields) + DataSecurity.ComputeHashMd5(Request.access_token + "" + Config.SecretKey);
            var sig = DataSecurity.ComputeHashMd5(data);
            return sig;
        }

        /// <summary>
        /// Получения тела запроса
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public override object GetRequestBody(OAuthMethod oAuthMethod, string sig = null)
        {
            if (sig != null)
            {
                Request.sig = sig;
            }
            var response = new ServerResponseItemData();
            if (GetHttpMethod(oAuthMethod) == Http.Post)
            {
                string uriProperties = GetQueryString(this.Request);
                string uri = GetUri(oAuthMethod) + "?" + uriProperties;
                response = HttpClient(Request, uri, "POST", "x-www-form-url-encoded", inAnswerResponse: false);
                return response.Data;
            }
            else
            {
                return base.GetRequestBody(oAuthMethod);
            }
        }
        /// <summary>
        /// Авторизация 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public override User ProcessLogin(string code, string state = null)
        {
            dynamic userReturn = null;
            Request = new RequestOkToken
            {
                client_id = Config.ClientId,
                code = code,
                client_secret = Config.SecretKey,
                redirect_uri = RedirectUri,
                grant_type = "authorization_code"
            };

            //Получения токена
            var response = SendRequest(OAuthMethod.GetToken);
            dynamic accessToken = null;
            if (response != null)
            {
                accessToken = JsonConvert.DeserializeObject<AccessOkToken>(response.ToString());
            }

            //Отправка запроса на получения User
            if (accessToken != null)
            {
                Request = new RequectOkUser
                {
                    application_key = Config.PublicKey,
                    format = "json",
                    method = "users.getCurrentUser",
                    access_token = accessToken.access_token,
                    sig = ""
                };
                string[] exceptFields = { "access_token", "sig" };
                response = SendRequest(OAuthMethod.GetUser, true, exceptFields);
                dynamic user = null;
                if (response != null)
                {
                    user = JsonConvert.DeserializeObject<UserOk>(response.ToString());
                }
                if (user != null)
                {
                    userReturn = new User
                    {
                        Email = user.email,
                        FirstName = user.first_name,
                        LastName = user.last_name,
                        Activated = true,
                    };
                }
            }

            return userReturn;
        }

        /// <summary>
        /// Данные для получения токена  Ok
        /// </summary>
        class RequestOkToken
        {
            /// <summary>
            /// Индификатор приложения
            /// </summary>
            public string client_id { get; set; }

            /// <summary>
            /// Закрытый ключ
            /// </summary>
            public string client_secret { get; set; }

            /// <summary>
            /// Код аторизации
            /// </summary>
            public string code { get; set; }

            /// <summary>
            /// Uri куда возврашать ответ
            /// </summary>
            public string redirect_uri { get; set; }

            /// <summary>
            /// Тип получаемого токена
            /// </summary>
            public string grant_type { get; set; }
        }

        /// <summary>
        /// Получения кода аунтификации  Ok
        /// </summary>
        class RequestOkCode
        {
            /// <summary>
            /// Индификатор приложения
            /// </summary>
            public string client_id { get; set; }

            /// <summary>
            /// Тип получаемого токена
            /// </summary>
            public string response_type { get; set; }

            /// <summary>
            /// Uri куда возврашать ответ
            /// </summary>
            public string redirect_uri { get; set; }

            /// <summary>
            /// Права доступа
            /// </summary>
            public string scope { get; set; }

            /// <summary>
            /// Состояние сессии
            /// </summary>
            public string state { get; set; }
        }

        /// <summary>
        /// Токен доступа  Ok
        /// </summary>
        class AccessOkToken
        {
            /// <summary>
            /// Токен доступа
            /// </summary>
            public string access_token { get; set; }

            /// <summary>
            /// Время жизни
            /// </summary>
            public string expires_in { get; set; }

            /// <summary>
            /// Токен для обновления токена доступа
            /// </summary>
            public string refresh_token { get; set; }

        }


        /// <summary>
        /// Получения ифнормации о пользователи Ok
        /// </summary>
        class RequectOkUser
        {
            /// <summary>
            /// Формат запроса JSON/XML
            /// </summary>
            public string format { get; set; }

            /// <summary>
            /// Поля через ;
            /// </summary>
            public string fields { get; set; }

            /// <summary>
            /// Имя метода
            /// </summary>
            public string method { get; set; }

            /// <summary>
            /// Токен доступа
            /// </summary>
            public string access_token { get; set; }

            /// <summary>
            /// Ключ приложения
            /// </summary>
            public string application_key { get; set; }

            /// <summary>
            /// Подпись запроса
            /// </summary>
            public string sig { get; set; }

        }
        /// <summary>
        /// Set User Ok
        /// </summary>
        class UserOk
        {
            /// <summary>
            /// Индификатор пользователя
            /// </summary>
            public string uid { get; set; }

            /// <summary>
            /// Имя
            /// </summary>
            public string first_name { get; set; }

            /// <summary>
            /// Фамилия
            /// </summary>
            public string last_name { get; set; }

            /// <summary>
            /// Пол
            /// </summary>
            public int sex { get; set; }

            /// <summary>
            /// Дата Рождения
            /// </summary>
            public string birthday { get; set; }

            /// <summary>
            /// Номера
            /// </summary>
            public LocationOk location { get; set; }

            /// <summary>
            /// Страна
            /// </summary>
            public string email { get; set; }

        }

        /// <summary>
        /// Контактная информация Ok
        /// </summary>
        class LocationOk
        {
            /// <summary>
            /// Город
            /// </summary>
            public string city { get; set; }

            /// <summary>
            /// Страна
            /// </summary>
            public string country { get; set; }

            /// <summary>
            /// Код страны
            /// </summary>
            public string countryCode { get; set; }

            /// <summary>
            /// Найменования страны
            /// </summary>
            public string countryName { get; set; }

        }
    }
}
