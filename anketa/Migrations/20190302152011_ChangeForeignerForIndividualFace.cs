﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class ChangeForeignerForIndividualFace : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsForeignFace",
                table: "UserIndividualFaces",
                newName: "IsForeigner");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsForeigner",
                table: "UserIndividualFaces",
                newName: "IsForeignFace");
        }
    }
}
