﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc
{
    public class SmsData
    {
        /// <summary>
        /// Тип
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Массив Номеров
        /// </summary>
        public string[] Receivers { get; set; }

        /// <summary>
        /// Сообщения
        /// </summary>
        public string Message { get; set; }

    }
}
