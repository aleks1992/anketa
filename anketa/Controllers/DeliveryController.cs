﻿using anketa.Misc;
using anketa.Models;
using anketa.Models.API;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using static anketa.Misc.SiteClient;
using static anketa.Misc.Yandex.Yandex;
using static anketa.Misc.DaData.DaData;
using anketa.Models.Application.Forms;
using Newtonsoft.Json;
using anketa.Misc.Services;
using MimeKit.Encodings;
using System.Text;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using anketa.Misc.GeoLocation;
using System.IO;

namespace anketa.Controllers
{
    public class DeliveryController : Controller
    {
        private readonly DatabaseContext dbContext;
        private readonly IAddressSuggestion iAddressSuggestion;
        private readonly List<SiteData> siteData;
        private IHttpContextAccessor _accessor;
        private IWebHostEnvironment HostingEnvironment;
        private GeoLocationClient GeoLocation;

        private const string URL = "https://labirint-master.ru/delivery-aggregator/api/values";


        public DeliveryController(DatabaseContext _dbContext, IAddressSuggestion _iAddressSuggestion, List<SiteData> _siteData, IHttpContextAccessor accessor, IWebHostEnvironment env, GeoLocationClient location)
        {
            _accessor = accessor;
            HostingEnvironment = env;
            GeoLocation = location;
            dbContext = _dbContext;
            iAddressSuggestion = _iAddressSuggestion;
            siteData = _siteData;

        }

        /// <summary>
        /// Страница отображени результата
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index(string deliveryAddress, string product, [FromServices] ModelEngine modelEngine, [FromServices] AppData appData, bool targetedDelivery = false, string shippingAddress = "Чита")
        {
            var listProduct = new List<ProductsDelivery>();
            if (!string.IsNullOrEmpty(product))
            {
                try
                {
                    byte[] data = Convert.FromBase64String(product);
                    string decodedString = Encoding.UTF8.GetString(data);

                    listProduct = JsonConvert.DeserializeObject<List<ProductsDelivery>>(decodedString);
                }
                catch (Exception)
                {

                }
            }


            string ip = HostingEnvironment.IsProduction() ? HttpContext.Connection.RemoteIpAddress.ToString() : "188.168.81.35";

            if (string.IsNullOrEmpty(deliveryAddress) && !string.IsNullOrEmpty(ip))
            {
                deliveryAddress = GeoLocation.GetCity(ip);
            }

            appData.AddData("delivery", new
            {
                ShippingAddress = shippingAddress,
                DeliveryAddress = deliveryAddress,
                Product = listProduct,
                TargetedDelivery = targetedDelivery
            });



            return View("Index");
        }

        /// <summary>
        /// Возвращает список подкгруженных товаров
        /// </summary>
        /// <param name="productsDelivery"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GetProduct([FromBody] List<ProductsDelivery> productsDelivery)
        {
            var listProduct = LoadingProduct(productsDelivery);
            return Json(listProduct);
        }

        /// <summary>
        /// Подгружает товары с сайтов
        /// </summary>
        /// <param name="productsDelivery"></param>
        /// <returns></returns>
        private List<ProductsDelivery> LoadingProduct(List<ProductsDelivery> productsDelivery)
        {

            var data = new
            {
                module = "product",
                action = "anketa_get_product_delivery",
                products = String.Join(",", productsDelivery.Select(p => p.Guid).ToArray())
            };

            var listProduct = new List<ProductsDelivery>();

            foreach (var item in siteData)
            {
                try
                {
                    string url = item.GetLink("wp-admin/admin-ajax.php");
                    var urlGet = SiteClient.GetQueryString(data);
                    var resultProduct = SiteClient.HttpClient(null, url + "?" + urlGet, inAnswerResponse: true, method: "GET");

                    if (!string.IsNullOrEmpty(resultProduct.Data.ToString()))
                    {
                        var itemProduct = JsonConvert.DeserializeObject<List<ProductsDelivery>>(resultProduct.Data.ToString());
                        foreach (var i in itemProduct)
                        {
                            var product = productsDelivery.FirstOrDefault(p => p.Guid == i.Guid);
                            if (product != null)
                            {
                                i.Count = product.Count;
                                listProduct.Add(i);
                            }
                        }
                    }

                }
                catch (Exception)
                {

                }

            }

            foreach (var item in listProduct)
            {
                item.IsValidDelivery = (item.Width != 0 && item.Length != 0 && item.Height != 0);
                item.Volume = (((item.Width == 0 ? 1 : item.Width) * (item.Length == 0 ? 1 : item.Length) * (item.Height == 0 ? 1 : item.Height)));
                double y = (double)1 / (double)3;
                item.Width = item.Length = item.Height = (decimal)Math.Pow((double)item.Volume, y);

            }

            return listProduct;

        }

        /// <summary>
        /// Получения Ид рассчета
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult GetSessionId([FromBody] DeliveryFrom form)
        {

            var response = new ServerResponseItemData();

            if (string.IsNullOrEmpty(form.DeliveryAddress))
            {
                response.Success = false;
                response.Message = "Проверьте правильность введенного адреса!";
                return Json(response);
            }

            var address = iAddressSuggestion.Get(form.DeliveryAddress);

            if (address.Count() == 0)
            {
                response.Success = false;
                response.Message = "Проверьте правильность введенного адреса!";
            }
            else
            {
                var listProduct = LoadingProduct(form.ProductsDelivery);

                if (listProduct.Where(p => p.IsValidDelivery = false).ToList().Count() > 0)
                {
                    response.Success = false;
                    response.Message = "Рассчитать доставку нельзя, т.к. вес или объём не указаны. Свяжитесь с нами, мы это исправим!";
                    return Json(response);
                }

                double volume = 0;
                double price = 0;
                double weight = 0;

                foreach (var item in listProduct)
                {
                    item.Volume = (((item.Width == 0 ? 1 : item.Width) * (item.Length == 0 ? 1 : item.Length) * (item.Height == 0 ? 1 : item.Height)));
                    volume += (double)(item.Volume * item.Count);
                    price += (double)item.Price * item.Count;
                    weight += (double)item.Weight * item.Count;
                    double y = (double)1 / (double)3;
                    item.Width = item.Length = item.Height = (decimal)Math.Pow((double)item.Volume, y);
                }


                DeliveryData deliveryDate = new DeliveryData
                {
                    Price = price,
                    ArrivalCountry = address[0].Country,
                    ArrivalLatitude = address[0].Lat,
                    ArrivalLongitude = address[0].Lon,
                    ArrivalPoint = address[0].City,
                    ArrivalFIAS = address[0].Fias,
                    SizedVolume = volume,
                    SizedHeight = Math.Pow(volume, (double)1 / (double)3),
                    SizedWidth = Math.Pow(volume, (double)1 / (double)3),
                    SizedLength = Math.Pow(volume, (double)1 / (double)3),
                    SizedWeight = weight,
                    IsAddress = form.IsAddress,
                    Distance = "",
                    DerivalPoint = (string.IsNullOrEmpty(form.ShippingAddress)) ? "Чита" : form.ShippingAddress

                };
                var result = HttpClient(deliveryDate, URL, "POST", inAnswerResponse: false);

                if (result.Data != null && !string.IsNullOrEmpty(result.Data.ToString()))
                {
                    response.Success = true;
                    response.Message = null;
                    response.Data = new
                    {
                        products = listProduct,
                        id = JsonConvert.DeserializeObject<String>(result.Data.ToString())
                    };
                }
                else
                {
                    response.Success = false;
                    response.Message = "Ошибка попробуйте еще раз!";
                }

            }

            return Json(response);

        }

        /// <summary>
        /// Получения результата рассчета по Id сессий
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetResult(string id)
        {
            var response = new ServerResponseItemData();

            if (string.IsNullOrEmpty(id))
            {
                response.Message = "Ошибка индификатора сессий";
                response.Success = false;
                return Json(response);
            }

            string url = URL + "/" + id;
            var result = HttpClient(null, url, "GET", inAnswerResponse: false);

            response.Success = true;
            response.Message = null;
            if (!string.IsNullOrEmpty(result.Data.ToString()))
                response.Data = JsonConvert.DeserializeObject<DeliveryResult>(result.Data.ToString());
            else
                response.Data = new DeliveryResult();

            return Json(response);
        }


    }
}
