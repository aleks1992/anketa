﻿import fetch from 'isomorphic-fetch'

export default {
    methods: {
        httpClient(url, requestBody, callback, method = "post") {
            fetch(url, {
                method: method,
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(requestBody)
            })
                .then(response => response.json())
                .then(response => {
                    callback(response);
                });
        }
    }
}