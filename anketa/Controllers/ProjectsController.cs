﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using anketa.Misc.SiteAPI;
using anketa.Models;
using anketa.Models.Database.Products;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace anketa.Controllers
{
    /// <summary>
    /// Отвечает за проекты пользователей
    /// </summary>
    [Authorize(Roles = "User")]
    public class ProjectsController : Controller
    {
        private readonly DatabaseContext DbContext;
        private readonly CartWishlistManager cartWishlistManager;

        public ProjectsController(DatabaseContext _dbContext, CartWishlistManager _cartWishlistManager)
        {
            DbContext = _dbContext;
            cartWishlistManager = _cartWishlistManager;
        }

        /// <summary>
        /// Список проектов
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            if (User.IsInRole("UnconfirmedUser"))
                return RedirectToAction("PersonalData", "Cabinet");

            var list = new HashSet<ProductProject>();            
            var user = await DbContext.GetUser(User);
            if (user != null)
            {
                // Теперь, когда у пользователя может быть несколько привязанных контактов, придётся проверять их все
                var regex = new Regex(@"[^\d]");

                var confirmedPhones = DbContext.UserConfirmedPhones.Where(p => p.UserId == user.ID).ToList()
                    .Select(p => regex.Replace(p.Phone, "")).ToList();

                foreach (var phone in confirmedPhones)
                {
                    var listForPhone = DbContext.ProductProjects.Where(p => p.Phone.Contains(phone)).ToList();
                    foreach (var l in listForPhone) list.Add(l);
                }

                var confirmedEmails = DbContext.UserConfirmedEmails.Where(p => p.UserId == user.ID).Select(p => p.Email.ToLower()).ToList();
                foreach (var email in confirmedEmails)
                {
                    var listForEmail = DbContext.ProductProjects.Where(p => p.Email.Contains(email)).ToList();
                    foreach (var l in listForEmail) list.Add(l);
                }
            }

            foreach(var item in list)
            {
                item.InCart = cartWishlistManager.Storage.Cart.FirstOrDefault(p => p.Type == Models.Database.CartWishlistItem.ItemType.Project && p.Guid == item.Guid.ToString()) != null;                
            }

            return View(list);
        }

        /// <summary>
        /// Добавляет проект в корзину
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        [Produces("application/json")]
        public IActionResult AddToCart([FromBody]ProductProject project)
        {
            project = DbContext.ProductProjects.FirstOrDefault(p => p.Id == project.Id);
            if (project != null)
            {
                var cartItem = cartWishlistManager.Storage.Cart.FirstOrDefault(p => p.Type == Models.Database.CartWishlistItem.ItemType.Project && p.Guid == project.Guid.ToString());
                if (cartItem == null)
                {
                    cartWishlistManager.Storage.Cart.Add(new Models.Database.CartWishlistItem
                    {
                        ByOrder = true,
                        Count = 1,
                        Guid = project.Guid.ToString(),
                        Name = project.Name,
                        Price = project.Price,
                        Type = Models.Database.CartWishlistItem.ItemType.Project
                    });
                    cartWishlistManager.Commit();
                }
            }
            return Json(true);
        }
    }
}