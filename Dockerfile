# Build bundles
FROM node:alpine as node
WORKDIR /app

# Configure builder
COPY anketa/package.json .
COPY anketa/package-lock.json .

RUN npm install

COPY anketa/webpack.config.js .
COPY anketa/.babelrc .
COPY anketa/wwwroot/src ./wwwroot/src/

RUN npm run production

# Build anketa
FROM mcr.microsoft.com/dotnet/core/sdk:3.1 as build
WORKDIR /app

# copy csproj
COPY *.sln .
COPY anketa/*.csproj ./anketa/
RUN dotnet restore anketa/anketa.csproj

# copy everything else and build
COPY anketa/. ./anketa/
COPY --from=node /app/wwwroot/dist ./anketa/wwwroot/dist/
WORKDIR /app/anketa

RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/core/aspnet:3.1 as runtime

# Enviroment variables
ENV TZ=Asia/Chita

EXPOSE 80

WORKDIR /app
COPY --from=build /app/anketa/out ./
RUN echo $(date +'%Y%m%d%H%M%S') > Version.txt
ENTRYPOINT ["dotnet", "anketa.dll"]