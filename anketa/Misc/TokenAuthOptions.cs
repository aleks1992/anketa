﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace anketa.Misc
{
    public class TokenAuthOptions
    {
        /// <summary>
        /// Издатель токена
        /// </summary>
        public string Issuer { get; set; }

        /// <summary>
        /// Потребитель токена
        /// </summary>
        public string Audience { get; set; }

        /// <summary>
        /// Секретный ключ
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Время жизни токена в минутах
        /// </summary>
        public int Lifetime { get; set; }

        /// <summary>
        /// Время жизни токена обновления в минутах
        /// </summary>
        public int RefreshTokenLifetime { get; set; }

        /// <summary>
        /// Возвращает секретный симметричный ключ
        /// </summary>
        /// <returns></returns>
        public SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Key));
        }
    }
}
