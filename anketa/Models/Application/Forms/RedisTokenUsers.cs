﻿using anketa.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Forms
{
    public class RedisTokenUsers
    {
        public string Ip { get; set; }

        public string Token { get; set; }

        public string Login { get; set; }

        public UserActivationTokenType TypeLogin { get; set; }

        public DateTime Expires { get; set; }

    }
}
