﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.Captcha
{
    public class CaptchaOptions
    {
        public Type CaptchaService { get; set; }

        public object Configuration { get; set; }
    }
}
