﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace anketa.Misc
{
    public class DataSecurity
    {
        private static readonly string SaltKey = "a=;YmNvFXbY5E-4u}9Ee)bS5Y_Rww2Kj'N-*nDc<=Aw@j{U72K/;22R:~=xE9z+2";
        private static readonly string IV = "wr(}[k$7VffzE5<T";

        /// <summary>
        /// Высчитывает хэш в формате SHA1
        /// </summary>
        /// <param name="data">Данные для хэширования</param>
        /// <returns>Шестнадцатиричная строка с хэшем</returns>
        public static string ComputeHash(string data)
        {
            using (var hasher = SHA1.Create())
            {
                if (data == null) data = "";
                var hash = hasher.ComputeHash(Encoding.UTF8.GetBytes(data));
                return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
            }
        }

        /// <summary>
        /// Вычисляет хэш в формате SHA256
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ComputeHashSHA256(string data)
        {
            using(var hasher = SHA256.Create())
            {
                var hash = hasher.ComputeHash(Encoding.UTF8.GetBytes(data ?? ""));
                return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
            }
        }

        /// <summary>
        /// Высчитывает хэш в формате MD5
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ComputeHashMd5(string data)
        {
            using (var hasher = MD5.Create())
            {
                var hash = hasher.ComputeHash(Encoding.UTF8.GetBytes(data));
                return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
            }
        }
        /// <summary>
        /// Генерирует токен в формате SHA256
        /// </summary>
        /// <returns></returns>
        public static string GetToken()
        {
            var random = new Random();
            var buffer = new byte[256];
            random.NextBytes(buffer);
            
            using (var hasher = SHA256.Create())
            {
                var hash = hasher.ComputeHash(buffer);
                return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
            }
        }

        /// <summary>
        /// Преобразует строку в base64
        /// </summary>
        /// <param name="message">Исходное сообщение</param>
        /// <returns>Строка в формате base64</returns>
        public static string EncodeBase64(string message)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(message));
        }

     
        public static string DecodeBase64(string message)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(message));
        }

        /// <summary>
        /// Шифрует сообщение открытым ключом
        /// </summary>
        /// <param name="message">Шифруемое сообщение</param>
        /// <param name="key">Ключ шифрования</param>
        /// <returns></returns>
        public static string EncryptMessage(string message, EncryptionKey key)
        {
            using (var provider = new RSACryptoServiceProvider(2048))
            {
                provider.ImportCspBlob(Convert.FromBase64String(key.PublicKey));
                provider.PersistKeyInCsp = false;
                var encryptedMessage = provider.Encrypt(Encoding.UTF8.GetBytes(message), RSAEncryptionPadding.Pkcs1);
                return Convert.ToBase64String(encryptedMessage);
            }
        }

        /// <summary>
        /// Дешифрует сообщение закрытым ключом
        /// </summary>
        /// <param name="message">Дешифруемое сообщение</param>
        /// <param name="key">Ключ шифрования</param>
        /// <returns></returns>
        public static string DecryptMessage(string message, EncryptionKey key)
        {
            using (var provider = new RSACryptoServiceProvider(2048))
            {
                provider.ImportCspBlob(Convert.FromBase64String(key.PrivateKey));
                provider.PersistKeyInCsp = true;
                var decryptedMessage = provider.Decrypt(Convert.FromBase64String(message), RSAEncryptionPadding.Pkcs1);
                return Encoding.UTF8.GetString(decryptedMessage);
            }
        }

        /// <summary>
        /// Возвращает хэш ключа с солью
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        private static byte[] GetKeyBytes(string password)
        {
            return new Rfc2898DeriveBytes(password, Encoding.UTF8.GetBytes(SaltKey), 1000, HashAlgorithmName.SHA1).GetBytes(256 / 8);
        }

        /// <summary>
        /// Шифрует сообщение симметричным ключом
        /// </summary>
        /// <param name="message">Шифруемое сообщение</param>
        /// <param name="key">Ключ</param>
        /// <returns>Зашифрованное сообщение</returns>
        public static string EncryptMessage(string message, string key)
        {
            try
            {
                string encrypted = null;
                var keyBytes = GetKeyBytes(key);

                using (var aes = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.PKCS7 })
                {
                    var encryptor = aes.CreateEncryptor(keyBytes, Encoding.UTF8.GetBytes(IV));
                    var ms = new MemoryStream();
                    using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    using (var sw = new StreamWriter(cs))
                    {
                        sw.Write(message);
                    }
                    encrypted = Convert.ToBase64String(ms.ToArray());
                }
                return encrypted;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return "";
        }

        /// <summary>
        /// Дешифрует сообщение, зашифрованное симметричным ключом
        /// </summary>
        /// <param name="message">Зашифрованное сообщение</param>
        /// <param name="key">Ключ</param>
        /// <returns>Расшифрованное сообщение</returns>
        public static string DecryptMessage(string message, string key)
        {
            try
            {
                string plaintext = null;
                var keyBytes = GetKeyBytes(key);

                using (var aes = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.PKCS7 })
                {
                    var decryptor = aes.CreateDecryptor(keyBytes, Encoding.UTF8.GetBytes(IV));
                    var ms = new MemoryStream(Convert.FromBase64String(message));
                    using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    using (var sr = new StreamReader(cs))
                    {
                        plaintext = sr.ReadToEnd();
                    }
                }
                return plaintext;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return "";
        }

        /// <summary>
        /// Создает пароль указанной длины
        /// </summary>
        /// <param name="length">Длина пароля</param>
        /// <returns></returns>
        public static string GeneratePassword(int length, PasswordGeneratorMode mode = PasswordGeneratorMode.All)
        {
            string password = "", chars = "";

            int m = (int)mode;
            if((m & 1) == 1) chars += "QWERTYUIOPASDFFGHJKLZXCVBNM";
            if((m & 2) == 2) chars += "qwertyuiopasdfghjklzxcvbnm";
            if((m & 4) == 4) chars += "0123456789";
            //chars += "~!@#$%^&*()<>?}{[]\\|/_-+=";

            Random rand = new Random();
            for (int i = 0; i < length; i++) password += chars.Substring(rand.Next(0, chars.Length), 1);

            return password;
        }

        public enum PasswordGeneratorMode : int
        {
            UpperCase = 1,
            LowerCase = 2,
            Numbers = 4,
            All = 7
        }
    }

    /// <summary>
    /// Класс для хранения ключей шифрования
    /// </summary>
    public class EncryptionKey
    {
        /// <summary>
        /// Публичный ключ
        /// </summary>
        public string PublicKey { get; set; }

        /// <summary>
        /// Приватный ключ
        /// </summary>
        public string PrivateKey { get; set; }
    }
}
