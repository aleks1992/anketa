﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.Validators
{
    public class PSRNValidator : ValidationAttribute, IClientModelValidator
    {
        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            MergeAttribute(context.Attributes, "data-val", "true");
            MergeAttribute(context.Attributes, "data-val-psrn", ErrorMessage);
        }

        private string GetErrorMessage(string name)
        {
            return name;
        }

        /// <summary>
        /// Добавляет или обновляет значение в словаре
        /// </summary>
        /// <param name="attributes"></param>
        /// <param name="key"></param>
        /// <param name="val"></param>
        private void MergeAttribute(IDictionary<string, string> attributes, string key, string val)
        {
            if (attributes.ContainsKey(key)) attributes[key] = val;
            else attributes.Add(key, val);
        }

        /// <summary>
        /// Производит валидацию объекта
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            return true;
        }

    }
}
