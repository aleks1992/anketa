﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Misc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace anketa.Controllers
{
    /// <summary>
    /// Контроллер для товаров
    /// </summary>
    [AllowAnonymous]
    public class ProductController : Controller
    {
        private List<SiteData> LinkedSites { get; set; }

        public ProductController(List<SiteData> linkedSites)
        {
            LinkedSites = linkedSites;
        }

        /// <summary>
        /// Открывает страницу товара
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Index(string id)
        {
            if(!string.IsNullOrEmpty(id))
            {
                var prefix = id.Substring(0, 2);
                var guid = id.Substring(3);

                var siteData = LinkedSites.Find(p => p.Prefix == prefix);
                if(siteData != null) return Redirect(siteData.URL + "?mgproduct=" + guid);
            }
            return RedirectToAction("Index", "Home");
        }
    }
}