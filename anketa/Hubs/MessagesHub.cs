﻿using anketa.Migrations;
using anketa.Misc;
using anketa.Misc.Redis;
using anketa.Misc.SiteAPI;
using anketa.Models.Application.Response;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Hubs
{
    [Authorize(Policy = "Hub")]
    [AllowAnonymous]
    public class MessagesHub : Hub
    {
        const string MESSAGE_USER_INFO = "UserInfo";
        const string MESSAGE_STORAGE_UPDATED = "StorageUpdated";

        const string STORAGE_TYPE_CART = "cart";
        const string STORAGE_TYPE_WISHLIST = "wishlist";

        const string STORAGE_ACTION_ADD = "add";
        const string STORAGE_ACTION_REMOVE = "remove";
        const string STORAGE_ACTION_SET_COUNT = "set_count";

        private readonly CartWishlistManager _cartWishlistManager;

        public MessagesHub(CartWishlistManager cartWishlistManager)
        {
            _cartWishlistManager = cartWishlistManager;
        }

        public async Task SendMessage(string user, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", user, message);
        }

        #region Взаимодействие с сайтами

        /// <summary>
        /// Возвращает информацию о пользователе
        /// </summary>
        /// <returns></returns>
        public async Task GetUserInfo()
        {
            var userInfo = new UserInfo
            {
                CartWishlistStorage = _cartWishlistManager.Storage,
            };

            if (Context.User.Identity.IsAuthenticated && Context.User.IsInRole(AuthorizeManager.ROLE_USER))
            {
                userInfo.Username = Context.User.Identity.Name;
                userInfo.IsAuthorized = true;
            }
           
            var response = new ServiceResponse
            {
                Success = true,
                Data = userInfo,
            };

            await Clients.User(Context.UserIdentifier).SendAsync(MESSAGE_USER_INFO, response);
        }

        /// <summary>
        /// Обновляет информацию в корзине
        /// </summary>
        /// <param name="storageType"></param>
        /// <param name="action"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task UpdateStorage(string storageType, string action, anketa.Models.Database.CartWishlistItem item)
        {
            var updated = false;
            if (storageType == STORAGE_TYPE_CART) updated = UpdateCart(action, item);
            else if (storageType == STORAGE_TYPE_WISHLIST) updated = UpdateWishlist(action, item);

            if(updated)
            {
                _cartWishlistManager.Commit();

                var response = new ServiceResponse
                {
                    Success = true,
                    Data = _cartWishlistManager.Storage,
                };

                await Clients.User(Context.UserIdentifier).SendAsync(MESSAGE_STORAGE_UPDATED, response);
            }
        }

        /// <summary>
        /// Обновляет данные корзины
        /// </summary>
        /// <param name="action"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool UpdateCart(string action, CartWishlistItem item)
        {
            var updated = false;

            switch(action)
            {
                case STORAGE_ACTION_ADD:
                    var addedItem = _cartWishlistManager.Storage.Cart.FirstOrDefault(p => p.Guid == item.Guid && p.PriceTypeId == item.PriceTypeId && p.SerialNumber == item.SerialNumber && p.StockId == item.StockId);
                    if (addedItem != null) addedItem.Count += item.Count;
                    else _cartWishlistManager.Storage.Cart.Add(item);
                    updated = true;
                    break;

                case STORAGE_ACTION_REMOVE:
                    _cartWishlistManager.Storage.Cart.RemoveAll(p => p.Guid == item.Guid);
                    updated = true;
                    break;

                case STORAGE_ACTION_SET_COUNT:
                    var updateItem = _cartWishlistManager.Storage.Cart.FirstOrDefault(p => p.Guid == item.Guid && p.PriceTypeId == item.PriceTypeId && p.SerialNumber == item.SerialNumber && p.StockId == item.StockId);
                    if (updateItem != null) updateItem.Count = item.Count;
                    else _cartWishlistManager.Storage.Cart.Add(item);
                    updated = true;
                    break;
            }

            return updated;
        }

        /// <summary>
        /// Обновляет данные избранного
        /// </summary>
        /// <param name="action"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        private bool UpdateWishlist(string action, CartWishlistItem item)
        {
            var updated = false;

            switch(action)
            {
                case STORAGE_ACTION_ADD:
                    var addedItem = _cartWishlistManager.Storage.Wishlist.FirstOrDefault(p => p.Guid == item.Guid);
                    if(addedItem == null)
                    {
                        _cartWishlistManager.Storage.Wishlist.Add(item);
                        updated = true;
                    }
                    break;

                case STORAGE_ACTION_REMOVE:
                    _cartWishlistManager.Storage.Wishlist.RemoveAll(p => p.Guid == item.Guid);
                    updated = true;
                    break;
            }

            return updated;
        }

        #endregion
    }
}
