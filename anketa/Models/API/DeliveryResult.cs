﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.API
{
    public class DeliveryResult
    {
        public List<DeliveryResultItem> Direct { get; set; }

        public List<DeliveryResultItem> Indirect { get; set; }

        public List<DeliveryResultItem> Alt { get; set; }


        public List<DeliveryResultItem> Other { get; set; }
    }
}
