﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using anketa.Misc.OAuthAPI;
using anketa.Misc;
using anketa.Models;
using anketa.Models.API;
using anketa.Models.API.Response;
using anketa.Models.Application;
using anketa.Models.Application.Forms;
using anketa.Models.Application.Response;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using static anketa.Misc.SiteClient;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Text;
using anketa.Misc.Redis;
using System.Net;
using Microsoft.AspNetCore.Identity;
using System.Web;
using anketa.Misc.GeoLocation;
using anketa.Models.Application.Request;
using Microsoft.Extensions.Caching.Distributed;
using anketa.Misc.MessageServices;
using anketa.Misc.Captcha;
using System.Threading;
using Microsoft.Extensions.Hosting;
using anketa.Misc.Content;
using anketa.Hubs;
using Microsoft.AspNetCore.SignalR;
using Microsoft.IdentityModel.Tokens;

namespace anketa.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        #region Константы

        /// <summary>
        /// Ключ идентификатора гостя в сессии
        /// </summary>
        public const string GUEST_ID = "guest_id";

        /// <summary>
        /// Ключ кодов верификации контактов в сессии
        /// </summary>
        const string SESSION_KEY_VERIFICATION_CODES = "VERIFICATION_CODES";

        const string SESSION_CONFIRMED_CONTACTS = "CONFIRMED_CONTACTS";

        #endregion

        #region Внешние зависимости

        private DatabaseContext DbContext { get; set; }
        private List<SiteData> LinkedSites { get; set; }
        private EncryptionKey EncryptionKey { get; set; }
        private TokenAuthOptions TokenAuthOptions { get; set; }
        private readonly IRedisClient _redisClient;
        private GeoLocationClient GeoLocation;
        private readonly AuthorizeManager _authorizeManager;
        private readonly IDistributedCache _cache;
        private readonly ISmsService _smsService;
        private readonly IEmailService _emailService;

        private readonly IHubContext<MessagesHub> _hubContext;

        /// <summary>
        /// Конфигурация
        /// </summary>
        private readonly IConfiguration Configuration;

        private IWebHostEnvironment HostingEnvironment;

        #endregion

        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="linkedSites"></param>
        /// <param name="encryptionKey"></param>
        public AccountController(
            DatabaseContext dbContext, List<SiteData> linkedSites,
            EncryptionKey encryptionKey, TokenAuthOptions tokenAuthOptions,
            IWebHostEnvironment env, IConfiguration con,
            IRedisClient redisClient, GeoLocationClient location,
            IDistributedCache cache, AuthorizeManager authorizeManager,
            ISmsService smsService, IEmailService emailService,
            IHubContext<MessagesHub> hubContext)
        {
            DbContext = dbContext;
            LinkedSites = linkedSites;
            EncryptionKey = encryptionKey;
            TokenAuthOptions = tokenAuthOptions;
            HostingEnvironment = env;
            Configuration = con;
            _redisClient = redisClient;
            GeoLocation = location;
            _cache = cache;
            _smsService = smsService;
            _emailService = emailService;
            _authorizeManager = authorizeManager;

            _hubContext = hubContext;
        }

        #region Авторизация

        // Содержит методы авторизации и деавторизации

        /// <summary>
        /// Показывает страницу авторизации
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Login([FromQuery] string returnUrl, [FromServices] AppData appData)
        {
            // Пользователь уже авторизован
            if (User.Identity.IsAuthenticated)
            {
                if (!string.IsNullOrEmpty(returnUrl)) return Redirect(returnUrl);
                else return RedirectToAction("Index", "Home");
            }

            appData.AddData("countryList", GeoLocation.GetFilterListCountry(defaultFilter: true));
            return View(new LoginForm()
            {
                RememberMe = true
            });
        }

        #region Авторизация по подтвержденному контакту

        /// <summary>
        /// Авторизация пользователя по подтвержденному контакту
        /// </summary>
        /// <param name="loginForm"></param>
        /// <param name="authorizeManager"></param>
        /// <param name="clientConfirmedContacts"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginByContact([FromBody]LoginForm loginForm, [FromServices]AuthorizeManager authorizeManager, [FromServices]ClientConfirmedContacts clientConfirmedContacts)
        {
            var response = new ServiceResponse();

            var phone = DataManager.PreparePhoneNumber(loginForm.Phone);
            var email = phone == null ? loginForm.Email : null;

            if(await clientConfirmedContacts.PhoneIsConfirmed(phone) || await clientConfirmedContacts.EmailIsConfirmed(email))
            {
                // Есть подтвержденный контакт
                var user = new User
                {
                    Email = email,
                    Phone = phone,
                };

                user = await authorizeManager.RegisterUserAsync(user, notifyUser: true);
                await authorizeManager.AuthorizeUser(user);

                response.Success = true;
            }
            else
            {
                response.Message = "Указанный контакт не найден среди подтвержденных.";
            }

            return Json(response);
        }

        #endregion

        #region Авторизация по почте и паролю

        /// <summary>
        /// Авторизация пользователя по электронной почте и паролю
        /// </summary>
        /// <param name="loginForm"></param>
        /// <returns></returns>
        [HttpPost]
        [Produces("application/json")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LoginUserByEmail([FromBody]LoginForm loginForm, [FromServices]AuthorizeManager authorizeManager)
        {
            var response = new ServiceResponse();

            var emailEmpty = string.IsNullOrWhiteSpace(loginForm.Email);
            var passwordEmpty = string.IsNullOrWhiteSpace(loginForm.Password);

            int timeoutSec = 2;

            if (emailEmpty) response.Message = "Электронная почта не указана.";
            else if (!emailEmpty && passwordEmpty)
            {
                // Указана только электронная почта, проверяем, существует ли данная учетка
                Thread.Sleep(TimeSpan.FromSeconds(timeoutSec)); // Принудительная задержка, чтобы усложнить перебор

                var email = loginForm.Email.ToLower();
                var user = await DbContext.Users.FirstOrDefaultAsync(p => p.Email.ToLower() == email);
                if (user != null) response.Success = true;
                else response.Message = "Пользователя с такой электронной почтой не существует.";
            }
            else if (!emailEmpty && !passwordEmpty)
            {
                // Переданы почта и пароль, проверяем пользователя
                Thread.Sleep(TimeSpan.FromSeconds(timeoutSec)); // Принудительная задержка, чтобы усложнить перебор

                var email = loginForm.Email.ToLower();
                var password = DataSecurity.ComputeHash(loginForm.Password);
                var user = await DbContext.Users.FirstOrDefaultAsync(p => p.Email.ToLower() == email && p.Password == password);
                if (user != null)
                {
                    // Пользователь найден, авторизуем                    
                    await authorizeManager.AuthorizeUser(user);
                    response.Success = true;
                }
                else
                {
                    response.Message = "Электронная почта или пароль введены неправильно.";
                }
            }

            return Json(response);
        }

        #endregion

        #region Кросс-сайтовая авторизация

        /// <summary>
        /// Выполняет процедуру кросс-сайтовой авторизации
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <param name="authorizeManager"></param>
        /// <param name="distributedCache"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> CrossLogin([FromQuery]string returnUrl, [FromQuery]int? logout, [FromServices]AuthorizeManager authorizeManager, [FromServices]IDistributedCache distributedCache)
        {
            var otherCabinets = LinkedSites.Select(p => p.CabinetUrl).ToList();

            var keys = new Dictionary<string, string>();
            if (User.Identity.IsAuthenticated && (logout ?? 0) != 1)
            {
                var user = await DbContext.GetUser(User);

                // Генерируем ключи для авторизации
                foreach (var cabinet in otherCabinets)
                {
                    var tokens = authorizeManager.GenerateJwtSecurityToken(user, new string[]
                    {
                        "Authorize " + cabinet
                    });

                    string timeStamp = DateTime.Now.ToString();
                    var hash = DataSecurity.ComputeHashSHA256(tokens.AccessToken + timeStamp);
                    await distributedCache.SetStringAsync("CROSSLOGIN_" + hash, tokens.AccessToken, new DistributedCacheEntryOptions
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(3)
                    });
                    
                    keys.Add(cabinet, hash);
                }
            }
            else
            {
                foreach (var cabinet in otherCabinets) keys.Add(cabinet, "none");
            }

            string url = returnUrl ?? "/";
            foreach(var key in keys) url = $"https://{key.Key}/Account/CrossLoginByCode?code={key.Value}&returnUrl=" + Uri.EscapeDataString(url);
            return Redirect(url);
        }

        /// <summary>
        /// Вызывается при кросс-сайтовой авторизации
        /// </summary>
        /// <param name="code"></param>
        /// <param name="returnUrl"></param>
        /// <param name="authorizeManager"></param>
        /// <param name="distributedCache"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> CrossLoginByCode([FromQuery]string code, [FromQuery]string returnUrl, [FromServices]AuthorizeManager authorizeManager, [FromServices]IDistributedCache distributedCache)
        {
            string token = await distributedCache.GetStringAsync("CROSSLOGIN_" + code);
            int userId = 0;
            if(token != null)
            {
                var jwtToken = authorizeManager.DecodeJwtToken(token);

                var sUserId = (string)jwtToken.Payload.FirstOrDefault(p => p.Key == ClaimTypes.Sid).Value;
                int.TryParse(sUserId, out userId);
            }
            if (userId > 0)
            {
                var user = await DbContext.Users.FirstOrDefaultAsync(p => p.ID == userId);
                if (user != null) await authorizeManager.AuthorizeUser(user);
            }
            else
            {
                await AuthorizeManager.Logout(HttpContext);
            }

            return View("CrossLoginByCode", returnUrl);
        }

        #endregion

        /// <summary>
        /// Авторизует пользователя и переадресует на страницу
        /// </summary>
        /// <param name="from"></param>
        /// <param name="authorizeManager"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> AfterLoginPage([FromQuery] string from, [FromQuery] string returnUrl, [FromServices] AuthorizeManager authorizeManager)
        {
            if (User.Identity.IsAuthenticated && authorizeManager.HasTokens)
            {
                var user = await DbContext.GetUser(User);
                var links = LinkedSites.Select(p => p.URL + Properties.Resources.ShadowAuthScript + "?data=" + authorizeManager.GetAuthorizeData(user, p.Host).GetAsQueryParameter()).ToList();
                authorizeManager.ResetToken();
                return View(links);
            }
            return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// Деавторизует пользователя
        /// </summary>
        /// <param name="from">URL, куда перенаправить после деавторизации.</param>
        /// <returns></returns>
        public async Task<IActionResult> Logout([FromQuery]string from, [FromServices]AuthorizeManager authorizeManager)
        {
            if (User.Identity.IsAuthenticated)
            {
                await AuthorizeManager.Logout(HttpContext);
                authorizeManager.ResetToken();
               
                // TODO: добавить инвалидацию токенов
                // -- Здесь будет инвалидация --

                return View("CrossLoginByCode", Url.Action("CrossLogin", "Account", new
                {
                    returnUrl = from,
                    logout = 1,
                }));                                  
            }

            if (!string.IsNullOrEmpty(from)) return Redirect(from);
            return RedirectToAction("Index", "Home");
        }

        #endregion

        #region Авторизация через соц. сети

        /// <summary>
        /// Возвращает список социальных сетей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public IActionResult SocialNetworks(string formId, [FromServices]OAuthProviders oAuthProviders)
        {
            var state = oAuthProviders.MakeState(state =>
            {
                state.FormId = formId;
            });

            return Json(oAuthProviders.GetList(state));
        }

        /// <summary>
        /// Авторизация через социальную сеть
        /// </summary>
        /// <param name="code"></param>
        /// <param name="provider"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> LoginSocial(string code, string provider, string state, [FromServices]AuthorizeManager authorizeManager, [FromServices]ClientConfirmedContacts clientConfirmedContacts, [FromServices]OAuthProviders oAuthProviders)
        {
            var oAuthProvider = oAuthProviders.GetProviderByName(provider, state);

            if (oAuthProvider != null)
            {
                var stateData = oAuthProviders.ParseState(state);

                var user = oAuthProvider.ProcessLogin(code, state);
                if (user != null && (user.Email != null || user.Phone != null))
                {
                    var hasConfirmedContact = false;

                    if (user.Email != null) hasConfirmedContact |= await clientConfirmedContacts.ConfirmEmailContact(user.Email);
                    if (user.Phone != null) hasConfirmedContact |= await clientConfirmedContacts.ConfirmPhoneContact(user.Phone);

                    if (hasConfirmedContact)
                    {
                        // Оповещаем пользователя, что есть подтвержденный контакт из соц. сети
                        var message = new
                        {
                            stateData.FormId,
                            user.Email,
                            user.Phone,
                        };

                        var messageBody = JsonConvert.SerializeObject(message);
                        await _hubContext.Clients.User(HttpContext.Session.Id).SendAsync("SocialNetworkContactConfirmed", messageBody);

                        return View(true);
                    }
                }
            }

            ModelState.AddModelError(null, "Ошибка авторизации.");
            return View(false);
        }

        #endregion

        #region Обратная совместимость

        // Здесь находятся методы для обратной совместимости

        /// <summary>
        /// Отображает страницу регистрации
        /// </summary>
        /// <param name="from"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Register([FromQuery]string returnUrl)
        {
            // Регистрация убрана, вместо этого производится только вход
            return RedirectToAction("Login", new
            {
                returnUrl = returnUrl,
            });
        }

        /// <summary>
        /// Авторизует на других сайтах
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> AfterLogin([FromServices]AuthorizeManager authorizeManager)
        {
            return await AfterLoginPage("/", "/", authorizeManager);
        }

        #endregion

        #region Взаимодействие с другими сайтами, API
       
        /// <summary>
        /// Выдает новую пару токенов
        /// </summary>
        /// <param name="host"></param>
        /// <returns></returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "RefreshToken")]
        [HttpPost]
        public async Task<IActionResult> RefreshToken([FromQuery]string host, [FromServices]AuthorizeManager authorizeManager)
        {
            AuthorizationToken token = new AuthorizationToken();

            var user = await DbContext.GetUser(User);
            if (user != null)
            {
                // Инвалидируем токен
                // TODO: добавить инвалидацию токена

                // Создаем токены авторизации для других сайтов
                string[] permissions = { "API" };
                var tokens = authorizeManager.GenerateJwtSecurityToken(user, permissions);

                token.UserId = user.ID;
                token.Host = host;
                token.Token = tokens.AccessToken;
                token.RefreshToken = tokens.RefreshToken;
                token.Expires = DateTime.Now.AddMinutes(TokenAuthOptions.RefreshTokenLifetime);

                // Добавляем токены в БД
                //await DbContext.AuthorizationTokens.AddAsync(token);
                //await DbContext.SaveChangesAsync();
            }

            var securedMessage = SecuredMessage.Create(token, EncryptionKey);
            return Json(securedMessage);
        }

        /// <summary>
        /// Авторизует сервисного пользователя под API
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost("/api/token")]
        public async Task<IActionResult> LoginServiceUser([FromBody]LoginFormApi form)
        {
            var hashedPassword = DataSecurity.ComputeHash(form.Password);
            var user = await DbContext.ServiceUsers.FirstOrDefaultAsync(p => p.User == form.User && p.Password == hashedPassword);
            if (user == null)
            {
                Response.StatusCode = 400;
                return Json("Wrong username or password.");
            }

            var permissions = DbContext.ServiceUserPermissions.Where(p => p.UserId == user.ID).Select(p => p.Permission).ToArray();
            var jwt = AuthorizeManager.AuthenticateToken(HttpContext, user.User, permissions, TokenAuthOptions, false);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return Json(new TokenApi
            {
                Token = encodedJwt,
                Username = user.User
            });
        }

        /// <summary>
        /// Устанавливает идентификатор сессии пользователя
        /// </summary>
        /// <returns></returns>
        private async Task SetSessionToken()
        {
            if (!HttpContext.Session.Keys.Contains(GUEST_ID))
            {
                HttpContext.Session.SetString(GUEST_ID, DataSecurity.ComputeHash(HttpContext.Session.Id));
                await HttpContext.Session.CommitAsync();
            }
        }

        /// <summary>
        /// Возвращает токен доступа для хаба
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetHubToken()
        {
            var claims = new List<Claim>();

            if(User.Identity.IsAuthenticated && User.IsInRole(AuthorizeManager.ROLE_USER))
            {
                claims = User.Claims.ToList();
            }
            else
            {
                claims.Add(new Claim(ClaimsIdentity.DefaultNameClaimType, "Guest"));
                claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, "Guest"));
                claims.Add(new Claim(ClaimTypes.Sid, HttpContext.Session.Id));
            }

            await SetSessionToken();

            var now = DateTime.Now;
            var identity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var token = new JwtSecurityToken(
                issuer: TokenAuthOptions.Issuer,
                audience: TokenAuthOptions.Audience,
                claims: identity.Claims,
                notBefore: now,
                expires: now.Add(TimeSpan.FromDays(3)),
                signingCredentials: new SigningCredentials(TokenAuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256)
            );

            var handler = new JwtSecurityTokenHandler();
            var encryptedToken = handler.WriteToken(token);

            return Json(new ServiceResponse
            {
                Data = encryptedToken,
                Success = true
            });
        }

        /// <summary>
        /// Проверяет, установлен ли идентификатор пользователя
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public IActionResult CheckHubToken()
        {
            bool tokenValid = false;

            if(HttpContext.Session.Keys.Contains(GUEST_ID))
            {
                tokenValid = true;
            }

            return Json(new ServiceResponse
            {
                Data = tokenValid,
                //Data = false,
                Success = true,
            });
        }

        /// <summary>
        /// Принудительно устанавливает идентификатор сессии пользователю
        /// </summary>
        /// <param name="to"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> UserDetect(string to)
        {            
            await SetSessionToken();
            return View(new UserDetect
            {
                RedirectTo = to,
            });
        }

        [HttpGet]
        public async Task<IActionResult> StorageAccessRequest()
        {
            await SetSessionToken();
            return View();
        }

        #endregion

        #region Верификация номера телефона и электронной почты

        /// <summary>
        /// Запрашивает код подтверждения email или номера телефона
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [Produces("application/json")]       
        [ValidateCaptcha]       
        public async Task<IActionResult> RequestConfirmationCode([FromBody]ConfirmationCodeRequest request, [FromServices]IEmailService emailService, [FromServices]Limits limits, [FromServices]ClientConfirmedContacts clientConfirmedContacts)
        {
            var response = new ServiceResponse();

            request.Normalize();

            // Проверяем, нужно ли подтверждать контакт
            var confirmationData = new ContactConfirmationData
            {
                NeedConfirmation = true,
            };

            if (await clientConfirmedContacts.ContactIsConfirmed(request))
            {
                // Контакт уже подтвержден
                response.Success = true;
                confirmationData.NeedConfirmation = false;
            }
            else
            {
                if (request.Intent == ConfirmationCodeRequest.ConfirmationCodeIntent.Registration)
                {
                    // Если регистрация или установка кода по умолчанию
                    var user = await FindUserByConfirmationCodeRequest(request);
                    if (user != null)
                    {
                        response.Message = "Пользователь с такими контактными данными уже зарегистрирован. Пожалуйста, авторизуйтесь.";
                        return Json(response);
                    }
                }

                if(!clientConfirmedContacts.RequestConfirmationCode(request, out string message))
                {
                    response.Message = message;
                    return Json(response);
                }              

                // Отправка сообщений
                if (HostingEnvironment.IsProduction())
                {
                    // Если продакшен, отправляем код на указанный контакт
                    if (request.IsPhone())
                    {
                        // Проверяем, что номер телефона российский
                        var regex = new Regex(@"^\+?79\d{9}$");
                        if (regex.IsMatch(request.Phone))
                        {
                            string content = $"Код подтверждения: {request.VerificationCode}\r\nООО MasterGroup.";
                            if (_smsService.SendMessage(request.Phone, content)) response.Success = true;
                        }
                        else
                        {
                            response.Message = "Подтвердить можно только мобильные номера Российской Федерации,<br />записанные в международном формате +7-9XX-XXX-XX-XX.<br />Если Вы нерезидент РФ, то Вы можете подтвердить электронную почту.";
                            return Json(response);
                        }
                    }
                    else if (request.IsEmail())
                    {
                        var template = new EmailTemplate("EmailConfirm", "Подтверждение электронной почты");
                        template.SetData("CODE", request.VerificationCode);
                        if (await emailService.SendMessage(request.Email, "Подтверждение электронной почты", template.Compile())) response.Success = true;
                    }

                    if (!response.Success) response.Message = "Ошибка при отправке.";
                }
                else
                {
                    // Не продакшен, возвращаем код
                    response.Message = request.VerificationCode;
                    response.Success = true;
                }
            }

            response.Data = confirmationData;

            return Json(response);
        }

        /// <summary>
        /// Ищет пользователя по контактам из запроса кода верификации
        /// </summary>
        /// <param name="codeRequest"></param>
        /// <returns></returns>
        public async Task<User> FindUserByConfirmationCodeRequest(ConfirmationCodeRequest codeRequest)
        {
            if(codeRequest.IsPhone()) return await _authorizeManager.FindUserAsync(codeRequest.Phone);                
            else if(codeRequest.IsEmail()) return await _authorizeManager.FindUserAsync(codeRequest.Email);
            return null;
        }

        /// <summary>
        /// Проверяет код авторизации
        /// </summary>
        /// <param name="codeRequest"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Produces("application/json")]
        public async Task<IActionResult> CheckConfirmationCode([FromBody]ConfirmationCodeRequest codeRequest, [FromServices]AuthorizeManager authorizeManager, [FromServices]ClientConfirmedContacts clientConfirmedContacts)
        {
            var response = new ServiceResponse();

            // Проверяем контакт
            if (!clientConfirmedContacts.CheckConfirmationCode(codeRequest, out string message))
            {
                // Контакт не подтвержден
                response.Message = message;
                return Json(response);
            }
            else
            {
                response.Success = true;

                // Контакт подтвержден
                var user = await DbContext.GetUser(User);

                // Подтверждение с целью авторизоваться
                if(user == null && codeRequest.Intent == ConfirmationCodeRequest.ConfirmationCodeIntent.Login)
                {
                    // Ищем или создаем пользователя
                    user = new User
                    {
                        Email = codeRequest.Email,
                        Phone = codeRequest.Phone,
                    };
                    user = await authorizeManager.RegisterUserAsync(user);
                    await authorizeManager.AuthorizeUser(user);
                }

                if (user != null)
                {
                    // Пользователь авторизован
                    var confirmedContract = codeRequest.GetConfirmedContact();
                    if (confirmedContract.Type == UserConfirmedContact.ContactType.Email)
                    {
                        // Проверяем, что контакт не добавлен
                        if (await DbContext.UserConfirmedEmails.FirstOrDefaultAsync(p => p.UserId == user.ID && p.Email == confirmedContract.Contact) == null)
                        {
                            var contact = new UserConfirmedEmail
                            {
                                Email = confirmedContract.Contact,
                                User = user,
                            };
                            await DbContext.UserConfirmedEmails.AddAsync(contact);
                        }
                    }
                    else if (confirmedContract.Type == UserConfirmedContact.ContactType.Phone)
                    {
                        // Проверяем, что контакт не добавлен
                        if (await DbContext.UserConfirmedPhones.FirstOrDefaultAsync(p => p.UserId == user.ID && p.Phone == confirmedContract.Contact) == null)
                        {
                            var contact = new UserConfirmedPhone
                            {
                                Phone = confirmedContract.Contact,
                                User = user,
                            };
                            await DbContext.UserConfirmedPhones.AddAsync(contact);
                        }
                    }

                    await DbContext.SaveChangesAsync();
                }
            }           

            return Json(response);
        }

        /// <summary>
        /// Получает объект из сессии пользователя
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetObjectFromSession<T>(string key)
        {
            var data = HttpContext.Session.GetString(key);
            return data != null ? JsonConvert.DeserializeObject<T>(data) : Activator.CreateInstance<T>();
        }

        /// <summary>
        /// Устанавливает объект в сессию пользователя
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        public void SetObjectToSession(string key, object obj)
        {
            HttpContext.Session.SetString(key, JsonConvert.SerializeObject(obj));
        }

        #endregion

        #region Вспомогательные методы

        /// <summary>
        /// Возвращает информацию о пользователе
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetPersonalData()
        {
            var user = await DbContext.GetUser(User);
            if (user != null)
            {
                user.Password = null;
                user.ConfirmedPhones = DbContext.UserConfirmedPhones.Where(p => p.UserId == user.ID).Select(p => p.Phone).ToList();
                user.ConfirmedEmails = DbContext.UserConfirmedEmails.Where(p => p.UserId == user.ID).Select(p => p.Email).ToList();
            }
            else
            {
                user = new User();

                var confirmedContacts = GetObjectFromSession<List<UserConfirmedContact>>(SESSION_CONFIRMED_CONTACTS);
                user.ConfirmedEmails = confirmedContacts.Where(p => p.Type == UserConfirmedContact.ContactType.Email).Select(p => p.Contact).ToList();
                user.ConfirmedPhones = confirmedContacts.Where(p => p.Type == UserConfirmedContact.ContactType.Phone).Select(p => p.Contact).ToList();
            }

            return Json(user);
        }

        /// <summary>
        /// Возвращает форму Возникли сложности
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetHavingDifficultiesForm()
        {
            return PartialView("Partial/HavingDifficultiesForm");
        }

        /// <summary>
        /// Отправляет сообщения об ошибка в JS
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult SaveJaveScriptError(string message, string filename, string lineno)
        {

            if (HostingEnvironment.IsProduction())
            {
                var userAgent = HttpContext.Request.Headers["User-Agent"];
                string uaString = Convert.ToString(userAgent[0]);

                string msg = "Anketa - Error JS message: " + message + "; filename: " + filename + "; lineno: " + lineno + " ;User-Agent:" + uaString;
                DataManager.GetMessageMychat(msg);
            }
            return Json(true);

        }

        #endregion

        #region Смена пароля

        /// <summary>
        /// Меняет пароль текущего пользователя
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> ChangePassword([FromBody]PasswordChangeRequest model)
        {
            var response = new ServiceResponse();

            var user = await DbContext.GetUser(User);
            if(user == null)
            {
                response.Message = "Пользователь не указан. Перезагрузите страницу.";
            }
            else if(model.Password == null || model.Password.Length < 6)
            {
                response.Message = "Пароль должен состоять минимум из 6 символов.";
            }
            else if(model.Password != null && model.Password.Contains(' '))
            {
                response.Message = "Пароль содержит пробелы.";
            }
            else if(model.Password != model.PasswordConfirm)
            {
                response.Message = "Пароль и его подтверждение не совпадают.";
            }
            else
            {
                // Ошибок нет, меняем пароль
                user.Password = DataSecurity.ComputeHash(model.Password);
                DbContext.Users.Update(user);
                await DbContext.SaveChangesAsync();

                response.Message = "Пароль изменен.";
                response.Success = true;            
            }

            return Json(response);            
        }

        #endregion

        #region Гость

        /// <summary>
        /// Возвращает интерфейс для возможности сквозной авторизации
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<IActionResult> RegisterGuest([FromQuery]int? userId, [FromServices]AuthorizeManager authorizeManager)
        {
            object response = null;

            if(User.Identity.IsAuthenticated)
            {
                // Получаем пользователя
                var user = await DbContext.GetUser(User);

                if (userId == null || user.ID != userId.Value)
                {
                    // Получаем хост
                    string host = null;
                    if (Request.Headers.ContainsKey("Origin"))
                    {
                        var regex = new Regex(@"https?:\/\/(?<host>[^\/]+)");
                        var match = regex.Match(Request.Headers["Origin"]);
                        host = match.Groups.ContainsKey("host") ? match.Groups["host"].Value : null;
                    }

                    AuthorizationToken token = new AuthorizationToken();

                    // Создаем токены авторизации для других сайтов
                    string[] permissions = { "API" };
                    var tokens = authorizeManager.GenerateJwtSecurityToken(user, permissions);

                    token.UserId = user.ID;
                    token.Host = host;
                    token.Token = tokens.AccessToken;
                    token.RefreshToken = tokens.RefreshToken;
                    token.Expires = DateTime.Now.AddMinutes(TokenAuthOptions.RefreshTokenLifetime);

                    response = token;
                }
            }
            else
            {
                var guestId = DataSecurity.ComputeHash(HttpContext.Session.Id);
                response = new GuestToken
                {
                    GuestId = guestId,
                };                
                HttpContext.Session.SetString(GUEST_ID, guestId);
                await HttpContext.Session.CommitAsync();
            }
            
            var responseData = response != null ? SecuredMessage.Create(response, EncryptionKey).GetAsQueryParameter() : null;
            return Json(responseData);
        }

        #endregion

        #region Капча

        /// <summary>
        /// Возвращает капчу
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Produces("application/json")]
        public IActionResult CaptchaConfiguration([FromServices]ICaptchaService captchaService)
        {
            //return View("Captcha");
            return Json(captchaService.GetConfiguration());
        }

        #endregion
    }
}