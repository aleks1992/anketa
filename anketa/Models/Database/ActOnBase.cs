﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Представляет собой описание основания действия
    /// </summary>
    [Serializable]
    public class ActOnBase
    {
        /// <summary>
        /// ID основания
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Наименование основания
        /// </summary>
        [Display(Name = "Наименование основания")]
        public string Name { get; set; }
    }
}
