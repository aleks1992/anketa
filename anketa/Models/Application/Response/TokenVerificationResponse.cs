﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Response
{
    public class TokenVerificationResponse
    {
        /// <summary>
        /// Показывает, что токен валидный
        /// </summary>
        public bool TokenValid { get; set; }
    }
}
