﻿using anketa.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Forms
{
    public class DeliveryFrom
    {
        /// <summary>
        /// Адрес доставки
        /// </summary>
        public string DeliveryAddress { get; set; }

        /// <summary>
        /// Адрес отправки
        /// </summary>
        public string ShippingAddress { get; set; }

        /// <summary>
        ///Список товаров
        /// </summary>
        public List<ProductsDelivery> ProductsDelivery { get; set; }

        /// <summary>
        /// Адресная доставка
        /// </summary>
        public bool IsAddress { get; set; }
    }

    /// <summary>
    /// Информация о товаре доставки
    /// </summary>
    public class ProductsDelivery
    {


        public int Id { get; set; }

        public string Guid { get; set; }

        public int Count { get; set; }

        public string Name { get; set; }

        public string Sku { get; set; }

        public string Description { get; set; }

        public string ProductStatus { get; set; }

        public string Prefix { get; set; }

        public decimal Volume { get; set; }

        public decimal Weight { get; set; }

        public decimal Width { get; set; }

        public decimal Length { get; set; }

        public decimal Height { get; set; }

        public decimal Price { get; set; }

        public bool IsValidDelivery { get; set; }

    }

}
