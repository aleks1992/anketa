﻿
/**
 * Форма входа в анкету
 */

import Vue from 'vue/dist/vue.esm.js';
import Login from './login.vue';

Vue.component('login', Login);
new Vue({
    el: '#app',
    render: r => r(Login),
});

