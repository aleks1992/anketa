﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace anketa.Misc
{
    public abstract class ModelConverter
    {
        public static Dictionary<string, Dictionary<string, object>> GetModel(Type type)
        {
            var info = new Dictionary<string, Dictionary<string, object>>();

            foreach(var property in type.GetProperties())
            {
                var data = new Dictionary<string, object>();
                data.Add("DefaultValue", GetDefaultValue(property.PropertyType));
                foreach(var customAttribute in property.CustomAttributes)
                {
                    if (customAttribute.AttributeType == typeof(System.ComponentModel.DataAnnotations.DisplayAttribute))
                    {
                        foreach (var namedArgument in customAttribute.NamedArguments)
                        {
                            if (namedArgument.TypedValue.Value is string argumentValue) data.Add(namedArgument.MemberName, argumentValue);
                        }
                    }
                }
                info.Add(property.Name, data);
            }

            return info;
        }


        private static object GetDefaultValue(Type type)
        {
            if(type.IsValueType) return Activator.CreateInstance(type);
            return null;
        }
    }
}
