﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.API
{
    public class LoginFormApi
    {
        public string User { get; set; }
        public string Password { get; set; }
    }
}
