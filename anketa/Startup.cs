﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using anketa.Misc;
using anketa.Misc.Content;
using anketa.Misc.GeoLocation;
using anketa.Misc.Redis;
using anketa.Misc.SiteAPI;
using anketa.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.SpaServices.Webpack;
using reCAPTCHA.AspNetCore;
using anketa.Misc.MessageServices;
using anketa.Models.Application;
using anketa.Misc.Services.Cookie;
using anketa.Models.Database;
using anketa.Hubs;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using anketa.Misc.Services;
using Microsoft.VisualStudio.Web.CodeGeneration.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using anketa.Misc.OAuthAPI;
using Microsoft.AspNetCore.Routing;
using anketa.Misc.Captcha;

namespace anketa
{
    public class Startup
    {
        private IWebHostEnvironment Env { get; set; }

        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Env = env;
        }

        /// <summary>Конфигурация приложения</summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// Добавляет сервисы в контейнер выполнения
        /// </summary>
        /// <param name="services">Интерфейс сервисов</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<AnketaVersion>();

            // Конфигурируем куки
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
                options.OnAppendCookie = cookieContext => CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
                options.OnDeleteCookie = cookieContext => CheckSameSite(cookieContext.Context, cookieContext.CookieOptions);
            });

            // Подключение к БД
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<DatabaseContext>(options => options.UseMySql(connectionString));

            // Подключение к Redis
            services.AddTransient<IRedisClient, StackExchangeRedisClient>();

            // Сохраняем ключи пользователей в БД
            services.AddDataProtection().PersistKeysToDbContext<DatabaseContext>();

            // Включаем систему авторизации
            var tokenAuthOptions = Configuration.GetSection("TokenAuthOptions").Get<TokenAuthOptions>();

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            })
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
                        {
                            options.LoginPath = "/Account/Login";
                            options.LogoutPath = "/Account/Logout";
                            options.AccessDeniedPath = "/Error?StatusCode=403";
                            options.Cookie.SameSite = SameSiteMode.None;
                            options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
                            options.Cookie.IsEssential = true;
                        })
                .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
                {
                    options.RequireHttpsMetadata = false;
                    //options.SaveToken = true;

                    options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidIssuer = tokenAuthOptions.Issuer,
                        ValidateAudience = true,
                        ValidAudience = tokenAuthOptions.Audience,
                        ValidateLifetime = true,
                        IssuerSigningKey = tokenAuthOptions.GetSymmetricSecurityKey(),
                        ValidateIssuerSigningKey = true
                    };

                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            var accessToken = context.Request.Query["access_token"];
                            var path = context.HttpContext.Request.Path;
                            if (!string.IsNullOrEmpty(accessToken) && path.StartsWithSegments("/hub/messages"))
                            {
                                context.Token = accessToken;
                            }
                            return Task.CompletedTask;
                        }
                    };
                });

            services.AddAuthorization(options =>
            {
                options.DefaultPolicy = new AuthorizationPolicyBuilder()
                    .RequireAuthenticatedUser()
                    .AddAuthenticationSchemes(CookieAuthenticationDefaults.AuthenticationScheme, JwtBearerDefaults.AuthenticationScheme)
                    .Build();

                options.AddPolicy("Hub", policy =>
                {
                    policy.AuthenticationSchemes = new List<string> { JwtBearerDefaults.AuthenticationScheme };
                    policy.RequireAuthenticatedUser();
                });
            });


            services.AddSingleton(tokenAuthOptions);

            // Включаем реализацию HttpContextAccessor, для доступа к Http контексту
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();

            // Включаем кэш для приложения
            var redisConfiguration = Configuration.GetSection("Redis").Get<RedisConfiguration>();
            if (redisConfiguration.HostAddress != null)
            {
                // Если настройки для Redis переданы, используем Redis как хранилище
                services.AddStackExchangeRedisCache(options =>
                {
                    options.Configuration = redisConfiguration.HostAddress;
                    options.InstanceName = "ANKETA_CACHE_";
                });
            }
            else
            {
                // Если настройки Redis не указаны, используем кэш в памяти
                services.AddDistributedMemoryCache();
            }

            // Включаем вспомогательный класс для кэша
            services.AddSingleton<ApplicationCache>();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromDays(30);
                options.Cookie.IsEssential = true;
                options.Cookie.MaxAge = TimeSpan.FromDays(30);
                options.Cookie.SameSite = SameSiteMode.None;
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
            });

            // Загружаем список сайтов для авторизации
            var sites = new List<SiteData>();
            Configuration.Bind("LinkedSites", sites);
            services.AddSingleton(sites);
            SiteData.InitInstance(sites);

            // Загружаем OAuth2 провайдеры
            services.AddSingleton<OAuthProviders>();

            // Подгрузка получения адресса
            var iAddressSuggestion = new IAddressSuggestion();
            services.AddSingleton(iAddressSuggestion);

            // Определение геолокации пользователя
            services.AddSingleton(typeof(GeoLocationClient));

            /*var location = new GeoLocationClient();
            location.LoadGeoIP();
            location.LoadListCountry();
            services.AddSingleton(location);*/

            // Загружаем ключи шифрования
            services.AddSingleton(Configuration.GetSection("EncryptionKey").Get<EncryptionKey>());

            #region Сообщения пользователям

            // Инициализируем почтовик
            services.AddTransient<IEmailService, BatutMasterEmailService>();

            // Инициализируем сервис отправки SMS сообщений
            services.AddSingleton<ISmsService, BatutMasterSMSService>();

            // Загружаем данные о лимитах
            services.AddSingleton<Limits>();

            #endregion

            // Загружаем модуль для хранения подтвержденных контактов клиента
            services.AddScoped<ClientConfirmedContacts>();

            // Загружаем модуль для обновления корзины\избранного на связанных сайтах
            services.AddTransient<CartWishlistManager>();
            services.AddTransient<ProductLoader>();

            // Включаем менеджер авторизации
            services.AddScoped<AuthorizeManager>();

            ModelEngine.UpdateClassesInfo();
            services.AddScoped<ModelEngine>();
            services.AddScoped<AppData>();
           

            // Добавляем разрешения для CORS
            services.AddCors();

            // Включаем поддержку Razor pages
            IMvcBuilder builder = services.AddRazorPages();

            builder.AddNewtonsoftJson();

            // Добавляем биндер для зашифрованных сообщений
            builder.AddMvcOptions(options => {
                options.ModelBinderProviders.Insert(0, new SecuredMessageBinderProvider());
            });

#if DEBUG
            if (Env.IsDevelopment())
            {
                // Добавляем runtime-компиляцию представлений
                builder.AddRazorRuntimeCompilation();
            }
#endif

            // Настраиваем капчу
            services.AddCaptcha(options =>
            {
                options.CaptchaService = typeof(RecaptchaImplementation);
                options.Configuration = Configuration.GetSection("CaptchaConfiguration:v2");
            });

            // Настраиваем RabbitMQ клиент
            RabbitMQClient.SetConfiguration(Configuration);

            // SignalR
            services.AddSignalR();
            services.AddSingleton<Microsoft.AspNetCore.SignalR.IUserIdProvider, SessionUserIdProvider>();

            // Отключаем автоматическую проверку моделей
            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressModelStateInvalidFilter = true;
            });

            //Сервис отправки сообщении
            services.AddHostedService<IEmailSending>();
        }

        /// <summary>
        /// Конфигурирование приложения
        /// </summary>
        /// <param name="app">Интерфейс сборщика приложения</param>
        /// <param name="env">Интерфейс среды развертывания</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, List<SiteData> linkedSites)
        {
            // Установим параметры глобализации
            var cultureInfo = new CultureInfo("ru-RU");
            cultureInfo.NumberFormat.CurrencySymbol = "руб.";
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

#if DEBUG
                // Включаем webpack
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
#endif

                app.UseHttpsRedirection();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            // Конфигурируем Kestrel для работы через nginx-proxy
            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseResponseBuffering();

            // Настройки CORS
            app.UseCors(builder =>
            {
                builder.WithOrigins(linkedSites.Select(p => p.URL.TrimEnd('/')).ToArray())
                    .AllowCredentials()
                    .AllowAnyHeader()
                    .AllowAnyMethod();
            });

            app.UseRouting();

            app.UseStaticFiles();

            app.UseCookiePolicy();

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseSession();
            

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapFallbackToController("Index", "Home");

                endpoints.MapHub<MessagesHub>("/hub/messages");
            });
        }

        /// <summary>
        /// Фильтр отдаваемых кук
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="options"></param>
        private void CheckSameSite(HttpContext httpContext, CookieOptions options)
        {
            if(options.SameSite == SameSiteMode.None)
            {
                var userAgent = httpContext.Request.Headers["User-Agent"].ToString();
                if (BrowserDetection.DisallowsSameSiteNone(userAgent)) options.SameSite = SameSiteMode.Unspecified;
            }

            /*if(options.Domain == null || options.Domain.Length == 0)
            {
                options.Domain = httpContext.Request.Host.Host;
            }*/
        }
    }
}
