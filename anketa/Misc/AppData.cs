﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc
{
    public class AppData
    {
        private Dictionary<string, object> appData = new Dictionary<string, object>();

        public AppData()
        {

        }

        public void AddData(string name, object data)
        {
            appData.Add(name, data);
        }

        public object RenderData()
        {
            if(appData.Count > 0)
            {
                List<string> output = new List<string>();

                foreach(var k in appData)
                {
                    var data = JsonConvert.SerializeObject(k.Value);
                    output.Add(string.Format("window.appData.{0} = {1};", k.Key, data));
                }

                return string.Join('\n', output);
            }

            return null;
        }

        public bool HasData
        {
            get
            {
                return appData.Count > 0;
            }
        }
    }
}
