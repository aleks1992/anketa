﻿using anketa.Models;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Authentication;

namespace anketa.Misc.MessageServices
{
    /// <summary>
    /// Сервис отправки электронных писем
    /// </summary>
    public class BatutMasterEmailService : IEmailService
    {
        /// <summary>
        /// Настройки почтовика
        /// </summary>
        private readonly EmailSettings _emailSettings;

        /// <summary>
        /// Параметры хостинга
        /// </summary>
        private readonly IWebHostEnvironment _hostingEnvironment;

        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private readonly DatabaseContext _dbContext;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="configuration"></param>
        public BatutMasterEmailService(IConfiguration configuration, DatabaseContext dbContext, IWebHostEnvironment hostingEnvironment)
        {
            _emailSettings = configuration.GetSection("Mailer").Get<EmailSettings>();
            _dbContext = dbContext;
            _hostingEnvironment = hostingEnvironment;
        }

        /// <summary>
        /// Показывает, готов ли сервис к работе
        /// </summary>
        private bool IsReady
        {
            get
            {
                return _emailSettings != null;
            }
        }

        /// <summary>
        /// Отправляет электронное письмо на указанный адрес
        /// </summary>
        /// <param name="receiver"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        public async Task<bool> SendMessage(string receiver, string subject, string body, bool isSave = true)
        {
            // Готовим сообщение
            receiver = receiver.Trim();
            var emailMessage = new MimeMessage();
            emailMessage.From.Add(new MailboxAddress(_emailSettings.FromName, _emailSettings.FromEmail));
            emailMessage.To.Add(new MailboxAddress("", receiver));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = body
            };

            // Пытаемся отправить сообщение
            var sendedSuccess = true;
            try
            {
                using (var client = new SmtpClient())
                {
                    await client.ConnectAsync(_emailSettings.SmtpHost, _emailSettings.SmtpPort, _emailSettings.UseSsl);
                    await client.AuthenticateAsync(_emailSettings.Username, _emailSettings.Password);
                    await client.SendAsync(emailMessage);
                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception e)
            {
                sendedSuccess = false;
                Console.WriteLine(e.Message);
                Console.WriteLine($"Сообщение адресату {receiver} не отправлено.");
            }

            // Отправка завершилась ошибкой, отправляем нам уведомление
            if (!sendedSuccess && _hostingEnvironment.IsProduction())
            {
                DataManager.GetMessageMychat($"Anketa - Error on send email to {receiver}.");
            }

            if (isSave)
            {
                // Записываем сообщение в базу
                await _dbContext.UserEmailMessages.AddAsync(new Models.Database.UserEmailMessage
                {
                    Email = receiver,
                    Subject = subject,
                    Meassage = body,
                    IsSend = sendedSuccess,
                    DateCreated = DateTime.Now,
                    DateSending = sendedSuccess ? DateTime.Now : DateTime.MinValue,
                });
                await _dbContext.SaveChangesAsync();
            }

            return sendedSuccess;
        }
    }
}
