﻿using anketa.Controllers;
using anketa.Misc.Redis;
using anketa.Models;
using anketa.Models.API;
using anketa.Models.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace anketa.Misc.SiteAPI
{
    public class CartWishlistManager
    {
        const int REDIS_DATABASE = 1;

        private readonly IRedisClient _redisClient;
        private readonly HttpContext _httpContext;

        private string UserId;

        public CartWishlistStorage Storage { get; set; }

        /// <summary>
        /// Возвращает статус менеджера
        /// </summary>
        public bool Status
        {
            get
            {
                return _redisClient.Status;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="redisClient"></param>
        /// <param name="httpContextAccessor"></param>
        /// <param name="dbContext"></param>
        public CartWishlistManager(IRedisClient redisClient, IHttpContextAccessor httpContextAccessor)
        {
            _redisClient = redisClient;
            _httpContext = httpContextAccessor.HttpContext;

            Reinit();
        }

        /// <summary>
        /// Возвращает настройки сериализатора
        /// </summary>
        /// <returns></returns>
        private Newtonsoft.Json.JsonSerializerSettings GetSerializerSettings()
        {
            var resolver = new DefaultContractResolver
            {
                NamingStrategy = new SnakeCaseNamingStrategy()
            };

            return new Newtonsoft.Json.JsonSerializerSettings
            {
                ContractResolver = resolver,
                Formatting = Newtonsoft.Json.Formatting.None
            };
        }

        /// <summary>
        /// Возвращает идентификатор хранилища пользователя
        /// </summary>
        /// <returns></returns>
        private string GetUserStorageId()
        {
            string userStorageId = null;

            // Если принудительно указан идентификатор пользователя, используем его
            if (!string.IsNullOrEmpty(UserId)) return "ANKETA_USER_" + UserId;

            if (_httpContext.User.Identity.IsAuthenticated && _httpContext.User.IsInRole("User"))
            {
                var claim = _httpContext.User.Claims.Where(p => p.Type == ClaimTypes.Sid).FirstOrDefault();
                if (claim != null)
                {
                    userStorageId = "ANKETA_USER_" + claim.Value;
                }
            }
            else
            {
                //if (_httpContext.Session.TryGetValue(AccountController.GUEST_ID, out byte[] val))
                //{
                //    userStorageId = "ANKETA_GUEST_" + Encoding.Default.GetString(val);
                //}
                userStorageId = "ANKETA_GUEST_" + DataSecurity.ComputeHash(_httpContext.Session.Id);
            }

            return userStorageId;
        }

        /// <summary>
        /// Читает данные из хранилища
        /// </summary>
        /// <param name="key">Ключ хранилища</param>
        /// <returns></returns>
        private CartWishlistStorage ReadStorage(string key)
        {
            if (string.IsNullOrEmpty(key)) return new CartWishlistStorage();

            _redisClient.Select(REDIS_DATABASE);

            string data = null;
            try
            {
                data = _redisClient.GetString(key);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            _redisClient.RollbackSelect();

            CartWishlistStorage storage = null;
            if(data != null)
            {
                try
                {
                    storage = JsonConvert.DeserializeObject<CartWishlistStorage>(data, GetSerializerSettings());
                    storage.Cart.ForEach(e => e.ContainsInOthers = storage.Wishlist.Where(w => w.Guid == e.Guid).FirstOrDefault() != null);
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    storage = null;
                }
            }

            // Если хранилище не инициализировано, создаем пустое хранилище
            if (storage == null) storage = new CartWishlistStorage();

            return storage;
        }

        /// <summary>
        /// Переинициализирует хранилище
        /// </summary>
        /// <param name="userId">Если указан, то используется этот идентификатор в дальнейшем</param>
        public void Reinit(string userId = null)
        {
            this.UserId = userId;
            string userStorageId = GetUserStorageId();
            Storage = ReadStorage(userStorageId);
        }


        /// <summary>
        /// Записывает данные в хранилище
        /// </summary>
        public bool Commit()
        {
            string userStorageId = GetUserStorageId();
            if(userStorageId != null)
            {
                var record = JsonConvert.SerializeObject(Storage, GetSerializerSettings());
                _redisClient.Select(REDIS_DATABASE);
                _redisClient.SetValue(userStorageId, record);
                _redisClient.RollbackSelect();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Переносит данные из сессии, если они есть, в хранилище определенного пользователя
        /// </summary>
        public void MoveDataFromSession()
        {
            if(_httpContext.User.Identity.IsAuthenticated || !string.IsNullOrEmpty(UserId))
            {
                string userStorageId = null;
                userStorageId = "ANKETA_GUEST_" + DataSecurity.ComputeHash(_httpContext.Session.Id);

                if(userStorageId != null)
                {
                    var sessionStorage = ReadStorage(userStorageId);

                    // Переносим корзину
                    Storage.Cart.AddRange(sessionStorage.Cart);

                    // Переносим избранное
                    Storage.Wishlist.AddRange(sessionStorage.Wishlist);

                    // Сохраняем изменения
                    Commit();

                    // Удаляем данные о госте
                    _httpContext.Session.Remove(AccountController.GUEST_ID);
                }
            }
        }
    }
}
