﻿using anketa.Misc.MessageServices;
using anketa.Misc.SiteAPI;
using anketa.Models;
using anketa.Models.Application;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace anketa.Misc
{
    public class AuthorizeManager
    {
        #region Константы

        const string SESSION_TOKENS_KEY = "TOKENS";

        public const string ROLE_USER = "User";
        public const string ROLE_API = "API";
        public const string ROLE_REFRESH_TOKEN = "RefreshToken";

        public const string AUTH_TYPE_COOKIE = "ApplicationCookie";

        #endregion

        #region Внешние зависимости

        private readonly IHttpContextAccessor _httpContext;
        private readonly EncryptionKey _encryptionKey;
        private readonly CartWishlistManager _cartWishlistManager;
        private readonly DatabaseContext _dbContext;
        private JwtSecurityTokens tokens;
        private readonly ISmsService _smsService;
        private readonly IEmailService _emailService;

        private static TokenAuthOptions tokenAuthOptions;

        #endregion

        public AuthorizeManager(IConfiguration configuration, IHttpContextAccessor httpContext, 
            EncryptionKey encryptionKey, CartWishlistManager cartWishlistManager, 
            DatabaseContext dbContext,
            ISmsService smsService, IEmailService emailService)
        {
            if (tokenAuthOptions == null) tokenAuthOptions = configuration.GetSection("TokenAuthOptions").Get<TokenAuthOptions>();

            _httpContext = httpContext;
            _encryptionKey = encryptionKey;
            _cartWishlistManager = cartWishlistManager;
            _dbContext = dbContext;
            _smsService = smsService;
            _emailService = emailService;

            var tokensData = httpContext.HttpContext.Session.GetString(SESSION_TOKENS_KEY);
            try
            {
                if (tokensData != null) tokens = JsonConvert.DeserializeObject<JwtSecurityTokens>(tokensData);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Авторизует пользователя
        /// </summary>
        /// <param name="user"></param>
        /// <param name="roles"></param>
        /// <param name="rememberMe"></param>
        /// <param name="withAccessToken"></param>
        /// <returns></returns>
        public async Task Authorize(User user, string[] roles, bool rememberMe = true, bool withAccessToken = true)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.GetUsername()),
                new Claim(ClaimTypes.Sid, user.ID.ToString()),
            };

            // Добавляем роли
            foreach (var role in roles) claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));

            ResetToken();
            if(withAccessToken)
            {
                // Создаем токен аутентификации для других сайтов
                tokens = GenerateJwtSecurityToken(user, new string[] { ROLE_API });
                _httpContext.HttpContext.Session.SetString(SESSION_TOKENS_KEY, JsonConvert.SerializeObject(tokens));
                claims.Add(new Claim(ClaimTypes.Hash, tokens.RefreshToken));
            }

            var identity = new ClaimsIdentity(claims, AUTH_TYPE_COOKIE, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var expiresAuth = DateTime.Now.AddDays(30);

            await _httpContext.HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(identity), new AuthenticationProperties
            {
                IsPersistent = rememberMe,
                ExpiresUtc = expiresAuth,
                IssuedUtc = expiresAuth,
                AllowRefresh = true,
            });

            // Переинициализируем хранилище данных о корзине и переносим данные из сессии гостя
            _cartWishlistManager.Reinit(user.ID.ToString());
            _cartWishlistManager.MoveDataFromSession();
        }

        /// <summary>
        /// Авторизует клиента как пользователя сайта
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <returns></returns>
        public async Task AuthorizeUser(User user)
        {
            await Authorize(user, new string[] { ROLE_USER }, true, true);
        }

        /// <summary>
        /// Ищет пользователя по контактным данным
        /// </summary>
        /// <param name="phone"></param>
        /// <param name="email"></param>
        /// <returns></returns>
        public async Task<User> FindUserAsync(string phone = null, string email = null)
        {
            if(!string.IsNullOrEmpty(phone))
            {
                phone = DataManager.PreparePhoneNumber(phone).TrimStart('+');
                var confirmedPhone = await _dbContext.UserConfirmedPhones.Include(p => p.User).FirstOrDefaultAsync(p => p.Phone.Contains(phone));
                if (confirmedPhone != null) return confirmedPhone.User;
            }

            if(!string.IsNullOrEmpty(email))
            {
                email = email.ToLower();
                var confirmedEmail = await _dbContext.UserConfirmedEmails.Include(p => p.User).FirstOrDefaultAsync(p => p.Email.ToLower() == email);
                if (confirmedEmail != null) return confirmedEmail.User;
            }

            return null;
        }

        /// <summary>
        /// Регистрирует нового пользователя.
        /// Если пользователь с такими контактными данными уже существует, возвращает его.
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <param name="password">Пароль</param>
        /// <param name="commitChanges">Сохранить изменения</param>
        /// <param name="notifyUser">Оповестить пользователя о новой учетной записи</param>
        /// <returns></returns>
        public async Task<User> RegisterUserAsync(User user, string password = null, bool commitChanges = true, bool notifyUser = false)
        {
            var foundedUser = await FindUserAsync(user.Phone, user.Email);
            if (foundedUser != null) return foundedUser; // Пользователя нашли, возвращаем

            // Пользователя не нашли, создаем нового
            var userPassword = password ?? DataSecurity.GeneratePassword(6);

            user.Activated = true;
            user.BirthdayDate = DateTime.Now;
            user.PersonalDataProcessing = true;
            user.RegistrationDate = DateTime.Now;
            user.Password = DataSecurity.ComputeHash(userPassword);

            // Подготовка контактных данных
            user.Email = !string.IsNullOrWhiteSpace(user.Email) ? user.Email.ToLower() : null;
            user.Phone = DataManager.PreparePhoneNumber(user.Phone);

            await _dbContext.Users.AddAsync(user);

            // Добавляем подтвержденные контактные данные
            if(!string.IsNullOrEmpty(user.Phone))
            {
                var confirmedPhone = new UserConfirmedPhone
                {
                    Phone = user.Phone,
                    User = user,
                };
                await _dbContext.UserConfirmedPhones.AddAsync(confirmedPhone);
            }

            if(!string.IsNullOrEmpty(user.Email))
            {
                var confirmedEmail = new UserConfirmedEmail
                {
                    Email = user.Email,
                    User = user,
                };
                await _dbContext.UserConfirmedEmails.AddAsync(confirmedEmail);
            }

            // Сохраняем изменения в БД
            if (commitChanges) await _dbContext.SaveChangesAsync();

            // Оповещаем пользователя о новой учетной записи
            if(notifyUser)
            {
                // Оповещение по смс
                // Временно отключил оповещение о новой учетной записи по смс
                //if(user.Phone != null)
                //{
                //    string content = $"Вам создана учетная запись на сайте https://my.batutmaster.ru.\r\nЛогин: {user.GetUsername()}\r\nПароль: {userPassword}";
                //    _smsService.SendMessage(user.Phone, content);
                //}

                // Оповещение по электронной почте
                //if(user.Email != null)
                //{
                //    var subject = "Новая учетная запись";
                //    var template = new EmailTemplate("NewAccount", subject, new
                //    {
                //        Login = user.GetUsername(),
                //        Password = userPassword,
                //    });

                //    await _emailService.SendMessage(user.Email, subject, template.Compile(false));
                //}
            }

            return user;
        }

        /// <summary>
        /// Генерирует два токена: токен доступа и рефреш токен
        /// </summary>
        /// <param name="user">Пользователь</param>
        /// <param name="roles">Роли токена доступа</param>
        /// <returns></returns>
        public JwtSecurityTokens GenerateJwtSecurityToken(User user, string[] roles)
        {
            JwtSecurityToken f(string[] permissions, TimeSpan lifeTime)
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimsIdentity.DefaultNameClaimType, user.GetUsername()),
                    new Claim(ClaimTypes.Sid, user.ID.ToString()),
                };

                foreach (var permission in permissions) claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, permission));

                var now = DateTime.Now;
                var identity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                var token = new JwtSecurityToken(
                    tokenAuthOptions.Issuer,
                    tokenAuthOptions.Audience,
                    identity.Claims,
                    now,
                    now.Add(lifeTime),
                    new SigningCredentials(tokenAuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256)
                );

                return token;
            }

            var handler = new JwtSecurityTokenHandler();
            var tokens = new JwtSecurityTokens
            {
                AccessToken = handler.WriteToken(f(roles, TimeSpan.FromMinutes(tokenAuthOptions.Lifetime))),
                RefreshToken = handler.WriteToken(f(new string[] { ROLE_REFRESH_TOKEN }, TimeSpan.FromMinutes(tokenAuthOptions.RefreshTokenLifetime))),
            };

            return tokens;
        }

        public JwtSecurityToken DecodeJwtToken(string tokenData)
        {
            var handler = new JwtSecurityTokenHandler();
            return handler.ReadJwtToken(tokenData);
        }

        /// <summary>
        /// Удаляет подготовленный токен
        /// </summary>
        internal void ResetToken()
        {
            tokens = null;
            _httpContext.HttpContext.Session.Remove(SESSION_TOKENS_KEY);
        }

        /// <summary>
        /// Указывает, если ли токены для отправки на сайты
        /// </summary>
        public bool HasTokens
        {
            get
            {
                return tokens != null;
            }
        }

        /// <summary>
        /// Возвращает данные авторизации
        /// </summary>
        public SecuredMessage GetAuthorizeData(User user, string host)
        {
            var authToken = new AuthorizationToken
            {
                UserId = user.ID,
                Host = host,
                Token = tokens.AccessToken,
                RefreshToken = tokens.RefreshToken,
                Expires = DateTime.Now.AddMinutes(tokenAuthOptions.RefreshTokenLifetime),
            };

            var securedMessage = SecuredMessage.Create(authToken, _encryptionKey);
            return securedMessage;
        }         

        /// <summary>
        /// Пара токенов доступа
        /// </summary>
        public class JwtSecurityTokens
        {
            public string AccessToken { get; set; }
            public string RefreshToken { get; set; }         
        }
           
        
        /// <summary>
        /// Выполняет аутентификацию пользователя
        /// </summary>
        /// <param name="httpContext">HTTP контекст</param>
        /// <param name="username">Имя пользователя</param>
        /// <param name="role">Роль пользователя</param>
        /// <param name="rememberMe">Указывает, нужно ли запоминать пользователя</param>
        /// <param name="authType">Тип аутентификации</param>
        /// <returns></returns>
        public static async Task Authenticate(HttpContext httpContext, string username, string[] roles, bool rememberMe = true, string authType = "ApplicationCookie", string sid = "", string token = "")        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, username),
               /* new Claim(ClaimsIdentity.DefaultRoleClaimType, role),*/
                new Claim(ClaimTypes.Sid, sid, ClaimValueTypes.String),
                new Claim(ClaimTypes.Hash, token),
            };

            foreach (var role in roles) claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));

            var id = new ClaimsIdentity(claims, authType, ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await httpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id), new AuthenticationProperties
            {
                IsPersistent = rememberMe,
                ExpiresUtc = DateTime.Now.AddDays(30),
                IssuedUtc = DateTime.Now.AddDays(30),
                AllowRefresh = true
            });
        }

        public static JwtSecurityToken AuthenticateToken(HttpContext httpContext, string username, string[] roles, TokenAuthOptions tokenAuthOptions, bool refreshToken = false, string sid = "")
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, username),
                new Claim(ClaimTypes.Sid, sid, ClaimValueTypes.String),
            };
            foreach (var role in roles) claims.Add(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));

            var now = DateTime.Now;
            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            var jwt = new JwtSecurityToken(
                tokenAuthOptions.Issuer,
                tokenAuthOptions.Audience,
                claimsIdentity.Claims,
                now,
                now.Add(TimeSpan.FromMinutes(!refreshToken ? tokenAuthOptions.Lifetime : tokenAuthOptions.RefreshTokenLifetime)),
                new SigningCredentials(tokenAuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256)
                );

            return jwt;
        }

        /// <summary>
        /// Выполняет деавторизацию пользователя
        /// </summary>
        /// <param name="httpContext">HTTP контект</param>
        /// <returns></returns>
        public static async Task Logout(HttpContext httpContext)
        {
            await httpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
        }


    }
}
