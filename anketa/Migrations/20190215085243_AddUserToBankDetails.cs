﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddUserToBankDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "BankDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_BankDetails_UserId",
                table: "BankDetails",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_BankDetails_Users_UserId",
                table: "BankDetails",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BankDetails_Users_UserId",
                table: "BankDetails");

            migrationBuilder.DropIndex(
                name: "IX_BankDetails_UserId",
                table: "BankDetails");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "BankDetails");
        }
    }
}
