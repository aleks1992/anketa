﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Response
{
    public class AnketaFormResponse
    {
        /// <summary>
        /// Имя формы
        /// </summary>
        public string FormName { get; set; }

        /// <summary>
        /// Поля формы
        /// </summary>
        public ICollection<object> Fields { get; set; }

        /// <summary>
        /// Обработчик формы
        /// </summary>
        public string Receiver { get; set; }
    }
}
