﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class DeleteAddressLinkInOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Addresses_DocumentsReceiverAddressId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Addresses_ProductReceiverAddressId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_DocumentsReceiverAddressId",
                table: "Orders");

            migrationBuilder.DropIndex(
                name: "IX_Orders_ProductReceiverAddressId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "DocumentsReceiverAddressId",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ProductReceiverAddressId",
                table: "Orders");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DocumentsReceiverAddressId",
                table: "Orders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ProductReceiverAddressId",
                table: "Orders",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DocumentsReceiverAddressId",
                table: "Orders",
                column: "DocumentsReceiverAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ProductReceiverAddressId",
                table: "Orders",
                column: "ProductReceiverAddressId");

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Addresses_DocumentsReceiverAddressId",
                table: "Orders",
                column: "DocumentsReceiverAddressId",
                principalTable: "Addresses",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Addresses_ProductReceiverAddressId",
                table: "Orders",
                column: "ProductReceiverAddressId",
                principalTable: "Addresses",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
