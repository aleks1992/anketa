﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class ExtendDeliveryInformationInOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DocumentDeliveryAddressBuildingNumber",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocumentDeliveryAddressFlat",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DocumentDeliveryAddressPostIndex",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProductDeliveryAddressBuildingNumber",
                table: "Orders",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProductDeliveryAddressFlat",
                table: "Orders",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocumentDeliveryAddressBuildingNumber",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "DocumentDeliveryAddressFlat",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "DocumentDeliveryAddressPostIndex",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ProductDeliveryAddressBuildingNumber",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ProductDeliveryAddressFlat",
                table: "Orders");
        }
    }
}
