﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc
{
    /// <summary>
    /// Cоц.Сети через, на которые можно авторизоваться
    /// </summary>
    public class SocialData
    {
        /// <summary>
        /// Имя
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Найименование ресурса
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Защищённый ключ
        /// </summary>
        public string SecretKey { get; set; }

        /// <summary>
        /// Сервисный ключ доступа
        /// </summary>
        public string PublicKey { get; set; }

        /// <summary>
        /// ID приложения
        /// </summary>
        public string ClientId { get; set; }




    }
}
