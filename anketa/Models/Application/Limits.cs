﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace anketa.Models.Application
{
    /// <summary>
    /// Описывает собой класс лимитов
    /// </summary>
    public class Limits
    {
        /// <summary>
        /// Лимит отправляемых кодов оповещений с IP адреса
        /// </summary>
        [DefaultValue(5)]
        public int ConfirmationCodeLimitFromIP { get; set; }

        /// <summary>
        /// Сообщение при превышении лимита сообщений с IP адреса
        /// </summary>
        [DefaultValue("Допустимо отправить только %s сообщений с вашего IP-адреса.")]
        public string ConfirmationCodeLimitFromIPMessage { get; set; }

        /// <summary>
        /// Лимит отправляемых кодов оповещений с браузера
        /// </summary>
        [DefaultValue(3)]
        public int ConfirmationCodeLimitFromBrowser { get; set; }

        /// <summary>
        /// Сообщение при превышении лимита сообщений с браузера
        /// </summary>
        [DefaultValue("Допустимо отправить только %s сообщения в сутки.")]
        public string ConfirmationCodeLimitFromBrowserMessage { get; set; }

        /// <summary>
        /// Количество сообщений на телефон с браузера за час
        /// </summary>
        [DefaultValue(3)]
        [LimitMessage("Допустимо отправить только %d %s в час.", "сообщение", "сообщения", "сообщений")]
        public int ConfirmationCodeInHourFromBrowser { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="configuration">Данные конфигурации приложения</param>
        public Limits(IConfiguration configuration)
        {
            configuration.Bind("Limits", this);
            CheckValues();
            PrepareMessages();
        }

        /// <summary>
        /// Проверяет значения, если они не заполнены, устанавливает значения по-умолчанию
        /// </summary>
        protected void CheckValues()
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                bool needSetValue = false;

                var val = prop.GetValue(this);
                if (val == null) needSetValue = true;
                else
                {
                    var emptyValue = Activator.CreateInstance(val.GetType());
                    needSetValue = val.Equals(emptyValue);
                }
                
                if (needSetValue)
                {                    
                    var defaultValueAttribute = (DefaultValueAttribute)Attribute.GetCustomAttribute(prop, typeof(DefaultValueAttribute));
                    prop.SetValue(this, defaultValueAttribute.Value);                        
                }
            }
        }

        /// <summary>
        /// Подготавливает сообщения к работе
        /// </summary>
        protected void PrepareMessages()
        {
            var regex = new Regex(@"Message$");
            
            foreach(var prop in GetType().GetProperties().Where(p => regex.IsMatch(p.Name)).ToList())
            {
                var propName = regex.Replace(prop.Name, "");
                var limitProp = GetType().GetProperties().FirstOrDefault(p => p.Name == propName);
                if(limitProp != null)
                {
                    var message = (string)prop.GetValue(this);
                    message = message.Replace("%s", limitProp.GetValue(this).ToString());
                    prop.SetValue(this, message);
                }
            }
        }

        /// <summary>
        /// Возвращает сообщение об ошибке, при превышении лимита
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        public string GetMessage(Expression<Func<Limits, int>> expression)
        {
            var count = expression.Compile()(this);

            if (expression.Body.NodeType == ExpressionType.MemberAccess)
            {
                var memberAccess = expression.Body as MemberExpression;
                var attribute = memberAccess.Member.GetCustomAttribute(typeof(LimitMessageAttribute));
                if(attribute != null)
                {
                    return ((LimitMessageAttribute)attribute).GetMessage(count);
                }
            }
            
            return "Неизвестная ошибка.";
        }

        class LimitMessageAttribute : Attribute
        {
            private string messageTemplate;
            private string nominativ;
            private string genetiv;
            private string plural;

            public LimitMessageAttribute(string message, string nominativ, string genetiv = null, string plural = null)
            {
                messageTemplate = message;
                this.nominativ = nominativ;
                this.genetiv = genetiv ?? nominativ;
                this.plural = plural ?? nominativ;                
            }

            public string GetMessage(int count)
            {
                var compiledMessage = messageTemplate.Replace("%d", count.ToString());
                compiledMessage = compiledMessage.Replace("%s", Generate(count, nominativ, genetiv, plural));
                return compiledMessage;
            }

            private static string Generate(int number, string nominativ, string genetiv, string plural)
            {
                var titles = new[] { nominativ, genetiv, plural };
                var cases = new[] { 2, 0, 1, 1, 1, 2 };
                return titles[number % 100 > 4 && number % 100 < 20 ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
            }
        }
    }
}
