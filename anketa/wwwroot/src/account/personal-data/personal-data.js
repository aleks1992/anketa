﻿// Форма персональных данных пользователя

import Vue from 'vue/dist/vue.esm.js'
import PersonalData from './personal-data.vue'

new Vue({
    el: '#app',
    render: h => h(PersonalData),
});

