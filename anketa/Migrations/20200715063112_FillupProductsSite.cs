﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class FillupProductsSite : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData("Products", "Site", null, "Site", "bm");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData("Products", "Site", "bm", "Site", null);
        }
    }
}
