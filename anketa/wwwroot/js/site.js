(function ($) {
    // Функция для генерации событий
    document.generateEvent = function (event_name, data) {
        var event = null;
        if (data === null) {
            event = document.createEvent('Event');
            event.initEvent(event_name, true, true);
            document.dispatchEvent(event);
        }
        else {
            document.generateCustomEvent(event_name, data, true, false);
        }
    };

    // Биндинги
    // mg-bind="selector" к которому элементу цепляться
    // mg-bind-equal - нужное значение
    // mg-bind-html - данные, если значение подходит
    // mg-bind-no-html - если значение не подходит
    document.applyBinding = function ($container) {
        $($container !== undefined ? $container : document).find('[mg-bind]').each(function (i, e, a) {
            el = $(this);
            var bind = el.attr('mg-bind');
            var equal = el.attr('mg-bind-equal');

            var eventProc = function (e) {
                var el = e.data;
                var target = $(e.target);
                var val = target.val();
                if (target.attr('type') === 'checkbox') val = target.is(':checked');

                if (val.toString() === equal) {
                    if (el.attr('mg-bind-html')) el.html(el.attr('mg-bind-html'));
                    if (el.attr('mg-bind-class')) el.addClass(el.attr('mg-bind-class').split(','));
                    if (el.attr('mg-bind-no-class')) el.removeClass(el.attr('mg-bind-no-class').split(','));
                    if (el.attr('mg-bind-attr')) {
                        var attr = el.attr('mg-bind-attr').split('=');
                        if (attr.length === 2) el.attr(attr[0], attr[1]);
                    }
                }
                else {
                    if (el.attr('mg-bind-no-html')) el.html(el.attr('mg-bind-no-html'));
                    if (el.attr('mg-bind-class')) el.removeClass(el.attr('mg-bind-class').split(','));
                    if (el.attr('mg-bind-no-class')) el.addClass(el.attr('mg-bind-no-class').split(','));
                    if (el.attr('mg-bind-attr')) {
                        var attr2 = el.attr('mg-bind-attr').split('=');
                        if (attr2.length === 2) el.attr(attr2[0], "");
                    }
                }
            };

            // Привяжем событие
            $('body').on('input propertychanged paste', bind, el, eventProc);

            // Вызовем обработчик сейчас
            eventProc({
                data: el,
                target: $(bind)
            });
        });
    };

    document.compareObjects = function () {
        var i, l, leftChain, rightChain;

        function compare2Objects(x, y) {
            var p;

            // remember that NaN === NaN returns false
            // and isNaN(undefined) returns true
            if (isNaN(x) && isNaN(y) && typeof x === 'number' && typeof y === 'number') {
                return true;
            }

            // Compare primitives and functions.     
            // Check if both arguments link to the same object.
            // Especially useful on the step where we compare prototypes
            if (x === y) {
                return true;
            }

            // Works in case when functions are created in constructor.
            // Comparing dates is a common scenario. Another built-ins?
            // We can even handle functions passed across iframes
            if ((typeof x === 'function' && typeof y === 'function') ||
                (x instanceof Date && y instanceof Date) ||
                (x instanceof RegExp && y instanceof RegExp) ||
                (x instanceof String && y instanceof String) ||
                (x instanceof Number && y instanceof Number)) {
                return x.toString() === y.toString();
            }

            // At last checking prototypes as good as we can
            if (!(x instanceof Object && y instanceof Object)) {
                return false;
            }

            if (x.isPrototypeOf(y) || y.isPrototypeOf(x)) {
                return false;
            }

            if (x.constructor !== y.constructor) {
                return false;
            }

            if (x.prototype !== y.prototype) {
                return false;
            }

            // Check for infinitive linking loops
            if (leftChain.indexOf(x) > -1 || rightChain.indexOf(y) > -1) {
                return false;
            }

            // Quick checking of one object being a subset of another.
            // todo: cache the structure of arguments[0] for performance
            for (p in y) {
                if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
                    return false;
                }
                else if (typeof y[p] !== typeof x[p]) {
                    return false;
                }
            }

            for (p in x) {
                if (y.hasOwnProperty(p) !== x.hasOwnProperty(p)) {
                    return false;
                }
                else if (typeof y[p] !== typeof x[p]) {
                    return false;
                }

                switch (typeof (x[p])) {
                    case 'object':
                    case 'function':

                        leftChain.push(x);
                        rightChain.push(y);

                        if (!compare2Objects(x[p], y[p])) {
                            return false;
                        }

                        leftChain.pop();
                        rightChain.pop();
                        break;

                    default:
                        if (x[p] !== y[p]) {
                            return false;
                        }
                        break;
                }
            }

            return true;
        }

        if (arguments.length < 1) {
            return true; //Die silently? Don't know how to handle such case, please help...
            // throw "Need two or more arguments to compare";
        }

        for (i = 1, l = arguments.length; i < l; i++) {

            leftChain = []; //Todo: this can be cached
            rightChain = [];

            if (!compare2Objects(arguments[0], arguments[i])) {
                return false;
            }
        }

        return true;
    }

    // Маска ввода
    document.applyMasks = function ($container) {
        $($container !== undefined ? $container : document).find('[mask]').each(function (e) {
            $(this).mask($(this).attr('mask'));
        });
    };

    // TODO: дописать привязку ко всем изображениям
    document.noImage = function (t) {
        t.src = '/images/no-image.png';
    };

    // Применение правил валидации для полей
    document.applyValidator = function ($container) {
        $($container !== undefined ? $container : document).find('form').each(function (e) {
            $.validator.unobtrusive.parse(this);
        });
    };

    // Загружает асинхронно контент
    document.LoadContentAsync = function ($container, showLoadAnim, asyncLoad) {
        var container = $($container);
        if (showLoadAnim) {
            container.html('');
            var loader = $('.loader-content').clone(container);
            loader.appendTo(container);
            loader.removeClass('hidden');
        }

        var shower = function (data) {
            container.html(data);
        };

        asyncLoad(shower);
    };

    document.formatNumber = function ($n) {
        $n = Math.round($n).toString().split('').reverse().join('');
        parts = [];
        for (var i = 0; i < $n.length; i += 3) {
            parts.unshift($n.substr(i, 3).split('').reverse().join(''));
        }
        return parts.join(',');
    };

    document.base64_encode = function (data) {
        return btoa(unescape(encodeURIComponent(data)));
    };

    document.base64_decode = function (enc) {
        return decodeURIComponent(escape(atob(enc)));
    };

    document.showLoader = function (state) {
        var loader = $('.loader-content');
        if (state) loader.removeClass('hidden');
        else loader.addClass('hidden');
    };

    document.addAlertBlock = function (text, level) {
        $('.main-content').append('<div class="alert alert-' + level + '">' + text + '</div>');
    };

    // Происходит при подтверждении email
    $('body').on('submit', '#confirm-email', function (e) {
        e.preventDefault();
        document.ModalForm.showLoader(e.target);
        $.post(e.target.action, $(e.target).serialize(), function (data) {
            if (!data) document.ModalForm.showContent(e.target, data);
            else {
                document.ModalForm.close(e.target);
                document.generateEvent('EmailConfirmed');
            }
        });
    });

    document.applyBinding();
    document.applyMasks();

    $('.open-menu').click(function () {
        var navbar = $('.navbar-anketa');
        var is_active = navbar.find('.menu-icon').hasClass("menu-icon-active");
        var collapse = navbar.find('.navbar-collapse');
        navbar.find('.menu-icon').toggleClass("menu-icon-active");
        collapse.toggleClass("active")
        if (is_active) {
            setTimeout(function () {
                navbar.find('.dropdown-menu').removeClass('active');
            }, 500);
        }
    });

    $('.open-dropdown-menu').click(function () {
        var p = $(this).parents(".dropdown")
        var dropmenu = $(this).next('.dropdown-menu');

        if (!dropmenu.length) {
            dropmenu = p.find('.dropdown-menu')
        }
        dropmenu.toggleClass("active");

    });

})(jQuery);


/**
 * Возрашает Get параметр
 * @param {any} index
 */
document.getParamsUrl = function (index) {
    var params = window
        .location
        .search
        .replace('?', '')
        .split('&')
        .reduce(function (p, e) {
            var a = e.split('=');
            p[decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
            return p;
        },
            {}
        );

    if (index in params) return params[index];
    return null;
}

/**
 *  Возвращает Регулярное выражения
 * @param {any} name
 * @returns Regex
 */
document.getPatternValue = function (name) {
    switch (name.toLowerCase()) {
        case 'email':
            return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        case 'phone':
            return /^((8|\+7|\+3|[1-9]|\+[1-9])[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{5,20}$/;

    }
    return null;
}

/**
 *  Обработка ошибок js
 */
var countError = 0;
window.addEventListener('error', function(e) {
    countError++;
    if (countError > 3) return;

    var d = {
        message: e.message,
        filename: e.filename,
        lineno: e.lineno
    };

    var p = [];
    for (var index in d) p.push(index + "=" + encodeURIComponent(d[index]));    
    var url = '/Account/SaveJaveScriptError?' + p.join("&");

    $.get(url);
});

function addApplicationModel(name, model) {
    if (typeof window.appData === 'undefined') window.appData = {};
    if (typeof window.appData.models === 'undefined') window.appData.models = {};
    window.appData.models[name] = model;
}

function getModel(name) {
    var obj = {};
    if (typeof window.appData.models[name] !== 'undefined') {
        var model = window.appData.models[name];
        var properties = Object.getOwnPropertyNames(model);
        for (var i = 0; i < properties.length; i++) obj[properties[i]] = null;        
    }
    return obj;
}

(function () {
    // Метод для инициализации рекапчи
    document.getRecaptcha = function () {
        var promise = new Promise(function (succeed, fail) {
            if (typeof grecaptcha !== 'undefined' && isValidRecaptcha) {
                grecaptcha.ready(function () {
                    grecaptcha.execute(key, { 'action': 'homepage' }).then(function (token) {
                        succeed(token);
                    });
                });
            } else succeed(null);
        });
        return promise;
    };

    // Метод для создания произвольного события
    document.generateCustomEvent = function (event_name, data, bubbles, cancelable) {
        data = data || undefined;
        bubbles = bubbles || false;
        cancelable = cancelable || false;
        
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event_name, bubbles, cancelable, data);
        document.dispatchEvent(evt);
    };

    // Переопределяем стандартное информационное сообщение
    (function () {
        var config = {
            list: [],
            showing: false
        };        

        window.alert = function (text) {
            var modal = jQuery('#modal-alert');

            var showAlert = function (text) {
                modal.find('.alert-text').html(text);
                modal.modal('show');
            };

            // Подписываемся на события модального окна
            modal.on('show.bs.modal', function (e) {
                config.showing = true;
            });

            modal.on('hidden.bs.modal', function (e) {
                config.showing = false;
                if (config.list.length > 0) {
                    var t = config.list.shift();
                    showAlert(t);
                }
            });

            if (!config.showing) showAlert(text);
            else config.list.push(text);
        };
    })();
    

    // Расширение прототипа числа, для форматирования чисел
    Number.prototype.format = function () {
        var $n = this;
        $n = Math.round($n).toString().split('').reverse().join('');
        parts = [];
        for (var i = 0; i < $n.length; i += 3) {
            parts.unshift($n.substr(i, 3).split('').reverse().join(''));
        }
        return parts.join(',');
    };

    window.appData = {
        models: {},
    };

    // Общие события документа
    document.addEventListener('cartwishlist-updated', function (e) {
        var count = 0;
        for (var i = 0; i < e.detail.list.length; i++) count += e.detail.list[i].count;
        if (count === 0) count = '';

        if (e.detail.type === 'wishlist') {
            $('.wishlistCount.count > .badge').html(count);
        }
        else if (e.detail.type === 'cart') {
            $('.cartCount.count > .badge').html(count);
        }
    });

})();