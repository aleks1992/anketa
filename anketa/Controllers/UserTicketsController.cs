﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using anketa.Misc;
using anketa.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using static anketa.Misc.SiteClient;

namespace anketa.Controllers
{
    /// <summary>
    /// Обработка обращении 
    /// </summary>
    [Authorize(Roles = "User")]
    public class UserTicketsController : Controller
    {
        private DatabaseContext DbContext;
        private List<SiteData> LinkedSites { get; set; }

        public UserTicketsController(DatabaseContext dbContext, List<SiteData> linkedSites)
        {
            DbContext = dbContext;
            LinkedSites = linkedSites;
        }

        /// <summary>
        /// Получения спика обращений пользователя
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            return RedirectToAction("Index", "Home");
        }


        /// <summary>
        /// Отпрвка запроса на сайты
        /// </summary>
        /// <param name="UserTickets"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> UpdateUserTickets([FromBody]UserTicketsModel userTickets)
        {
            var user = await DbContext.GetUser(User);
            if (user != null && userTickets != null)
            {
                userTickets.UserEmail = user.Email;
                userTickets.UserName = user.LastName + " " + user.FirstName + " " + user.MiddleName;
                userTickets.UserPnone = user.Phone;
                userTickets.UserId = user.ID;
                userTickets.Module = "usertickets";
                userTickets.Action = "anketa_user_tickets";
   
                var prifexGuid = userTickets.Guid.Substring(0, 2);
                List<ServerResponseItemData> listResponse = new List<ServerResponseItemData>();

                //Отправка запроса
                foreach (var urlItems in LinkedSites)
                {
                    if (prifexGuid != urlItems.Prefix)
                    {
                        continue;
                    }
                    var response = SiteClient.HttpClient(userTickets, urlItems.URL + "/wp-content/plugins/anketa/include/ajax.php");
                    listResponse.Add(response);                
                }
                return Json(listResponse);
            }

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Модель Обращении
        /// </summary>
        public class UserTicketsModel
        {
            public string TicketsType { get; set; }
            public int UserId { get; set; }
            public int ProductCount { get; set; }
            public string Guid { get; set; }
            public string UserName { get; set; }
            public string UserEmail { get; set; }
            public string UserPnone { get; set; }
            public string TicketsComments { get; set; }
            public string Module { get; set; }
            public string Action { get; set; }



        }

    }
}