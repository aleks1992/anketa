﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models
{
    public class DeliveryTransportCompany
    {
        /// <summary>
        /// Индификатор рассчета 
        /// </summary>
        public int ResultId { get; set; }

        /// <summary>
        /// Индификатор рассчета
        /// </summary>
        public string SessionId { get; set; }

        /// <summary>
        /// Расстояния
        /// </summary>
        public string Distance { get; set; }

        /// <summary>
        /// Дата рассчета
        /// </summary>
        public string Date { get; set; }

        /// <summary>
        /// Тип рассчета
        /// </summary>
        public string TypeDelivery { get; set; }

        /// <summary>
        /// Названия Транспортной компаний
        /// </summary>
        [JsonProperty("Name_TK")]
        public string NameTC { get; set; }

        /// <summary>
        /// Цена (авто)
        /// </summary>
        public string Price { get; set; }

        /// <summary>
        /// Время доставки
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// Город
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Цена (авия)
        /// </summary>
        public string PriceAir { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Lat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Log { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Доставка до адреса
        /// </summary>
        [DefaultValue(false)]
        public bool IsAddress { get; set; }

        /// <summary>
        /// Адрес доставки
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Груз находится в городе
        /// </summary>
        [DefaultValue(false)]
        public bool InYourCity { get; set; }

        /// <summary>
        /// Суммарный вес товаров
        /// </summary>
        public decimal TotalWeight { get; set; }

        /// <summary>
        /// Суммарный объем
        /// </summary>
        public decimal TotalVolume { get; set; }

    }
}
