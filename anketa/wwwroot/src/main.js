const resolve = relativePath => path.resolve(__dirname, relativePath);

import Vue from 'vue/dist/vue.esm.js';

export {
    Vue
};
