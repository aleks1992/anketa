﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddProjectReferenceToOrderItem : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProjectId",
                table: "OrderProductItems",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_OrderProductItems_ProjectId",
                table: "OrderProductItems",
                column: "ProjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_OrderProductItems_ProductProjects_ProjectId",
                table: "OrderProductItems",
                column: "ProjectId",
                principalTable: "ProductProjects",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            // Добавляем ссылки на проекты
            migrationBuilder.Sql("CREATE TEMPORARY TABLE items\n" +
                                 "SELECT Id FROM OrderProductItems;\n" +
                                "UPDATE OrderProductItems pi\n" + 
                                "INNER JOIN ProductProjects p ON pi.Name = p.Name\n" + 
                                    "SET pi.ProjectId = p.Id\n" + 
                                    "WHERE pi.Id IN(SELECT Id FROM items pi WHERE pi.ProductId IS NULL);\n" +
                                "DROP TABLE items; ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_OrderProductItems_ProductProjects_ProjectId",
                table: "OrderProductItems");

            migrationBuilder.DropIndex(
                name: "IX_OrderProductItems_ProjectId",
                table: "OrderProductItems");

            migrationBuilder.DropColumn(
                name: "ProjectId",
                table: "OrderProductItems");
        }
    }
}
