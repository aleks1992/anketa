﻿import Vue from 'vue/dist/vue.esm.js'

import fetch from 'isomorphic-fetch'
import Delivery from './delivery.vue'

new Vue({
    el: '#app',
    render: h => h(Delivery),
});