﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using anketa.Misc;
using anketa.Misc.GeoLocation;
using anketa.Models;
using anketa.Models.Application;
using anketa.Models.Application.Response;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace anketa.Controllers
{
    [Authorize(Roles = "User")]
    public class CabinetController : Controller
    {
        private DatabaseContext DbContext;
        private GeoLocationClient GeoLocation;
        /// <summary>
        /// Конструктор класса
        /// </summary>
        /// <param name="dbContext"></param>
        public CabinetController(DatabaseContext dbContext, GeoLocationClient location)
        {
            DbContext = dbContext;
            GeoLocation = location;
        }

        /// <summary>
        /// Отображает страницу персональных данных
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> PersonalData([FromServices]ModelEngine modelEngine, [FromServices]AppData appData)
        {
            modelEngine.RegisterModels(typeof(User));

            ViewBag.ListCountry = GeoLocation.GetFilterListCountry(defaultFilter: true);
            var ip = HttpContext.Connection.RemoteIpAddress.ToString();
            ViewBag.Country = GeoLocation.GetCountry(ip);
            var user = await DbContext.GetUser(User);

            ViewBag.Phone = DbContext.UserConfirmedPhones.FirstOrDefault(p => p.Phone == user.Phone);

            if (ViewBag.Phone != null) ViewBag.Phone.User = null;
            ViewBag.Email = DbContext.UserConfirmedEmails.FirstOrDefault(p => p.Email == user.Email);
            if (ViewBag.Email != null) ViewBag.Email.User = null;

            // Пароль клиенту не отдаем
            user.Password = null;

            return View(user);
        }

        /// <summary>
        /// Обновляет информацию о пользователе
        /// </summary>
        /// <param name="userData"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PersonalData([FromBody]User userData)
        {
            var response = new ServiceResponse();

            try
            {
                var currentUser = await DbContext.GetUser(User);
                currentUser.BirthdayDate = userData.BirthdayDate;
                currentUser.ConsentToNewsletter = userData.ConsentToNewsletter;
                currentUser.Email = userData.Email;
                currentUser.FirstName = userData.FirstName;
                currentUser.LastName = userData.LastName;
                currentUser.MiddleName = userData.MiddleName;
                currentUser.PersonalDataProcessing = userData.PersonalDataProcessing;
                currentUser.Phone = userData.Phone;
                currentUser.Sex = userData.Sex;

                DbContext.Users.Update(currentUser);
                await DbContext.SaveChangesAsync();
            }
            catch(Exception e)
            {
                response.Success = false;
                response.Message = e.Message;
            }

            return Json(response);
        }
    }
}