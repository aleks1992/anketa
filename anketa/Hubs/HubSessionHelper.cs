﻿using anketa.Misc;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Components.Forms;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace anketa.Hubs
{
    public class HubSessionHelperAttribute : TypeFilterAttribute
    {
        public HubSessionHelperAttribute() : base(typeof(HubSessionHelperImplementation))
        {
        }

        private class HubSessionHelperImplementation : IAsyncResultFilter
        {
            const string COOKIE_HUB_SESSION = "anketa_hub_session";
            const string SESSION_GUEST_ID = "GUEST_ID";

            private static TokenAuthOptions TokenAuthOptions;

            public HubSessionHelperImplementation(IConfiguration configuration)
            {
                if (TokenAuthOptions == null) TokenAuthOptions = configuration.GetSection("TokenAuthOptions").Get<TokenAuthOptions>();
            }

            public async Task OnResultExecutionAsync(ResultExecutingContext context, ResultExecutionDelegate next)
            {
                bool createToken = false;

                // Добавляем куку для сессии                
                if (!context.HttpContext.Session.Keys.Contains(SESSION_GUEST_ID))
                {
                    context.HttpContext.Session.SetString("GUEST_ID", DataSecurity.ComputeHash(context.HttpContext.Session.Id));
                    await context.HttpContext.Session.CommitAsync();
                    createToken = true;
                }

                var handler = new JwtSecurityTokenHandler();

                // Проверяем токен
                if (!createToken)
                {
                    if (context.HttpContext.Request.Cookies.ContainsKey(COOKIE_HUB_SESSION))
                    {
                        var tokenData = context.HttpContext.Request.Cookies[COOKIE_HUB_SESSION];
                        try
                        {
                            var token = handler.ReadToken(tokenData);
                            if (token.ValidFrom > DateTime.Now || token.ValidTo < DateTime.Now) createToken = true;
                        }
                        catch (Exception e)
                        {
                            createToken = true;
                            Console.WriteLine(e.Message);
                        }
                    }
                    else createToken = true;
                }

                if (createToken)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimsIdentity.DefaultNameClaimType, "Guest"),
                        new Claim(ClaimsIdentity.DefaultRoleClaimType, "Guest"),
                        new Claim(ClaimTypes.Sid, context.HttpContext.Session.Id),
                    };

                    var now = DateTime.Now;
                    var identity = new ClaimsIdentity(claims, "Token", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
                    var token = new JwtSecurityToken(
                        TokenAuthOptions.Issuer,
                        TokenAuthOptions.Audience,
                        identity.Claims,
                        now,
                        now.Add(TimeSpan.FromDays(3)),
                        new SigningCredentials(TokenAuthOptions.GetSymmetricSecurityKey(), SecurityAlgorithms.HmacSha256)
                    );

                    var encryptedToken = handler.WriteToken(token);
                    context.HttpContext.Response.Cookies.Append(COOKIE_HUB_SESSION, encryptedToken, new CookieOptions
                    {
                        HttpOnly = false,
                        Path = "/",
                        IsEssential = true,
                        SameSite = SameSiteMode.None,
                        Secure = true,
                    });
                }
                await next();
            }
        }
    }
}
