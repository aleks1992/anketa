﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Виды цены
    /// </summary>
    public class PriceType
    {
        /// <summary>
        /// ID  
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        ///  Вид цены
        /// </summary>
        public string Name{get;set;}
    }
}
