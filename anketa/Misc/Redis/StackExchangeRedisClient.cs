﻿using Microsoft.Extensions.Configuration;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace anketa.Misc.Redis
{
    public class StackExchangeRedisClient : IRedisClient
    {
        private ConnectionMultiplexer _redis;

        private Stack<int> history = new Stack<int>();
        private int currentDatabase = 0;

        public StackExchangeRedisClient(IConfiguration configuration)
        {
            var redisConfiguration = configuration.GetSection("Redis").Get<RedisConfiguration>();
            if(redisConfiguration != null)
            {
                try
                {
                    if (!IPAddress.TryParse(redisConfiguration.Host, out IPAddress address))
                    {
                        IPHostEntry hostEntry = Dns.GetHostEntry(redisConfiguration.Host);
                        address = hostEntry.AddressList[0];
                    }

                    _redis = ConnectionMultiplexer.Connect(address.ToString() + ":" + redisConfiguration.Port.ToString());
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    _redis = null;
                }
            }
        }

        private IDatabase Database
        {
            get
            {
                return _redis.GetDatabase(currentDatabase);
            }
        }

        public bool SetValue(string key, object val)
        {
            if(_redis != null && (val is string || val is byte[] || val is int || val is long || val is bool || val is double || val is float))
            {
                Database.StringSet(key, RedisValue.Unbox(val));               
                return true;
            }
            return false;
        }

        public string GetString(string key)
        {
            if (Status) return Database.StringGet(key);
            return null;
        }

        public byte[] GetBytes(string key)
        {
            if (Status) return Database.StringGet(key);
            return null;
        }

        public bool GetBool(string key)
        {
            if (Status) return (bool)Database.StringGet(key);
            return false;
        }

        public int GetInt(string key)
        {
            if (Status) return (int)Database.StringGet(key);
            return 0;
        }

        public long GetLong(string key)
        {
            if (Status) return (long)Database.StringGet(key);
            return 0;
        }

        public double GetDouble(string key)
        {
            if (Status) return (double)Database.StringGet(key);
            return 0;
        }

        public float GetFloat(string key)
        {
            if (Status) return (float)Database.StringGet(key);
            return 0;
        }

        public bool StorageIsReady()
        {
            return _redis != null;
        }

        public bool Exists(string key)
        {
            if (Status) return Database.KeyExists(key);
            return false;
        }

        public void Select(int db)
        {
            history.Push(currentDatabase);
            currentDatabase = db;
        }

        public void RollbackSelect()
        {
            if(history.Count > 0) currentDatabase = history.Pop();
        }

        public bool Status
        {
            get
            {
                return StorageIsReady();
            }
        }
    }
}
