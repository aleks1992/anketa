﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.Services
{
    public class SessionUserIdProvider : IUserIdProvider
    {
        public string GetUserId(HubConnectionContext connection)
        { 
            string userId = null;
            userId = connection.User?.Claims.FirstOrDefault(p => p.Type == System.Security.Claims.ClaimTypes.Sid)?.Value;
            return userId;
        }
    }
}
