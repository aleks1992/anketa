﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.Validators
{
    /// <summary>
    /// 
    /// </summary>
    public class DateRangeValidator : ValidationAttribute, IClientModelValidator
    {
        private DateTime MinDate;
        private DateTime MaxDate;

        private IFormatProvider GetFormatProvider()
        {
            return CultureInfo.GetCultureInfo("en-US").DateTimeFormat;
        }

        public DateRangeValidator()
        {
            MaxDate = DateTime.Now;
            MinDate = MaxDate.AddYears(-100);
        }

        public DateRangeValidator(string minDate)
        {
            MinDate = DateTime.Parse(minDate, GetFormatProvider());
            MaxDate = DateTime.Now;
        }

        public DateRangeValidator(string minDate, string maxDate)
        {
            MinDate = DateTime.Parse(minDate, GetFormatProvider());
            MaxDate = DateTime.Parse(maxDate, GetFormatProvider());
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));
            MergeAttribute(context.Attributes, "data-val", "true");     
            MergeAttribute(context.Attributes, "min", MinDate.ToString("yyyy-MM-dd"));
            MergeAttribute(context.Attributes, "max", MaxDate.ToString("yyyy-MM-dd"));
        }

        /// <summary>
        /// Добавляет или обновляет значение в словаре
        /// </summary>
        /// <param name="attributes"></param>
        /// <param name="key"></param>
        /// <param name="val"></param>
        private void MergeAttribute(IDictionary<string, string> attributes, string key, string val)
        {
            if (attributes.ContainsKey(key)) attributes[key] = val;
            else attributes.Add(key, val);
        }

        /// <summary>
        /// Производит валидацию объекта
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            return true;
        }
    }
}
