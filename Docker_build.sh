#!/bin/bash
# Docker build script
# Bronnikov Maksim, 2019

[ -z "$1" ] && echo "File name of archive is not passed." && exit 1
[ -z "$2" ] && echo "No config file passed." && exit 1
[ -z "$3" ] && echo "No destination for config passed." && exit 1

echo "Copy config files..."
yes | cp $2 $3

echo "Start build docker image..."
id=`date | md5sum | awk '{print $1}'`
docker build -t $1 .
docker save -o $1.tar $1
docker image rm -f $1

echo "Compressing image..."
gzip < $1.tar > $1.zip

rm $1.tar
echo $(date) > lastedit.txt
exit 0