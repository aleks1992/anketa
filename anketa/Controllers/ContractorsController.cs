﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using anketa.Misc;
using anketa.Misc.Content;
using anketa.Misc.GeoLocation;
using anketa.Models;
using anketa.Models.Application.Forms;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace anketa.Controllers
{
    [Authorize(Roles = "User")]
    public class ContractorsController : Controller
    {
        private DatabaseContext DbContext;
        private GeoLocationClient GeoLocation;

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="_dbContext"></param>
        public ContractorsController(DatabaseContext _dbContext, GeoLocationClient location)
        {
            DbContext = _dbContext;
            GeoLocation = location;
        }

        /// <summary>
        /// Список контрагентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            if (User.IsInRole("UnconfirmedUser"))
                return RedirectToAction("PersonalData", "Cabinet");
            var user = await DbContext.GetUser(User);
            var list = DbContext.Faces
                .Include(p => p.EntityFace)
                    .ThenInclude(p => p.LegalForm)
                .Include(p => p.IndividualFace)
                .Where(p => p.UserId == user.ID)
                .ToList();

            return View(list);
        }

        /// <summary>
        /// Создает юридическое лицо
        /// </summary>
        /// <param name="face"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SaveEntityFace(
            [FromForm]UserEntityFace faceForm, [FromForm]UserIndividualFace head,
            [FromForm]UserIndividualFace actFromOrgFace, [FromForm]BankDetails bankDetails,
            [FromQuery]string key)
        {
            var user = DbContext.Users.FirstOrDefault(p => string.Compare(p.Email, User.Identity.Name) == 0);
            var face = faceForm;
            if (faceForm.ID != 0)
            {
                face = DbContext.UserEntityFaces
                    .Include(p => p.ActFromOrgFace)
                    .Include(p => p.ActOnBase)
                    .Include(p => p.BankDetails)
                    .Include(p => p.Head)
                    .Include(p => p.LegalForm)
                    .FirstOrDefault(p => p.ID == faceForm.ID);

                if (face.UserId != user.ID) return RedirectToAction("Index", "Contrators");
                DataManager.UpdateFields(faceForm, face);
            }

            face.Email = user.Email; // используем email пользователя

            // Записывает директора
            var headOrg = head;
            if (face.HeadId != 0)
            {
                // Директор найден, обновим его данные
                headOrg = DbContext.UserIndividualFaces.FirstOrDefault(p => p.ID == face.HeadId);
                DataManager.UpdateFields(head, headOrg);
                DbContext.UserIndividualFaces.Update(headOrg);
            }
            else
            {
                // Директора нет, пишем нового
                head.User = user;
                head.Position = "Руководитель";
                await DbContext.UserIndividualFaces.AddAsync(head);
            }
            headOrg.Email = user.Email;
            DataManager.Disnull(headOrg);
            face.Head = headOrg;

            // Действующий в лице
            if (face.ActFromOrganization)
            {
                if (face.ActFromOrgFaceId != null && face.ActFromOrgFaceId != 0)
                {
                    // Есть действующий, обновим поля
                    var originalActFace = DbContext.UserIndividualFaces.FirstOrDefault(p => p.ID == face.ActFromOrgFaceId);
                    DataManager.UpdateFields(actFromOrgFace, originalActFace);
                    actFromOrgFace = originalActFace;
                    actFromOrgFace.Disnull();
                    DbContext.UserIndividualFaces.Update(actFromOrgFace);
                }
                else
                {
                    // Нет действующего, добавим
                    actFromOrgFace.User = user;
                    actFromOrgFace.Disnull();
                    await DbContext.UserIndividualFaces.AddAsync(actFromOrgFace);
                }

                actFromOrgFace.Email = user.Email;
                DataManager.Disnull(actFromOrgFace);
                face.ActFromOrgFace = actFromOrgFace;
            }
            else
            {
                face.ActFromOrgFace = null;
                face.ActFromOrgFaceId = null;
            }

            // Записываем или обновляем банковские реквизиты
            if (face.BankDetailsId != null && face.BankDetailsId != 0)
            {
                var originBankDetails = DbContext.BankDetails.FirstOrDefault(p => p.ID == face.BankDetailsId);
                DataManager.UpdateFields(bankDetails, originBankDetails);
                bankDetails = originBankDetails;
                DbContext.BankDetails.Update(bankDetails);
            }
            else
            {
                bankDetails.User = user;
                await DbContext.BankDetails.AddAsync(bankDetails);
            }
            face.BankDetails = bankDetails;


            // Записываем общее лицо
            UserFace userFace;
            if (face.ID != 0) userFace = DbContext.Faces.FirstOrDefault(p => p.EntityFaceId == face.ID);
            else
            {
                userFace = new UserFace()
                {
                    User = user
                };
                await DbContext.Faces.AddAsync(userFace);
            }

            // Записываем юр. лицо
            face.User = user;
            face.Disnull();

            if (face.ID != 0) DbContext.UserEntityFaces.Update(face);
            else await DbContext.UserEntityFaces.AddAsync(face);

            userFace.EntityFace = face;

            await DbContext.SaveChangesAsync();

            return Json(new
            {
                ID = userFace.ID,
                View = userFace.ToString()
            });
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> AddEntityFace([FromBody]UserEntityFace face)
        {
            var user = await DbContext.GetUser(User);

            // Используем email пользователя
            face.Email = user.Email;

            // Сохраняем руководителя
            if(face.Head.ID == 0)
            {
                // Данных по руководителю еще нет в базе
                face.Head.User = user;
                face.Head.Position = "Руководитель";
                await DbContext.UserIndividualFaces.AddAsync(face.Head);
            }
            else
            {
                // Обновляем данные руководителя
                DbContext.UserIndividualFaces.Update(face.Head);
            }
            DataManager.Disnull(face.Head);

            // Сохраняем действующего от организации
            face.ActFromOrganization = true;
            if(face.ActFromOrganization)
            {
                face.ActFromOrgFace.Email = user.Email;
                if(face.ActFromOrgFace.ID == 0)
                {
                    face.ActFromOrgFace.User = user;
                    face.ActFromOrgFace = (await DbContext.UserIndividualFaces.AddAsync(face.ActFromOrgFace)).Entity;
                }
                else
                {
                    face.ActFromOrgFace = DbContext.UserIndividualFaces.Update(face.ActFromOrgFace).Entity;
                }
                DataManager.Disnull(face.ActFromOrgFace);
            }
            else
            {
                face.ActFromOrgFace = null;
                face.ActFromOrgFaceId = null;
            }

            // Сохраняем банковские реквизиты
            if(face.BankDetails.ID == 0)
            {
                face.BankDetails.User = user;
                face.BankDetails = (await DbContext.BankDetails.AddAsync(face.BankDetails)).Entity;
            }
            else
            {
                face.BankDetails = DbContext.BankDetails.Update(face.BankDetails).Entity;
            }
            DataManager.Disnull(face.BankDetails);

            // Записываем общее лицо
            UserFace userFace;
            if (face.ID != 0) userFace = await DbContext.Faces.FirstOrDefaultAsync(p => p.EntityFaceId == face.ID);
            else
            {
                userFace = new UserFace
                {
                    User = user
                };
                await DbContext.Faces.AddAsync(userFace);
            }

            // Устанавливаем дополнительные данные
            face.ActOnBase = await DbContext.Bases.FirstOrDefaultAsync(p => p.ID == face.ActOnBase.ID);
            face.LegalForm = await DbContext.LegalForms.FirstOrDefaultAsync(p => p.Id == face.LegalForm.Id);

            // Записываем юр. лицо
            face.User = user;
            face.Disnull();

            if (face.ID != 0) DbContext.UserEntityFaces.Update(face);
            else await DbContext.UserEntityFaces.AddAsync(face);
            await DbContext.SaveChangesAsync();

            userFace.EntityFace = face;

            var success = true;
            int objectId = 0;
            
            try
            {
                await DbContext.SaveChangesAsync();
                objectId = userFace.ID;
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
                success = false;
            }

            return Json(new
            {
                Success = success,
                ID = objectId,
            });
        }

        /// <summary>
        /// Создает физическое лицо
        /// </summary>
        /// <param name="face"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddIndividualFace([FromForm]UserIndividualFace face, [FromQuery]string key)
        {
            var user = DbContext.Users.FirstOrDefault(p => string.Compare(p.Email, User.Identity.Name) == 0);
            var individualFace = face;
            if (face.ID != 0)
            {
                individualFace = DbContext.UserIndividualFaces.FirstOrDefault(p => p.ID == face.ID);
                if (individualFace.UserId != user.ID) return RedirectToAction("Index", "Contractors");
                DataManager.UpdateFields(face, individualFace);
            }

            individualFace.User = user;
            individualFace.Email = user.Email; // используем email пользователя
            DataManager.Disnull(individualFace);

            UserFace userFace;
            if (face.ID != 0)
            {
                DbContext.UserIndividualFaces.Update(individualFace);
                userFace = DbContext.Faces.FirstOrDefault(p => p.IndividualFaceId == face.ID);
            }
            else
            {
                await DbContext.UserIndividualFaces.AddAsync(individualFace);
                userFace = new UserFace
                {
                    User = user,
                    IndividualFace = individualFace
                };
                await DbContext.Faces.AddAsync(userFace);
            }

            await DbContext.SaveChangesAsync();

            return Json(new
            {
                ID = userFace.ID,
                View = userFace.ToString()
            });
        }

        [HttpPost]
        public async Task<IActionResult> AddIndividualFaceApi([FromBody]UserIndividualFace face)
        {
            var user = await DbContext.GetUser(User);
            bool success = true;

            if(face.ID == 0)
            {
                face.User = user;
                face.Email = user.Email;
                DataManager.Disnull(face);
                await DbContext.UserIndividualFaces.AddAsync(face);
            }
            else
            {
                var savedFace = await DbContext.UserIndividualFaces.FirstOrDefaultAsync(p => p.ID == face.ID);
                if(savedFace.UserId != user.ID) success = false;
                else DbContext.UserIndividualFaces.Update(face);
            }

            var objId = 0;

            if (success)
            {
                UserFace userFace;
                
                if(face.ID <= 0)
                {
                    userFace = new UserFace
                    {
                        IndividualFace = face,
                        User = user,
                    };
                    await DbContext.Faces.AddAsync(userFace);
                }
                else
                {
                    userFace = await DbContext.Faces.FirstOrDefaultAsync(p => p.IndividualFaceId == face.ID);
                }

                try
                {
                    await DbContext.SaveChangesAsync();
                    objId = userFace.ID;
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    success = false;
                }
            }

            return Json(new
            {
                Success = success,
                ID = objId,
            });
        }

        

        /// <summary>
        /// Возвращает представление по ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetView([FromQuery]int id)
        {
            var user = await DbContext.GetUser(User);
            var contractor = DbContext.Faces
                .Include(p => p.User)
                .Include(p => p.EntityFace)
                .Include(p => p.IndividualFace)
                .FirstOrDefault(p => p.ID == id);
            var view = "";
            if (contractor != null && contractor.UserId == user.ID) view = contractor.ToString();
            return Json(view);
        }

        /// <summary>
        /// Показывает частичное представление с информациекй о контрагенте
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult ShowContractor(int id)
        {
            var contractor = DbContext.Faces
                .Include(p => p.EntityFace)
                    .ThenInclude(p => p.BankDetails)
                .Include(p => p.IndividualFace)
                    .ThenInclude(p => p.BankDetails)
                .FirstOrDefault(p => p.ID == id);
            return PartialView(contractor);
        }

    }
}