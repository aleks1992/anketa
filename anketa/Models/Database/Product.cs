﻿using anketa.Misc;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Товары
    /// </summary>
    [Serializable]
    public class Product
    {
        /// <summary>
        /// Ид товара
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Глобальный ид товара
        /// </summary>
        [MaxLength(42)]
        public string Guid { get; set; }

        /// <summary>
        /// Наименование товара
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Артикул товара
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Длина товара
        /// </summary>
        public decimal Length { get; set; }

        /// <summary>
        /// Ширина товара
        /// </summary>
        public decimal Width { get; set; }

        /// <summary>
        /// Высота товара
        /// </summary>
        public decimal Height { get; set; }

        /// <summary>
        /// Описания товара
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Статус товара
        /// </summary>
        [JsonIgnore]
        public int ProductStatus { get; set; }

        /// <summary>
        /// Цена товара
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public decimal Price { get; set; }

        /// <summary>
        /// Максимальная цена
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public decimal PriceMax { get; set; }

        /// <summary>
        /// Минимальная цена
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public decimal PriceMin { get; set; }

        /// <summary>
        /// Url продукта
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public string ProductUrl { get; set; }

        /// <summary>
        /// Img url продукта
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public string ProductImgUrl { get; set; }

        /// <summary>
        /// Base64 Date Price
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public string Base64PriceData { get; set; }

        /// <summary>
        /// Описания характеристик цены
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        public string DeliveryOption { get; set; }

        /// <summary>
        /// Представляет собой сайт, на котором представлен товар
        /// </summary>
        public string Site { get; set; }

        /// <summary>
        /// Возвращает представление товара
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name + " " + Sku;
        }

        public string GetProductLink()
        {
            var sites = SiteData.GetSites();
            var site = sites.FirstOrDefault(p => string.Compare(p.Prefix, Site, true) == 0);
            if (site != null) return site.GetLink($"/?mgproduct={Guid}");
            return null;
        }

        /// <summary>
        /// Возвращает ссылку на изображение товара
        /// </summary>
        /// <param name="sites"></param>
        /// <returns></returns>
        public string GetProductImageLink()
        {
            var sites = SiteData.GetSites();
            var site = sites.FirstOrDefault(p => string.Compare(p.Prefix, Site, true) == 0);
            if (site != null) return site.GetLink($"/wp-admin/admin-ajax.php?action=anketa-get-image&guid={Guid}");
            return "";
        }

        /// <summary>
        /// Получения base64 картинки
        /// </summary>
        /// <returns></returns>
        public string GetBase64Image()
        {

            string path = GetProductImageLink();

            string base64 = null;

            if (!string.IsNullOrEmpty(path))
            {
                WebClient myWebClient = new WebClient();
                byte[] b = myWebClient.DownloadData(path);
                if (b.Count() > 0)
                    base64 = Convert.ToBase64String(b);
            }
            
            // Если не удалось собрать base64 вставляем нет изображения
            if (string.IsNullOrEmpty(base64))
            {
                byte[] b1 = System.IO.File.ReadAllBytes("wwwroot/images/no-image.png");
                base64 = Convert.ToBase64String(b1);
            }

            return "data:image/png;base64," + base64;
        }

    }

    /// <summary>
    /// статусы продукта
    /// </summary>
    enum ProductStatus
    {
        Draft = 0,
        Publish = 1
    }

    class ProductStatusHelper
    {
        public static ProductStatus GetStatusByName(string name)
        {
            if (name == "publish") return ProductStatus.Publish;
            if (name == "draft") return ProductStatus.Draft;
            return ProductStatus.Draft;
        }
    }
}
