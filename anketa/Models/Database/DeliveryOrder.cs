﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    [Serializable]
    public class DeliveryOrder
    {
        /// <summary>
        /// Ид 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Тип рассчета
        /// </summary>
        public string TypeDelivery { get; set; }

        /// <summary>
        /// Название транспортной компаний
        /// </summary>
        public string NameTransportCompany { get; set; }

        /// <summary>
        /// Цена доставки
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Время доставки
        /// </summary>
        public string Time { get; set; }

        /// <summary>
        /// Город доставки
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Lat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Log { get; set; }

        /// <summary>
        /// Регион
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Доставка до адреса
        /// </summary>
        [DefaultValue(false)]
        public bool IsAddress { get; set; }

        /// <summary>
        /// Груз находится в городе
        /// </summary>
        [DefaultValue(false)]
        public bool InYourCity { get; set; }

        /// <summary>
        /// Суммарный вес товаров
        /// </summary>
        public decimal TotalWeight { get; set; }

        /// <summary>
        /// Суммарный объем
        /// </summary>
        public decimal TotalVolume { get; set; }

        /// <summary>
        /// Город отправки 
        /// </summary>
        public string ShippingAddress { get;set;}

    }
}
