﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static anketa.Misc.Yandex.Yandex;
using static anketa.Misc.DaData.DaData;
using anketa.Misc.Yandex;

namespace anketa.Misc.Services
{
    public class IAddressSuggestion
    {

        /// <summary>
        /// Получения Адреса
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        public List<YandexCity> Get(string address)
        {

            var addressDaData = GetCity(address);


            if (addressDaData.Suggestions.Count() == 0)
            {
                var addressYandex = GetAddress(address);
                return addressYandex.GetCityList();
            }
            else
            {
                return addressDaData.GetCityList();
            }

        }

    }
}
