﻿using anketa.Misc;
using anketa.Models.Application.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    public class UserConfirmedContact
    {
        /// <summary>
        /// ID контакта
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Тип контакта
        /// </summary>
        public ContactType Type { get; set; }

        /// <summary>
        /// Контакт
        /// </summary>
        public string Contact { get; set; }

        /// <summary>
        /// Перечисление типов контактов
        /// </summary>
        public enum ContactType : int
        {
            /// <summary>
            /// Номер телефона
            /// </summary>
            Phone = 1,

            /// <summary>
            /// Электронная почта
            /// </summary>
            Email = 2,
        }

    }
}
