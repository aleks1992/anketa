﻿using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.API
{
    public class DeliveryData
    {
        /// <summary>
        /// Город по отправления по умолчанию
        /// </summary>
        const string DERIVAL_POINT = "Чита";

        public DeliveryData()
        {
            DerivalPoint = DERIVAL_POINT;
            ArrivalFIAS = "";
        }

        /// <summary>
        /// Город отправления
        /// </summary>
        [JsonProperty("derivalPoint")]
        public string DerivalPoint { get; set; }

        /// <summary>
        /// Город доставки
        /// </summary>
       [JsonProperty("arrivalPoint")]
        public string ArrivalPoint { get; set; }

        /// <summary>
        /// Страна доставки
        /// </summary>
        [JsonProperty("arrivalCountry")]
        public string ArrivalCountry { get; set; }


        /// <summary>
        /// Код ФИАС
        /// </summary>
        [JsonProperty("arrivalFIAS")]
        public string ArrivalFIAS { get; set; }

        [JsonProperty("arrivalLatitude")]
        public double ArrivalLatitude { get; set; }

        [JsonProperty("arrivalLongitude")]
        public double ArrivalLongitude { get; set; }

        [JsonProperty("sizedVolume")]
        public double SizedVolume { get; set; }

        [JsonProperty("sizedWeight")]
        public double SizedWeight { get; set; }


        [JsonProperty("sizedWidth")]
        public double SizedWidth { get; set; }

        [JsonProperty("sizedLength")]
        public double SizedLength { get; set; }

        [JsonProperty("sizedHeight")]
        public double SizedHeight { get; set; }

        [JsonProperty("price")]
        public double Price { get; set; }

        [JsonProperty("distance")]
        public string Distance { get; set; }

        /// <summary>
        /// Адресная доставка
        /// </summary>
        [JsonProperty("isAddress")]
        public bool IsAddress { get; set; }

    }
}
