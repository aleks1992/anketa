﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database.Orders
{
    /// <summary>
    /// Платеж по заказу
    /// </summary>
    public class OrderPayment
    {
        /// <summary>
        /// ID платежа
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// ID заказа
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// Заказ
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// Токен подтверждения оплаты
        /// </summary>
        public string ConfirmationToken { get; set; }

        /// <summary>
        /// ID способа оплаты
        /// </summary>
        public int PaymentMethodId { get; set; }
        
        /// <summary>
        /// Способ оплаты
        /// </summary>
        public OrderPaymentMethods PaymentMethod { get; set; }

        /// <summary>
        /// Дата платежа
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Сумма платежа
        /// </summary>
        public decimal Sum { get; set; }
    }
}
