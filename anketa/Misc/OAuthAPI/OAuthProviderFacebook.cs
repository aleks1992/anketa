﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Models.Database;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using static System.Net.WebRequestMethods;
using static anketa.Misc.SiteClient;

namespace anketa.Misc.OAuthAPI
{
    /// <summary>
    /// Аторизация через Facebook
    /// </summary>
    public class OAuthProviderFacebook : OAuthProvider
    {
        /// <summary>
        /// Получения типа запроса метода
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        protected override string GetHttpMethod(OAuthMethod method)
        {
            return Http.Get;
        }

        /// <summary>
        /// Получения Uri
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public override string GetUri(OAuthMethod method)
        {
            if (method == OAuthMethod.GetCode)
            {
                string[] scope = { "public_profile", "email" };
                var dataCode = new RequestFacebookCode
                {
                    client_id = Config.ClientId,
                    response_type = "code",
                    redirect_uri = RedirectUri,
                    scope = string.Join(",", scope),
                    state = State,
                };
                string getParameter = GetQueryString(dataCode);
                return "https://www.facebook.com/dialog/oauth?" + getParameter;
            }
            else if (method == OAuthMethod.GetToken) return "https://graph.facebook.com/oauth/access_token";
            else if (method == OAuthMethod.GetUser) return "https://graph.facebook.com/v3.0/me";
            return null;
        }

        /// <summary>
        /// Получения подпись для запроса
        /// </summary>
        /// <returns></returns>
        protected override string GetSignature(string[] exceptFields)
        {
            return null;
        }

        /// <summary>
        /// Получения тела запроса
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public override object GetRequestBody(OAuthMethod oAuthMethod, string sig = null)
        {
            return base.GetRequestBody(oAuthMethod);
        }

        /// <summary>
        /// Авторизация 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public override User ProcessLogin(string code, string state = null)
        {
            var userReturn = new User();
            Request = new RequestFacebookToken
            {
                code = code,
                redirect_uri = RedirectUri,
                client_id = Config.ClientId,
                client_secret = Config.SecretKey,
            };

            //Получения токена
            var response = SendRequest(OAuthMethod.GetToken);
            dynamic accessToken = null;
            if (response != null)
            {
                accessToken = JsonConvert.DeserializeObject<AccessFacebookToken>(response.ToString());
            }

            //Отправка запроса на получения User
            if (accessToken != null)
            {

                string[] fields = { "id", "name", "email", "first_name", "last_name"};
                Request = new RequectFacebookUser
                {
                    access_token = accessToken.access_token,
                    fields = string.Join(",", fields)
                };

                response = SendRequest(OAuthMethod.GetUser);
                dynamic user = null;
                if (response != null)
                {
                    user = JsonConvert.DeserializeObject<UserFacebook>(response.ToString());
                }
                if (user != null)
                {
                    userReturn = new User
                    {
                        Email = user.email,
                        FirstName = user.first_name,
                        LastName = user.last_name,
                        Activated = true,
                    };
                }
            }

            return userReturn;
        }

        /// <summary>
        /// Данные для получения токена доступа UserFacebook
        /// </summary>
        class RequestFacebookToken
        {
            /// <summary>
            /// Код авторизации
            /// </summary>
            public string code { get; set; }

            /// <summary>
            /// Uri куда возврашать ответ
            /// </summary>
            public string redirect_uri { get; set; }

            /// <summary>
            ///  Индификатор приложения
            /// </summary>
            public string client_id { get; set; }

            /// <summary>
            /// Закрытый ключ
            /// </summary>
            public string client_secret { get; set; }


        }

        /// <summary>
        /// Получения кода аунтификации  UserFacebook
        /// </summary>
        class RequestFacebookCode
        {
            /// <summary>
            /// Индификатор приложения
            /// </summary>
            public string client_id { get; set; }

            /// <summary>
            /// Типа ответа
            /// </summary>
            public string response_type { get; set; }

            /// <summary>
            /// Uri куда возврашать ответ
            /// </summary>
            public string redirect_uri { get; set; }

            /// <summary>
            /// Права доступа
            /// </summary>
            public string scope { get; set; }

            /// <summary>
            /// Состояние сессии
            /// </summary>
            public string state { get; set; }
        }

        /// <summary>
        /// Токен достпуа UserFacebook
        /// </summary>
        class AccessFacebookToken
        {
            /// <summary>
            /// Токен доступа
            /// </summary>
            public string access_token { get; set; }

            /// <summary>
            /// Время жизни токена
            /// </summary>
            public string expires_in { get; set; }

            /// <summary>
            /// Идентификатор токена
            /// </summary>
            public string id_token { get; set; }

            /// <summary>
            /// Тип токена
            /// </summary>
            public string token_type { get; set; }

        }


        /// <summary>
        /// Получения ифнормации о пользователи UserFacebook
        /// </summary>
        class RequectFacebookUser
        {
            /// <summary>
            /// Токен доступа
            /// </summary>
            public string access_token { get; set; }

            public string fields { get; set; }


        }
        /// <summary>
        /// Set User UserFacebook
        /// </summary>
        class UserFacebook
        {

            /// <summary>
            /// Индификатор Usrer
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// Имя
            /// </summary>
            public string first_name { get; set; }

            /// <summary>
            /// Фамилия
            /// </summary>
            public string last_name { get; set; }

            /// <summary>
            /// Локаль
            /// </summary>
            public string locale { get; set; }

            /// <summary>
            /// Дата рождения
            /// </summary>
            public string birthday { get; set; }
            /// <summary>
            /// Email
            /// </summary>
            public string email { get; set; }

        }
    }
}
