﻿using anketa.Misc;
using anketa.Misc.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Forms
{
    public class OrderListFilter
    {
        /// <summary>
        /// Статус заказов
        /// </summary>
        public string Status { get; set; }

        // От даты
        [DataType(DataType.Date)]
        [DateRangeValidator("2019-01-01")]
        [Display(Name = "От")]
        [DefaultValue(null)]
        public DateTime FromDate { get; set; }

        // До даты
        [DataType(DataType.Date)]
        [DateRangeValidator]
        [Display(Name = "До")]
        [DefaultValue(null)]
        public DateTime ToDate { get; set; }

        // Строка поиска
        public string Search { get; set; }

        /// <summary>
        /// Хэш фильтра, нужен для кэширования
        /// </summary>
        public string FilterHash
        {
            get
            {
                string data = "";
                foreach (var prop in this.GetType().GetProperties())
                {
                    if (prop.Name == "FilterHash") continue;
                    data += (prop.GetValue(this) ?? "").ToString();
                }
                return DataSecurity.ComputeHashMd5(data);
            }
        }
    }
}
