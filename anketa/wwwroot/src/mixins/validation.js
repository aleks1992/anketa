﻿/***
 * Миксин для валидации форм
 */

const LEVELS = {
    ERROR: 0,
    WARNING: 1,
    SUCCESS: 2,
};

function ValidationService() {
    this.checks = [];
    this.watching = [];
    this.options = {
        classes: {
            success: 'has-success',
            warning: 'has-warning',
            error: 'has-error',
        },
    };
    this.reset();
};

ValidationService.prototype.setWatching = function (watching) {
    this.watching = watching;
};

ValidationService.prototype.setOptions = function (options) {
    if (options !== null) {
        for (let o in options) {
            let tokens = o.split(',').map(e => e.trim());
            for (let token of tokens) {
                this.options[token] = options[o];
            }
        }
    }
};

ValidationService.prototype.reset = function() {
    this.validationInfo = [];
};

ValidationService.prototype.setValidationInfo = function (level, token, text, predicate) {
    let computePredicate = function (predicate) {
        let val = false;
        if (typeof predicate === 'boolean') val = predicate;
        else if (typeof predicate === 'function') val = predicate();
        return val;
    };
    if (computePredicate(predicate)) this.validationInfo.push({ level, token, text });
};

ValidationService.prototype.setError = function (token, text, predicate) {
    this.setValidationInfo(LEVELS.ERROR, token, text, predicate);
};

ValidationService.prototype.setWarning = function (token, text, predicate) {
    this.setValidationInfo(LEVELS.WARNING, token, text, predicate);
};

ValidationService.prototype.setSuccess = function (token, text, predicate) {
    this.setValidationInfo(LEVELS.SUCCESS, token, text, predicate);
};

// getValidationInfo
ValidationService.prototype.getValidationInfo = function (level, token) {
    let el = this.validationInfo.find(e => e.level === level && e.token === token);
    return el !== undefined ? el.text : null;    
};

ValidationService.prototype.getError = function (token) {
    return this.getValidationInfo(LEVELS.ERROR, token);
};

ValidationService.prototype.getWarning = function (token) {
    return this.getValidationInfo(LEVELS.WARNING, token);
};

ValidationService.prototype.getSuccess = function (token) {
    return this.getValidationInfo(LEVELS.SUCCESS, token);
};

// hasValidationInfo
ValidationService.prototype.hasValidationInfo = function (level, token) {
    return this.validationInfo.find(e => e.level === level && e.token === token) !== undefined;
};

ValidationService.prototype.hasError = function (token) {
    return this.hasValidationInfo(LEVELS.ERROR, token);
};

ValidationService.prototype.hasWarning = function (token) {
    return this.hasValidationInfo(LEVELS.WARNING, token);
};

ValidationService.prototype.hasSuccess = function (token) {
    return this.hasValidationInfo(LEVELS.SUCCESS, token);
};

ValidationService.prototype.hasAnyErrors = function () {
    return this.validationInfo.find(e => e.level === LEVELS.ERROR) !== undefined;
};

ValidationService.prototype.getClasses = function (w, success, warning, error) {
    let def = {};
    Object.assign(def, this.options.classes);
    if (w in this.options && 'classes' in this.options[w]) {
        for (let o in this.options[w].classes) def[o] = this.options[w].classes[o];
    }

    let applyClasses = (targetClasses, sourceClasses) => {
        if (typeof sourceClasses === 'string') targetClasses[sourceClasses] = true;
        else if (sourceClasses instanceof Array) {
            for (let c of sourceClasses) targetClasses[c] = true;
        }
    };

    let classes = {};
    if (success) applyClasses(classes, def.success);
    if (warning) applyClasses(classes, def.warning);
    if (error) applyClasses(classes, def.error);
    
    return classes;
};

let validationService = new ValidationService();

export default {
    data() {
        return {
            validationService: new ValidationService(),
        };
    },
    computed: {
        validation() {
            let data = {};

            let hasErrors = false;
            let hasWarnings = false;

            for (let w of this.validationService.watching) {
                let hasError = this.validationService.hasError(w);
                let hasWarning = this.validationService.hasWarning(w);
                let hasSuccess = !hasError && !hasWarning || this.validationService.hasSuccess(w);

                let components = w.trim().split('.');
                let ref = data;
                for (let c of components) {
                    if (typeof ref[c] === 'undefined') ref[c] = {
                        classes: {},
                        error: null,
                        text: null,
                        hasError: () => false,
                    };
                    ref = ref[c];
                }

                ref.classes = this.validationService.getClasses(w, hasSuccess, hasWarning, hasError);
                ref.error = this.validationService.getError(w);
                ref.hasError = () => hasError;

                ref.text = null;
                if (hasError) ref.text = this.validationService.getError(w);
                else if (hasWarning) ref.text = this.validationService.getWarning(w);
                else if (hasSuccess) ref.text = this.validationService.getSuccess(w);

                hasErrors |= hasError;
                hasWarnings |= hasWarning;
            }

            data.isValid = (options) => {
                if (typeof options === 'undefined') return !hasErrors && !hasWarnings;
                else {
                    let names = this.validationService.watching;
                    if ('include' in options) {
                        if (typeof options.include === 'string') options.include = [options.include];
                        names = names.filter(e => options.include.indexOf(e) > -1);
                    }

                    if ('exclude' in options) {
                        if (typeof options.exclude === 'string') options.exclude = [options.exclude];
                        names = names.filter(e => options.exclude.indexOf(e) === -1);
                    }

                    for (let name of names) {
                        let components = name.trim().split('.');
                        let ref = data;
                        for (let c of components) ref = ref[c];
                        if (ref.hasError()) return false;
                    }
                }
                return true;
            };

            return data;
        },
    },
    methods: {
        initValidation(variables, validator, options) {
            this.validationService.setWatching(variables);
            this.validationService.setOptions(options);

            let watcher = () => {
                this.validationService.reset();
                validator(this.validationService);
            };

            for (var v of variables) this.$watch(v, watcher);
            validator(this.validationService);
        },
        preparePhoneNumber(phone) {
            if (phone === null || phone === undefined) return null;
            let hasPlus = phone.match(/^\+/g);
            phone = phone.replace(/[^\d]/g, '');
            if (hasPlus) phone = '+' + phone;
            return phone;
        },
        emailIsValid(email) {
            return email.match(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        },
    },
};