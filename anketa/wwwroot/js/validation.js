﻿(function ($) {
    jQuery.extend(jQuery.validator.messages, {
        required: "Это поле обязательно для заполнения.",
        remote: "Пожалуйста, исправьте данные.",
        email: "Пожалуйста, введите правильный Email.",
        url: "Пожалуйста, введите правильный URL.",
        date: "Пожалуйста, введите правильную дату.",
        dateISO: "Пожалуйста, введите действительную дату (ФОРМАТ ГГГГ.ММ.ДД.).",
        number: "Пожалуйста, введите правильный номер.",
        digits: "Пожалуйста, введите только цифры.",
        creditcard: "Пожалуйста, введите действительный номер кредитной карты.",
        equalTo: "Пожалуйста, введите значение еще раз",
        accept: "Пожалуйста, введите значение с допустимым расширением",
        maxlength: jQuery.validator.format("Пожалуйста, введите не более {0} символов."),
        minlength: jQuery.validator.format("Пожалуйста, введите не менее {0} символов."),
        rangelength: jQuery.validator.format("Пожалуйста, введите значение длиной от {0} до {1} символов"),
        range: jQuery.validator.format("Пожалуйста, введите значение между {0} и {1}."),
        max: jQuery.validator.format("Пожалуйста, введите значение, меньшее чем {0}."),
        min: jQuery.validator.format("Пожалуйста, введите значение, большее чем {0}.")
    });

    // Проверка ИНН
    $.validator.addMethod('itin', function (value, element, params) {
        // Проверяем, что элемент должен проходить валидацию
        if (element.attributes.hasOwnProperty('data-val-itin-disable') && element.attributes['data-val-itin-disable'].value == 1) {
            return true;
        }

        if (element.attributes.hasOwnProperty("data-val-itin-len")) {
            var checkDigit = function (itin, cs) {
                var n = 0;
                for (var i in cs) n += cs[i] * itin[i];
                return parseInt(n % 11 % 10);
            };

            var len = parseInt(element.attributes['data-val-itin-len'].value);
            if (isNaN(len)) return true; // неправильная длина
            if ((len === 10 || len === 0) && value.length === 10) {
                var n10 = checkDigit(value, [2, 4, 10, 3, 5, 9, 4, 6, 8]);
                if (n10 === parseInt(value[9])) return true;
            }
            else if ((len === 12 || len === 0) && value.length === 12) {
                var n11 = checkDigit(value, [7, 2, 4, 10, 3, 5, 9, 4, 6, 8, 0]);
                var n12 = checkDigit(value, [3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8, 0]);
                if (n11 === parseInt(value[10]) && n12 === parseInt(value[11])) return true;
            }
            
            return false;
        }
        return true;
    });

    $.validator.unobtrusive.adapters.addBool('itin');

    // Валидатор ОГРН
    $.validator.addMethod('psrn', function (value, elements, params) {
        if (value.length === 13) {
            var n13 = parseInt((parseInt(value.slice(0, -1)) % 11).toString().slice(-1));
            if (n13 === parseInt(value[12])) return true;
        }
        else if (value.length === 15) {
            var n15 = parseInt((parseInt(value.slice(0, -1)) % 13).toString().slice(-1));
            if (n15 === parseInt(value[14])) return true;
        }
        return false;
    });
    $.validator.unobtrusive.adapters.addBool('psrn');
    
    function setStepButtonActive(buttonId) {
        var els = $('.step-button');
        els.addClass('btn-default');
        els.removeClass('btn-primary');
        var el = $('.step-button#step' + buttonId);
        el.addClass('btn-primary');
        el.removeAttr('disabled');
    }

})(jQuery);