﻿// Миксин для вызова подсказок

export default {
    data: function () {
        return {
            _popoverTimer: null,
        };
    },
    methods: {
        showPopover(element, text) {
            if (this._popoverTimer !== null) clearTimeout(this._popoverTimer);

            let options = {
                container: 'body',
                content: text,
                placement: 'auto bottom',
                trigger: 'manual',
            };

            let el = jQuery(element);
            el.popover(options);
            el.popover('show');

            this._popoverTimer = setTimeout(() => {
                el.popover('hide');
            }, 2000);
        },
    },
};