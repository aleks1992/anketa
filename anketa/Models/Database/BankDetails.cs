﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Представляет собой банковские реквизиты
    /// </summary>
    [Serializable]
    public class BankDetails
    {
        /// <summary>
        /// Ид счёта
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        [JsonIgnore]
        public User User { get; set; }

        /// <summary>
        /// Ид пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Расчётный счет
        /// </summary>
        [Display(Name = "Расчётный счёт")]
        public string CheckingAccount { get; set; }

        /// <summary>
        /// Банк
        /// </summary>
        [Display(Name = "Банк")]
        public string BankName { get; set; }

        /// <summary>
        /// Корреспондентный счет
        /// </summary>
        [Display(Name = "Корреспондентный счёт")]
        public string CorrespondentAccount { get; set; }

        /// <summary>
        /// БИК
        /// </summary>
        [Display(Name = "БИК")]
        public string BIC { get; set; }

        /// <summary>
        /// Возвращает представление банковских реквизитов
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if(string.IsNullOrWhiteSpace(CheckingAccount) && string.IsNullOrWhiteSpace(BankName) && string.IsNullOrWhiteSpace(CorrespondentAccount) && string.IsNullOrWhiteSpace(BIC))
            {
                return "";
            }

            return
                $"Расчётный счет: {CheckingAccount}\n" +
                $"Банк: {BankName}\n" +
                $"Кор. счет: {CorrespondentAccount}\n" +
                $"БИК: {BIC}";
        }
    }
}
