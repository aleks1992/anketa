﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class addProductUrl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ProductUrl",
                table: "UserCartItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ProductUrl",
                table: "UserCartItems");
        }
    }
}
