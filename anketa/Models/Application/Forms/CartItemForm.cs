﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application
{
    /// <summary>
    /// Форма отправки запроса на сайты
    /// </summary>
    public class CartItemForm
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string GuId { get; set; }
        public int ProductId { get; set;}
        public int ProductCount { get; set; }
        public string Identifier { get; set; }
        public string Actions { get; set; }

    }
}
