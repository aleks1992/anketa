﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Response
{
    /// <summary>
    /// Представляет собой ответ сервиса
    /// </summary>
    public class ServiceResponse
    {
        /// <summary>
        /// Указывает, успешный ли запрос
        /// </summary>
        [DefaultValue(true)]
        public bool Success {  get; set; }

        /// <summary>
        /// Возвращаемые данные
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }

    }
}
