﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Misc;
using anketa.Misc.MessageServices;
using anketa.Models;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace anketa.Controllers
{
    [AllowAnonymous]
    public class CreditController : Controller
    {
        private readonly DatabaseContext dbContext;
        private readonly IEmailService _emailService;
        private IWebHostEnvironment HostingEnvironment;

        public CreditController(DatabaseContext _dbContext, IEmailService emailService, IWebHostEnvironment env)
        {
            dbContext = _dbContext;
            _emailService = emailService;
            HostingEnvironment = env;
        }

        /// <summary>
        /// Показывает страницу с формой заявки
        /// </summary>
        /// <param name="sum"></param>
        /// <param name="product"></param>
        /// <param name="credit"></param>
        /// <returns></returns>
        public IActionResult Index(int sum, string[] product, int credit, int period = 0)
        {
            ViewBag.Sum = sum;
            ViewBag.Products = product.ToList();
            ViewBag.IsCredit = credit;
            ViewBag.Period = period == 0 ? 12 : period;
            return View();
        }

        /// <summary>
        /// Сохраняет кредитную заявку
        /// </summary>
        /// <param name="application"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Index(CreditApplication application, int credit, int period)
        {
            if (ModelState.IsValid)
            {
                application.BorrowerName = application.GetBorrowerName();
                application.Date = DateTime.Now;

                if (User.Identity.IsAuthenticated)
                {
                    var user = await dbContext.GetUser(User);
                    application.User = user;
                }

                await dbContext.CreditApplications.AddAsync(application);

                // Сохраняем товары
                foreach (var product in application.ProductsViews)
                {
                    var p = new CreditApplicationProduct
                    {
                        CreditApplication = application,
                        Name = product
                    };
                    await dbContext.CreditApplicationProducts.AddAsync(p);
                }

              

                await dbContext.SaveChangesAsync();
                if (HostingEnvironment.IsProduction())
                {
                    await SendCreditInformation(application);
                }

                return RedirectToAction("Success", "Credit");
            }

            ViewBag.Sum = application.CreditSum;
            ViewBag.Products = application.ProductsViews;
            ViewBag.IsCredit = credit;
            ViewBag.Period = period;
            return View(application);
        }

        /// <summary>
        /// Информирования о запросе на кредит
        /// </summary>
        /// <param name="creditApplications"></param>
        /// <param name="creditApplicationProducts"></param>
        /// <returns></returns>
        private async Task<bool> SendCreditInformation(CreditApplication application)
        {
            EmailTemplate template = new EmailTemplate("CreditAlerts", "MasterGroup: Заявка на кредит");
            var product = application.ProductsViews.ToArray();
            template.SetData("COMMENT", application.Comment);
            template.SetData("EMAIL", application.BorrowerEmail ?? "Не указан");
            template.SetData("BORROWER_EMAIL", application.BorrowerEmail ?? "Не указан");
            template.SetData("BORROWER_NAME", application.BorrowerName);
            template.SetData("PASSPORT_SERIAL", application.PassportSerial);
            template.SetData("PASSPORT_NUMBER", application.PassportNumber);
            template.SetData("CREDIT_SUM", application.CreditSum.ToString());
            template.SetData("FIRST_PAY", application.FirstPay.ToString());
            template.SetData("CREDIT_PERIOD", application.CreditPeriod.ToString());
            template.SetData("BORROWER_PHONE", application.BorrowerPhone ?? "Не указан");
            template.SetData("DATE", application.Date.Date.ToString());
            template.SetData("PRODUCT", string.Join("<br>", product));

            await _emailService.SendMessage("torg@batutmaster.ru", "MasterGroup: Заявка на кредит", template.Compile(false));
            return true;
        }

        /// <summary>
        /// Показывает страницу с информацией об успешной регистрации заявки
        /// </summary>
        /// <returns></returns>
        public IActionResult Success()
        {
            return View();
        }
    }
}