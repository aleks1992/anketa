﻿
let precisions = [
    'other',
    'street',
    'range',
    'near',
    'number',
    'exact',
];

export default {
    data: function() {
        return {
            geocodeTimer: null,
        };
    },
    methods: {
        checkAddress: function (address, precision) {
            let promise = new Promise((resolve, reject) => {
                if (window.appData.disableYandexSuggestions !== true) {
                    let needLevel = precisions.indexOf(precision);
                    if (needLevel === -1) resolve(false);

                    let checkAddress = function () {
                        if (typeof window.ymaps.geocode !== 'undefined') {
                            ymaps.geocode(address)
                                .then(response => {
                                    let obj = response.geoObjects.get(0);
                                    if (obj) {
                                        let p = obj.properties.get('metaDataProperty.GeocoderMetaData.precision');
                                        let level = precisions.indexOf(p);
                                        resolve(level >= needLevel);
                                    } else {
                                        console.error('Адрес не найден.');
                                        resolve(false);
                                    }
                                });
                        } else {
                            setTimeout(checkAddress, 200);
                        }
                    };   

                    checkAddress();
                } else {
                    resolve(true);
                }
            });

            return promise;
        },
        getAddress(address) {
            let promise = new Promise((resolve, reject) => { 
                if (window.appData.disableYandexSuggestions !== true) {
                    let checkAddress = function () {
                        if (typeof window.ymaps.geocode !== 'undefined') {
                            ymaps.geocode(address)
                                .then(response => {
                                    let obj = response.geoObjects.get(0);
                                    if (obj) {
                                        let p = obj.properties.get('metaDataProperty.GeocoderMetaData.AddressDetails');
                                        resolve(p);
                                    } else {

                                        resolve(false);
                                    }
                                });
                        } else {
                            setTimeout(checkAddress, 200);
                        }
                    };

                    checkAddress();
                } else {
                    resolve(true);
                }
            });

            return promise;
        }
    },
    created: function () {
        if (typeof window.ymaps === 'undefined') {
            window.ymaps = { init: true };

            let api = document.createElement('script');
            api.src = "https://api-maps.yandex.ru/2.1/?lang=ru_RU&apikey=8420b7f7-1a1e-46bc-b0a4-940c0ae1073c";
            document.body.appendChild(api);
        }
    }
};