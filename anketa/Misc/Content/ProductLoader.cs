﻿using anketa.Models.Database;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace anketa.Misc.Content
{
    public class ProductLoader
    {
        private readonly List<SiteData> linkedSites;

        public ProductLoader(List<SiteData> _linkedSites)
        {
            linkedSites = _linkedSites;
        }

        public Product GetProduct(string guid)
        {
            foreach (var site in linkedSites)
            {
                var request = new
                {
                    module = "product",
                    action = "anketa_product_data",
                    products = new List<object>
                    {
                        new {
                            guid = guid,
                            price = 0
                        }
                    }
                };

                var response = SiteClient.HttpClient(request, site.URL + Properties.Resources.SiteAjax);
                if(response.Success)
                {
                    var s = JsonConvert.SerializeObject(response.Data);
                    var list = JsonConvert.DeserializeObject<List<Product>>(s);
                    if (list.Count > 0)
                    {
                        list[0].Site = site.Prefix;
                        return list[0];
                    }
                }

            }
            return null;
        }
    }
}
