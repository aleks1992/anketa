﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Models.Application.Response;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace anketa.Misc.Captcha
{
    /// <summary>
    /// Представляет собой атрибут для валидации Recaptcha
    /// </summary>
    public class ValidateCaptchaAttribute : TypeFilterAttribute
    {
        /// <summary>
        /// Конструктор
        /// </summary>
        public ValidateCaptchaAttribute() : base(typeof(ValidateRecaptchaImplementation))
        {
        }

        /// <summary>
        /// Представляет собой имплементацию фильтра
        /// </summary>
        private class ValidateRecaptchaImplementation : IAsyncActionFilter
        {
            /// <summary>
            /// Сервис капчи
            /// </summary>
            ICaptchaService captchaService;

            /// <summary>
            /// Параметры хостинга
            /// </summary>
            IWebHostEnvironment webHostEnvironment;

            /// <summary>
            /// Конструктор
            /// </summary>
            /// <param name="serviceProvider"></param>
            public ValidateRecaptchaImplementation(IServiceProvider serviceProvider)
            {
                var service = serviceProvider.GetService(typeof(ICaptchaService));
                if (service is ICaptchaService) captchaService = (ICaptchaService)service;

                service = serviceProvider.GetService(typeof(IWebHostEnvironment));
                if (service is IWebHostEnvironment) webHostEnvironment = (IWebHostEnvironment)service;
            }

            /// <summary>
            /// Вызывается перед выполнением экшена
            /// </summary>
            /// <param name="context"></param>
            /// <param name="next"></param>
            /// <returns></returns>
            public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
            {
                if (!await CheckRecaptcha(context.HttpContext))
                {
                    var response = new ServiceResponse
                    {
                        Message = "Ошибка проверки капчи.",
                        Success = false,
                    };

                    var responseBody = JsonConvert.SerializeObject(response, new JsonSerializerSettings
                    {
                        ContractResolver = new DefaultContractResolver
                        {
                            NamingStrategy = new SnakeCaseNamingStrategy()
                        },
                        Formatting = Formatting.None,
                    });

                    context.Result = new ContentResult
                    {
                        Content = responseBody,
                        ContentType = "application/json",
                        StatusCode = 403,
                    };
                }
                else
                {
                    await next();
                }
            }

            

            /// <summary>
            /// Проверяет правильность рекапчи, токен получает из GET-параметра или заголовка
            /// </summary>
            /// <param name="httpContext"></param>
            /// <returns></returns>
            public async Task<bool> CheckRecaptcha(HttpContext httpContext)
            {
                const string tokenKey = "captcha";

                if(captchaService != null)
                {
                    string token = null;

                    var queryToken = httpContext.Request.Query.FirstOrDefault(p => p.Key.ToLower() == tokenKey);
                    if (queryToken.Key != null && queryToken.Value.Count > 0) token = queryToken.Value[0];

                    var headerToken = httpContext.Request.Headers.FirstOrDefault(p => p.Key.ToLower() == tokenKey);
                    if (token == null && headerToken.Key != null && headerToken.Value.Count > 0) token = headerToken.Value[0];

                    if (token != null) return await captchaService.IsValid(token);
                    return false;
                }

                return true;
            }

        }
    }
}
