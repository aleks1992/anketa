﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Кредитная заявка
    /// </summary>
    public class CreditApplication
    {
        /// <summary>
        /// ID заявки
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public int? UserId { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        [JsonIgnore]
        public User User { get; set; }

        /// <summary>
        /// Дата заявки
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Фамилия заемщика
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        [Display(Name = "Фамилия заемщика")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите фамилию.")]        
        public string BorrowerLastName { get; set; }

        /// <summary>
        /// Имя заемщика
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        [Display(Name = "Имя заемщика")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите имя.")]
        public string BorrowerFirstName { get; set; }

        /// <summary>
        /// Отчество заемщика
        /// </summary>
        [NotMapped]
        [JsonIgnore]
        [Display(Name = "Отчество заемщика")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите отчество.")]
        public string BorrowerMiddleName { get; set; }

        /// <summary>
        /// ФИО заемщика
        /// </summary>
        [Display(Name = "ФИО заемщика")]
        public string BorrowerName { get; set; }

        /// <summary>
        /// Серия паспорта
        /// </summary>
        [Display(Name = "Серия паспорта")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите серию паспорта.")]
        [StringLength(4, MinimumLength = 4, ErrorMessage = "Серия паспорта должна состоять из 4 цифр.")]
        public string PassportSerial { get; set; }

        /// <summary>
        /// Номер паспорта
        /// </summary>
        [Display(Name = "Номер паспорта")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите номер паспорта.")]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "Серия паспорта должна состоять из 6 цифр.")]
        public string PassportNumber { get; set; }

        /// <summary>
        /// Сумма кредита
        /// </summary>
        [Display(Name = "Сумма кредита")]
        public int CreditSum { get; set; }

        /// <summary>
        /// Первоначальный взнос
        /// </summary>
        [Display(Name = "Первоначальный взнос (%)")]
        [Range(0, 90, ErrorMessage = "Первоначальный взнос должен быть от 0 до 90%.")]
        public int FirstPay { get; set; }

        /// <summary>
        /// Срок кредитования
        /// </summary>
        [Display(Name = "Срок кредитования, мес.")]
        [Range(3, 36, ErrorMessage = "Срок кредитования должен быть от 3 до 36 месяцев.")]
        public int CreditPeriod { get; set; }

        /// <summary>
        /// Email заемщика
        /// </summary>
        [Display(Name = "Email заемщика")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите email заемщика.")]
        [EmailAddress(ErrorMessage = "Электронная почта введена неправильно.")]
        public string BorrowerEmail { get; set; }

        /// <summary>
        /// Номер телефона заемщика
        /// </summary>
        [Display(Name = "Номер телефона")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите номер телефона.")]
        public string BorrowerPhone { get; set; }

        /// <summary>
        /// Комментарий
        /// </summary>
        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

        /// <summary>
        /// Показывает, выгружена ли заявка
        /// </summary>
        public bool Uploaded { get; set; }

        /// <summary>
        /// Представление товаров в кредитной заявке
        /// </summary>
        [NotMapped]
        public List<string> ProductsViews { get; set; }

        /// <summary>
        /// Возвращает ФИО заемщика
        /// </summary>
        /// <returns></returns>
        internal string GetBorrowerName()
        {
            return string.Format("{0} {1} {2}", BorrowerLastName.Trim(), BorrowerFirstName.Trim(), BorrowerMiddleName.Trim());
        }
    }
}
