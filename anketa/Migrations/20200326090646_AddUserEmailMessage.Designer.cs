﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using anketa.Models;

namespace anketa.Migrations
{
    [DbContext(typeof(DatabaseContext))]
    [Migration("20200326090646_AddUserEmailMessage")]
    partial class AddUserEmailMessage
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("Microsoft.AspNetCore.DataProtection.EntityFrameworkCore.DataProtectionKey", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FriendlyName");

                    b.Property<string>("Xml");

                    b.HasKey("Id");

                    b.ToTable("DataProtectionKeys");
                });

            modelBuilder.Entity("anketa.Models.Database.ActOnBase", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("ID");

                    b.ToTable("Bases");
                });

            modelBuilder.Entity("anketa.Models.Database.Address", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BuildingNumber")
                        .IsRequired();

                    b.Property<string>("City")
                        .IsRequired();

                    b.Property<string>("Country")
                        .IsRequired();

                    b.Property<string>("FlatNumber")
                        .IsRequired();

                    b.Property<string>("Index")
                        .IsRequired();

                    b.Property<string>("Street")
                        .IsRequired();

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("Addresses");
                });

            modelBuilder.Entity("anketa.Models.Database.AuthorizationToken", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Expires");

                    b.Property<string>("Host");

                    b.Property<string>("Identifier");

                    b.Property<string>("RefreshToken");

                    b.Property<string>("Token");

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("AuthorizationTokens");
                });

            modelBuilder.Entity("anketa.Models.Database.BankDetails", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BIC");

                    b.Property<string>("BankName");

                    b.Property<string>("CheckingAccount");

                    b.Property<string>("CorrespondentAccount");

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("BankDetails");
                });

            modelBuilder.Entity("anketa.Models.Database.CreditApplication", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BorrowerEmail")
                        .IsRequired();

                    b.Property<string>("BorrowerName")
                        .IsRequired();

                    b.Property<string>("BorrowerPhone")
                        .IsRequired();

                    b.Property<string>("Comment");

                    b.Property<int>("CreditPeriod");

                    b.Property<int>("CreditSum");

                    b.Property<DateTime>("Date");

                    b.Property<int>("FirstPay");

                    b.Property<string>("PassportNumber")
                        .IsRequired();

                    b.Property<string>("PassportSerial")
                        .IsRequired();

                    b.Property<bool>("Uploaded");

                    b.Property<int?>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("CreditApplications");
                });

            modelBuilder.Entity("anketa.Models.Database.CreditApplicationProduct", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CreditApplicationId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("CreditApplicationId");

                    b.ToTable("CreditApplicationProducts");
                });

            modelBuilder.Entity("anketa.Models.Database.LegalForm", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("HeadMustExists");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("LegalForms");
                });

            modelBuilder.Entity("anketa.Models.Database.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("DocumentsReceiverAddressId");

                    b.Property<int>("DocumentsReceiverId");

                    b.Property<string>("DocumentsReceiverInformation");

                    b.Property<DateTime>("ModifyDate");

                    b.Property<string>("OrderData");

                    b.Property<DateTime>("OrderDate");

                    b.Property<int>("OrderPaymentMethodId");

                    b.Property<int>("OrdererFaceId");

                    b.Property<string>("OrdererInformation");

                    b.Property<int>("PayerFaceId");

                    b.Property<string>("PayerInformation");

                    b.Property<int>("ProductReceiverAddressId");

                    b.Property<int>("ProductReceiverId");

                    b.Property<string>("ProductReceiverInformation");

                    b.Property<string>("StatusComment");

                    b.Property<int>("StatusId");

                    b.Property<bool>("TargetedDelivery");

                    b.Property<bool>("Uploaded");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("DocumentsReceiverAddressId");

                    b.HasIndex("DocumentsReceiverId");

                    b.HasIndex("OrderPaymentMethodId");

                    b.HasIndex("OrdererFaceId");

                    b.HasIndex("PayerFaceId");

                    b.HasIndex("ProductReceiverAddressId");

                    b.HasIndex("ProductReceiverId");

                    b.HasIndex("StatusId");

                    b.HasIndex("UserId");

                    b.ToTable("Orders");
                });

            modelBuilder.Entity("anketa.Models.Database.OrderPaymentMethods", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<Guid>("Guid");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("OrderPaymentMethods");
                });

            modelBuilder.Entity("anketa.Models.Database.OrderProductItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("ByOrder");

                    b.Property<int>("OrderId");

                    b.Property<string>("PriceTypeId");

                    b.Property<string>("PriceTypeName");

                    b.Property<int>("ProductCount");

                    b.Property<int?>("ProductId");

                    b.Property<decimal>("ProductPrice");

                    b.Property<string>("ProductUri");

                    b.Property<int?>("ProjectId");

                    b.Property<string>("SerialNumber");

                    b.Property<string>("StockId");

                    b.Property<int>("Type");

                    b.HasKey("Id");

                    b.HasIndex("OrderId");

                    b.HasIndex("ProductId");

                    b.HasIndex("ProjectId");

                    b.ToTable("OrderProductItems");
                });

            modelBuilder.Entity("anketa.Models.Database.OrderStatus", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("FilterStatus");

                    b.Property<string>("Name");

                    b.Property<string>("View");

                    b.HasKey("ID");

                    b.ToTable("OrderStatuses");
                });

            modelBuilder.Entity("anketa.Models.Database.PriceType", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("PriceTypes");
                });

            modelBuilder.Entity("anketa.Models.Database.Product", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<string>("Guid")
                        .HasMaxLength(42);

                    b.Property<decimal>("Height");

                    b.Property<decimal>("Length");

                    b.Property<string>("Name");

                    b.Property<int>("ProductStatus");

                    b.Property<string>("Site");

                    b.Property<string>("Sku");

                    b.Property<decimal>("Width");

                    b.HasKey("Id");

                    b.ToTable("Products");
                });

            modelBuilder.Entity("anketa.Models.Database.Products.ProductProject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<Guid>("Guid");

                    b.Property<string>("Name");

                    b.Property<string>("Phone");

                    b.Property<decimal>("Price");

                    b.Property<string>("ProjectNumber");

                    b.Property<string>("Sku");

                    b.HasKey("Id");

                    b.ToTable("ProductProjects");
                });

            modelBuilder.Entity("anketa.Models.Database.ServiceUser", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Password");

                    b.Property<string>("Type");

                    b.Property<string>("User");

                    b.HasKey("ID");

                    b.ToTable("ServiceUsers");
                });

            modelBuilder.Entity("anketa.Models.Database.ServiceUserPermission", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Permission");

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("ServiceUserPermissions");
                });

            modelBuilder.Entity("anketa.Models.Database.User", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Activated");

                    b.Property<DateTime?>("BirthdayDate");

                    b.Property<bool>("ConsentToNewsletter");

                    b.Property<string>("Email");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("MiddleName");

                    b.Property<string>("Password");

                    b.Property<bool>("PersonalDataProcessing");

                    b.Property<string>("Phone");

                    b.Property<DateTime?>("RegistrationDate");

                    b.Property<bool>("Sex");

                    b.HasKey("ID");

                    b.ToTable("Users");
                });

            modelBuilder.Entity("anketa.Models.Database.UserActivationToken", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Data");

                    b.Property<DateTime>("Expires");

                    b.Property<string>("Token");

                    b.Property<int>("Type");

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("UserActivationTokens");
                });

            modelBuilder.Entity("anketa.Models.Database.UserConfirmedEmail", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Email");

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("UserConfirmedEmails");
                });

            modelBuilder.Entity("anketa.Models.Database.UserConfirmedPhone", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Phone");

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("UserConfirmedPhones");
                });

            modelBuilder.Entity("anketa.Models.Database.UserEmailMessage", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("DateCreated");

                    b.Property<DateTime>("DateSending");

                    b.Property<string>("Email");

                    b.Property<bool>("IsSend");

                    b.Property<string>("Meassage");

                    b.Property<string>("Subject");

                    b.Property<int?>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("UserEmailMessages");
                });

            modelBuilder.Entity("anketa.Models.Database.UserEntityFace", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ActFromOrgFaceId");

                    b.Property<bool>("ActFromOrganization");

                    b.Property<DateTime>("ActOnBaseDocumentDate");

                    b.Property<string>("ActOnBaseDocumentSerialNumber")
                        .IsRequired();

                    b.Property<int?>("ActOnBaseId");

                    b.Property<string>("AdditionalInfo");

                    b.Property<int?>("BankDetailsId");

                    b.Property<string>("Country");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("FullName")
                        .IsRequired();

                    b.Property<int>("HeadId");

                    b.Property<string>("ITIN")
                        .IsRequired();

                    b.Property<bool>("IsForeignFace");

                    b.Property<int>("LegalFormId");

                    b.Property<string>("PSRN")
                        .IsRequired();

                    b.Property<string>("Phones")
                        .IsRequired();

                    b.Property<string>("ShortName")
                        .IsRequired();

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("ActFromOrgFaceId");

                    b.HasIndex("ActOnBaseId");

                    b.HasIndex("BankDetailsId");

                    b.HasIndex("HeadId");

                    b.HasIndex("LegalFormId");

                    b.HasIndex("UserId");

                    b.ToTable("UserEntityFaces");
                });

            modelBuilder.Entity("anketa.Models.Database.UserFace", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("EntityFaceId");

                    b.Property<int?>("IndividualFaceId");

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("EntityFaceId");

                    b.HasIndex("IndividualFaceId");

                    b.HasIndex("UserId");

                    b.ToTable("Faces");
                });

            modelBuilder.Entity("anketa.Models.Database.UserIndividualFace", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AdditionalInfo");

                    b.Property<int?>("BankDetailsId");

                    b.Property<string>("Country");

                    b.Property<string>("Email")
                        .IsRequired();

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<string>("ITIN")
                        .IsRequired();

                    b.Property<bool>("IsForeigner");

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<string>("MiddleName");

                    b.Property<string>("PassportAuthority")
                        .IsRequired();

                    b.Property<DateTime>("PassportDate");

                    b.Property<string>("PassportNumber")
                        .IsRequired();

                    b.Property<string>("PassportSerial")
                        .IsRequired();

                    b.Property<string>("Phones")
                        .IsRequired();

                    b.Property<string>("Position")
                        .IsRequired();

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("BankDetailsId");

                    b.HasIndex("UserId");

                    b.ToTable("UserIndividualFaces");
                });

            modelBuilder.Entity("anketa.Models.Database.UserPasswordRestoreToken", b =>
                {
                    b.Property<int>("ID")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("Expires");

                    b.Property<string>("Token");

                    b.Property<int>("UserId");

                    b.HasKey("ID");

                    b.HasIndex("UserId");

                    b.ToTable("UserPasswordRestoreTokens");
                });

            modelBuilder.Entity("anketa.Models.UserSocial", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Social");

                    b.Property<int>("UserId");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("UserSocials");
                });

            modelBuilder.Entity("anketa.Models.Database.Address", b =>
                {
                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.AuthorizationToken", b =>
                {
                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.BankDetails", b =>
                {
                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.CreditApplication", b =>
                {
                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("anketa.Models.Database.CreditApplicationProduct", b =>
                {
                    b.HasOne("anketa.Models.Database.CreditApplication", "CreditApplication")
                        .WithMany()
                        .HasForeignKey("CreditApplicationId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.Order", b =>
                {
                    b.HasOne("anketa.Models.Database.Address", "DocumentsReceiverAddress")
                        .WithMany()
                        .HasForeignKey("DocumentsReceiverAddressId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("anketa.Models.Database.UserFace", "DocumentsReceiver")
                        .WithMany()
                        .HasForeignKey("DocumentsReceiverId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("anketa.Models.Database.OrderPaymentMethods", "OrderPaymentMethod")
                        .WithMany()
                        .HasForeignKey("OrderPaymentMethodId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("anketa.Models.Database.UserFace", "OrdererFace")
                        .WithMany()
                        .HasForeignKey("OrdererFaceId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("anketa.Models.Database.UserFace", "PayerFace")
                        .WithMany()
                        .HasForeignKey("PayerFaceId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("anketa.Models.Database.Address", "ProductReceiverAddress")
                        .WithMany()
                        .HasForeignKey("ProductReceiverAddressId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("anketa.Models.Database.UserFace", "ProductReceiver")
                        .WithMany()
                        .HasForeignKey("ProductReceiverId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("anketa.Models.Database.OrderStatus", "Status")
                        .WithMany()
                        .HasForeignKey("StatusId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.OrderProductItem", b =>
                {
                    b.HasOne("anketa.Models.Database.Order", "Order")
                        .WithMany()
                        .HasForeignKey("OrderId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("anketa.Models.Database.Product", "Product")
                        .WithMany()
                        .HasForeignKey("ProductId");

                    b.HasOne("anketa.Models.Database.Products.ProductProject", "Project")
                        .WithMany()
                        .HasForeignKey("ProjectId");
                });

            modelBuilder.Entity("anketa.Models.Database.ServiceUserPermission", b =>
                {
                    b.HasOne("anketa.Models.Database.ServiceUser", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.UserActivationToken", b =>
                {
                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.UserConfirmedEmail", b =>
                {
                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.UserConfirmedPhone", b =>
                {
                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.UserEmailMessage", b =>
                {
                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("anketa.Models.Database.UserEntityFace", b =>
                {
                    b.HasOne("anketa.Models.Database.UserIndividualFace", "ActFromOrgFace")
                        .WithMany()
                        .HasForeignKey("ActFromOrgFaceId");

                    b.HasOne("anketa.Models.Database.ActOnBase", "ActOnBase")
                        .WithMany()
                        .HasForeignKey("ActOnBaseId");

                    b.HasOne("anketa.Models.Database.BankDetails", "BankDetails")
                        .WithMany()
                        .HasForeignKey("BankDetailsId");

                    b.HasOne("anketa.Models.Database.UserIndividualFace", "Head")
                        .WithMany()
                        .HasForeignKey("HeadId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("anketa.Models.Database.LegalForm", "LegalForm")
                        .WithMany()
                        .HasForeignKey("LegalFormId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.UserFace", b =>
                {
                    b.HasOne("anketa.Models.Database.UserEntityFace", "EntityFace")
                        .WithMany()
                        .HasForeignKey("EntityFaceId");

                    b.HasOne("anketa.Models.Database.UserIndividualFace", "IndividualFace")
                        .WithMany()
                        .HasForeignKey("IndividualFaceId");

                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.UserIndividualFace", b =>
                {
                    b.HasOne("anketa.Models.Database.BankDetails", "BankDetails")
                        .WithMany()
                        .HasForeignKey("BankDetailsId");

                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.Database.UserPasswordRestoreToken", b =>
                {
                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("anketa.Models.UserSocial", b =>
                {
                    b.HasOne("anketa.Models.Database.User", "User")
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
