﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class RemoveIndividualUserITINLen : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ITIN",
                table: "UserIndividualFaces",
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 10);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ITIN",
                table: "UserIndividualFaces",
                maxLength: 10,
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}
