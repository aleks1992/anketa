﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddHeadStatusInLegalForms : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "MiddleName",
                table: "UserIndividualFaces",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<bool>(
                name: "HeadMustExists",
                table: "LegalForms",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData("LegalForms", "Name", "ООО", "HeadMustExists", true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HeadMustExists",
                table: "LegalForms");

            migrationBuilder.AlterColumn<string>(
                name: "MiddleName",
                table: "UserIndividualFaces",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
