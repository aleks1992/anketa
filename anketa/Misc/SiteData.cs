﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace anketa.Misc
{
    /// <summary>
    /// Связанные сайты, на которых тоже нужно авторизоваться
    /// </summary>
    public class SiteData
    {
        /// <summary>
        /// Наименование сайта
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Префикс сайта
        /// </summary>
        public string Prefix { get; set; }

        /// <summary>
        /// Адрес сайта
        /// </summary>
        public string URL { get; set; }

        /// <summary>
        /// Имя хоста
        /// </summary>
        public string Host
        {
            get
            {
                var regex = new Regex(@"(?:https?:\/\/)([^\/]+)");
                var matches = regex.Matches(URL);
                if(matches.Count > 0)
                {
                    return matches[0].Groups[1].Value;
                }
                return "";
            }
        }

        public string CabinetUrl
        {
            get
            {
                var regex = new Regex(@"(https?:\/\/)");
                var url = regex.Replace(URL, "my.");

                regex = new Regex(@"\/$");
                return regex.Replace(url, "");
            }
        }

        /// <summary>
        /// Возвращает ссылку
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        internal string GetLink(string v)
        {
            return "http://" + Host + "/" + v.TrimStart('/');
        }

        private static List<SiteData> Storage;
        public static void InitInstance(List<SiteData> storage)
        {
            Storage = storage;
        }

        public static List<SiteData> GetSites()
        {
            return Storage;
        }
    }
}
