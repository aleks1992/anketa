﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.API
{
    public class ProductFormApi
    {
        /// <summary>
        /// Глобальный ид товара
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// Найменования товара
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Артикул товара
        /// </summary>
        public string Sku { get; set; }

        /// <summary>
        /// Длина товара
        /// </summary>
        public decimal Length { get; set; }

        /// <summary>
        /// Ширина товара
        /// </summary>
        public decimal Width { get; set; }

        /// <summary>
        /// Высота товара
        /// </summary>
        public decimal Height { get; set; }

        /// <summary>
        /// Описания товара
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Статус товара
        /// </summary>
        public string ProductStatus { get; set; }

        /// <summary>
        /// Цены товара
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// Url продукта
        /// </summary>
        public string ProductUrl { get; set; }
    }
}
