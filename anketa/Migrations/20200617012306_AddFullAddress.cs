﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddFullAddress : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FullAddress",
                table: "Addresses",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsDefault",
                table: "Addresses",
                nullable: false,
                defaultValue: false);

            // Заполняем полный адрес
            migrationBuilder.Sql("UPDATE Addresses SET FullAddress = CONCAT(Country, \", \", City, \", \", Street, \", \", BuildingNumber, \",\", FlatNumber);");

            // Заполняем полные адреса в заказах
            migrationBuilder.Sql("SET SQL_SAFE_UPDATES = 0;");
            migrationBuilder.Sql("UPDATE Orders SET DocumentDeliveryAddress = (SELECT Addresses.FullAddress FROM Addresses WHERE ID = Orders.DocumentsReceiverAddressId);");
            migrationBuilder.Sql("UPDATE Orders SET ProductDeliveryAddress = (SELECT Addresses.FullAddress FROM Addresses WHERE ID = Orders.ProductReceiverAddressId);");
            migrationBuilder.Sql("SET SQL_SAFE_UPDATES = 1;");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullAddress",
                table: "Addresses");

            migrationBuilder.DropColumn(
                name: "IsDefault",
                table: "Addresses");
        }
    }
}
