﻿export default {
    data: function () {
        return {
            captchaToken: null,
        };
    },
    methods: {
        getRecaptchaToken() {
            let token = this.captchaToken;
            this.captchaToken = null;
            return token;
        }
    }
}