﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.API.Response
{
    public class DataUploadStatus
    {
        /// <summary>
        /// Показывает статус
        /// </summary>
        public bool Success { get; set; }


        private string _errorMessage = null;

        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string ErrorMessage
        {
            get
            {
                return _errorMessage;
            }
            set
            {
                _errorMessage = value;
                Success = (_errorMessage == null);
            }
        }

        /// <summary>
        /// Объекты, отражающие статус
        /// </summary>
        public List<object> Objects { get; set; }

        public DataUploadStatus()
        {
            Success = true;
        }

        /// <summary>
        /// Добавляет объект в список объектов, отражающих статус операции
        /// </summary>
        /// <param name="obj">Объект</param>
        public void AddObject(object obj)
        {
            if (Objects == null) Objects = new List<object>();
            Objects.Add(obj);
        }

        /// <summary>
        /// Возвращает этот объект в виде JsonResult
        /// </summary>
        /// <returns>Сериализованный объект</returns>
        public JsonResult GetJsonResult(HttpContext httpContext)
        {
            if(httpContext != null) httpContext.Response.StatusCode = (Success ? 200 : 500);
            return new JsonResult(this);
        }
    }
}
