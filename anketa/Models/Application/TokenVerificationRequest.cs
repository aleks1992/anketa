﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application
{
    public class TokenVerificationRequest
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Токен аутентификации
        /// </summary>
        public string Token { get; set; }
    }
}
