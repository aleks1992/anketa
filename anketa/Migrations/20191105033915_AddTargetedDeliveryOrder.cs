﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddTargetedDeliveryOrder : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "TargetedDelivery",
                table: "Orders",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TargetedDelivery",
                table: "Orders");
        }
    }
}
