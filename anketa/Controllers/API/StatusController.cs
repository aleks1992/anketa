﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Models;
using anketa.Models.API.Response;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace anketa.Controllers.API
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "API-Status")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class StatusController : ControllerBase
    {
        private DatabaseContext DbContext;

        public StatusController(DatabaseContext dbContext)
        {
            DbContext = dbContext;
        }

        /// <summary>
        /// Возвращает список статусов заказов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var statuses = DbContext.OrderStatuses.ToList();
            return Ok(new
            {
                statuses
            });            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statuses"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]List<OrderStatus> statuses)
        {
            foreach(var status in statuses)
            {
                var oldStatus = DbContext.OrderStatuses.FirstOrDefault(p => p.Name == status.Name);
                if(oldStatus == null)
                {
                    oldStatus = new OrderStatus
                    {
                        Name = status.Name,
                        View = status.View
                    };
                    await DbContext.OrderStatuses.AddAsync(oldStatus);
                }
                else
                {
                    if(oldStatus.View != status.View)
                    {
                        oldStatus.View = status.View;
                        DbContext.Update(oldStatus);
                    }
                }
            }
            await DbContext.SaveChangesAsync();

            // TODO: добавить удаление статусов

            return Ok(true);
        }
    }
}