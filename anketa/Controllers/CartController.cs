﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Misc;
using anketa.Misc.SiteAPI;
using anketa.Models;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using static anketa.Misc.SiteClient;

namespace anketa.Controllers
{
    [AllowAnonymous]
    public class CartController : Controller
    {
        private readonly CartWishlistManager cartWishlistManager;

        public CartController(DatabaseContext dbContext, CartWishlistManager cartWishlistUpdater)
        {
            cartWishlistManager = cartWishlistUpdater;
        }

        /// <summary>
        /// Отображает список товаров в корзине
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            if (User.IsInRole("UnconfirmedUser"))
                return RedirectToAction("PersonalData", "Cabinet");

            return View("List", cartWishlistManager.Storage.Cart);
        }

        #region API

        /// <summary>
        /// Удаляет товар из корзины
        /// </summary>
        /// <param name="cartForUpdate"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Delete([FromBody]CartWishlistItem item)
        {
            var response = new ServerResponseItemData();
            cartWishlistManager.Storage.Cart.RemoveAll(p =>
                p.Guid == item.Guid && p.ByOrder == item.ByOrder
                && p.PriceTypeId == item.PriceTypeId
                && (p.SerialNumber == item.SerialNumber || (string.IsNullOrEmpty(item.SerialNumber) && string.IsNullOrEmpty(p.SerialNumber))) 
                && p.StockId == item.StockId);
          
            if (cartWishlistManager.Commit())
            {
                response.Success = true;
                response.Data = cartWishlistManager.Storage.Cart;
            }

            return Json(response);
        }

        /// <summary>
        /// Обновляет товар в корзине
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Update([FromBody]CartWishlistItem item)
        {
            var response = new ServerResponseItemData();

            if (item != null)
            {
                var itemInCart = cartWishlistManager.Storage.Cart.FirstOrDefault(p =>
                    p.Guid == item.Guid && p.ByOrder == item.ByOrder
                    && p.PriceTypeId == item.PriceTypeId
                    && (p.SerialNumber == item.SerialNumber || (string.IsNullOrEmpty(p.SerialNumber) && string.IsNullOrEmpty(item.SerialNumber)))
                    && p.StockId == item.StockId);

                if (itemInCart != null)
                {
                    itemInCart.Count = item.Count;
                    if (cartWishlistManager.Commit()) response.Success = true;
                }
            }

            return Json(response);
        }

        /// <summary>
        /// Добавления товара в корзину
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Insert([FromBody]CartWishlistItem item)
        {
            var response = new ServerResponseItemData();

            var itemInCart = cartWishlistManager.Storage.Cart.FirstOrDefault(p =>
                p.Guid == item.Guid && p.ByOrder == item.ByOrder
                && p.PriceTypeId == item.PriceTypeId
                && p.SerialNumber == item.SerialNumber && p.StockId == item.StockId);

            if (itemInCart == null)
            {
                cartWishlistManager.Storage.Cart.Add(item);
                if (cartWishlistManager.Commit())
                {
                    response.Message = "Товар добавлен в корзину";
                    response.Success = true;
                }
                else
                {
                    response.Message = "Ошибка добавления товара в корзину";
                }
            }
            else
            {
                response.Message = "Товар с данными характеристиками поставки уже есть в вашей корзине";
            }

            response.Data = cartWishlistManager.Storage.Cart;

            return Json(response);

        }

        #endregion
    }
}