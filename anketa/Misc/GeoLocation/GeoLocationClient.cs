﻿using MaxMind.GeoIP2;
using MaxMind.GeoIP2.Model;
using MaxMind.GeoIP2.Responses;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;

namespace anketa.Misc.GeoLocation
{
    /// <summary>
    /// Обработка страны клиента
    /// </summary>
    public class GeoLocationClient
    {
        private DatabaseReader database;

        private DatabaseReader databaseCity;

        private List<GeoLocationCountry> countryList;

        private string[] defaultCountries;

        private string webRootPath;

        public GeoLocationClient(IWebHostEnvironment env)
        {
            webRootPath = env.WebRootPath;

            // Инициализируем базу данных
            try
            {
                database = new DatabaseReader(GetFilePath("GeoLite2-Country.mmdb"));
                databaseCity = new DatabaseReader(GetFilePath("GeoLite2-City.mmdb"));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            // Считываем дополнительные данные по странам
            var countriesData = File.ReadAllText(GetFilePath("ListCountry.json"));
            countryList = JsonConvert.DeserializeObject<List<GeoLocationCountry>>(countriesData).Distinct().ToList();

            // Указываем страны по умолчанию
            defaultCountries = new string[] { "AZ", "AM", "BY", "KZ", "KG", "MD", "TJ", "UA", "UZ", "RU" };
        }

        /// <summary>
        /// Возвращает полное имя файла в каталоге 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private string GetFilePath(string filename)
        {
            return Path.Combine(webRootPath, "geolocation", filename);
        }

        /// <summary>
        /// Возвращает отфильтрованный список данных стран
        /// </summary>
        /// <param name="filterCountry"></param>
        /// <param name="defaultFilter"></param>
        /// <returns></returns>
        public List<GeoLocationCountry> GetFilterListCountry(string[] filterCountry = null, bool defaultFilter = false)
        {
            if (defaultFilter) filterCountry = defaultCountries;
            else if (!defaultFilter && filterCountry == null) filterCountry = new string[0];
            return countryList.Where(c => c.IsoCode != null && filterCountry.Contains(c.IsoCode)).OrderBy(c => c.Country).ToList();
        }

        /// <summary>
        /// Возвращает страну по IP-адресе клиента
        /// </summary>
        /// <param name="ipUser"></param>
        /// <returns></returns>
        public GeoLocationCountry GetCountry(string ipUser)
        {
            var isoCode = "RU";
            if (database != null && database.TryCountry(ipUser, out CountryResponse response)) isoCode = response.Country.IsoCode;
            return countryList.Where(p => p.IsoCode == isoCode).FirstOrDefault();
        }

        /// <summary>
        /// Возвращает Город по IP-адресе клиента
        /// </summary>
        /// <param name="ipUser"></param>
        /// <returns></returns>
        public string GetCity(string ipUser)
        {
          
            if (databaseCity != null && databaseCity.TryCity(ipUser, out CityResponse response)) return response.City.Names["ru"];

            return null;
        }

    }

    /// <summary>
    /// Модель Страны
    /// </summary>
    public class GeoLocationCountry
    {
        /// <summary>
        /// Наименованя страны
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Iso код страны
        /// </summary>
        public string IsoCode { get; set; }

        /// <summary>
        /// Маска номера телефона
        /// </summary>
        public string Mask { get; set; }

        /// <summary>
        /// Код телефона
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Страна пользователя
        /// </summary>
        public bool Active { get; set; }
    }

}
