﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddUploadedFlagToCreditApplication : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CreditApplications_Users_UserId",
                table: "CreditApplications");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "CreditApplications",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<bool>(
                name: "Uploaded",
                table: "CreditApplications",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddForeignKey(
                name: "FK_CreditApplications_Users_UserId",
                table: "CreditApplications",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CreditApplications_Users_UserId",
                table: "CreditApplications");

            migrationBuilder.DropColumn(
                name: "Uploaded",
                table: "CreditApplications");

            migrationBuilder.AlterColumn<int>(
                name: "UserId",
                table: "CreditApplications",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_CreditApplications_Users_UserId",
                table: "CreditApplications",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
