﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace anketa.Misc.Yandex
{
    public class FeatureMember
    {
        public GeoObject GeoObject { get; set; }
    }

    public class GeoObject
    {

        [JsonProperty("metaDataProperty")]
        public MetaDataPropertyGetObject MetaDataProperty { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("boundedBy")]
        public Envelope BoundedBy { get; set; }

        public Point Point { get; set; }

    }

    public class Envelope
    {
        [JsonProperty("lowerCorner")]
        public string LowerCorner { get; set; }

        [JsonProperty("upperCorner")]
        public string UpperCorner { get; set; }

    }

    public class Point
    {
        [JsonProperty("pos")]
        public string Pos { get; set; }

        public Pos Get()
        {
            string[] spearator = { " " };
            Int32 count = 2;
            string[] pos = Pos.Split(spearator, count, StringSplitOptions.RemoveEmptyEntries);
            string spearator1 = pos[0];
        
            return new Pos
            {
                Lon = Convert.ToDouble(pos[0].Replace(".", ",")),
                Lat = Convert.ToDouble(pos[1].Replace(".", ",")),
            };

        }
    }

    public class Pos
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
    }

    public class MetaDataPropertyGetObject
    {
        public GeocoderMetaData GeocoderMetaData { get; set; }
    }

    public class GeocoderMetaData
    {
        [JsonProperty("precision")]
        public string Precision { get; set; }

        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("kind")]
        public string Kind { get; set; }

        public AddressYandex Address { get; set; }

        public AddressDetailsYandex AddressDetails { get; set; }

    }

    public class AddressYandex
    {

        [JsonProperty("country_code")]
        public string CountryCode { get; set; }

        [JsonProperty("formatted")]
        public string Formatted { get; set; }



        public List<AddressYandexComponents> Components { get; set; }


    }

    public class AddressYandexComponents
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("kind")]
        public string Kind { get; set; }

    }

    public class AddressDetailsYandex
    {
        public CountryYandex Country { get; set; }
    }

    public class CountryYandex
    {

        public string AddressLine { get; set; }

        public string CountryNameCode { get; set; }

        public string CountryName { get; set; }

        public AdministrativeAreaYandex AdministrativeArea { get; set; }

    }

    public class AdministrativeAreaYandex
    {
        public string AdministrativeAreaName { get; set; }

        public SubAdministrativeAreaYandex SubAdministrativeArea { get; set; }

    }

    public class SubAdministrativeAreaYandex
    {
        public string SubAdministrativeAreaName { get; set; }

        public LocalityYandex Locality { get; set; }

    }

    public class LocalityYandex
    {
        public string LocalityName { get; set; }
    }
}
