﻿using anketa.Misc;
using anketa.Misc.Generators;
using anketa.Models;
using anketa.Models.Application.Forms;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Controllers
{
    /// <summary>
    /// Класс для администрирования
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly DatabaseContext DbContext;

        public AdminController(DatabaseContext dbContext)
        {
            DbContext = dbContext;
        }

        /// <summary>
        /// Показывает страницу входа
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated) return RedirectToAction("Index", "Home");
            return View();
        }

        /// <summary>
        /// Пытается авторизовать пользователя по логин\паролю
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(LoginForm model)
        {
            if (ModelState.IsValid)
            {
                var passwordHash = DataSecurity.ComputeHash(model.Password);
                var serviceUser = (from user in DbContext.ServiceUsers
                                   join permission in DbContext.ServiceUserPermissions on user.ID equals permission.UserId
                                   where user.User == model.Email && user.Password == passwordHash && permission.Permission == "Admin"
                                   select user).FirstOrDefault();

                if (serviceUser == null) ModelState.AddModelError("", "Неправильный логин или пароль.");
                else
                {
                    string[] role = new string[] { "Admin" };
                    await AuthorizeManager.Authenticate(HttpContext, serviceUser.User, role, model.RememberMe);
                    return RedirectToAction("Index");
                }
            }
            return View(model);
        }

        /// <summary>
        /// Основная страница
        /// </summary>
        /// <returns></returns>
        public IActionResult Index(string search, int page = 1)
        {
            const int perPage = 10;
            var orders = DbContext.Orders
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.EntityFace)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.IndividualFace)
                .OrderByDescending(p => p.OrderDate);
            if (!string.IsNullOrWhiteSpace(search))
            {
                orders = orders
                    .Where(p => p.GetOrderGuid().Contains(search, StringComparison.InvariantCultureIgnoreCase)
                    || p.OrdererFace.EntityFace.FullName.Contains(search, StringComparison.InvariantCultureIgnoreCase)
                    || p.OrdererFace.IndividualFace.FirstName.Contains(search, StringComparison.InvariantCultureIgnoreCase)
                    || p.OrdererFace.IndividualFace.LastName.Contains(search, StringComparison.InvariantCultureIgnoreCase)
                    || p.OrdererFace.IndividualFace.MiddleName.Contains(search, StringComparison.InvariantCultureIgnoreCase))
                    .OrderByDescending(p => p.OrderDate);
            }

            ViewBag.maxPage = (orders.Count() / perPage) + 1;
            ViewBag.previousPage = page - 1 <= 0 ? 1 : page - 1;
            ViewBag.nextPage = page + 1 > ViewBag.maxPage ? ViewBag.maxPage : page + 1;
            ViewBag.currentPage = page;
            ViewBag.search = search;

            orders = orders.Skip((page - 1) * perPage).Take(perPage).OrderByDescending(p => p.OrderDate);
            return View(orders.ToList());
        }

        public IActionResult ViewProfile(int id)
        {
            var order = DbContext.Orders
                .Include(p => p.Status)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.LegalForm)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.ActFromOrgFace)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.ActOnBase)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.Head)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.IndividualFace)
                .Include(p => p.PayerFace)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.LegalForm)
                .Include(p => p.PayerFace)
                    .ThenInclude(p => p.IndividualFace)
                .Include(p => p.ProductReceiver)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.LegalForm)
                .Include(p => p.ProductReceiver)
                    .ThenInclude(p => p.IndividualFace)
                .Include(p => p.DocumentsReceiver)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.LegalForm)
                .Include(p => p.DocumentsReceiver)
                    .ThenInclude(p => p.IndividualFace)
                .Include(p => p.User)
                .FirstOrDefault(p => p.Id == id);

            var items = DbContext.OrderProductItems
                .Include(p => p.Product)
                .Where(p => p.OrderId == id).ToList();

            order.Products = items;

            return PartialView(order);
        }

        /// <summary>
        /// Инициирует генерацию и скачивание анкеты в формате Word
        /// </summary>
        /// <param name="id">ID заказа</param>
        /// <param name="key">Ключ доступа</param>
        /// <returns></returns>
        [AllowAnonymous]
        public IActionResult GetProfile(int id, string key)
        {
            // Проверка ключа
            if (!User.IsInRole("Admin"))
            {
                var k = id.ToString() + "anketa-secret-key";
                if (string.Compare(DataSecurity.ComputeHash(k), key, true) != 0)
                {
                    ModelState.AddModelError("", "Ошибка доступа.");
                    return View();
                }
            }

            var order = DbContext.Orders
                .Include(p => p.Status)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.LegalForm)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.ActFromOrgFace)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.ActOnBase)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.Head)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.BankDetails)
                .Include(p => p.OrdererFace)
                    .ThenInclude(p => p.IndividualFace)
                        .ThenInclude(p => p.BankDetails)
                .Include(p => p.PayerFace)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.LegalForm)
                .Include(p => p.PayerFace)
                    .ThenInclude(p => p.IndividualFace)
                .Include(p => p.ProductReceiver)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.LegalForm)
                .Include(p => p.ProductReceiver)
                    .ThenInclude(p => p.IndividualFace)
                .Include(p => p.DocumentsReceiver)
                    .ThenInclude(p => p.EntityFace)
                        .ThenInclude(p => p.LegalForm)
                .Include(p => p.DocumentsReceiver)
                    .ThenInclude(p => p.IndividualFace)
                .Include(p => p.User)
                .Include(p => p.OrderPaymentMethod)
                .Include(p => p.DeliveryOrder)
                .FirstOrDefault(p => p.Id == id);

            if (order != null && (order.OrdererFace.IsEntity() || (order.OrdererFace.IsIndividual() /*&& order.OrdererFace.IndividualFace.Country != "RU"*/)))
            {
                order.Products = DbContext.OrderProductItems
                    .Include(p => p.Product)
                    .Include(p => p.Project)
                    .Where(p => p.OrderId == id).ToList();
                string filename = null;
                if (!order.OrdererFace.IsIndividual())
                    filename = order.OrdererFace.EntityFace.LegalForm.Name == "ИП" ? "ip.docx" : "org.docx";
                else
                    filename = "fl.docx";

                var ms = new MemoryStream();
                string rootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "documents", filename);
                using (var fs = new FileStream(rootPath, FileMode.Open)) fs.CopyTo(ms);

                using (var filler = new WordTemplateFiller(ms))
                {
                    filler.GetBookmark("FullName").InsertTextAfter(order.OrdererFace.ToString());
                    filler.GetBookmark("ITIN").InsertTextAfter(order.OrdererFace.GetITIN());
                    filler.GetBookmark("PSRN").InsertTextAfter(order.OrdererFace.GetPSRN());

                    var paymentMethod = order.OrderPaymentMethod.Name;
                    if (order.SumForCredit > 0)
                        paymentMethod += " (сумма кредитныхс средств: " + Math.Round(Convert.ToDouble(order.SumForCredit), 2).ToString() + " )";

                    filler.GetBookmark("PaymentMethod").InsertTextAfter(paymentMethod);
                    filler.GetBookmark("AdditionalInfo").InsertTextAfter(order.OrdererInformation);

                    filler.GetBookmark("ProductReceiver").InsertTextAfter(() =>
                    {
                        if (order.ProductReceiver.IsEntity()) return order.ProductReceiver.ToString();
                        else
                            return $"{order.ProductReceiver.IndividualFace.ToString()}\n" +
                                   $"{order.ProductReceiver.IndividualFace.PassportSerial} {order.ProductReceiver.IndividualFace.PassportNumber}\n" +
                                   $"Выдан {order.ProductReceiver.IndividualFace.PassportAuthority} {order.ProductReceiver.IndividualFace.PassportDate.ToShortDateString()}";
                    });

                    //filler.GetBookmark("Email").InsertTextAfter(order.User.Email);
                    //filler.GetBookmark("Phone").InsertTextAfter(order.OrdererFace.GetPhones());                    

                    filler.GetBookmark("Email").InsertTextAfter(order.Email);
                    filler.GetBookmark("Phone").InsertTextAfter(order.Phone);


                    var orderDelivery = "Нет информации";
                    if (order.DeliveryOrder != null)
                    {
                        try
                        {
                            var deliveryInfomation = order.DeliveryOrder;

                            ///Формирования URL для доставки
                            var productDelivery = new List<ProductsDelivery>();
                            foreach (var item in order.Products)
                            {
                                var productDeliveryItem = new ProductsDelivery
                                {
                                    Guid = item.Product.Guid,
                                    Count = item.ProductCount
                                };

                                productDelivery.Add(productDeliveryItem);
                            }

                            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(productDelivery));
                            var siteDelivery = new
                            {
                                deliveryAddress = order.ProductDeliveryAddress,
                                product = System.Convert.ToBase64String(plainTextBytes)
                            };

                            string urlDelivery = Url.ActionLink("Index", "Delivery") + "?" + SiteClient.GetQueryString(siteDelivery);

                            orderDelivery = "";
                            if (deliveryInfomation.NameTransportCompany == "InYourCity")
                                orderDelivery += "Груз находиться в вашем городе";
                            else
                                orderDelivery += "Транспортная компания:" + deliveryInfomation.NameTransportCompany + " ";


                            orderDelivery += "Стоимость:" + Math.Round(Convert.ToDouble(deliveryInfomation.Price), 2).ToString() + " " +
                                  "Откуда:" + (deliveryInfomation.ShippingAddress ?? "г. Чита") +
                                   "Срок:" + deliveryInfomation.Time + " дн. " +
                                    "Адрес доставки:" + order.ProductDeliveryAddress +
                                    "Ссылка:" + urlDelivery;

                        }
                        catch (JsonException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                    }

                    filler.GetBookmark("DeliveryInfomation").InsertTextAfter(orderDelivery);

                    filler.GetBookmark("ProductReceiverAddress").InsertTextAfter(order.GetProductDeliveryAddress());
                    filler.GetBookmark("DocumentsReceiverAddress").InsertTextAfter(order.GetDocumentDeliveryAddress());

                    filler.GetBookmark("DocumentsReceiver").InsertTextAfter(() =>
                    {
                        if (order.DocumentsReceiver.IsEntity()) return order.DocumentsReceiver.ToString();
                        else
                            return $"{order.DocumentsReceiver.IndividualFace.ToString()}\n" +
                                   $"{order.DocumentsReceiver.IndividualFace.PassportSerial} {order.DocumentsReceiver.IndividualFace.PassportNumber}\n" +
                                   $"Выдан {order.DocumentsReceiver.IndividualFace.PassportAuthority} {order.DocumentsReceiver.IndividualFace.PassportDate.ToShortDateString()}";
                    });

                    if (order.OrdererFace.IsEntity() && order.OrdererFace.EntityFace.ActFromOrganization)
                    {
                        filler.GetBookmark("ActOnBaseName").InsertTextAfter(order.OrdererFace.EntityFace.ActFromOrgFace.ToString());
                        filler.GetBookmark("ActOnBase").InsertTextAfter(order.OrdererFace.EntityFace.ActOnBase.Name + " " + order.OrdererFace.EntityFace.ActOnBaseDocumentSerialNumber + " " + order.OrdererFace.EntityFace.ActOnBaseDocumentDate.ToShortDateString());
                        filler.GetBookmark("Head").InsertTextAfter(order.OrdererFace.EntityFace.Head.ToString());
                        if (order.OrdererFace.GetBankDetails() != null)
                            filler.GetBookmark("BankDetails").InsertTextAfter(order.OrdererFace.GetBankDetails().ToString());
                    }

                    // Заполняем товары
                    var table = filler.SearchTable("ProductsTable");
                    if (table != null)
                    {
                        table.DuplicateRow(1, order.Products.Count);

                        int i = 0;
                        foreach (var p in order.Products)
                        {
                            table.SetTextRow(i++, new Dictionary<string, string>()
                            {
                                { "ProductName", p.GetName() + " " +p.Product.Sku },
                                { "ProductDeliveryOption", (p.ByOrder ? "под заказ" : "в наличии " + p.SerialNumber) },
                                { "ProductPriceType", p.PriceTypeName },
                                { "ProductCount", p.ProductCount.ToString() }
                            });
                        }
                    }
                }

                ms.Flush();
                ms.Seek(0, SeekOrigin.Begin);
                return File(ms, "application/octet-stream", "anketa_" + id.ToString() + ".docx");
            }
            /* else if (order != null && order.OrdererFace.IsIndividual())
             {
                 ModelState.AddModelError("", "Заказ оформлен на физическое лицо. Обратитесь в отдел АСУ.");
                 return View();
             }*/

            ModelState.AddModelError("", "Заказ не найден или удален.");
            return View();
        }
    }
}