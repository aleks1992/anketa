﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.DaData
{
    public class DaDataCity
    {

        /// <summary>
        /// Город
        /// </summary>
        [JsonProperty("city")]
        public string City { get; set; }

        /// <summary>
        /// Адрес полностью
        /// </summary>
        [JsonProperty("value")]
        public string Label { get; set; }

        /// <summary>
        /// Стран
        /// </summary>
        [JsonProperty("country")]
        public string Country { get; set; }

        /// <summary>
        /// Код Фиас
        /// </summary>
        [JsonProperty("fias_id")]
        public string Fias { get; set; }

        /// <summary>
        /// Iso Коде страны
        /// </summary>
        [JsonProperty("country_iso_code")]
        public string CodeIsoCountry { get; set; }

        /// <summary>
        /// Кординаты Lat
        /// </summary>
        [JsonProperty("geo_lat")]
        public string Lat { get; set; }

        /// <summary>
        /// Кординаты Lon
        /// </summary>
        [JsonProperty("geo_lon")]
        public string Lon { get; set; }


    };

    public class DaDataResponse
    {

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("unrestricted_value")]
        public string UrestrictedValue { get; set; }

        [JsonProperty("data")]
        public DaDataCity Data { get; set; }
    }
}
