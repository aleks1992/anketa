﻿using anketa.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application
{
    [Serializable]
    public class OrderData
    {
        /// <summary>
        /// Заказ
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// Товара заказа
        /// </summary>
        public OrderProductItem Product { get; set; }

        /// <summary>
        /// Товары заказа
        /// </summary>
        public IEnumerable<OrderProductItem> Products { get; set; }
    }
}
