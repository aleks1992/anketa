﻿(function () {

    function ServiceCommunication() {
        var self = this;

        this.listeners = [];
        this.messagesQueue = [];
        this.connection = null;
        this.token = null;

        var xhr = new XMLHttpRequest();
        xhr.open('get', '/Account/GetHubToken', true);
        xhr.withCredentials = true;
        xhr.send();

        xhr.onreadystatechange = function () {
            if (xhr.readyState !== 4) return;

            var tokenData = JSON.parse(xhr.responseText);
            self.token = tokenData.data;

            self.connection = new signalR.HubConnectionBuilder().withUrl('/hub/messages', {
                accessTokenFactory: function () {
                    return self.token;
                },
            }).build();
            self.connection.start().then(function () {
                self.bindListeners();
                setTimeout(function () {
                    self.sendQueuedMessages();
                }, 1000);
            }).catch(function (err) {
                console.error(err);
            });

        };
    }

    ServiceCommunication.prototype.bindListeners = function () {
        if (this.connection === null) return;
        var len = this.listeners.length;
        for (var i = 0; i < len; i++)
            this.connection.on(this.listeners[i].message, this.listeners[i].callback);

        this.listeners = this.listeners.splice(0, len);
    };

    ServiceCommunication.prototype.on = function (message, callback) {
        this.listeners.push({ message: message, callback: callback });
        this.bindListeners();
    };

    ServiceCommunication.prototype.send = function () {
        if (this.connection === null || this.connection.state !== 'Connected') {           
            this.messagesQueue.push(arguments);
        }
        else {
            this.connection.invoke.apply(this.connection, arguments).catch(function (err) {
                console.error(err);
            });            
        }
    };

    ServiceCommunication.prototype.sendQueuedMessages = function() {
        var len = this.messagesQueue.length;
        for (var i = 0; i < len; i++) this.send.apply(this, this.messagesQueue[i]);
        this.messagesQueue = this.messagesQueue.splice(0, len);
    };

    document.communication = new ServiceCommunication();

})();