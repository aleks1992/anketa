﻿using anketa.Misc;
using anketa.Models.Database;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static System.Net.WebRequestMethods;

namespace anketa.Misc.OAuthAPI
{
    /// <summary>
    /// Аторизация через Vk
    /// </summary>
    public class OAuthProviderVk : OAuthProvider
    {
        /// <summary>
        /// Получения типа запроса метода
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        protected override string GetHttpMethod(OAuthMethod method)
        {
            return Http.Get;
        }

        /// <summary>
        /// Получения Uri
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public override string GetUri(OAuthMethod method)
        {
            if (method == OAuthMethod.GetCode)
            {
                var dataCode = new RequestVkCode
                {
                    client_id = Config.ClientId,
                    code = "code",
                    scope = "4194304",
                    redirect_uri = RedirectUri,
                    state = State,
                };
                string getParameter = SiteClient.GetQueryString(dataCode);
                return "https://oauth.vk.com/authorize?" + getParameter;
            }
            else if (method == OAuthMethod.GetToken) return "https://oauth.vk.com/access_token";
            else if (method == OAuthMethod.GetUser) return "https://api.vk.com/method/users.get";
            return null;
        }

        /// <summary>
        /// Получения тела запроса
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public override object GetRequestBody(OAuthMethod method, string sig = null)
        {
            return base.GetRequestBody(method);
        }

        /// <summary>
        /// Авторизация 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public override User ProcessLogin(string code, string state = null)
        {
            dynamic userReturn = null;
            Request = new RequestVkToken
            {
                client_id = Config.ClientId,
                code = code,
                client_secret = Config.SecretKey,
                redirect_uri = RedirectUri
            };

            //Получения токена доступа
            var response = SendRequest(OAuthMethod.GetToken);
            dynamic accessToken = null;
            if (response != null)
            {
                accessToken = JsonConvert.DeserializeObject<AccessVKToken>(response.ToString());
            }

            //Отправка запроса на получения User
            if (accessToken != null)
            {
                Request = new RequectVkUser
                {
                    access_token = accessToken.access_token,
                    fields = "uid,first_name,last_name,screen_name,sex,bdate,contacts,country",
                    uids = accessToken.user_id,
                    v = "5.101",
                };
                response = SendRequest(OAuthMethod.GetUser);
                dynamic user = null;
                if (response != null)
                {
                    //Убираем из тела ответа json массив
                    string user_string = response.ToString();
                    Regex regex = new Regex("{\"response\":\\[");
                    user_string = regex.Replace(user_string, "");
                    regex = new Regex("\\]}");
                    user_string = regex.Replace(user_string, "");
                    user = JsonConvert.DeserializeObject<UserVk>(user_string);
                }
                if (user != null)
                {
                    userReturn = new User
                    {
                        Email = accessToken.email,
                        FirstName = user.first_name,
                        LastName = user.last_name,
                        Activated = true,
                        Sex = (user.sex == 2) ? true : false,
                        Phone = (user.contacts != null) ? user.contacts.mobile_phone : ""
                    };
                }
            }

            return userReturn;
        }

        /// <summary>
        /// Данные для получения токена доступа  VK
        /// </summary>
        class RequestVkToken
        {
            /// <summary>
            /// Индификатор приложения
            /// </summary>
            public string client_id { get; set; }

            /// <summary>
            /// Закрытый ключ
            /// </summary>
            public string client_secret { get; set; }

            /// <summary>
            /// Код авторизации
            /// </summary>
            public string code { get; set; }

            /// <summary>
            /// Uri куда возврашать ответ
            /// </summary>
            public string redirect_uri { get; set; }
        }

        /// <summary>
        /// Получения кода аунтификации  VK
        /// </summary>
        class RequestVkCode
        {
            /// <summary>
            /// Индфикатор приложения
            /// </summary>
            public string client_id { get; set; }

            /// <summary>
            /// Тип ответа
            /// </summary>
            public string code { get; set; }

            /// <summary>
            /// Uri куда возврашать ответ
            /// </summary>
            public string redirect_uri { get; set; }

            /// <summary>
            /// Тип запрсоа
            /// </summary>
            public string grant_type { get; set; }

            /// <summary>
            /// Права доступа
            /// </summary>
            public string scope { get; set; }

            /// <summary>
            /// Состояние сессии
            /// </summary>
            public string state { get; set; }
        }

        /// <summary>
        /// Токен доступа  VK
        /// </summary>
        class AccessVKToken
        {
            /// <summary>
            /// Токен доступа
            /// </summary>
            public string access_token { get; set; }

            /// <summary>
            /// Время жизни токнеа
            /// </summary>
            public string expires_in { get; set; }

            /// <summary>
            /// Индификатор User
            /// </summary>
            public string user_id { get; set; }

            /// <summary>
            /// Email User
            /// </summary>
            public string email { get; set; }

        }


        /// <summary>
        /// Получения ифнормации о пользователи VK
        /// </summary>
        class RequectVkUser
        {
            /// <summary>
            /// Индификатор пользователя
            /// </summary>
            public string uids { get; set; }

            /// <summary>
            /// Поля ответа через ;
            /// </summary>
            public string fields { get; set; }

            /// <summary>
            /// Токен доступа
            /// </summary>
            public string access_token { get; set; }

            /// <summary>
            /// Версия API
            /// </summary>
            public string v { get; set; }

        }
        /// <summary>
        /// Set User Vk
        /// </summary>
        class UserVk
        {
            /// <summary>
            /// Индификатор User
            /// </summary>
            public string uid { get; set; }

            /// <summary>
            /// Имя
            /// </summary>
            public string first_name { get; set; }

            /// <summary>
            /// Фамилия
            /// </summary>
            public string last_name { get; set; }

            /// <summary>
            /// Пол
            /// </summary>
            public int sex { get; set; }

            /// <summary>
            /// Дата Рождения
            /// </summary>
            public string bdate { get; set; }

            /// <summary>
            /// Номера
            /// </summary>
            public ContactsVk contacts { get; set; }

            /// <summary>
            /// Страна
            /// </summary>
            public object country { get; set; }

        }

        /// <summary>
        /// Контактная информация VK
        /// </summary>
        class ContactsVk
        {
            /// <summary>
            /// номер мобильного телефона пользователя
            /// </summary>
            public string mobile_phone { get; set; }

            /// <summary>
            /// дополнительный номер телефона пользователя.
            /// </summary>
            public string home_phone { get; set; }
        }

    }
}
