﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace anketa.Misc.Yandex
{

    public class ResponseYnadex
    {

        public GeoObjectCollection GeoObjectCollection { get; set; }

    }

    public class GeoObjectCollection
    {

        [JsonProperty("metaDataProperty")]
        public MetaDataProperty MetaDataProperty { get; set; }


        [JsonProperty("featureMember")]
        public List<FeatureMember> FeatureMember { get; set; }
    }

    public class MetaDataProperty
    {
        public GeocoderResponseMetaData GeocoderResponseMetaData { get; set; }
    }

    public class GeocoderResponseMetaData
    {
        [JsonProperty("request")]
        public string Request { get; set; }

        [JsonProperty("results")]
        public double Results { get; set; }

        [JsonProperty("found")]
        public double Found { get; set; }

    }
}
