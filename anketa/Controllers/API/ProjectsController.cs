﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Misc;
using anketa.Models;
using anketa.Models.API.Response;
using anketa.Models.Database.Products;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace anketa.Controllers.API
{
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "API-Orders")]
    [AllowAnonymous]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly DatabaseContext DbContext;
   
        public ProjectsController(DatabaseContext dbContext)
        {
            DbContext = dbContext;
        }

        /// <summary>
        /// Получает список проектов и сохраняет их в базе
        /// </summary>
        /// <param name="projects"></param>
        /// <returns></returns>
        public async Task<IActionResult> Post([FromBody]List<ProductProject> projects)        
        {
            DataUploadStatus status = new DataUploadStatus();
            if (projects != null)
            {
                try
                {
                    foreach (var pr in projects)
                    {
                        var project = DbContext.ProductProjects.FirstOrDefault(p => p.Guid == pr.Guid);
                        if (project == null) project = new ProductProject();
                        DataManager.CopyFields(pr, project, except: new string[] { "Id" });
                        if (project.Emails != null) project.Email = ";" + string.Join(";", pr.Emails) + ";";
                        else project.Email = null;

                        if (pr.Phones != null) project.Phone = ";" + string.Join(";", pr.Phones) + ";";
                        else project.Phone = null;

                        if (project.Guid != Guid.Empty)
                        {
                            if (project.Id == 0) await DbContext.ProductProjects.AddAsync(project);
                            else DbContext.Update(project);
                            await DbContext.SaveChangesAsync();
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    status.ErrorMessage = e.Message;
                    status.AddObject(projects);
                }
            }
            else
            {
                status.ErrorMessage = "Проекты не переданы.";
            }

            return Ok(true);
        }
    }
}