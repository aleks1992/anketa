﻿ using anketa.Misc.AttributeMetadata;
using anketa.Misc.Validators;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Описание пользователя в БД
    /// </summary>
    [Serializable]
    public class User
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Email пользователя
        /// </summary>
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Электронная почта введена некорректно.")]
        [Remote("VerifyEmail", "Account", AdditionalFields = nameof(ID))]
        public string Email { get; set; }

        /// <summary>
        /// Хэш пароля пользователя
        /// </summary>
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        /// <summary>
        /// Показывает, что учетная запись активирована
        /// </summary>
        public bool Activated { get; set; }

        /// <summary>
        /// Телефон пользователя
        /// </summary>
        [Display(Name = "Телефон")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Номер телефона введен неправильно.")]
        //[HTMLAttribute("mask", "9 (999) 999-99-99")]
        public string Phone { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        [Display(Name = "Имя")]
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество пользователя
        /// </summary>
        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        /// <summary>
        /// Фамилия пользователя
        /// </summary>
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }

        /// <summary>
        /// Дата рождения
        /// </summary>
        [Display(Name = "Дата рождения")]
        [DataType(DataType.Date)]
        [DateRangeValidator(minDate: "1900-01-01")]
        public DateTime? BirthdayDate { get; set; }

        /// <summary>
        /// Пол
        /// </summary>
        [Display(Name = "Пол")]
        [DefaultValue(true)]
        public bool Sex { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        [DataType(DataType.Date)]
        public DateTime? RegistrationDate { get; set; }

        /// <summary>
        /// Указывает, нужна ли подписка на рекламные материалы
        /// </summary>
        [Display(Name = "Даю согласие на получение рекламных материалов в виде смс, e-mail-рассылок, телефонных звонков")]
        public bool ConsentToNewsletter { get; set; }

        /// <summary>
        /// Обработка персональный данных
        /// </summary>
        [Display(Name = "Согласен на обработку персональных данных.")]
        public bool PersonalDataProcessing { get; set; }

        /// <summary>
        /// Подтвержденные номера телефонов
        /// </summary>
        [NotMapped]
        public List<string> ConfirmedPhones { get; set; }
            
        /// <summary>
        /// Подтвержденные emails
        /// </summary>
        [NotMapped]
        public List<string> ConfirmedEmails { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        public User()
        {
            ConfirmedPhones = new List<string>();
            ConfirmedEmails = new List<string>();
        }

        /// <summary>
        /// Возвращает представление пользователя
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var name = $"{LastName ?? ""} {FirstName ?? ""} {MiddleName ?? ""}";
            var regex = new Regex(@"\s{2,}");
            return regex.Replace(name, " ").Trim();
        }

        /// <summary>
        /// Возвращает имя пользователя
        /// </summary>
        /// <returns></returns>
        public string GetUsername()
        {
            if (!string.IsNullOrEmpty(Email)) return Email;
            else if (!string.IsNullOrEmpty(Phone)) return Phone;
            return null;
        }
    }
}
