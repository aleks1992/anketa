﻿using anketa.Models.Application;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace anketa.Misc
{
    public class SecuredMessageBinder : IModelBinder
    {
        private EncryptionKey EncryptionKey;

        public SecuredMessageBinder(EncryptionKey encryptionKey)
        {
            EncryptionKey = encryptionKey;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bindingContext"></param>
        /// <returns></returns>
        public Task BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null) throw new ArgumentNullException(nameof(bindingContext));

            // Анонимная функция для преобразования данных
            Func<string, ModelBindingResult> f = (body) =>
            {
                try
                {
                    var securedMessage = Newtonsoft.Json.JsonConvert.DeserializeObject<SecuredMessage>(body);
                    if (securedMessage.IsValid())
                    {
                        // Если защищенное сообщение валидно, расшифровываем
                        var type = Type.GetType("anketa.Models.Application." + securedMessage.StructureName, true);

                        var key = DataSecurity.DecryptMessage(securedMessage.Key, EncryptionKey);
                        var message = DataSecurity.DecryptMessage(securedMessage.Message, key);
                        object data = Newtonsoft.Json.JsonConvert.DeserializeObject(message, type);
                        return ModelBindingResult.Success(data);
                    }
                    else
                    {
                        object data = Newtonsoft.Json.JsonConvert.DeserializeObject(body, bindingContext.ModelType);
                        return ModelBindingResult.Success(data);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                return ModelBindingResult.Failed();
            };


            if (bindingContext.BindingSource == BindingSource.Body)
            {
                string rawBody;
                using (var sr = new StreamReader(bindingContext.HttpContext.Request.Body, Encoding.UTF8)) rawBody = sr.ReadToEnd();
                if (!string.IsNullOrWhiteSpace(rawBody)) bindingContext.Result = f(rawBody);
            }
            else if(bindingContext.BindingSource == BindingSource.Query)
            {
                if(bindingContext.HttpContext.Request.Query.TryGetValue(bindingContext.FieldName, out Microsoft.Extensions.Primitives.StringValues values))
                {
                    if (values.Count > 0) bindingContext.Result = f(DataSecurity.DecodeBase64(values[0]));
                }
            }
            return Task.CompletedTask;
        }
    }
}
