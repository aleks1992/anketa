﻿export default {
    data: function () {
        return {
            _aspModel: null,
            _validation: {},
            _bindedElements: [],
        };
    },
    directives: {
        'asp': {
            inserted: function (el, binding, vnode) {
                (function () {
                    if (this._aspModel === null) return;
                    let model = this._aspModel;

                    // asp-for
                    let aspFor = el.getAttribute('asp-for');
                    if (aspFor !== null) {
                        if (el.tagName === 'LABEL') {
                            // Устанавливаем заголовок
                            if (aspFor in model) el.innerText = model[aspFor].Name;

                            // Устанавливаем атрибут for
                            el.for = aspFor;
                        }
                        else if (el.tagName === 'INPUT') {
                            // Устанавливаем id
                            el.id = aspFor;
                        }
                    }

                    // asp-validation-for
                    let aspValidationFor = el.getAttribute('asp-validation-for');
                    if (aspValidationFor !== null) {
                        this.$data._bindedElements.push({
                            type: 'info',
                            for: aspValidationFor,
                            element: el,
                        });
                    }

                }).call(vnode.context);               
            },
        }
    },
    methods: {
        setAspModel(model) {
            this._aspModel = model;
        },
        resetValidation() {
            this._validation = {};
        },
        setValidationError(prop, error) {
            this._validation[prop] = error;
        },
        isValid() {
            let hasErrors = false;

            // Очищаем от ошибок другие элементы
            let infoElements = this.$data._bindedElements.filter(e => e.type === 'info');
            for (let el of infoElements) el.element.innerText = '';

            for (let v in this._validation) {
                let errorContent = this._validation[v];
                if (errorContent.length > 0) {
                    hasErrors = true;
                    let elementData = this.$data._bindedElements.find(e => e.for === v && e.type === 'info');
                    if (elementData !== undefined) elementData.element.innerText = errorContent;
                }                
            }

            return !hasErrors;
        },
    },
};