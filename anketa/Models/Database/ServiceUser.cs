﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    public class ServiceUser
    {
        /// <summary>
        /// Ид пользователя
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string User { get; set; }    

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Тип пользователя
        /// </summary>
        public string Type { get; set; }

    }
}
