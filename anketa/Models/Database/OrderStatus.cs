﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    [Serializable]
    public class OrderStatus
    {
        /// <summary>
        /// Ид статуса
        /// </summary>
        [JsonIgnore]
        public int ID { get; set; }

        /// <summary>
        ///  Статус
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Представление статуса
        /// </summary>
        public string View { get; set; }

        /// <summary>
        ///  Группирующий статус для фильтра
        /// </summary>
        public string FilterStatus { get; set; }
    }
}
