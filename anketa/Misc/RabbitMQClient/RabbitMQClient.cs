﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace anketa.Misc
{
    public class RabbitMQClient : IDisposable
    {
        private IConnection conn;

        private readonly IModel channel;

        private static RabbitMQConfiguration RabbitConfiguration;

        public static void SetConfiguration(IConfiguration configuration)
        {
            RabbitConfiguration = configuration.GetSection("RabbitMQConfiguration").Get<RabbitMQConfiguration>();
        }

        public void Dispose()
        {
            channel.Close();
            conn.Close();
        }

        public RabbitMQClient()
        {
            var Factory = new ConnectionFactory
            {
                UserName = RabbitConfiguration.UserName,
                Password = RabbitConfiguration.Password,
                VirtualHost = RabbitConfiguration.VirtualHost,
                HostName = RabbitConfiguration.HostName,
                Port = RabbitConfiguration.Port
            };
            try
            {
                conn = Factory.CreateConnection();
                channel = conn.CreateModel();
            }
            catch (IOException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Добавления в сообщения в очередь
        /// </summary>
        /// <param name="exchange"></param>
        /// <param name="body"></param>
        /// <returns></returns>
        public bool PublishMessage(string exchange, byte[] body)
        {
            var result = false;
            if (IsReady)
            {
                var basicProperties = channel.CreateBasicProperties();
                channel.BasicPublish(exchange, "", null, body);
                result = true;
            }
            return result;
        }

        public bool IsReady
        {
            get
            {
                return conn != null && channel != null;
            }
        }


    }

}
