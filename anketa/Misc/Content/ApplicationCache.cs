﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading.Tasks;

namespace anketa.Misc.Content
{
    public class ApplicationCache
    {
        private readonly IDistributedCache _cache;

        public ApplicationCache(IDistributedCache cache)
        {
            _cache = cache;
        }

        /// <summary>
        /// Получает объект из кэша
        /// </summary>
        /// <typeparam name="T">Тип объекта в кэше</typeparam>
        /// <param name="key">Ключ</param>
        /// <returns></returns>
        public async Task<T> GetAsync<T>(string key)
        {
            var value = await _cache.GetAsync(key);
            if(value != null)
            {
                var bf = new BinaryFormatter();
                using (var ms = new MemoryStream())
                {
                    ms.Write(value, 0, value.Length);
                    ms.Seek(0, SeekOrigin.Begin);

                    object result = bf.Deserialize(ms);
                    if(result is T) return (T)result;
                }
            }

            return default(T);
        }

        /// <summary>
        /// Помещает объект в кэш
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="value">Значение</param>
        /// <returns></returns>
        public async Task SetAsync(string key, object value)
        {
            var bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, value);
                await _cache.SetAsync(key, ms.ToArray());
            }
        }

        /// <summary>
        /// Удаляет значение из кэша
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <returns></returns>
        public async Task RemoveAsync(string key)
        {
            await _cache.RemoveAsync(key);
        }
    }
}
