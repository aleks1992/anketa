﻿// Содержит методы для работы с AntiForgeryToken

export default {
    methods: {
        getRequestVerificationToken() {
            // Получает токен из содержимого страницы
            let elements = document.getElementsByName('__RequestVerificationToken');
            if (elements.length > 0) return elements[0].value;
            console.error('Not found element with name __RequestVerificationToken.');
            return null;
        },
        buildHeadersWithVerificationToken(headers) {
            // Добавляет к переданным заголовкам заголовок верификации запроса
            let token = this.getRequestVerificationToken();
            if (token !== null) headers['RequestVerificationToken'] = token;
            return headers;
        },
    },
};