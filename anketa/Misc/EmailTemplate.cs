﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace anketa.Misc
{
    public class EmailTemplate
    {
        private string MainTemplate;
        private Dictionary<string, string> Data;

        public EmailTemplate(string mainTemplate, string title = "", object data = null)
        {
            MainTemplate = mainTemplate;
            Data = new Dictionary<string, string>();

            FillDefaultData(data);
            Data.Add("TITLE", title);
        }

        /// <summary>
        /// Устанавливает данные по умолчанию
        /// </summary>
        private void FillDefaultData(object data = null)
        {
            Data.Add("YEAR", DateTime.Now.Year.ToString());
            Data.Add("MONTH", DateTime.Now.Month.ToString());
            Data.Add("DAY", DateTime.Now.Day.ToString());

            if(data != null)
            {
                foreach(var prop in data.GetType().GetProperties()) Data.Add(prop.Name.ToUpper(), prop.GetValue(data).ToString());
            }
        }

        /// <summary>
        /// Устанавливает данные
        /// </summary>
        /// <param name="key">Ключ</param>
        /// <param name="value">Значение</param>
        public EmailTemplate SetData(string key, string value)
        {
            if (Data.ContainsKey(key)) Data.Remove(key);
            Data.Add(key, value);
            return this;
        }

        /// <summary>
        /// Устанавливает данные из объекта
        /// </summary>
        /// <param name="data"></param>
        public EmailTemplate SetData(object data)
        {
            foreach(var p in data.GetType().GetProperties()) SetData(p.Name, (string)p.GetValue(data));
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Compile(bool encodeHtml = true)
        {
            string rootPath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "email");
            string contentPath = Path.Combine(rootPath, MainTemplate + ".html");
            if(!File.Exists(contentPath)) throw new FileNotFoundException("File not found.", contentPath);

            var content = File.ReadAllText(contentPath);
            DoChanges(ref content, encodeHtml);

            SetData("CONTENT", content);

            string layoutPath = Path.Combine(rootPath, "Layout.html");
            if (!File.Exists(layoutPath)) throw new FileNotFoundException("File not found.", layoutPath);

            var layout = File.ReadAllText(layoutPath);
            DoChanges(ref layout);

            return layout;            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dat"></param>
        private void DoChanges(ref string dat, bool encodeHtml = false)
        {
            
            foreach (var e in Data) dat = dat.Replace("%" + e.Key.ToUpper() + "%", encodeHtml ? WebUtility.HtmlEncode(e.Value) : e.Value);
        }
    }
}
