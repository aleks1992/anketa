﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddActFromOrg : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserEntityFaces_UserIndividualFaces_ActFromOrgFaceId",
                table: "UserEntityFaces");

            migrationBuilder.AlterColumn<int>(
                name: "ActFromOrgFaceId",
                table: "UserEntityFaces",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_UserEntityFaces_UserIndividualFaces_ActFromOrgFaceId",
                table: "UserEntityFaces",
                column: "ActFromOrgFaceId",
                principalTable: "UserIndividualFaces",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserEntityFaces_UserIndividualFaces_ActFromOrgFaceId",
                table: "UserEntityFaces");

            migrationBuilder.AlterColumn<int>(
                name: "ActFromOrgFaceId",
                table: "UserEntityFaces",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_UserEntityFaces_UserIndividualFaces_ActFromOrgFaceId",
                table: "UserEntityFaces",
                column: "ActFromOrgFaceId",
                principalTable: "UserIndividualFaces",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
