﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Forms
{
    /// <summary>
    /// Модель для авторизации пользователя
    /// </summary>
    public class LoginForm
    {
        /// <summary>
        /// Email пользователя
        /// </summary>
        [Display(Name = "Логин", Description = "Введите ваш email")]
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона пользователя
        /// </summary>
        [Display(Name = "Номер телефона")]
        public string Phone { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        /// <summary>
        /// Указывает, нужно ли запомнить пользователя
        /// </summary>
        [Display(Name = "Запомнить меня")]
        public bool RememberMe { get; set; }
    }

}
