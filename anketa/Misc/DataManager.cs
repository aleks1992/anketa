﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using anketa.Models.API;
using anketa.Models.Database;
using Newtonsoft.Json;
using static anketa.Misc.SiteClient;

namespace anketa.Misc
{
    public class DataManager
    {
        /// <summary>
        /// Копирует поля одного объекта в другой
        /// </summary>
        /// <param name="src">Источник</param>
        /// <param name="dest">Назначение</param>
        public static void CopyFields(object src, object dest, bool nonEmpty = false, string[] except = null)
        {
            if (src == null || dest == null) return;
            foreach (var p1 in src.GetType().GetProperties())
            {
                if (except != null && except.Contains(p1.Name)) continue;
                foreach (var p2 in dest.GetType().GetProperties())
                {
                    if (p1.Name == p2.Name && p1.PropertyType == p2.PropertyType)
                    {
                        if (nonEmpty && IsDefault(p1.PropertyType, p1.GetValue(src))) continue;
                        p2.SetValue(dest, p1.GetValue(src));
                        break;
                    }
                }
            }
        }

        public static bool IsDefault(Type type, object value)
        {
            object def = GetDefault(type);
            if (def is IComparable)
            {
                return ((IComparable)value).CompareTo((IComparable)def) == 0;
            }
            return def == value;
        }

        /// <summary>
        /// Возвращает значение по умолчанию для типа
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static object GetDefault(Type type)
        {
            if (type.IsValueType) return Activator.CreateInstance(type);
            return null;
        }

        public static void UpdateFields(object src, object dest)
        {
            CopyFields(src, dest, true);
        }

        /// <summary>
        /// Создает объект и копирует в его поля данные из другого объекта
        /// </summary>
        /// <typeparam name="T">Тип объекта</typeparam>
        /// <param name="src">Источник</param>
        /// <returns></returns>
        public static T CopyFields<T>(object src)
        {
            object dest = Activator.CreateInstance<T>();
            CopyFields(src, dest, true);
            return (T)dest;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="head"></param>
        internal static void Disnull(object obj)
        {
            foreach (var prop in obj.GetType().GetProperties())
            {
                if (prop.CustomAttributes.Count(p => p.AttributeType == typeof(RequiredAttribute)) > 0)
                {
                    if (prop.PropertyType == typeof(string) && (string)prop.GetValue(obj) == null)
                        prop.SetValue(obj, "");
                }
            }
        }

        /// <summary>
        /// Отправка сообщение в Mychat
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static void GetMessageMychat(string message)
        {
            object data = new
            {
                ServerKey = "76QOyURN7el9EjJaL8i0",
                cmd = "0004",
                UserFrom = "0",
                UID = 8,
                APIStype = "jenkins",
                Msg = message
            };


            var url = "https://office.batutmaster.ru:8085/API/?data=" + HttpUtility.UrlEncode(JsonConvert.SerializeObject(data).ToString());
            HttpClient(obj: null, url: url, method: "GET", inAnswerResponse: false);

            //Отправка в Telegram
            HttpClient(url: "https://telegram.bronnikovy.ru/notification?token=hSskJV2KEaLYaLn8xAqEXhtC", obj: new
            {
                theme = "zabbix",
                text = message
            }, method: "POST", inAnswerResponse: false);


        }

        /// <summary>
        /// Преобразует номер телефона в нужный формат, удаляя все символы
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static string PreparePhoneNumber(string phone, bool removePlus = false)
        {
            if (!string.IsNullOrWhiteSpace(phone))
            {
                var hasPlus = phone.StartsWith('+');

                var regex = new Regex(@"[^\d]");
                phone = regex.Replace(phone, "");

                // Телефон пустой
                if (string.IsNullOrEmpty(phone)) return null;

                if (hasPlus && !removePlus) phone = "+" + phone;
                return phone;
            }

            return null;
        }

    }
}
