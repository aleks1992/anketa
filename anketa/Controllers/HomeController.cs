﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using anketa.Models;
using anketa.Models.Application;
using anketa.Misc;
using anketa.Models.Application.Forms;
using Microsoft.AspNetCore.Authorization;
using anketa.Models.Database;
using Microsoft.EntityFrameworkCore;

namespace anketa.Controllers
{
    [Authorize(Roles = "User")]
    public class HomeController : Controller
    {
        private DatabaseContext dbContext;
        private List<SiteData> linkedSites;

        public HomeController(DatabaseContext dbContext, List<SiteData> linkedSites)
        {
            this.dbContext = dbContext;
            this.linkedSites = linkedSites;
        }

        /// <summary>
        /// Выводит список заказок
        /// </summary>
        /// <returns></returns>
        public IActionResult Index([FromServices] anketa.Misc.Content.ClientConfirmedContacts clientConfirmedContacts)
        {
            if (User.IsInRole("UnconfirmedUser"))
                return RedirectToAction("PersonalData", "Cabinet");

            return View();
        }

        /// <summary>
        /// Возвращает код с анимацией загрузки
        /// </summary>
        /// <returns></returns>
        public IActionResult Loader()
        {
            return PartialView("_Loader");
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult LoginLinkedSites()
        {
            List<string> sitesList = new List<string>();
            foreach (var urlItem in linkedSites)
            {
                sitesList.Add(urlItem.URL + "/wp-content/plugins/anketa/ShadowAuth.php");
            }
            return View(sitesList);
        }

        public IActionResult Login([FromBody]LoginForm loginForm)
        {
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Logout()
        {
            await AuthorizeManager.Logout(HttpContext);
            return RedirectToAction("Index");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
