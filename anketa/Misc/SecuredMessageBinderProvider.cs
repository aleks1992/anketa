﻿using System;
using System.Collections.Generic;
using anketa.Models.Application;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ModelBinding.Binders;

namespace anketa.Misc
{
    public class SecuredMessageBinderProvider : IModelBinderProvider
    {
        /// <summary>
        /// Реализует собой связывальщик данных и объектов
        /// </summary>
        /// <param name="context"></param>
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context == null) return null;

            var bindingTypes = new List<Type>()
            {
                typeof(TokenVerificationRequest),
                typeof(AnketaFormRequest),
                typeof(GuestToken)
            };

            if(bindingTypes.Contains(context.Metadata.ModelType)) 
            {
                return new BinderTypeModelBinder(typeof(SecuredMessageBinder));
            }
            return null;
        }
    }
}
