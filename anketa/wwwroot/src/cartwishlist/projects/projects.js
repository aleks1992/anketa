﻿import Vue from 'vue/dist/vue.esm.js'

import Projects from './projects.vue'

new Vue({
    el: '#app',
    render: h => h(Projects),
});