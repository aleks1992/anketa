﻿﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddDateRegistrationUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "RegistrationDate",
                table: "Users",
                nullable: true);
            
            // Устанавливаем пользователям, у которых не заполнена дата регистрации, дату регистрации по-умолчанию
            migrationBuilder.UpdateData("Users", "RegistrationDate", null, "RegistrationDate", "2019-01-01");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RegistrationDate",
                table: "Users");
        }
    }
}
