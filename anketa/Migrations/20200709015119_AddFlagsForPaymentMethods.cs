﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace anketa.Migrations
{
    public partial class AddFlagsForPaymentMethods : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ForEntityFace",
                table: "OrderPaymentMethods",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "ForIndividualFace",
                table: "OrderPaymentMethods",
                nullable: false,
                defaultValue: false);

            // Устанавливаем флаги предыдущим способам оплаты
            migrationBuilder.UpdateData("OrderPaymentMethods", "Id", 1, new string[] { "ForIndividualFace", "ForEntityFace" },
                new object[]
                {
                    true, true
                });

            migrationBuilder.UpdateData("OrderPaymentMethods", "Id", 2, "ForEntityFace", true);
            migrationBuilder.UpdateData("OrderPaymentMethods", "Id", 3, "ForEntityFace", true);
            migrationBuilder.UpdateData("OrderPaymentMethods", "Id", 4, "ForEntityFace", true);

            // Добавляем новые способы оплаты для физ. лиц
            migrationBuilder.InsertData("OrderPaymentMethods", new string[] { "Id", "Guid", "Name", "ForIndividualFace" },
                new object[,]
                {
                    { 5, Guid.NewGuid(), "Банковской картой (онлайн)", true },
                    { 6, Guid.NewGuid(), "Наличными при получении (коммиссия +3%)", true },
                    { 7, Guid.NewGuid(), "Покупка в кредит", true },
                    { 8, Guid.NewGuid(), "Оплата по счёту", true },
                    { 9, Guid.NewGuid(), "Оплатить через Сбербанк.Онлайн", true },
                    { 10, Guid.NewGuid(), "Оплатить потом", true }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ForEntityFace",
                table: "OrderPaymentMethods");

            migrationBuilder.DropColumn(
                name: "ForIndividualFace",
                table: "OrderPaymentMethods");
        }
    }
}
