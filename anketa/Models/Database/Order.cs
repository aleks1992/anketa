﻿using anketa.Misc.Validators;
using anketa.Models.API;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    [Serializable]
    public class Order
    {
        /// <summary>
        /// ID заказа
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        [JsonIgnore]
        public User User { get; set; }

        /// <summary>
        /// Данные заказа
        /// </summary>
        [Display(Name = "Дополнительная информация о заказе")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Заполните информацию о заказе.")]
        public string OrderData { get; set; }

        /// <summary>
        /// Дата создания заказа
        /// </summary>
        public DateTime OrderDate { get; set; }

        /// <summary>
        /// Дата последнего изменения заказа
        /// </summary>
        public DateTime ModifyDate { get; set; }

        /// <summary>
        /// Статус заказа
        /// </summary>
        public OrderStatus Status { get; set; }

        /// <summary>
        /// Комментарий статуса
        /// </summary>
        public string StatusComment { get; set; }

        /// <summary>
        /// Ид статуса заказа
        /// </summary>
        [JsonIgnore]
        public int StatusId { get; set; }

        #region Заказчик

        /// <summary>
        /// Ид заказчика
        /// </summary>
        [Display(Name = "Покупатель")]
        [Required(ErrorMessage = "Укажите покупателя.")]
        [JsonIgnore]
        public int OrdererFaceId { get; set; }

        /// <summary>
        /// Заказчик
        /// </summary>
        public UserFace OrdererFace { get; set; }

        /// <summary>
        /// Информация о заказчике
        /// </summary>
        [Display(Name = "Дополнительная информация")]
        public string OrdererInformation { get; set; }

        #endregion

        #region Плательщик

        /// <summary>
        /// Ид плательщика
        /// </summary>
        [Display(Name = "Плательщик")]
        [Required(ErrorMessage = "Укажите плательщика.")]
        [JsonIgnore]
        public int PayerFaceId { get; set; }

        /// <summary>
        /// Плательщик
        /// </summary>
        public UserFace PayerFace { get; set; }

        /// <summary>
        /// Информация о плательщике
        /// </summary>
        [Display(Name = "Дополнительная информация о плательщике")]
        public string PayerInformation { get; set; }

        #endregion

        #region Получатель

        /// <summary>
        /// Ид получателя товара
        /// </summary>
        [Display(Name = "Получатель товара")]
        [Required(ErrorMessage = "Укажите получателя товара.")]
        [JsonIgnore]
        public int ProductReceiverId { get; set; }

        /// <summary>
        /// Получатель товара
        /// </summary>
        public UserFace ProductReceiver { get; set; }

        /// <summary>
        /// Информация о получателе товара
        /// </summary>
        [Display(Name = "Дополнительная информация о получателе товара")]
        public string ProductReceiverInformation { get; set; }

        /// <summary>
        /// Ид получателя документов
        /// </summary>
        [Display(Name = "Получатель документов")]
        [Required(ErrorMessage = "Укажите получателя документов.")]
        [JsonIgnore]
        public int DocumentsReceiverId { get; set; }

        /// <summary>
        /// Получатель документов
        /// </summary>
        public UserFace DocumentsReceiver { get; set; }

        /// <summary>
        /// Информация о получателе документов
        /// </summary>
        [Display(Name = "Дополнительная информация о получателе документов")]
        public string DocumentsReceiverInformation { get; set; }

        /// <summary>
        /// Показывает, что заказ выгружен
        /// </summary>
        public bool Uploaded { get; set; }

        #endregion

        /// <summary>
        /// Показывает, что получатель документов это заказчик
        /// </summary>
        [Display(Name = "Получатель документов совпадает с заказчиком")]
        [NotMapped]
        [DefaultValue(true)]
        public bool DocumentReceiverIsOrderer { get; set; }

        /// <summary>
        /// Показывает, что получатель товара это заказчик
        /// </summary>
        [Display(Name = "Получатель товара совпадает с заказчиком")]
        [NotMapped]
        [DefaultValue(true)]
        public bool ProductReceiverIsOrderer { get; set; }

        /// <summary>
        /// Показывает, что плательщик это заказчик
        /// </summary>
        [Display(Name = "Плательщик совпадает с заказчиком")]
        [NotMapped]
        [DefaultValue(true)]
        public bool PayerFaceIsOrderer { get; set; }

        /// <summary>
        /// Показывает, можно ли редактировать заказ
        /// </summary>
        /// <returns></returns>
        public bool CanModify()
        {
            return !Uploaded;
        }

        /// <summary>
        /// Возвращает номер заказа
        /// </summary>
        /// <returns></returns>
        public string GetOrderGuid()
        {
            string id = Id.ToString();
            while (id.Length < 6) id = "0" + id;
            return string.Format("A-{0}-{1}", id.Substring(0, 3), id.Substring(3));
        }

        [NotMapped]
        public List<string> ProductInfo { get; set; }

        /// <summary>
        /// Товары заказа
        /// </summary>
        [NotMapped]
        public List<OrderProductItem> Products { get; set; }

        /// <summary>
        /// Обработка персональный данных
        /// </summary>
        [NotMapped]
        public bool PersonalDataProcessing { get; set; }

        /// <summary>
        /// Способ оплаты заказа
        /// </summary>
        public OrderPaymentMethods OrderPaymentMethod { get; set; }

        /// <summary>
        /// Ид способа оплаты
        /// </summary>
        [Display(Name = "Способ оплаты")]
        [Required(ErrorMessage = "Выберите способ оплаты.")]
        [JsonIgnore]
        public int OrderPaymentMethodId { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        [Display(Name = "Страна")]
        [Required(ErrorMessage = "Выберите страну")]
        [DefaultValue("RU")]
        [JsonIgnore]
        [NotMapped]
        public string Country { get; set; }

        /// <summary>
        /// Указывает, нужна ли подписка на рекламные материалы
        /// </summary>
        [Display(Name = "Даю согласие на получение рекламных материалов в виде смс, e-mail-рассылок, телефонных звонков")]
        //[Required(ErrorMessage = "Необходимо дать согласия на рассылку")]
        [DefaultValue(true)]
        [JsonIgnore]
        [NotMapped]
        public bool ConsentToNewsletter { get; set; }

        #region Заказ от физического лица

        /// <summary>
        /// Заказ от физического лица
        /// </summary>
        [DefaultValue(true)]
        public bool OrderByIndividualFace { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        [Display(Name = "Номер телефона")]
        public string Phone { get; set; }

        /// <summary>
        /// Показывает, подтвержден ли номер телефона
        /// </summary>
        public bool PhoneIsConfirmed { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Display(Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        /// Показывает, подтверждена ли электронная почта
        /// </summary>
        public bool EmailIsConfirmed { get; set; }

        /// <summary>
        /// ФИО покупателя
        /// </summary>
        [Display(Name = "ФИО")]
        public string OrdererFullName { get; set; }

        /// <summary>
        /// Адрес доставки товаров
        /// </summary>
        [Display(Name = "Адрес доставки товара (город, улица)")]
        public string ProductDeliveryAddress { get; set; }

        /// <summary>
        /// Дом
        /// </summary>
        [Display(Name = "Дом")]
        public string ProductDeliveryAddressBuildingNumber { get; set; }

        /// <summary>
        /// Квартира, офис
        /// </summary>
        [Display(Name = "Квартира, офис")]
        public string ProductDeliveryAddressFlat { get; set; }

        /// <summary>
        /// Адрес доставки документов
        /// </summary>
        [Display(Name = "Адрес доставки документов (город, улица)")]
        public string DocumentDeliveryAddress { get; set; }

        /// <summary>
        /// Дом
        /// </summary>
        [Display(Name = "Дом")]
        public string DocumentDeliveryAddressBuildingNumber { get; set; }

        /// <summary>
        /// Квартира, офис
        /// </summary>
        [Display(Name = "Квартира, офис")]
        public string DocumentDeliveryAddressFlat { get; set; }

        /// <summary>
        /// Индекс
        /// </summary>
        [Display(Name = "Индекс")]
        public string DocumentDeliveryAddressPostIndex { get; set; }

        #endregion

        /// <summary>
        /// Инормация о доставки
        /// </summary>
        [Display(Name ="Транспортная компания")]
        public string DeliveryInformation { get; set; }

        /// <summary>
        /// Информация о транспортной компаний
        /// </summary>
        [Display(Name = "Транспортная компания")]
        public DeliveryOrder DeliveryOrder { get; set; }

        /// <summary>
        /// Id информации о доставки
        /// </summary>
        [JsonIgnore]
        public int DeliveryOrderId { get; set; }


        /// <summary>
        /// Guid заказа
        /// </summary>
        public string Guid
        {
            get
            {
                return GetOrderGuid();
            }
        }

        /// <summary>
        /// Возвращает полный адрес доставки товара
        /// </summary>
        /// <returns></returns>
        public string GetProductDeliveryAddress()
        {
            return BuildAddress(ProductDeliveryAddress, new Dictionary<string, string>()
            {
                { "дом", ProductDeliveryAddressBuildingNumber },
                { "кв.", ProductDeliveryAddressFlat },
            });            
        }

        public string GetDocumentDeliveryAddress()
        {
            return BuildAddress(DocumentDeliveryAddress, new Dictionary<string, string>()
            {
                { "дом", DocumentDeliveryAddressBuildingNumber },
                { "кв.", DocumentDeliveryAddressFlat },
                { "индекс", DocumentDeliveryAddressPostIndex },
            });            
        }

        private string BuildAddress(string address, Dictionary<string, string> additions)
        {
            foreach(var addition in additions)
            {
                if(!string.IsNullOrEmpty(addition.Value)) address += $", {addition.Key} {addition.Value} ";
            }
            return address;
        }

        /// <summary>
        /// Указывает, нужна ли адресная доставка или нет
        /// </summary>
        [Display(Name = "Довезти товар до адреса (адресная доставка, дороже)")]
        [DefaultValue(false)]
        public bool TargetedDelivery { get; set; }

        /// <summary>
        /// Какую сумму планируете взять в кредит
        /// </summary>
        [Display(Name = "Какую сумму планируете взять в кредит")]
        public int SumForCredit { get; set; }


        /// <summary>
        /// Возвращает ФИО заказчика по компонентам
        /// </summary>
        /// <returns></returns>
        private string[] GetOrdererNameComponents()
        {
            string lastName = null, firstName = null, middleName = null;

            var regexp = new Regex(@"^(?<lastName>\S+)\s{0,}(?<firstName>\S+)\s{0,}(?<middleName>.*?)\s{0,}$");

            var nameComponent = regexp.Match(OrdererFullName);
            if (nameComponent.Groups.TryGetValue("lastName", out Group lastNameGroup)) lastName = lastNameGroup.Value;
            if (nameComponent.Groups.TryGetValue("firstName", out Group firstNameGroup)) firstName = firstNameGroup.Value;
            if (nameComponent.Groups.TryGetValue("middleName", out Group middleNameGroup)) middleName = middleNameGroup.Value;

            return new string[] { lastName, firstName, middleName };
        }

        /// <summary>
        /// Возвращает имя заказчика
        /// </summary>
        /// <returns></returns>
        public string GetOrdererFirstName()
        {
            return GetOrdererNameComponents()[1];
        }

        /// <summary>
        /// Возвращает фамилию заказчика
        /// </summary>
        /// <returns></returns>
        public string GetOrdererLastName()
        {
            return GetOrdererNameComponents()[0];
        }

        /// <summary>
        /// Возвращает отчество заказчика
        /// </summary>
        /// <returns></returns>
        public string GetOrdererMiddleName()
        {
            return GetOrdererNameComponents()[2];
        }
    }
}
