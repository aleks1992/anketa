﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class InitialState : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Bases",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bases", x => x.ID);
                });

            migrationBuilder.InsertData("Bases", "Name", new string[]
            {
                "Свидетельство о регистрации",
                "Устав",
                "Доверенность"
            });

            migrationBuilder.CreateTable(
                name: "LegalForms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LegalForms", x => x.Id);
                });

            migrationBuilder.InsertData("LegalForms", "Name", new string[] {
                "ИП", "ООО"
            });

            migrationBuilder.CreateTable(
                name: "OrderStatuses",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    View = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OrderStatuses", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ServiceUsers",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    User = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Type = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceUsers", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Email = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Activated = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ServiceUserPermissions",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    Permission = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceUserPermissions", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ServiceUserPermissions_ServiceUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "ServiceUsers",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Addresses",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    Country = table.Column<string>(nullable: false),
                    City = table.Column<string>(nullable: false),
                    Street = table.Column<string>(nullable: false),
                    BuildingNumber = table.Column<string>(nullable: false),
                    FlatNumber = table.Column<string>(nullable: false),
                    Index = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Addresses", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Addresses_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AuthorizationTokens",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Token = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    Host = table.Column<string>(nullable: true),
                    Expires = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorizationTokens", x => x.ID);
                    table.ForeignKey(
                        name: "FK_AuthorizationTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserActivationTokens",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Token = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    Expires = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserActivationTokens", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserActivationTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserIndividualFaces",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    FirstName = table.Column<string>(nullable: false),
                    MiddleName = table.Column<string>(nullable: false),
                    LastName = table.Column<string>(nullable: false),
                    PassportSerial = table.Column<string>(nullable: false),
                    PassportNumber = table.Column<string>(nullable: false),
                    PassportDate = table.Column<DateTime>(nullable: false),
                    PassportAuthority = table.Column<string>(nullable: false),
                    ITIN = table.Column<string>(nullable: false),
                    AdditionalInfo = table.Column<string>(nullable: true),
                    Phones = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    Position = table.Column<string>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserIndividualFaces", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserIndividualFaces_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserPasswordRestoreTokens",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Token = table.Column<string>(nullable: true),
                    Expires = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserPasswordRestoreTokens", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserPasswordRestoreTokens_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserEntityFaces",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    LegalFormId = table.Column<int>(nullable: false),
                    FullName = table.Column<string>(nullable: false),
                    ShortName = table.Column<string>(nullable: false),
                    HeadId = table.Column<int>(nullable: false),
                    ActFromOrganization = table.Column<bool>(nullable: false),
                    ActFromOrgFaceId = table.Column<int>(nullable: false),
                    ActOnBaseId = table.Column<int>(nullable: true),
                    ActOnBaseDocumentSerialNumber = table.Column<string>(nullable: false),
                    ActOnBaseDocumentDate = table.Column<DateTime>(nullable: false),
                    ITIN = table.Column<string>(nullable: false),
                    PSRN = table.Column<string>(nullable: false),
                    AdditionalInfo = table.Column<string>(nullable: true),
                    Phones = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserEntityFaces", x => x.ID);
                    table.ForeignKey(
                        name: "FK_UserEntityFaces_UserIndividualFaces_ActFromOrgFaceId",
                        column: x => x.ActFromOrgFaceId,
                        principalTable: "UserIndividualFaces",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserEntityFaces_Bases_ActOnBaseId",
                        column: x => x.ActOnBaseId,
                        principalTable: "Bases",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_UserEntityFaces_UserIndividualFaces_HeadId",
                        column: x => x.HeadId,
                        principalTable: "UserIndividualFaces",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserEntityFaces_LegalForms_LegalFormId",
                        column: x => x.LegalFormId,
                        principalTable: "LegalForms",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserEntityFaces_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Faces",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    EntityFaceId = table.Column<int>(nullable: true),
                    IndividualFaceId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Faces", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Faces_UserEntityFaces_EntityFaceId",
                        column: x => x.EntityFaceId,
                        principalTable: "UserEntityFaces",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Faces_UserIndividualFaces_IndividualFaceId",
                        column: x => x.IndividualFaceId,
                        principalTable: "UserIndividualFaces",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Faces_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    OrderDate = table.Column<DateTime>(nullable: false),
                    StatusId = table.Column<int>(nullable: false),
                    OrderData = table.Column<string>(nullable: true),
                    OrdererFaceId = table.Column<int>(nullable: false),
                    OrdererInformation = table.Column<string>(nullable: true),
                    PayerFaceId = table.Column<int>(nullable: false),
                    PayerInformation = table.Column<string>(nullable: true),
                    ProductReceiverId = table.Column<int>(nullable: false),
                    ProductReceiverAddressId = table.Column<int>(nullable: false),
                    ProductReceiverInformation = table.Column<string>(nullable: true),
                    DocumentsReceiverId = table.Column<int>(nullable: false),
                    DocumentsReceiverAddressId = table.Column<int>(nullable: false),
                    DocumentsReceiverInformation = table.Column<string>(nullable: true),
                    Uploaded = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Orders_Addresses_DocumentsReceiverAddressId",
                        column: x => x.DocumentsReceiverAddressId,
                        principalTable: "Addresses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Faces_DocumentsReceiverId",
                        column: x => x.DocumentsReceiverId,
                        principalTable: "Faces",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Faces_OrdererFaceId",
                        column: x => x.OrdererFaceId,
                        principalTable: "Faces",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Faces_PayerFaceId",
                        column: x => x.PayerFaceId,
                        principalTable: "Faces",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Addresses_ProductReceiverAddressId",
                        column: x => x.ProductReceiverAddressId,
                        principalTable: "Addresses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Faces_ProductReceiverId",
                        column: x => x.ProductReceiverId,
                        principalTable: "Faces",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_OrderStatuses_StatusId",
                        column: x => x.StatusId,
                        principalTable: "OrderStatuses",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Orders_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Addresses_UserId",
                table: "Addresses",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorizationTokens_UserId",
                table: "AuthorizationTokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Faces_EntityFaceId",
                table: "Faces",
                column: "EntityFaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Faces_IndividualFaceId",
                table: "Faces",
                column: "IndividualFaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Faces_UserId",
                table: "Faces",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DocumentsReceiverAddressId",
                table: "Orders",
                column: "DocumentsReceiverAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_DocumentsReceiverId",
                table: "Orders",
                column: "DocumentsReceiverId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_OrdererFaceId",
                table: "Orders",
                column: "OrdererFaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_PayerFaceId",
                table: "Orders",
                column: "PayerFaceId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ProductReceiverAddressId",
                table: "Orders",
                column: "ProductReceiverAddressId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ProductReceiverId",
                table: "Orders",
                column: "ProductReceiverId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_StatusId",
                table: "Orders",
                column: "StatusId");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_UserId",
                table: "Orders",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceUserPermissions_UserId",
                table: "ServiceUserPermissions",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserActivationTokens_UserId",
                table: "UserActivationTokens",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserEntityFaces_ActFromOrgFaceId",
                table: "UserEntityFaces",
                column: "ActFromOrgFaceId");

            migrationBuilder.CreateIndex(
                name: "IX_UserEntityFaces_ActOnBaseId",
                table: "UserEntityFaces",
                column: "ActOnBaseId");

            migrationBuilder.CreateIndex(
                name: "IX_UserEntityFaces_HeadId",
                table: "UserEntityFaces",
                column: "HeadId");

            migrationBuilder.CreateIndex(
                name: "IX_UserEntityFaces_LegalFormId",
                table: "UserEntityFaces",
                column: "LegalFormId");

            migrationBuilder.CreateIndex(
                name: "IX_UserEntityFaces_UserId",
                table: "UserEntityFaces",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserIndividualFaces_UserId",
                table: "UserIndividualFaces",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserPasswordRestoreTokens_UserId",
                table: "UserPasswordRestoreTokens",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuthorizationTokens");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "ServiceUserPermissions");

            migrationBuilder.DropTable(
                name: "UserActivationTokens");

            migrationBuilder.DropTable(
                name: "UserPasswordRestoreTokens");

            migrationBuilder.DropTable(
                name: "Addresses");

            migrationBuilder.DropTable(
                name: "Faces");

            migrationBuilder.DropTable(
                name: "OrderStatuses");

            migrationBuilder.DropTable(
                name: "ServiceUsers");

            migrationBuilder.DropTable(
                name: "UserEntityFaces");

            migrationBuilder.DropTable(
                name: "UserIndividualFaces");

            migrationBuilder.DropTable(
                name: "Bases");

            migrationBuilder.DropTable(
                name: "LegalForms");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
