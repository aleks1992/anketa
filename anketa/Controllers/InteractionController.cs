﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Models.Application;
using anketa.Models.Application.Forms;
using anketa.Models.Application.Response;
using Microsoft.AspNetCore.Mvc;

namespace anketa.Controllers
{
    public class InteractionController : Controller
    {
        /// <summary>
        /// Возвращает форму
        /// </summary>
        /// <param name="formRequest"></param>
        /// <returns></returns>
        public IActionResult GetForm()
        {
            /*string name = "anketa.Models.Application.Forms." + formRequest.FormName;
            Type type = Type.GetType(name);

            List<object> fields = new List<object>();
            var props = type.GetProperties();
            foreach(var p in type.GetProperties())
            {
                Dictionary<string, object> attributes = new Dictionary<string, object>();
                foreach(var a in p.CustomAttributes)
                {
                    AddAttribute(a, ref attributes);
                }

                var obj = new 
                {
                    name = p.Name,
                    attributes = attributes
                };
                fields.Add(obj);
            }

            // Опреляем обработчика
            Dictionary<string, string> receivers = new Dictionary<string, string>()
            {
                { "LoginForm", Url.Action("Login", "Account", null, Request.Scheme) }
            };

            string receiver = receivers.FirstOrDefault(p => p.Key == formRequest.FormName).Value ?? "";

            var response = new AnketaFormResponse()
            {
                FormName = formRequest.FormName,
                Fields = fields,
                Receiver = receiver
            };

            return Json(response);*/
            return Json(null);
        }

        private void AddAttribute(object attribute, ref Dictionary<string, object> dict)
        {
            var a = (System.Reflection.CustomAttributeData)attribute;
            foreach(var na in a.NamedArguments)
            {
                dict.Add(na.MemberName, na.TypedValue.Value);
            }
        }
    }
}