﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Товар в кредитной заявке
    /// </summary>
    public class CreditApplicationProduct
    {
        /// <summary>
        /// ID товара в кредитной заявке
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// ID кредитной заявки
        /// </summary>
        public int CreditApplicationId { get; set; }

        /// <summary>
        /// Кредитная заявка
        /// </summary>
        public CreditApplication CreditApplication { get; set; }

        /// <summary>
        /// Наименование товара
        /// </summary>
        public string Name { get; set; }
    }
}
