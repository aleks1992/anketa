﻿
import Vue from 'vue/dist/vue.esm.js';

import OrderData from './order-data.vue';

Vue.component('order-data', OrderData);

new Vue({
    el: '#app',
    render: h => h(OrderData)
});