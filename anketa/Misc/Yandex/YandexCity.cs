﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.Yandex
{
    public class YandexCity
    {
        /// <summary>
        /// Город
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Адрес полностью
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Стран
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Код Фиас
        /// </summary>
        public string Fias { get; set; }

        /// <summary>
        /// Iso Коде страны
        /// </summary>
        public string CodeIsoCountry { get; set; }

        /// <summary>
        /// Кординаты Lat
        /// </summary>
        public double Lat { get; set; }

        /// <summary>
        /// Кординаты Lon
        /// </summary>
        public double Lon { get; set; }
    }
}
