﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models
{
    public class ErrorModel
    {
        public int StatusCode { get; set; }

        public string Message { get; set; }

        /// <summary>
        /// Устанавливает код
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private static ErrorModel SetCode(int code)
        {
            return new ErrorModel()
            {
                StatusCode = code
            };
        }

        /// <summary>
        /// Устанавливает ошибку - Bad request
        /// </summary>
        /// <returns></returns>
        public static ErrorModel BadRequest()
        {
            return SetCode(400);
        }

        /// <summary>
        /// Устанавливает ошибку - Access denied
        /// </summary>
        /// <returns></returns>
        public static ErrorModel AccessDenied()
        {
            return SetCode(403);
        }

        /// <summary>
        /// Устанавливает ошибку - Not found
        /// </summary>
        /// <returns></returns>
        public static ErrorModel NotFound()
        {
            return SetCode(404);
        }

        /// <summary>
        /// Создает модель ошибки с собственным описанием
        /// </summary>
        /// <param name="code">Код ответа сервера</param>
        /// <param name="message">Сообщение</param>
        /// <returns></returns>
        public static ErrorModel CustomError(int code, string message)
        {
            return new ErrorModel()
            {
                Message = message,
                StatusCode = code
            };
        }

        /// <summary>
        /// Получает описание кода состояния
        /// </summary>
        /// <returns></returns>
        public string GetDescription()
        {
            if (!string.IsNullOrEmpty(Message)) return Message;

            if (StatusCode == 400) return "Сервер не может выполнить ваш запрос.";
            else if (StatusCode == 403) return "Доступ запрещен.";
            else if (StatusCode == 404) return "Страница не найдена.";
            return "";
        }
    }
}
