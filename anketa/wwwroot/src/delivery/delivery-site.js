﻿import Vue from 'vue/dist/vue.esm.js'

import fetch from 'isomorphic-fetch'
import DelivaryComponet from './delivery.vue'


if (typeof document.application === 'undefined') console.error('BM application is not defined.');
else {
    document.application.getServices(['vue', 'vuex']).then(([Vue, store]) => {
        // Создаем сервис для Доставки
        document.application.registerService('delivery-anketa', DelivaryComponet);
    });
}

