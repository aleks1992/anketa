﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddOrderItemTypePrice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ByOrder",
                table: "OrderProductItems",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "PriceTypeId",
                table: "OrderProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PriceTypeName",
                table: "OrderProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProductUri",
                table: "OrderProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SerialNumber",
                table: "OrderProductItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StockId",
                table: "OrderProductItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ByOrder",
                table: "OrderProductItems");

            migrationBuilder.DropColumn(
                name: "PriceTypeId",
                table: "OrderProductItems");

            migrationBuilder.DropColumn(
                name: "PriceTypeName",
                table: "OrderProductItems");

            migrationBuilder.DropColumn(
                name: "ProductUri",
                table: "OrderProductItems");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "OrderProductItems");

            migrationBuilder.DropColumn(
                name: "StockId",
                table: "OrderProductItems");
        }
    }
}
