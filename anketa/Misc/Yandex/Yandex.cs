﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using static anketa.Misc.SiteClient;

namespace anketa.Misc.Yandex
{
    public class Yandex
    {
        /// <summary>
        /// Url адрес методо
        /// </summary>
        const string URL_YANDEX = "http://geocode-maps.yandex.ru/1.x/?";

        /// <summary>
        /// Ключ доступа
        /// </summary>
        const string KEY = "a037b074-55c2-4df7-b216-0407f20b1ec9";

        /// <summary>
        /// Возращает модель адреса
        /// </summary>
        /// <param name="city"></param>
        /// <param name="countResult"></param>
        /// <returns></returns>
        public static YandexData GetAddress(string city, int countResult = 5)
        {
            var yandex = new YandexData();

            object data = new
            {
                apikey = KEY,
                geocode = city,
                format = "json",
                result = countResult
            };

            string query = GetQueryString(data);

            var response = HttpClient(data, URL_YANDEX + query, "GET", inAnswerResponse: false);

            return JsonConvert.DeserializeObject<YandexData>(response.Data.ToString());

        }
    }
}
