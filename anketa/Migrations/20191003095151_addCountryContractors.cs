﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class addCountryContractors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "UserIndividualFaces",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Country",
                table: "UserEntityFaces",
                nullable: true);

            migrationBuilder.UpdateData("UserIndividualFaces", "Country", null, "Country", "RU");
            migrationBuilder.UpdateData("UserEntityFaces", "Country", null, "Country", "RU");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Country",
                table: "UserIndividualFaces");

            migrationBuilder.DropColumn(
                name: "Country",
                table: "UserEntityFaces");
        }
    }
}
