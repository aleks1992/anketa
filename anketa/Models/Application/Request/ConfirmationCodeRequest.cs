﻿using anketa.Misc;
using anketa.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace anketa.Models.Application.Request
{
    /// <summary>
    /// Класс описывает модель для запроса подтверждения контакта - email или номера телефона
    /// </summary>
    public class ConfirmationCodeRequest
    {
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Код верификации
        /// </summary>
        public string VerificationCode { get; set; }

        /// <summary>
        /// Время генерации кода
        /// </summary>
        public DateTime DateGenerated { get; set; }

        /// <summary>
        /// Время действия кода
        /// </summary>
        public TimeSpan Expires { get; set; }

        /// <summary>
        /// Намерение, для чего происходит верификация контакта
        /// </summary>
        public ConfirmationCodeIntent Intent { get; set; }

        /// <summary>
        /// Список намерений
        /// </summary>
        public enum ConfirmationCodeIntent
        {
            /// <summary>
            /// Только подтверждение
            /// </summary>
            ConfirmOnly = 0,

            /// <summary>
            /// Установить контакт по умолчанию
            /// </summary>
            SetAsDefault = 1,

            /// <summary>
            /// Регистрация
            /// </summary>
            Registration = 2,

            /// <summary>
            /// Авторизация
            /// </summary>
            Login = 3,
        }

        /// <summary>
        /// Возвращает хэш данных для хранилища
        /// </summary>
        public string Hash
        {
            get
            {
                var data = DataSecurity.ComputeHash(IsPhone() ? Phone : Email) + DataSecurity.ComputeHash(Intent.ToString()) + DataSecurity.ComputeHash(VerificationCode);
                return DataSecurity.ComputeHash(data);
            }
        }

        /// <summary>
        /// Возвращает подтвержденный контакт пользователя
        /// </summary>
        /// <returns></returns>
        public UserConfirmedContact GetConfirmedContact()
        {
            var contact = new UserConfirmedContact();

            if(IsPhone())
            {
                contact.Contact = Phone;
                contact.Type = UserConfirmedContact.ContactType.Phone;
            }
            else if(IsEmail())
            {
                contact.Contact = Email;
                contact.Type = UserConfirmedContact.ContactType.Email;
            }

            return contact;
        }

        /// <summary>
        /// Указывает, что запрос кода для номера телефона
        /// </summary>
        /// <returns></returns>
        public bool IsPhone()
        {
            return !string.IsNullOrEmpty(Phone);
        }

        /// <summary>
        /// Указывает, что запрос кода для электронной почты
        /// </summary>
        /// <returns></returns>
        public bool IsEmail()
        {
            return !string.IsNullOrEmpty(Email);
        }

        /// <summary>
        /// Нормализует контактные данные
        /// В номере телефона убирает лишние символы и добавляет в начале +
        /// Электронную почту преобразует к нижнему регистру
        /// </summary>
        public void Normalize()
        {
            if (IsPhone())
            {
                Phone = DataManager.PreparePhoneNumber(Phone);
            }
            else if(IsEmail())
            {
                Email = Email.ToLower().Trim();
            }

            if(VerificationCode != null)
            {
                var regex = new Regex(@"[^\d]");
                VerificationCode = regex.Replace(VerificationCode, "");
            }
        }
    }
}
