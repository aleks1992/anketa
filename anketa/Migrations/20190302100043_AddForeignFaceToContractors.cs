﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddForeignFaceToContractors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsForeignFace",
                table: "UserIndividualFaces",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsForeignFace",
                table: "UserEntityFaces",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsForeignFace",
                table: "UserIndividualFaces");

            migrationBuilder.DropColumn(
                name: "IsForeignFace",
                table: "UserEntityFaces");
        }
    }
}
