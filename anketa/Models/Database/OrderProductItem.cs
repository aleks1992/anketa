﻿using anketa.Misc.Content;
using anketa.Models.Database.Products;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Элемент заказа клиента - товар
    /// </summary>
    [Serializable]
    public class OrderProductItem
    {
        // <summary>
        /// ID элемента
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Заказ
        /// </summary>
        [JsonIgnore]
        public Order Order { get; set; }

        /// <summary>
        /// ID заказа
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// ID товара
        /// </summary>
        [JsonIgnore]
        public int? ProductId { get; set; }

        /// <summary>
        /// Заказанный товар
        /// </summary>
        public Product Product { get; set; }

        /// <summary>
        /// ID проекта
        /// </summary>
        [JsonIgnore]
        public int? ProjectId { get; set; }

        /// <summary>
        /// Проект
        /// </summary>
        public ProductProject Project { get; set; }

        /// <summary>
        /// Количество товара
        /// </summary>
        public int ProductCount { get; set; }

        /// <summary>
        /// Цена товара
        /// </summary>
        public decimal ProductPrice { get; set; }

        /// <summary>
        /// Серийный номер товара
        /// </summary>
        public string SerialNumber { get; set; }

        /// <summary>
        /// Товар под заказ
        /// </summary>
        public bool ByOrder { get; set; }

        /// <summary>
        /// Индентификатор склада
        /// </summary>
        public string StockId { get; set; }

        /// <summary>
        /// ID вида цены
        /// </summary>      
        public string PriceTypeId { get; set; }

        /// <summary>
        /// Наименование вида цены
        /// </summary>
        public string PriceTypeName { get; set; }

        /// <summary>
        /// Ссылка на товар
        /// </summary>
        public string ProductUri { get; set; }

        /// <summary>
        ///  Тип товара
        /// </summary>
        public CartWishlistItem.ItemType Type { get; set; }

        /// <summary>
        /// Создает товар заказа из элемента корзины
        /// </summary>
        /// <param name="items"></param>
        public static List<OrderProductItem> Make(DatabaseContext dbContext, ProductLoader productLoader, List<CartWishlistItem> items)
        {
            var list = new List<OrderProductItem>();
            foreach(var item in items)
            {
                var obj = new OrderProductItem
                {
                    ByOrder = item.ByOrder.Value,
                    ProductPrice = item.Price,
                    PriceTypeId = item.PriceTypeId,
                    PriceTypeName = item.PriceTypeName,                
                    ProductCount = item.Count,
                    SerialNumber = item.SerialNumber,
                    StockId = item.StockId,
                    Type = item.Type,
                };

                if (item.Type == CartWishlistItem.ItemType.Product)
                {                  
                    var product = dbContext.Products.FirstOrDefault(p => p.Guid == item.Guid);
                    if (product == null)
                    {
                        product = productLoader.GetProduct(item.Guid);
                        dbContext.Products.Add(product);
                        dbContext.SaveChanges();
                    }

                    obj.Product = product;
                    obj.ProductUri = product.GetProductLink();
                    list.Add(obj);
                }
                else if(item.Type == CartWishlistItem.ItemType.Project)
                {
                    var project = dbContext.ProductProjects.FirstOrDefault(p => p.Guid.ToString() == item.Guid);
                    obj.Project = project ?? throw new Exception("Project is not found.");
                    list.Add(obj);
                }
            }
            return list;
        }

        /// <summary>
        /// Возвращает наименование позиции заказа
        /// </summary>
        /// <returns></returns>
        public string GetName()
        {
            if (Type == CartWishlistItem.ItemType.Product) return Product.Name;
            else if (Type == CartWishlistItem.ItemType.Project) return Project.Name;
            return null;
        }

        public string GetLink()
        {
            if (Type == CartWishlistItem.ItemType.Product) return Product.GetProductLink();
            return "#";
        }

        public string GetImageLink()
        {
            if (Type == CartWishlistItem.ItemType.Product) return Product.GetProductImageLink();
            return "#";
        }
    }
}
