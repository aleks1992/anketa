﻿using anketa.Misc.Validators;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    [Serializable]
    public class UserIndividualFace
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// ID пользователя, создавшего юридическое лицо
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Пользователь, создавший юридическое лицо
        /// </summary>
        [JsonIgnore]
        public User User { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        [Display(Name = "Имя")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите имя.")]
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        [Display(Name = "Отчество")]
        public string MiddleName { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        [Display(Name = "Фамилия")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите фамилию.")]
        public string LastName { get; set; }

        /// <summary>
        /// Серия паспорта
        /// </summary>
        [Display(Name = "Серия паспорта")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Введите серию паспорта.")]
        public string PassportSerial { get; set; }

        /// <summary>
        /// Номер паспорта
        /// </summary>
        [Display(Name = "Номер паспорта")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Введите номер паспорта.")]
        public string PassportNumber { get; set; }

        /// <summary>
        /// Дата выдачи паспорта
        /// </summary>
        [Display(Name = "Дата выдачи паспорта")]
        [DataType(DataType.Date)]
        //[Required(ErrorMessage = "Введите дату выдачи паспорта.")]
        [DateRangeValidator(minDate: "1950-01-01")]
        public DateTime PassportDate { get; set; }

        /// <summary>
        /// Кем выдан паспорт
        /// </summary>
        [Display(Name = "Кем выдан паспорт")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Введите, кем был выдан паспорт.")]
        public string PassportAuthority { get; set; }

        /// <summary>
        /// ИНН
        /// </summary>
        [Display(Name = "ИНН")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Введите ИНН.")]
        //[ITINValidator(12, ErrorMessage = "ИНН введен некорректно.")]
        public string ITIN { get; set; }

        /// <summary>
        /// Дополнительная информация
        /// </summary>
        [Display(Name = "Дополнительная информация")]
        [DataType(DataType.MultilineText)]
        public string AdditionalInfo { get; set; }

        /// <summary>
        /// Контактный номер
        /// </summary>
        [Display(Name = "Контактные номера")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Введите хотя бы один контактный номер")]
        public string Phones { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Display(Name = "Email")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Введите электронную почту.")]
        public string Email { get; set; }

        /// <summary>
        /// Должность
        /// </summary>
        [Display(Name = "Должность")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Введите должность.")]
        public string Position { get; set; }

        /// <summary>
        /// Показывает, является ли контрагент иностранным лицом
        /// </summary>
        [Display(Name = "Иностранное лицо")]
        public bool IsForeigner { get; set; }

        /// <summary>
        /// Банковские реквизиты
        /// </summary>
        public BankDetails BankDetails { get; set; }

        /// <summary>
        /// Ид банковских реквизитов
        /// </summary>
        [JsonIgnore]
        public int? BankDetailsId { get; set; }

        /// <summary>
        /// Страна контрагента
        /// </summary>
        [Display(Name = "Выберите страну")]
        [DefaultValue("RU")]
        public string Country { get; set; }

        /// <summary>
        /// Это основной контрагент
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Устанавливает пустую строку non-nullable атрибутам
        /// </summary>
        internal void Disnull()
        {
            foreach (var prop in this.GetType().GetProperties())
            {
                if (prop.CustomAttributes.Count(p => p.AttributeType == typeof(RequiredAttribute)) > 0)
                {
                    if (prop.PropertyType == typeof(string) && (string)prop.GetValue(this) == null)
                        prop.SetValue(this, "");
                }
            }
        }

        /// <summary>
        /// Возвращает имя лица
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("{0} {1} {2}", LastName, FirstName, MiddleName);
        }
    }
}
