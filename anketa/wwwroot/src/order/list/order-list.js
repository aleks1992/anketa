﻿
import Vue from 'vue/dist/vue.esm.js'
import OrderList from './order-list.vue'

Vue.component('order-list', OrderList);

new Vue({
    el: '#app',
    render: h => h(OrderList)
})