﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Представляет собой представление организационно-правовой формы
    /// </summary>
    [Serializable]
    public class LegalForm
    {
        /// <summary>
        /// Ид ОПФ
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Наименование ОПФ
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Указывает, что директор должен быть
        /// </summary>
        public bool HeadMustExists { get; set; }
    }
}
