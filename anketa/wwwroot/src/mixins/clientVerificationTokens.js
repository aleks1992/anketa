﻿// Миксин для вспомогательных методов

import AntiForgeryTokenMixin from './antiForgeryToken'
import RecaptchaMixin from './recaptcha'

export default {
    mixins: [
        AntiForgeryTokenMixin,
        RecaptchaMixin,
    ],
    methods: {
        getRequestHeaders(contentType) {
            let headers = {};

            if (contentType === undefined) contentType = "application/json";
            if (contentType !== null) headers['Content-Type'] = contentType;

            let antiforgeryToken = this.getRequestVerificationToken();
            if (antiforgeryToken !== null) headers['RequestVerificationToken'] = antiforgeryToken;

            let recaptchaToken = this.getRecaptchaToken();
            if (recaptchaToken !== null) headers['Captcha'] = recaptchaToken;

            return headers;
        }
    },
};