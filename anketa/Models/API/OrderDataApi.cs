﻿using anketa.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.API
{
    // TODO: удалить
    public class OrderDataApi
    {
        /// <summary>
        /// Ид заказа
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Выгружен
        /// </summary>
        public bool Uploaded { get; set; }

        /// <summary>
        /// Статус
        /// </summary>
        public OrderStatus Status { get; set; }

        /// <summary>
        /// Комментарий статуса
        /// </summary>
        public string StatusComment { get; set; }
    }
}
