﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace anketa.Models.API
{
    public class DeliveryResultItem
    {

        /// <summary>
        /// Населенный пукнт 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Тип доставка
        /// </summary>
        public string TypeDelivery { get; set; }

        /// <summary>
        ///Дистаниция до Города
        /// </summary>
        public decimal Distance { get; set; }

        /// <summary>
        /// Список доступных доставок
        /// </summary>
        [JsonProperty("result")]
        public List<DeliveryTransportCompany> Result { get; set; }
    }
}
