﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddPriceTypeName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserCartItems_PriceTypes_PriceTypeId",
                table: "UserCartItems");

            migrationBuilder.DropIndex(
                name: "IX_UserCartItems_PriceTypeId",
                table: "UserCartItems");

            migrationBuilder.AlterColumn<string>(
                name: "PriceTypeId",
                table: "UserCartItems",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PriceTypeName",
                table: "UserCartItems",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PriceTypeName",
                table: "UserCartItems");

            migrationBuilder.AlterColumn<string>(
                name: "PriceTypeId",
                table: "UserCartItems",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserCartItems_PriceTypeId",
                table: "UserCartItems",
                column: "PriceTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserCartItems_PriceTypes_PriceTypeId",
                table: "UserCartItems",
                column: "PriceTypeId",
                principalTable: "PriceTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
