﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Response
{
    public class ContactConfirmationData
    {
        /// <summary>
        /// Показывает, что нужно подтверждение контакта
        /// </summary>
        public bool NeedConfirmation { get; set; }
    }
}
