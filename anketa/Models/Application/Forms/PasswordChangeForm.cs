﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Forms
{
    /// <summary>
    /// Представляет собой форму для сброса пароля
    /// </summary>
    public class PasswordChangeForm
    {
        /// <summary>
        /// Пароль
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите новый пароль.")]
        [MinLength(3, ErrorMessage = "Минимальная длина пароля - 3 символа.")]
        [Display(Name = "Новый пароль")]
        public string Password { get; set; }

        /// <summary>
        /// Подтверждение пароля
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите подтверждение пароля.")]
        [Compare("Password", ErrorMessage = "Пароли не совпадают.")]
        [Display(Name = "Подтверждение пароля")]
        public string PasswordConfirm { get; set; }

        /// <summary>
        /// Токен для сброса
        /// </summary>
        public string Token { get; set; }

    }
}
