﻿using anketa.Models.Database;
using anketa.Models.Database.Products;
using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace anketa.Models
{
    public class DatabaseContext : DbContext, IDataProtectionKeyContext
    {
        /// <summary>
        /// Конструктор контекста БД
        /// </summary>
        /// <param name="options"></param>
        public DatabaseContext(DbContextOptions options) : base(options)
        {
        }

        /// <summary>
        /// Таблица пользователей
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Возвращает текущего пользователя
        /// </summary>
        /// <returns></returns>
        public async Task<User> GetUser(ClaimsPrincipal claims)
        {
            var sidClaim = claims.Claims.FirstOrDefault(p => p.Type == ClaimTypes.Sid);
            if(sidClaim != null && int.TryParse(sidClaim.Value, out int userId))
                return await Users.FirstOrDefaultAsync(p => p.ID == userId);
            return null;
        }

        /// <summary>
        /// Таблица токенов пользователей
        /// </summary>
        public DbSet<AuthorizationToken> AuthorizationTokens { get; set; }

        /// <summary>
        /// Таблица токенов активации пользователей
        /// </summary>
        public DbSet<UserActivationToken> UserActivationTokens { get; set; }

        /// <summary>
        /// Таблица токенов сброса паролей пользователей
        /// </summary>
        public DbSet<UserPasswordRestoreToken> UserPasswordRestoreTokens { get; set; }

        /// <summary>
        /// Таблица физических лиц пользователей
        /// </summary>
        public DbSet<UserIndividualFace> UserIndividualFaces { get; set; }

        /// <summary>
        /// Таблица юридических лиц пользователей
        /// </summary>
        public DbSet<UserEntityFace> UserEntityFaces { get; set; }

        /// <summary>
        /// Таблица для хранения ОПФ
        /// </summary>
        public DbSet<LegalForm> LegalForms { get; set; }

        /// <summary>
        /// Таблица заказов пользователей
        /// </summary>
        public DbSet<Order> Orders { get; set; }

        /// <summary>
        /// Таблица оснований
        /// </summary>
        public DbSet<ActOnBase> Bases { get; set; }

        /// <summary>
        /// Адреса доставки
        /// </summary>
        public DbSet<Address> Addresses { get; set; }

        /// <summary>
        /// Таблица для группировки лиц
        /// </summary>
        public DbSet<UserFace> Faces { get; set; }

        /// <summary>
        /// Статусы заказов
        /// </summary>
        public DbSet<OrderStatus> OrderStatuses { get; set; }

        /// <summary>
        /// Сервисные пользователи
        /// </summary>
        public DbSet<ServiceUser> ServiceUsers { get; set; }

        /// <summary>
        /// Разрешения сервисных пользователей
        /// </summary>
        public DbSet<ServiceUserPermission> ServiceUserPermissions { get; set; }

        /// <summary>
        /// Банковские реквизиты
        /// </summary>
        public DbSet<BankDetails> BankDetails { get; set; }

        /// <summary>
        /// Подтвержденные почтовые ящики пользователя
        /// </summary>
        public DbSet<UserConfirmedEmail> UserConfirmedEmails { get; set; }

        /// <summary>
        ///  Товары
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Выбранные товары заказа
        /// </summary>
        public DbSet<OrderProductItem> OrderProductItems { get; set; }

        /// <summary>
        /// Таблица для постоянного хранения
        /// </summary>
        public DbSet<DataProtectionKey> DataProtectionKeys { get; set; }

        /// <summary>
        /// Таблица хранения аторизации через соц.сети
        /// </summary>
        public DbSet<UserSocial> UserSocials { get; set; }

        /// <summary>
        /// Тип цены
        /// </summary>
        public DbSet<PriceType> PriceTypes { get; set; }

        /// <summary>
        /// Таблица проектов
        /// </summary>
        public DbSet<ProductProject> ProductProjects { get; set; }

        /// <summary>
        /// Таблица способов оплаты заказа
        /// </summary>
        public DbSet<OrderPaymentMethods> OrderPaymentMethods { get; set; }

        /// <summary>
        /// Талица потвержденных телефонов
        /// </summary>
       public DbSet<UserConfirmedPhone> UserConfirmedPhones { get; set; }

        /// <summary>
        /// Таблица кредитных заявок
        /// </summary>
        public DbSet<CreditApplication> CreditApplications { get; set; }

        /// <summary>
        /// Таблица товаров в кредитных заявках
        /// </summary>
        public DbSet<CreditApplicationProduct> CreditApplicationProducts { get; set; }

        /// <summary>
        /// Собщения User
        /// </summary>
        public DbSet<UserEmailMessage> UserEmailMessages { get; set; }

        #region Заказы

        /// <summary>
        /// Таблица данных по оплате заказов
        /// </summary>
        public DbSet<Database.Orders.OrderPayment> OrderPayments { get; set; }

        /// <summary>
        /// Информация о доставки заказа
        /// </summary>
        public DbSet<DeliveryOrder> DeliveryOrders { get; set; }
        #endregion
    }
}
