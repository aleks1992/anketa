﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace anketa.Misc.Redis
{
    public interface IRedisClient
    {
        bool Status { get; }

        bool StorageIsReady();
        bool SetValue(string key, object val);
        string GetString(string key);
        byte[] GetBytes(string key);
        bool GetBool(string key);
        int GetInt(string key);
        long GetLong(string key);
        double GetDouble(string key);
        float GetFloat(string key);

        void Select(int db);
        void RollbackSelect();

        bool Exists(string key);
    }

    public class RedisConfiguration
    {
        public string Host { get; set; }
        public int Port { get; set; }

        public string HostAddress
        {
            get
            {
                try
                {
                    if (!IPAddress.TryParse(Host, out IPAddress address))
                    {
                        IPHostEntry hostEntry = Dns.GetHostEntry(Host);
                        if (hostEntry.AddressList.Count() > 0) address = hostEntry.AddressList[0];
                    }

                    return address.ToString();
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                return null;
            }
        }
    }
}
