﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Models;
using Microsoft.AspNetCore.Mvc;

namespace anketa.Controllers
{
    public class ErrorController : Controller
    {
        /// <summary>
        /// Показывает ошибку
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public IActionResult Index(ErrorModel model, int StatusCode)
        {
            HttpContext.Response.StatusCode = model.StatusCode;
            return View(model);
        }
    }
}