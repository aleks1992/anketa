﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    public class UserPasswordRestoreToken
    {
        /// <summary>
        /// ID пользователя
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Токен пользователя
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Время действия токена
        /// </summary>
        public DateTime Expires { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public int UserId { get; set; }
    }
}
