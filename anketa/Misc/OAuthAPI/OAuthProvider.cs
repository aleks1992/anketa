﻿using anketa.Misc;
using anketa.Models.Database;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Xsl;
using static anketa.Misc.SiteClient;
using static System.Net.WebRequestMethods;

namespace anketa.Misc.OAuthAPI
{
    /// <summary>
    ///  Абстрактный класс авторизации соц.сети 
    /// </summary>
    public abstract class OAuthProvider
    {
        public static OAuthProvider GetProvider(Type providerType, SocialData configuration, string redirectUri, string state)
        {
            OAuthProvider instance = (OAuthProvider)Activator.CreateInstance(providerType);
            instance.Config = configuration;
            instance.RedirectUriBase = redirectUri;
            instance.State = state;
            return instance;
        }


        /// <summary>
        /// Тело запроса
        /// </summary>
        protected dynamic Request;

        /// <summary>
        /// Конфигурация
        /// </summary>
        protected SocialData Config;

        /// <summary>
        /// Uri Аунтификации
        /// </summary>
        protected string RedirectUriBase;

        /// <summary>
        /// Состояние сессии
        /// </summary>
        protected string State;

        protected string RedirectUri
        {
            get
            {
                return RedirectUriBase + "?provider=" + ProviderName;
            }
        }

        /// <summary>
        /// Наименование провайдера
        /// </summary>
        public string ProviderName
        {
            get
            {
                return GetType().Name.Replace("OAuthProvider", "");
            }
        }

        /// <summary>
        /// Показывает, доступен ли провайдер
        /// </summary>
        public bool IsReady
        {
            get
            {
                return Config != null;
            }
        }

        /// <summary>
        /// Получения Uri
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public abstract string GetUri(OAuthMethod method);

        /// <summary>
        /// Получения Http метод
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        protected abstract string GetHttpMethod(OAuthMethod method);

        /// <summary>
        /// Производит авторизацию через соц. сети
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public abstract User ProcessLogin(string code, string state = null);

        /// <summary>
        /// Получения подпись для запроса
        /// </summary>
        /// <returns></returns>
        protected virtual string GetSignature(string[] exceptFields)
        {
            return "";
        }

        /// <summary>
        /// Возвращает ссылку на изображение соц. сети
        /// </summary>
        /// <returns></returns>
        public virtual string GetImageUri()
        {
            return ProviderName.ToLower() + ".png";
        }
          
        /// <summary>
        /// Проверяет налчие подписи у метода
        /// </summary>
        /// <param name="oAuthMethod"></param>
        /// <param name="needSig"></param>
        /// <param name="exceptFields"></param>
        /// <returns></returns>
        protected object SendRequest(OAuthMethod oAuthMethod, bool needSig = false, string[] exceptFields = null)
        {
            string sig = null;
            if (needSig) sig = GetSignature(exceptFields);
            return GetRequestBody(oAuthMethod, sig);
        }

        /// <summary>
        /// Получения тела ответа
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public virtual object GetRequestBody(OAuthMethod oAuthMethod, string sig = null)
        {
            var response = new ServerResponseItemData();
            if (GetHttpMethod(oAuthMethod) == Http.Get)
            {
                string uriProperties = GetQueryString(this.Request);
                string uri = GetUri(oAuthMethod) + "?" + uriProperties;
                response = HttpClient(null, uri, method: Http.Get, inAnswerResponse: false);
            }
            else
            {
                string uri = GetUri(oAuthMethod);
                response = HttpClient(Request, uri, "POST", "x-www-form-url-encoded", inAnswerResponse: false);

            }
            return response.Data;
        }


        /// <summary>
        /// Методы
        /// </summary>
        public enum OAuthMethod
        {
            GetCode,
            GetToken,
            GetUser
        }

        /// <summary>
        /// Представляет собой состояние запроса авторизации
        /// </summary>
        public class OAuthState
        {
            public string FormId;
        }
    }
}
