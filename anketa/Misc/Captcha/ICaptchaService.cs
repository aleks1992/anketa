﻿using Microsoft.AspNetCore.Html;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Org.BouncyCastle.Crypto.Tls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.Captcha
{
    public interface ICaptchaService
    {
        public abstract void SetConfiguration(IServiceCollection services, object configuration);

        public abstract HtmlString RenderCaptcha(Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper htmlHelper);

        public object GetConfiguration();

        public abstract Task<bool> IsValid(string token);
    }
}
