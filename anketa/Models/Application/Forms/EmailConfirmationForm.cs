﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Forms
{
    public class EmailConfirmationForm
    {
        /// <summary>
        /// Ид пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        [Display(Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        /// Код верификации
        /// </summary>
        [Display(Name = "Проверочный код")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите проверочный код.")]
        [Remote("ConfirmEmailCheckCode", "Account", AdditionalFields = nameof(Email) + "," + nameof(UserId))]
        public string Code { get; set; }
    }
}
