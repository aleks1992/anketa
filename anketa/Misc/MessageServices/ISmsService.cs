﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.MessageServices
{
    /// <summary>
    /// Представляет собой интерфейс для сервиса отправки SMS сообщений
    /// </summary>
    public interface ISmsService
    {
        /// <summary>
        /// Отправляет SMS сообщение
        /// </summary>
        /// <param name="receiver">Получатель сообщения</param>
        /// <param name="content">Текст сообщения</param>
        /// <returns></returns>
        bool SendMessage(string receiver, string content);
    }
}
