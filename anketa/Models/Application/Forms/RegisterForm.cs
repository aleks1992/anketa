﻿using anketa.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Forms
{
    public class RegisterForm
    {
        /// <summary>
        /// E-mail пользователя
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите логин.")]
        [Display(Name = "Электронная почта", Description = "Введите ваш email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        /// <summary>
        /// Пароль пользователя
        /// </summary        
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите пароль.")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        /// <summary>
        /// Подтверждение пароля
        /// </summary        
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите подтверждение пароля.")]
        [DataType(DataType.Password)]
        [Display(Name = "Подтверждение пароля")]
        public string PasswordConfirmations { get; set; }

        /// <summary>
        /// Согласие на обработку персональных данных
        /// </summary>
        [Display(Name = "Я согласен на обработку персональных данных")]
        [Required(ErrorMessage = "Чтобы зарегистрироваться, Вы должны согласиться на обработку персональных данных.")]
        public bool IAgree { get; set; }

        /// <summary>
        /// Код потверждения
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Номер телефона
        /// </summary>
        public string Phone { get; set; }


    }
}
