﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace anketa.Controllers.API
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "API-NotUsed")]
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentMethodsController : Controller
    {
        private readonly DatabaseContext DbContext;

        public PaymentMethodsController(DatabaseContext dbContext)
        {
            DbContext = dbContext;
        }

        /// <summary>
        /// Возвращает список методов оплаты
        /// </summary>
        /// <returns></returns>
        public IActionResult Get()
        {
            var methods = DbContext.OrderPaymentMethods.ToList();
            return Ok(methods);
        }
    }
}