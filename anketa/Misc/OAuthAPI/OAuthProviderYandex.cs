﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using anketa.Models.Database;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using static System.Net.WebRequestMethods;
using static anketa.Misc.SiteClient;

namespace anketa.Misc.OAuthAPI
{
    /// <summary>
    /// Аторизация через Yandex
    /// </summary>
    public class OAuthProviderYandex : OAuthProvider
    {

        /// <summary>
        /// Получения типа запроса метода
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        protected override string GetHttpMethod(OAuthMethod method)
        {
            if (method == OAuthMethod.GetToken)
            {
                return Http.Post;
            }
            return Http.Get;
        }

        /// <summary>
        /// Получения Uri
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public override string GetUri(OAuthMethod method)
        {
            if (method == OAuthMethod.GetCode)
            {
                var dataCode = new RequestYandexCode
                {
                    client_id = Config.ClientId,
                    response_type = "code",
                    redirect_uri = RedirectUri,
                    state = State,

                };
                string getParameter = SiteClient.GetQueryString(dataCode);
                return "https://oauth.yandex.ru/authorize?" + getParameter;
            }
            else if (method == OAuthMethod.GetToken) return "https://oauth.yandex.ru/token";
            else if (method == OAuthMethod.GetUser) return "https://login.yandex.ru/info";
            return null;
        }

        /// <summary>
        /// Получения подписи для запроса
        /// </summary>
        /// <returns></returns>
        protected override string GetSignature(string[] exceptFields)
        {
            return null;
        }

        /// <summary>
        /// Получения тела запроса
        /// </summary>
        /// <param name="method"></param>
        /// <returns></returns>
        public override object GetRequestBody(OAuthMethod oAuthMethod, string sig = null)
        {

            var response = new ServerResponseItemData();
            if (GetHttpMethod(oAuthMethod) == Http.Post)
            {
                string uri = GetUri(oAuthMethod);
                var headers = new Dictionary<string, string>();
               /// headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.87 Safari/537.36");
                response = HttpClient(Request, uri, "POST", "x-www-form-url-encoded", inAnswerResponse: false, headers: headers);
                return response.Data;
            }

            return base.GetRequestBody(oAuthMethod);
        }

        /// <summary>
        /// Авторизация 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        public override User ProcessLogin(string code, string state = null)
        {
            dynamic userReturn = null;
            Request = new RequestYandexToken
            {
                code = code,
                redirect_uri = RedirectUri,
                client_id = Config.ClientId,
                client_secret = Config.SecretKey,
                grant_type = "authorization_code"
            };

            //Получения токена
            var response = SendRequest(OAuthMethod.GetToken);
            dynamic accessToken = null;
            if (response != null)
            {
                accessToken = JsonConvert.DeserializeObject<AccessYandexToken>(response.ToString());
            }

            //Отправка запроса на получения User
            if (accessToken != null)
            {
                Request = new RequectYandexUser
                {
                    oauth_token = accessToken.access_token,
                    format = "json"
                };

                response = SendRequest(OAuthMethod.GetUser);
                dynamic user = null;
                if (response != null)
                {
                    user = JsonConvert.DeserializeObject<UserYandex>(response.ToString());
                }
                if (user != null)
                {
                    userReturn = new User
                    {
                        Email = user.default_email,
                        FirstName = user.first_name,
                        LastName = user.last_name,
                        Activated = true,
                    };
                }
            }

            return userReturn;
        }

        /// <summary>
        /// Данные для получения токена доступа  yandex
        /// </summary>
        class RequestYandexToken
        {
            /// <summary>
            /// Код авторизации
            /// </summary>
            public string code { get; set; }

            /// <summary>
            /// Uri куда возврашать ответ
            /// </summary>
            public string redirect_uri { get; set; }

            /// <summary>
            /// Тип получаемого токена
            /// </summary>
            public string grant_type { get; set; }

            /// <summary>
            ///  Индификатор приложения
            /// </summary>
            public string client_id { get; set; }

            /// <summary>
            /// Закрытый ключ
            /// </summary>
            public string client_secret { get; set; }


        }

        /// <summary>
        /// Получения кода аунтификации  Yandex
        /// </summary>
        class RequestYandexCode
        {
            /// <summary>
            /// Индификатор приложения
            /// </summary>
            public string client_id { get; set; }
            /// <summary>
            /// Типа ответа
            /// </summary>
            public string response_type { get; set; }

            /// <summary>
            /// Uri куда возврашать ответ
            /// </summary>
            public string redirect_uri { get; set; }

            /// <summary>
            /// Состояние сессии
            /// </summary>
            public string state { get; set; }

        }

        /// <summary>
        /// Токен доступа Yandex
        /// </summary>
        class AccessYandexToken
        {
            /// <summary>
            /// Токен доступа
            /// </summary>
            public string access_token { get; set; }

            /// <summary>
            /// Тип токена
            /// </summary>
            public string token_type { get; set; }

            /// <summary>
            /// Время жизни
            /// </summary>
            public string expires_in { get; set; }

            /// <summary>
            /// Токен для обновления токена доступа
            /// </summary>
            public string refresh_token { get; set; }

        }


        /// <summary>
        /// Получения ифнормации о пользователи Yandex
        /// </summary>
        class RequectYandexUser
        {
            /// <summary>
            /// Токен доступа
            /// </summary>
            public string oauth_token { get; set; }

            /// <summary>
            /// Формат Json
            /// </summary>
            public string format { get; set; }
                 
        }

        /// <summary>
        /// Set User Yandex
        /// </summary>
        class UserYandex
        {

            /// <summary>
            /// Индификатор Usrt
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// Имя
            /// </summary>
            public string first_name { get; set; }

            /// <summary>
            /// Фамилия
            /// </summary>
            public string last_name { get; set; }

            /// <summary>
            /// Email
            /// </summary>
            public string default_email { get; set; }

        }

    }
}
