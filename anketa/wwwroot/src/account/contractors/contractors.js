﻿/**
 * Контрагенты пользователя
 */

import Vue from 'vue/dist/vue.esm.js'
import Contractors from './contractors.vue'

new Vue({
    el: '#app',
    render: h => h(Contractors),
});