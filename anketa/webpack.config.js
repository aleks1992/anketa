﻿/// <binding ProjectOpened='Watch - Development' />
"use strict";

const path = require('path');
const resolve = relativePath => path.resolve(__dirname, relativePath);
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const bundleOutputDir = resolve('./wwwroot/dist/');

const config = {
	entry: {
        main: resolve('./wwwroot/src/main.js'),
        order: resolve('./wwwroot/src/order/order.js'),
        orderList: resolve('./wwwroot/src/order/list/order-list.js'),
        wishlist: resolve('./wwwroot/src/cartwishlist/wishlist.js'),
        cart: resolve('./wwwroot/src/cartwishlist/cart.js'),
        login: resolve('./wwwroot/src/login/login.js'),
        'personal-data': resolve('./wwwroot/src/account/personal-data/personal-data.js'),
        projects: resolve('./wwwroot/src/cartwishlist/projects/projects.js'),
        contractors: resolve('./wwwroot/src/account/contractors/contractors.js'),
        delivery: resolve('./wwwroot/src/delivery/delivery.js'),
        'delivery-site': resolve('./wwwroot/src/delivery/delivery-site.js')
	},
    mode: 'development',
    output: {
        path: bundleOutputDir,
		publicPath: '/',
        filename: '[name].min.js',
        library: 'app',
        libraryTarget: 'umd',
        globalObject: 'window'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader'
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    loaders: {
                        js: [
                            {
                                loader: 'babel-loader'
                            }
                        ]
                    }
                }
            },
            {
                test: /\.s?css$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            }
        ]
    },
    devServer: {
        contentBase: resolve('./wwwroot'),
        hot: true,
        port: 8083
    },
    watchOptions: {
        poll: true
    },
    plugins: [
        new VueLoaderPlugin()
    ],
    resolve: {
        alias: {
            vue: 'vue/dist/vue.esm.js'
        }
    },
    node: {
        dns: 'mock',
        net: 'mock',
    },
};

module.exports = config;