﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.API.Response
{
    public class OrderUpdateApi
    {
        public bool OrdersUpdated { get; set; }
    }
}
