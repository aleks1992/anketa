﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Представляет собой описание адреса
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Ид адреса
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Ид пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        [JsonIgnore]
        public User User { get; set; }

        /// <summary>
        /// Страна
        /// </summary>
        [Display(Name = "Страна")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите страну.")]
        public string Country { get; set; }

        /// <summary>
        /// Населенный пункт
        /// </summary>
        [Display(Name = "Населенный пункт")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите населенный пункт.")]
        public string City { get; set; }

        /// <summary>
        /// Улица
        /// </summary>
        [Display(Name = "Улица")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите улицу.")]
        public string Street { get; set; }

        /// <summary>
        /// Номер дома, корпус или строение
        /// </summary>
        [Display(Name = "Номер дома, корпус или строение")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите номер дома, корпус или строение.")]
        public string BuildingNumber { get; set; }

        /// <summary>
        /// Номер квартиры, офиса
        /// </summary>
        [Display(Name = "Номер квартиры\\офиса")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите номер квартиры или офиса.")]
        public string FlatNumber { get; set; }

        /// <summary>
        /// Индекс
        /// </summary>
        [Display(Name = "Индекс")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Введите индекс.")]
        public string Index { get; set; }

        /// <summary>
        /// Полный адрес
        /// </summary>
        [Display(Name = "Адрес")]
        public string FullAddress { get; set; }

        /// <summary>
        /// Адрес по умолчанию
        /// </summary>
        [Display(Name = "Основной адрес")]
        public bool IsDefault { get; set; }

        /// <summary>
        /// Собирает адрес из компонент в строку
        /// </summary>
        /// <returns>Строка с адресом</returns>
        public override string ToString()
        {
            string address = string.Format("{0},{1},{2},{3},{4},{5}", Index.Trim(), Country.Trim(), City.Trim(), Street.Trim(), BuildingNumber.Trim(), FlatNumber.Trim());
            while (address.Contains(",,")) address = address.Replace(",,", ",");
            return address.Replace(",", ", ");
        }
    }
}
