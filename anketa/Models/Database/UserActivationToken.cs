﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    /// <summary>
    /// Представляет собой токен активации\верификации
    /// </summary>
    public class UserActivationToken
    {
        /// <summary>
        /// ID токена
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Токен активации
        /// </summary>
        public string Token { get; set; }
        
        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; set; }
        
        /// <summary>
        /// ID пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Срок действия токена
        /// </summary>
        public DateTime Expires { get; set; }

        /// <summary>
        /// Данные токена
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Тип
        /// </summary>
        public UserActivationTokenType Type { get; set; }

    }

    /// <summary>
    /// Тип токена
    /// </summary>
    public enum UserActivationTokenType: int
    {
        Phone = 0,
        Email = 1
    }


}
