﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class updateOrderProductItemProductCount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Price",
                table: "OrderProductItems",
                newName: "ProductPrice");

            migrationBuilder.RenameColumn(
                name: "Count",
                table: "OrderProductItems",
                newName: "ProductCount");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ProductPrice",
                table: "OrderProductItems",
                newName: "Price");

            migrationBuilder.RenameColumn(
                name: "ProductCount",
                table: "OrderProductItems",
                newName: "Count");
        }
    }
}
