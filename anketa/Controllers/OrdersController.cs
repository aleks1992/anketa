﻿using anketa.Misc;
using anketa.Misc.Content;
using anketa.Misc.GeoLocation;
using anketa.Misc.MessageServices;
using anketa.Misc.Redis;
using anketa.Misc.SiteAPI;
using anketa.Models;
using anketa.Models.API;
using anketa.Models.Application;
using anketa.Models.Application.Forms;
using anketa.Models.Application.Response;
using anketa.Models.Database;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static anketa.Misc.SiteClient;

namespace anketa.Controllers
{
    /// <summary>
    /// Отвечает за отображение и создание заказов
    /// </summary>
    [AllowAnonymous]
    public class OrdersController : Controller
    {
        #region Внешние зависимости

        private DatabaseContext DbContext;
        private List<SiteData> LinkedSites { get; set; }
        public string AutorizeManager { get; private set; }

        private IWebHostEnvironment HostingEnvironment;
        private CartWishlistManager _cartWishlistUpdater;
        private GeoLocationClient GeoLocation;

        private readonly ISmsService _smsService;
        private readonly IEmailService _emailService;
        private readonly IRedisClient _redisClient;

        #endregion

        #region Константы

        public const string USER_ORDERS_DATA = "User_Orders_Cache_";

        #endregion

        public OrdersController(
            DatabaseContext dbContext, List<SiteData> linkedSites,
            IWebHostEnvironment env, CartWishlistManager cartWishlistUpdater,
            IRedisClient redisClient, GeoLocationClient location,
            ISmsService smsService, IEmailService emailService)
        {
            DbContext = dbContext;
            LinkedSites = linkedSites;
            HostingEnvironment = env;
            _cartWishlistUpdater = cartWishlistUpdater;
            GeoLocation = location;

            _smsService = smsService;
            _emailService = emailService;
            _redisClient = redisClient;
        }

        #region Отображение заказов

        /// <summary>
        /// Список заказов
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        #endregion

        #region Создание заказов

        /// <summary>
        /// Создание нового заказа
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Create([FromForm] string productsData, [FromServices] ModelEngine modelEngine, [FromServices] AppData appData)
        {
            if (!string.IsNullOrEmpty(productsData))
            {
                var items = JsonConvert.DeserializeObject<List<CartWishlistItem>>(productsData);
                if (items != null && items.Count > 0)
                {
                    modelEngine.RegisterModels(typeof(Order), typeof(UserFace), typeof(UserEntityFace), typeof(UserIndividualFace),
                        typeof(BankDetails), typeof(LegalForm), typeof(ActOnBase), typeof(OrderPaymentMethods), typeof(Address), typeof(DeliveryOrder));

                    appData.AddData("legalForms", DbContext.LegalForms.ToList().Select(m => new SelectListItem { Text = m.Name, Value = m.Id.ToString() }));
                    appData.AddData("bases", DbContext.Bases.ToList().Select(m => new SelectListItem { Text = m.Name, Value = m.ID.ToString() }));
                    appData.AddData("countryList", GeoLocation.GetFilterListCountry(defaultFilter: true));
                    appData.AddData("paymentMethods", DbContext.OrderPaymentMethods.Where(p => p.Id != 1).ToList());


                    ViewBag.CartItems = items;
                    var ip = HttpContext.Connection.RemoteIpAddress.ToString();
                    ViewBag.Country = GeoLocation.GetCountry(ip);
                    return View("OrderDetails");
                }
            }

            return RedirectToAction("Index", "Cart");
        }

        /// <summary>
        /// Получения 5 последних ФИО из заказа
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult GetLastOrdersData(string phone, string email)
        {

            var userOrders = DbContext.Orders.Where(p => p.Phone.Contains(phone.Trim()) || p.Email.Contains(email.Trim())).OrderByDescending(p => p.Id).Select(p => new { p.Id, p.ProductDeliveryAddress, p.OrdererFullName }).ToList();
            var productDeliveryAddress = new List<string>();
            var ordererFullName = new List<string>();

            foreach (var item in userOrders)
            {
                if (ordererFullName.Count() < 5 && !ordererFullName.Contains(item.OrdererFullName))
                    ordererFullName.Add(item.OrdererFullName);
                if (productDeliveryAddress.Count() < 5 && !productDeliveryAddress.Contains(item.ProductDeliveryAddress))
                    productDeliveryAddress.Add(item.ProductDeliveryAddress);
            }

            return Json(new
            {
                OrdererFullName = ordererFullName.ToArray(),
                ProductDeliveryAddress = productDeliveryAddress.ToArray()

            });
        }


        /// <summary>
        /// Создание нового заказа
        /// </summary>
        /// <param name="order"></param>
        /// <param name="updater"></param>
        /// <param name="productLoader"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> NewOrder([FromBody] Order order, [FromServices] CartWishlistManager updater,
            [FromServices] ProductLoader productLoader, [FromServices] AuthorizeManager authorizeManager,
            [FromServices] IDistributedCache cache, [FromServices] ClientConfirmedContacts clientConfirmedContacts)
        {
            var response = new ServiceResponse
            {
                Success = true,
            };

            // Подготавливаем контактные данные
            order.Phone = DataManager.PreparePhoneNumber(order.Phone);
            order.Email = !string.IsNullOrWhiteSpace(order.Email) ? order.Email.ToLower() : null;

            // Проверяем, подтверждены ли контакты
            order.PhoneIsConfirmed = await clientConfirmedContacts.PhoneIsConfirmed(order.Phone);
            order.EmailIsConfirmed = await clientConfirmedContacts.EmailIsConfirmed(order.Email);

            // Заказ от физ. лица, но нет ни одного подтвержденного контакта
            if (order.OrderByIndividualFace && !order.PhoneIsConfirmed && !order.EmailIsConfirmed)
            {
                response.Success = false;
                response.Message = "Для оформления заказа нужно подтвердить хотя бы один контакт.";
                return Json(response);
            }

            // Определяем пользователя
            var user = await DbContext.GetUser(User);
            if (user == null)
            {
                // Пользователь не авторизован и заказ от юридического лица
                // Выдаем ошибку
                if (!order.OrderByIndividualFace)
                {
                    response.Success = false;
                    response.Message = "Для оформления заказа от юридического лица нужно предварительно авторизоваться.";
                    return Json(response);
                }

                // Пытаемся найти пользователя. Если пользователь не существует создаем нового.
                user = new User
                {
                    ConsentToNewsletter = order.ConsentToNewsletter,
                    Email = order.EmailIsConfirmed && order.Email != null ? order.Email : null, // Добавляем почту пользователю, если он её подтвердил
                    FirstName = order.GetOrdererFirstName(),
                    LastName = order.GetOrdererLastName(),
                    MiddleName = order.GetOrdererMiddleName(),
                    Phone = order.PhoneIsConfirmed && order.Phone != null ? order.Phone : null, // Добавляем телефон пользователю, если он его подтвердил
                };
                user = await authorizeManager.RegisterUserAsync(user, null, false, true);

                // Проверяем согласие на получение рекламы и обработку ПД
                if (order.ConsentToNewsletter && !user.ConsentToNewsletter || !user.PersonalDataProcessing)
                {
                    user.ConsentToNewsletter = user.ConsentToNewsletter || order.ConsentToNewsletter;
                    user.PersonalDataProcessing = true;
                    DbContext.Users.Update(user);
                }
            }

            // Получаем выбранные товары
            var productsInOrder = new List<CartWishlistItem>();
            order.ProductInfo.ForEach(p =>
            {
                productsInOrder.Add(JsonConvert.DeserializeObject<CartWishlistItem>(p));
            });
            var products = OrderProductItem.Make(DbContext, productLoader, productsInOrder);

            order.User = user;
            order.OrderDate = DateTime.Now;
            order.ModifyDate = DateTime.Now;
            order.Status = await DbContext.OrderStatuses.FirstOrDefaultAsync(p => p.Name == "ReadyToSync");

            // Копируем покупателя
            order.OrdererFaceId = order.OrdererFace.ID;
            order.OrdererFace = null;

            // Копируем плательщика
            order.PayerFaceId = order.PayerFaceIsOrderer ? order.OrdererFaceId : order.PayerFace.ID;
            order.PayerFace = null;

            // Копируем получателя документов
            order.DocumentsReceiverId = order.DocumentReceiverIsOrderer ? order.OrdererFaceId : order.DocumentsReceiver.ID;
            order.DocumentsReceiver = null;

            // Копируем получателя товаров
            order.ProductReceiverId = order.ProductReceiverIsOrderer ? order.OrdererFaceId : order.ProductReceiver.ID;
            order.ProductReceiver = null;

            // Проверяем, если заказ от физического лица, получаем или создаем контрагента
            if (order.OrderByIndividualFace)
            {
                // Заказ от физического лица, создаем или находим подходящего контрагента
                string lastName = null, firstName = null, middleName = null;

                var regexp = new Regex(@"^(?<lastName>\S+)\s{0,}(?<firstName>\S+)\s{0,}(?<middleName>.*?)\s{0,}$");

                var nameComponent = regexp.Match(order.OrdererFullName);
                if (nameComponent.Groups.TryGetValue("lastName", out Group lastNameGroup)) lastName = lastNameGroup.Value;
                if (nameComponent.Groups.TryGetValue("firstName", out Group firstNameGroup)) firstName = firstNameGroup.Value;
                if (nameComponent.Groups.TryGetValue("middleName", out Group middleNameGroup)) middleName = middleNameGroup.Value;

                UserFace userFace = null;
                UserIndividualFace faceFounded = null;

                var faces = DbContext.UserIndividualFaces.Where(p => p.UserId == user.ID).ToList();
                foreach (var face in faces)
                {
                    if (face.LastName == lastName && face.FirstName == firstName && (middleName == null || face.MiddleName == middleName))
                    {
                        faceFounded = face;
                        break;
                    }
                }

                if (faceFounded == null)
                {
                    // Создаем нового контрагента
                    faceFounded = new UserIndividualFace
                    {
                        User = user,
                        LastName = lastName,
                        FirstName = firstName,
                        MiddleName = middleName,
                    };
                    await DbContext.UserIndividualFaces.AddAsync(faceFounded);

                    userFace = new UserFace
                    {
                        User = user,
                        IndividualFace = faceFounded,
                    };
                    await DbContext.Faces.AddAsync(userFace);
                }
                else
                {
                    userFace = await DbContext.Faces.FirstOrDefaultAsync(p => p.IndividualFaceId == faceFounded.ID);

                    if (userFace == null)
                    {
                        // Найдено физическое лицо, но оно не привязано к общей таблице
                        // Создаем связь
                        // FIXME: нужно найти место, где можно записать физ. лицо без привязки
                        userFace = new UserFace
                        {
                            User = user,
                            IndividualFace = faceFounded,
                        };
                        await DbContext.Faces.AddAsync(userFace);
                    }
                }

                order.OrdererFaceId = order.PayerFaceId = order.DocumentsReceiverId = order.ProductReceiverId = 0;
                order.OrdererFace = order.PayerFace = order.DocumentsReceiver = order.ProductReceiver = userFace;
            }

            order.OrderPaymentMethodId = order.OrderPaymentMethod.Id;
            order.OrderPaymentMethod = null;

            // Добавляем товары
            foreach (var p in products)
            {
                p.Order = order;
                await DbContext.OrderProductItems.AddAsync(p);
            }
            await DbContext.DeliveryOrders.AddAsync(order.DeliveryOrder);

            await DbContext.Orders.AddAsync(order);



            try
            {
                await DbContext.SaveChangesAsync();

                // Очищаем кэш заказов клиента
                await cache.RemoveAsync(USER_ORDERS_DATA + user.ID.ToString());
            }
            catch (Exception e)
            {
                ///Сохраняем данные в Redis И отправляем инофрмацию на почту

                string redisIdOrder = "Order_" + (DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds.ToString();
                _redisClient.SetValue(redisIdOrder, JsonConvert.SerializeObject(order));
                var subject = "MasterGroup: Заказ №" + redisIdOrder;
                EmailTemplate template;
                template = new EmailTemplate("OrderError", subject);
                template.SetData("ORDER_ID", redisIdOrder);
                template.SetData("ERROR", e.Message);
                await _emailService.SendMessage("torg@batutmaster.ru", subject, template.Compile(false));

                response.Success = false;
                response.Message = e.Message;
            }

            // Очищаем корзину от заказанных товаров
            foreach (var item in productsInOrder)
            {
                updater.Storage.Cart.RemoveAll(p =>
                    p.Guid == item.Guid && p.ByOrder == item.ByOrder
                    && p.PriceTypeId == item.PriceTypeId
                    && p.SerialNumber == item.SerialNumber && p.StockId == item.StockId);
            }
            updater.Commit();

            if (response.Success)
            {
                // Переадресуем пользователя на страницу заказов
                response.Data = Url.Action("Index", "Home");

                // Проверяем, нужно ли авторизовать пользователя
                if (!User.Identity.IsAuthenticated)
                {
                    // Пользователь не авторизован, авторизуем
                    await authorizeManager.AuthorizeUser(user);
                }

                try
                {
                    // Отправляем информацию о заказе клиенту
                    await SendOrderInformation(order);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    var subject = "MasterGroup: Заказ №" + order.GetOrderGuid();
                    EmailTemplate template;
                    template = new EmailTemplate("OrderError", subject);
                    template.SetData("ORDER_ID", order.GetOrderGuid());
                    template.SetData("ERROR", e.Message);
                    await _emailService.SendMessage("torg@batutmaster.ru", subject, template.Compile(false));
                }


            }

            return Json(response);
        }

        #region Уведомление о заказе

        /// <summary>
        /// Отправляет пользователю информацию о заказе по смс и\или электронной почте
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        private async Task<bool> SendOrderInformation(Order order)
        {
            string phone = null;
            string email = null;

            // Получаем пользователя
            var user = await DbContext.Users.FirstOrDefaultAsync(p => p.ID == order.UserId) ?? new User();

            if (order.OrderByIndividualFace) // заказ от физического лица
            {
                if (!string.IsNullOrEmpty(order.Phone)) phone = order.Phone;
                if (!string.IsNullOrEmpty(order.Email)) email = order.Email;
                else if (!string.IsNullOrEmpty(user.Email)) email = user.Email;
            }
            else
            {
                // Заказ от юридического лица
                if (!string.IsNullOrEmpty(user.Email)) email = user.Email;
            }

            if (HostingEnvironment.IsProduction())
            {
                // Отправляем смс с информацией о заказе
                if (!string.IsNullOrEmpty(phone))
                {
                    string content = $"Новый заказ {order.GetOrderGuid()}\r\nООО MasterGroup.";
                    _smsService.SendMessage(phone, content);
                }

                // Не менять тему письма!
                var subject = "MasterGroup: Заказ №" + order.GetOrderGuid();
                EmailTemplate template;


                // Инициализируем шаблон
                template = new EmailTemplate("Order", "MasterGroup: Заказ");

                var orderItems = GetProductOrder(order.Id);
                string orderHtml = "";
                decimal totalPrice = 0;
                foreach (var item in orderItems)
                {
                    totalPrice += item.ProductPrice * item.ProductCount;
                    var sku = "";
                    var productUri = "";
                    var deliveryOption = "";
                    if (item.Type == CartWishlistItem.ItemType.Product)
                    {
                        sku = $"Артикул: {item.Product.Sku}";
                        productUri = item.GetLink();
                        deliveryOption = item.Product.DeliveryOption;
                    }

                    orderHtml += "<tr style='border-bottom:1px dotted #54545478'>" +
                        "<td style='padding:5px;'><img style='width: 60px; height: auto;' src='" + item.Product.GetBase64Image() + "' ></td>" +
                        "<td style='padding:5px;'><a href='" + item.ProductUri + "'>" + item.GetName() + $"</a><br>{sku}</td>" +
                        "<td style='padding:5px;color:#5cb85c;text-align:center;'>" + Math.Round(item.ProductPrice).ToString("N0") + "</td>" +
                        "<td style='padding:5px;text-align:center;'>" + item.ProductCount.ToString() + "</td>" +
                        "<td style='padding:5px;color:#5cb85c;text-align:center;'>" + Math.Round((item.ProductPrice * item.ProductCount)).ToString("N0") + "</td>" +
                        "</tr>";
                }

                var sumForCredit = "";
                if (order.SumForCredit > 0)
                {
                    sumForCredit = "<tr style='border-bottom:1px dotted #54545478'>" +
                                    "<td style = 'padding:5px;' bgcolor = '#E6E7E8'>Сумма кредитный средст:</td>" +
                                    "<td style = 'padding:5px'>" + Math.Round(Convert.ToDouble(order.SumForCredit), 2).ToString() + "</td></tr>";
                }

                order.OrderPaymentMethod = DbContext.OrderPaymentMethods.FirstOrDefault(p => p.Id == order.OrderPaymentMethodId);

                template.SetData("ORDER_STATUS", order.Status.View);
                template.SetData("ORDER_PAYMENT", "не оплачен");
                template.SetData("USER_NAME", string.IsNullOrEmpty(user.ToString()) ? order.OrdererFullName : (user.ToString()));
                template.SetData("USER_EMAIL", email);
                template.SetData("USER_PHONE", phone ?? "Не указан");
                template.SetData("ORDER_DATE", order.OrderDate.Date.ToString("d"));
                template.SetData("ORDER", orderHtml);
                template.SetData("ORDER_ID", order.GetOrderGuid());
                template.SetData("TOTAL_PRICE", Math.Round(totalPrice).ToString("N0"));
                template.SetData("SUM_FOR_CREDIT", sumForCredit);
                template.SetData("ORDER_PAYMENT_METHOD", order.OrderPaymentMethod.Name);
            

                // Информация о доставки
                var orderDelivery = "Нет информации";
                if (order.DeliveryOrder != null)
                {
                    try
                    {
                        var deliveryInfomation = order.DeliveryOrder;

                        ///Формирования URL для доставки
                        var productDelivery = new List<ProductsDelivery>();
                        foreach (var item in orderItems)
                        {
                            var productDeliveryItem = new ProductsDelivery
                            {
                                Guid = item.Product.Guid,
                                Count = item.ProductCount
                            };

                            productDelivery.Add(productDeliveryItem);
                        }

                        var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(productDelivery));
                        var siteDelivery = new
                        {
                            deliveryAddress = order.ProductDeliveryAddress,
                            product = System.Convert.ToBase64String(plainTextBytes)
                        };

                        string urlDelivery = Url.ActionLink("Index", "Delivery") + "?" + SiteClient.GetQueryString(siteDelivery);

                        orderDelivery = "<table>";
                        if (deliveryInfomation.NameTransportCompany == "InYourCity")
                            orderDelivery += "<tr><td>Груз находиться в вашем городе</td></tr>";
                        else
                            orderDelivery += "<tr><td>Транспортная компания:</td><td>" + deliveryInfomation.NameTransportCompany + "</td></tr>";

                        orderDelivery += "<tr><td>Стоимость(руб.):</td><td>" + Math.Round(Convert.ToDouble(deliveryInfomation.Price), 2).ToString() + "</td></tr>" +
                            "<tr><td>Откуда:</td><td>" + (deliveryInfomation.ShippingAddress ?? "г. Чита") + "</td></tr>" +
                            "<tr><td>Срок(дн):</td><td>" + deliveryInfomation.Time + "</td></tr>" +
                             "<tr><td>Адрес доставки:</td><td>" + order.ProductDeliveryAddress + "</td></tr>" +
                             "<tr><td>Доставка:</td><td><a href='" + urlDelivery + "'>Ссылка на рассчет доставки</a></td></tr>";

                        orderDelivery += "</table>";
                    }
                    catch (JsonException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }

                template.SetData("ORDER_DELIVERY", orderDelivery);

                // Отправляем электронное письмо с информацией о заказе
                if (!string.IsNullOrEmpty(email))
                {
                    // Отправляем письмо
                    await _emailService.SendMessage(email, subject, template.Compile(false));
                }

                // Отправляем на наш ящик информацию о новом заказе для менеджеров
                var secretKey = DataSecurity.ComputeHash(order.Id.ToString() + "anketa-secret-key");
                var uriParameter = new
                {
                    id = order.Id,
                    key = secretKey
                };


                var uri = Url.Action("GetProfile", "Admin", null, HttpContext.Request.Scheme) + "?" + GetQueryString(uriParameter);
                template = new EmailTemplate("OrderAlerts", subject);
                template.SetData("ORDER_ID", order.GetOrderGuid());
                template.SetData("URI", uri);
                template.SetData("EMAIL", email ?? "Не указан");
                template.SetData("ORDER_STATUS", order.Status.View);
                template.SetData("ORDER_PAYMENT", "не оплачен");
                template.SetData("USER_NAME", string.IsNullOrEmpty(user.ToString()) ? order.OrdererFullName : (user.ToString()));
                template.SetData("USER_EMAIL", email);
                template.SetData("USER_PHONE", phone ?? "Не указан");
                template.SetData("ORDER_DATE", order.OrderDate.Date.ToString("d"));
                template.SetData("ORDER", orderHtml);
                template.SetData("ORDER_ID", order.GetOrderGuid());
                template.SetData("TOTAL_PRICE", Math.Round(totalPrice).ToString("N0"));
                template.SetData("ORDER_DELIVERY", orderDelivery);
                template.SetData("ORDER_DATA", order.OrdererInformation);
                template.SetData("SUM_FOR_CREDIT", sumForCredit);
                template.SetData("ORDER_PAYMENT_METHOD", order.OrderPaymentMethod.Name);

                await _emailService.SendMessage("torg@batutmaster.ru", subject, template.Compile(false));
            }

            return true;
        }

        /// <summary>
        /// Возвращает список товаров заказа
        /// </summary>
        /// <returns></returns>
        private List<OrderProductItem> GetProductOrder(int orderId = 0)
        {
            if (orderId == 0) return null;
            var items = DbContext.OrderProductItems
                .Include(p => p.Product)
                .Include(p => p.Project)
                .Where(p => p.OrderId == orderId).ToList();
            return items;
        }

        #endregion

        #endregion

        #region Редактирование заказа

        /// <summary>
        /// Открывает форму редактирования заказа
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Edit(int id)
        {
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Изменяет время модификации, чтобы не выгрузить заказ во время редактирования
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public IActionResult EditingNow(int id)
        {
            return Json(true);
        }

        #endregion

        #region Вспомогательные методы при оформлении заказа

        /// <summary>
        /// Получает список контрагентов
        /// </summary>
        /// <param name="e">Какие типы исключить из ответа - individual или entity.</param>
        /// <param name="country">Отбор по стране</param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetContractors([FromQuery] string e, [FromQuery] string country, [FromServices] ProductLoader productLoader, [FromQuery] List<string> productInfo)
        {
            var user = await DbContext.GetUser(User);

            var productsInOrder = new List<CartWishlistItem>();
            productInfo.ForEach(p =>
            {
                productsInOrder.Add(JsonConvert.DeserializeObject<CartWishlistItem>(DataSecurity.DecodeBase64(p)));
            });


            var q = DbContext.Faces
                .Include(p => p.IndividualFace)
                .Include(p => p.EntityFace)
                .Where(p => p.UserId == user.ID);

            // что это?
            ViewBag.CountContractors = q.Count();

            // Отбор по стране
            if (!string.IsNullOrEmpty(country) && country != "ALL")
            {
                q = q.Where(p => (p.EntityFace != null && p.EntityFace.Country == country) || (p.IndividualFace != null && p.IndividualFace.Country == country));
            }

            // Отбор по типу
            if (!string.IsNullOrEmpty(e))
            {
                // Для категорий с id 12040 разрешаем продажу ФЛ
                bool isSaleIndividuals = productsInOrder.Where(p => p.CategoryID != 12040).ToList().Count() == 0;
                var isIndividual = e == "individual" && !isSaleIndividuals;
                var isEntity = e == "entity";
                q = q.Where(p => (p.EntityFace != null && !isEntity) || (p.IndividualFace != null && (!isIndividual)));
                if (isSaleIndividuals)
                    e = null;
            }

            ViewBag.Contractors = q.ToList();
            ViewBag.Country = country;
            ViewBag.Exclude = e;

            return PartialView("Partial/ContractorsList");
        }

        [HttpGet]
        public async Task<IActionResult> FindContractors([FromQuery] int individual, [FromQuery] int entity, [FromQuery] string country)
        {
            var user = await DbContext.GetUser(User);
            List<UserFace> contractors;

            if (user != null)
            {
                var q = DbContext.Faces
                    .Include(p => p.IndividualFace)
                    .Include(p => p.EntityFace)
                    .Where(p => p.UserId == user.ID);

                // Отбор по стране
                if (country != null && country != "ALL")
                {
                    q = q.Where(p => (p.EntityFace != null && p.EntityFace.Country == country) || (p.IndividualFace != null && p.IndividualFace.Country == country));
                }

                if (individual == 0) q = q.Where(p => p.IndividualFace == null);
                if (entity == 0) q = q.Where(p => p.EntityFace == null);
                contractors = q.ToList();
            }
            else
            {
                contractors = new List<UserFace>();
            }

            return Json(contractors);
        }

        /// <summary>
        /// Получает представление лица
        /// </summary>
        /// <param name="contractorId"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetContractorView([FromQuery] string contractorId)
        {
            if (!string.IsNullOrEmpty(contractorId))
            {
                if (int.TryParse(contractorId, out int id))
                {
                    var user = await DbContext.GetUser(User);
                    var f = DbContext.Faces.Include(p => p.IndividualFace).Include(p => p.EntityFace).FirstOrDefault(p => p.ID == id && p.UserId == user.ID);
                    if (f != null)
                    {
                        var view = "";
                        if (f.EntityFace != null)
                        {
                            var face = f.EntityFace;
                            view = face.FullName;
                        }
                        else if (f.IndividualFace != null)
                        {
                            var face = f.IndividualFace;
                            view = string.Format("{0} {1} {2}", face.LastName, face.FirstName, face.MiddleName);
                        }
                        return Json(view);
                    }
                }
            }
            return Json("");
        }

        [HttpGet]
        public async Task<IActionResult> GetContractor(int id)
        {
            var user = DbContext.GetUser(User);
            var face = await DbContext.Faces.FirstOrDefaultAsync(p => p.UserId == user.Id && p.ID == id);
            return Json(face);
        }

        #endregion

        #region Список заказов клиента

        /// <summary>
        /// Возвращает отфильтрованный список заказов 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> List([FromBody] OrderListFilter filter, [FromServices] IDistributedCache cache, [FromServices] ApplicationCache appCache)
        {
            var user = await DbContext.GetUser(User);

            // Объявляем параметры сериализации
            //var serializerOptions = new System.Text.Json.JsonSerializerOptions
            //{
            //    PropertyNamingPolicy = null,
            //};

            var serializerOptions = new JsonSerializerSettings
            {
                TypeNameAssemblyFormatHandling = TypeNameAssemblyFormatHandling.Full,
            };

            // Проверяем, есть ли данные в кэше
            var cacheKey = USER_ORDERS_DATA + user.ID.ToString();
            var cachedOrders = await appCache.GetAsync<Dictionary<string, List<OrderData>>>(cacheKey) ?? new Dictionary<string, List<OrderData>>();
            if (cachedOrders.ContainsKey(filter.FilterHash)) return Json(cachedOrders[filter.FilterHash], serializerOptions);

            // Загружаем заказы пользователя
            var userOrders = DbContext.Orders.Where(p => p.UserId == user.ID)
                            .Include(p => p.Status)
                            .Include(p => p.OrdererFace)
                                .ThenInclude(p => p.EntityFace)
                                .ThenInclude(p => p.LegalForm)
                            .Include(p => p.OrdererFace)
                                .ThenInclude(p => p.IndividualFace)
                            .Include(p => p.PayerFace)
                                .ThenInclude(p => p.EntityFace)
                            .Include(p => p.PayerFace)
                                .ThenInclude(p => p.IndividualFace)
                            .Include(p => p.ProductReceiver)
                                .ThenInclude(p => p.EntityFace)
                            .Include(p => p.ProductReceiver)
                                .ThenInclude(p => p.IndividualFace)
                            .Include(p => p.DocumentsReceiver)
                                .ThenInclude(p => p.EntityFace)
                            .Include(p => p.DocumentsReceiver)
                                .ThenInclude(p => p.IndividualFace);

            // Загружаем товары заказов
            var ordersWithProducts = from o in userOrders
                                     join product in DbContext.OrderProductItems.Include(p => p.Product) on o.Id equals product.OrderId into Products
                                     from product in Products.DefaultIfEmpty()
                                     orderby o.OrderDate descending
                                     select new OrderData { Order = o, Product = product };

            var deliveryOrders = DbContext.DeliveryOrders.ToList();

            foreach (var item in userOrders)
                item.DeliveryOrder = deliveryOrders.FirstOrDefault(p => p.Id == item.DeliveryOrderId);


            // Фильтр по статусам
            if (!string.IsNullOrEmpty(filter.Status) && filter.Status != "all")
            {
                ordersWithProducts = ordersWithProducts.Where(p => p.Order.Status.FilterStatus == filter.Status);
            }

            // Фильтр по датам
            if (filter.FromDate != DateTime.MinValue) ordersWithProducts = ordersWithProducts.Where(p => p.Order.OrderDate >= filter.FromDate);
            if (filter.ToDate != DateTime.MinValue)
            {
                TimeSpan ts = new TimeSpan(24, 0, 0);
                filter.ToDate = filter.ToDate + ts;
                ordersWithProducts = ordersWithProducts.Where(p => p.Order.OrderDate <= filter.ToDate);
            }

            // Фильтр по наименованию товара       
            if (!string.IsNullOrEmpty(filter.Search))
            {
                var foundedOrders = ordersWithProducts.Where(p => (p.Product != null && p.Product.Product.Name.Contains(filter.Search, StringComparison.InvariantCultureIgnoreCase))).Select(p => p.Order);
                ordersWithProducts = ordersWithProducts.Where(p => foundedOrders.Contains(p.Order));
            }

            // Группируем товары по заказам
            var orderList = (await ordersWithProducts.ToListAsync())
                .GroupBy(p => p.Order)
                .Select(g => new OrderData
                {
                    Order = g.Key,
                    Products = g.First().Product != null ? g.Select(p => p.Product).ToList() : null,
                });




            // Сохраняем данные о заказах в кэш
            cachedOrders[filter.FilterHash] = orderList.ToList();
            await appCache.SetAsync(cacheKey, cachedOrders);

            return Json(orderList, serializerOptions);
        }

        #endregion
    }
}