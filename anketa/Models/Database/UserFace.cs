﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    [Serializable]
    public class UserFace
    {
        public int ID { get; set; }

        public int UserId { get; set; }

        [JsonIgnore]
        public User User { get; set; }

        public UserEntityFace EntityFace { get; set; }

        [JsonIgnore]
        public int? EntityFaceId { get; set; }

        public UserIndividualFace IndividualFace { get; set; }

        [JsonIgnore]
        public int? IndividualFaceId { get; set; }

        /// <summary>
        /// Возвращает представление лица
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (EntityFace != null) return EntityFace.FullName;
            else if (IndividualFace != null) return string.Format("{0} {1} {2}", IndividualFace.LastName, IndividualFace.FirstName, IndividualFace.MiddleName);
            return base.ToString();
        }

        /// <summary>
        /// Получает ИНН контрагента
        /// </summary>
        /// <returns></returns>
        public string GetITIN()
        {
            var itin = "";
            if (EntityFace != null) itin = EntityFace.ITIN;
            else if (IndividualFace != null) itin = IndividualFace.ITIN;
            if (string.IsNullOrWhiteSpace(itin)) itin = "ИНН не указан.";
            return itin;
        }

        /// <summary>
        /// Получает ОГРН контрагента
        /// </summary>
        /// <returns></returns>
        public string GetPSRN()
        {
            if(EntityFace != null) return !string.IsNullOrWhiteSpace(EntityFace.PSRN) ? EntityFace.PSRN : "ОГРН не указан.";
            return "-";
        }

        /// <summary>
        /// Проверяет, является ли контрагент юридическим лицом
        /// </summary>
        /// <returns></returns>
        public bool IsEntity()
        {
            return EntityFace != null;
        }

        /// <summary>
        /// Проверяет, является ли контрагент физическим лицом
        /// </summary>
        /// <returns></returns>
        public bool IsIndividual()
        {
            return IndividualFace != null;
        }

        /// <summary>
        /// Возвращает банковские реквизиты
        /// </summary>
        /// <returns></returns>
        public BankDetails GetBankDetails()
        {
            return IsIndividual() ? IndividualFace.BankDetails ?? null : EntityFace.BankDetails ?? null;
        }

        /// <summary>
        /// Возвращает телефонные номера
        /// </summary>
        /// <returns></returns>
        internal string GetPhones()
        {
            return IsIndividual() ? IndividualFace.Phones ?? null : EntityFace.Phones ?? null;
        }
    }
}
