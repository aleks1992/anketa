﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Policy;
using System.Threading.Tasks;
using static anketa.Misc.OAuthAPI.OAuthProvider;

namespace anketa.Misc.OAuthAPI
{
    public class OAuthProviders
    {
        /// <summary>
        /// Общий URL, куда будет перенаправляться клиент
        /// </summary>
        private string RedirectUri;

        /// <summary>
        /// Список провайдеров OAuth и их конфиги
        /// </summary>
        private Dictionary<Type, SocialData> Providers = new Dictionary<Type, SocialData>();

        /// <summary>
        /// Порядок провайдеров для отображения
        /// </summary>
        private List<string> ProviderOrder;

        public OAuthProviders(Microsoft.AspNetCore.Http.IHttpContextAccessor httpContextAccessor, LinkGenerator linkGenerator, IConfiguration configuration)
        {
            RedirectUri = linkGenerator.GetUriByAction(httpContextAccessor.HttpContext, "LoginSocial", "Account");

            // Читаем конфигурацию
            var socialDatas = new List<SocialData>();
            configuration.Bind("SocialNetwork", socialDatas);

            ProviderOrder = socialDatas.Select(p => p.Name).ToList();

            // Ищем провайдеров, которые можно использовать
            var list = Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(p => p.Namespace == GetType().Namespace && p.IsSubclassOf(typeof(OAuthProvider)));

            foreach (var t in list)
            {
                string name = t.Name.Replace("OAuthProvider", "");
                var socialConfig = socialDatas.FirstOrDefault(p => p.Name == name);
                if (socialConfig != null) Providers.Add(t, socialConfig);
            }
        }

        /// <summary>
        /// Возвращает список провайдеров
        /// </summary>
        /// <param name="state">Состояние сессии</param>
        /// <returns></returns>
        public object[] GetList(string state)
        {
            var data = Providers.Select(p =>
            {
                var provider = OAuthProvider.GetProvider(p.Key, p.Value, RedirectUri, state);
                return new
                {
                    Name = provider.ProviderName,
                    Url = provider.GetUri(OAuthProvider.OAuthMethod.GetCode),
                    Image = provider.GetImageUri(),
                };
            }).OrderBy(p => ProviderOrder.IndexOf(p.Name));

            return data.ToArray();
        }

        /// <summary>
        /// Получает провайдера по имени
        /// </summary>
        /// <param name="providerName">Имя провайдера</param>
        /// <param name="state">Состояние сессии</param>
        /// <returns></returns>
        public OAuthProvider GetProviderByName(string providerName, string state)
        {
            var providerData = Providers.FirstOrDefault(p => p.Key.Name.Replace("OAuthProvider", "") == providerName);
            return providerData.Key != null ? OAuthProvider.GetProvider(providerData.Key, providerData.Value, RedirectUri, state) : null;
        }
        
        /// <summary>
        /// Создает объект состояния
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public string MakeState(Action<OAuthState> options)
        {
            OAuthState state = new OAuthState();
            options(state);
            return DataSecurity.EncodeBase64(JsonConvert.SerializeObject(state));

        }

        /// <summary>
        /// Парсит состояние сессии
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        public OAuthState ParseState(string state)
        {
            return state != null ? JsonConvert.DeserializeObject<OAuthState>(DataSecurity.DecodeBase64(state)) : null;
        }
    }
}
