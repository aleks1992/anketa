﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.Captcha
{
    public static class CaptchaServiceExtention
    {
        /// <summary>
        /// Добавляет метод в коллекцию сервисов
        /// </summary>
        /// <param name="services"></param>
        /// <param name="setup"></param>
        public static void AddCaptcha(this IServiceCollection services, Action<CaptchaOptions> setup)
        {
            var settings = new CaptchaOptions();
            setup(settings);

            if (settings.CaptchaService != null)
            {
                ICaptchaService instance = (ICaptchaService)Activator.CreateInstance(settings.CaptchaService);
                instance.SetConfiguration(services, settings.Configuration);
                services.AddSingleton(typeof(ICaptchaService), instance);
            }
            else throw new Exception("Captcha service implementation is not set.");
        }

        /// <summary>
        /// Добавляет метод в HtmlHelper
        /// </summary>
        /// <param name="htmlHelper"></param>
        /// <returns></returns>
        public static HtmlString Captcha(this IHtmlHelper htmlHelper)
        {
            // Ищем ICaptchaService
            var service = htmlHelper.ViewContext.HttpContext.RequestServices.GetService(typeof(ICaptchaService));
            if (service != null)
            {
                return ((ICaptchaService)service).RenderCaptcha(htmlHelper);
            }
            return null;
        }
    }
}
