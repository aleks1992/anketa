﻿using anketa.Models.Database;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using static anketa.Controllers.UserTicketsController;

namespace anketa.Misc
{
    /// <summary>
    /// Работа с удаленными сайтами
    /// </summary>
    public class SiteClient
    {


        /// <summary>
        /// Отправляет HTTP-запрос
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="url"></param>
        /// <param name="method"></param>
        /// <param name="contentType"></param>
        /// <param name="inBackground"></param>
        /// <param name="inAnswerResponse"></param>
        /// <param name="headers"></param>
        /// <returns></returns>
        public static ServerResponseItemData HttpClient(object obj, string url, string method = "POST", string contentType = "application/json", bool inBackground = false, bool inAnswerResponse = true, Dictionary<string,string> headers = null)
        {
            var action = new Func<ServerResponseItemData>(() =>
            {
                var response = new ServerResponseItemData
                {
                    Success = false
                };

                try
                {
                    var httpRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpRequest.Method = method;
                    httpRequest.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; 
                    httpRequest.ContentType = contentType;
                    if (headers != null) foreach (var header in headers) httpRequest.Headers.Add(header.Key, header.Value);

                    if (method == "POST")
                    {
                        if (contentType == "application/json")
                        {
                            using (var requestStream = httpRequest.GetRequestStream())
                            using (var writer = new StreamWriter(requestStream))
                                writer.Write(JsonConvert.SerializeObject(obj).ToString());
                        }
                        else
                        {
                            using (var requestStream = httpRequest.GetRequestStream())
                            using (var writer = new StreamWriter(requestStream))
                                writer.Write(GetQueryString(obj));

                        }
                    }

                    using (var httpResponse = httpRequest.GetResponse())
                    using (var responseStream = httpResponse.GetResponseStream())
                    using (var reader = new StreamReader(responseStream))
                    {
                        var json = reader.ReadToEnd();
                        if (inAnswerResponse) response = JsonConvert.DeserializeObject<ServerResponseItemData>(json);
                        else
                        {
                            response.Data = json;
                            response.Message = "result";
                            response.Success = true;
                        }

                    }
                }
                catch (WebException e)
                {
                    response.Message = e.ToString();
                }

                return response;
            });

            var task = new Task<ServerResponseItemData>(action);

            if (inBackground)
            {
                // Выполняем запрос в фоне
                task.Start();
                return null;
            }

            // Иначе выполняет запрос синхронно
            task.RunSynchronously();
            return task.Result;
        }

        /// <summary>
        /// Конвертирует Объект в Get параметры
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string GetQueryString(object obj, string separator = "&", string[] exceptFields = null)
        {
            if (exceptFields == null) exceptFields = new string[0];

            var properties = from p in obj.GetType().GetProperties()
                             where p.GetValue(obj, null) != null && !exceptFields.Contains(p.Name)
                             orderby p.Name
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());
            return string.Join(separator, properties.ToArray());
        }

        /// <summary>
        /// Ответ от Сервера
        /// </summary>
        public class ServerResponseItemData
        {
            public ServerResponseItemData()
            {
                Success = false;
            }

            /// <summary>
            /// Успешное удаления товара из корзины
            /// </summary>
            public bool Success { get; set; }

            /// <summary>
            /// Сообщения об ошибки удаления
            /// </summary>
            public string Message { get; set; }

            /// <summary>
            /// Данные 
            /// </summary>
            public object Data { get; set; }
        }
    }
}
