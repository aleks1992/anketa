﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc.AttributeMetadata
{
    /// <summary>
    /// Представляет собой произвольный пользовательский HTML атрибут
    /// </summary>
    public sealed class HTMLAttributeAttribute : ValidationAttribute, IClientModelValidator
    {
        /// <summary>
        /// Имя атрибута
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; private set; }

        /// <summary>
        /// Создает пользовательский HTML атрибут
        /// </summary>
        /// <param name="name">Имя атрибута</param>
        /// <param name="value">Значение</param>
        public HTMLAttributeAttribute(string Name, string Value)
        {
            if (Name == null) throw new ArgumentNullException(nameof(Name));
            this.Name = Name;
            this.Value = Value;            
        }

        public void AddValidation(ClientModelValidationContext context)
        {
            MergeAttribute(context.Attributes, Name, Value);
        }

        /// <summary>
        /// Добавляет или обновляет значение в словаре
        /// </summary>
        /// <param name="attributes"></param>
        /// <param name="key"></param>
        /// <param name="val"></param>
        private void MergeAttribute(IDictionary<string, string> attributes, string key, string val)
        {
            if (attributes.ContainsKey(key)) attributes[key] = val;
            else attributes.Add(key, val);
        }

        /// <summary>
        /// Заглушка для проверки валидности
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            return true;
        }
    }
}
