﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class updateWishlistItems : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserWishlistItems_PriceTypes_PriceTypeId",
                table: "UserWishlistItems");

            migrationBuilder.DropIndex(
                name: "IX_UserWishlistItems_PriceTypeId",
                table: "UserWishlistItems");

            migrationBuilder.DropColumn(
                name: "PriceTypeId",
                table: "UserWishlistItems");

            migrationBuilder.DropColumn(
                name: "SerialNumber",
                table: "UserWishlistItems");

            migrationBuilder.DropColumn(
                name: "StockId",
                table: "UserWishlistItems");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PriceTypeId",
                table: "UserWishlistItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SerialNumber",
                table: "UserWishlistItems",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StockId",
                table: "UserWishlistItems",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserWishlistItems_PriceTypeId",
                table: "UserWishlistItems",
                column: "PriceTypeId");

            migrationBuilder.AddForeignKey(
                name: "FK_UserWishlistItems_PriceTypes_PriceTypeId",
                table: "UserWishlistItems",
                column: "PriceTypeId",
                principalTable: "PriceTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
