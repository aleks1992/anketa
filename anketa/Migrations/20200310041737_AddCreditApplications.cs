﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class AddCreditApplications : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CreditApplications",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UserId = table.Column<int>(nullable: false),
                    BorrowerName = table.Column<string>(nullable: false),
                    PassportSerial = table.Column<string>(nullable: false),
                    PassportNumber = table.Column<string>(nullable: false),
                    CreditSum = table.Column<int>(nullable: false),
                    FirstPay = table.Column<int>(nullable: false),
                    CreditPeriod = table.Column<int>(nullable: false),
                    BorrowerEmail = table.Column<string>(nullable: false),
                    BorrowerPhone = table.Column<string>(nullable: false),
                    Comment = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreditApplications", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CreditApplications_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CreditApplicationProducts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CreditApplicationId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreditApplicationProducts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CreditApplicationProducts_CreditApplications_CreditApplicati~",
                        column: x => x.CreditApplicationId,
                        principalTable: "CreditApplications",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CreditApplicationProducts_CreditApplicationId",
                table: "CreditApplicationProducts",
                column: "CreditApplicationId");

            migrationBuilder.CreateIndex(
                name: "IX_CreditApplications_UserId",
                table: "CreditApplications",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CreditApplicationProducts");

            migrationBuilder.DropTable(
                name: "CreditApplications");
        }
    }
}
