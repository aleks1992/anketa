﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Database
{
    public class ServiceUserPermission
    {
        /// <summary>
        /// Ид разрешения
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Ид пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public ServiceUser User { get; set; }

        /// <summary>
        /// Разрешение
        /// </summary>
        public string Permission { get; set; }
    }
}
