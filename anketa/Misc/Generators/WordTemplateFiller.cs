﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace anketa.Misc.Generators
{
    /// <summary>
    /// Класс для взаимодействия с документами Word
    /// </summary>
    public class WordTemplateFiller : IDisposable
    {
        private ZipArchive zipArchive;
        private Stream zipArchiveEntryStream;
        public XmlDocument Document;
        private XmlNamespaceManager nsmgr;

        public WordTemplateFiller(Stream fileStream)
        {
            zipArchive = new ZipArchive(fileStream, ZipArchiveMode.Update, true);
            var entry = zipArchive.GetEntry("word/document.xml");

            zipArchiveEntryStream = entry.Open();
            Document = new XmlDocument();
            Document.Load(zipArchiveEntryStream);

            // Устанавливаем пространства имен
            nsmgr = new XmlNamespaceManager(Document.NameTable);
            nsmgr.AddNamespace("ns", "http://schemas.openxmlformats.org/package/2006/relationships");
            nsmgr.AddNamespace("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
            nsmgr.AddNamespace("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
        }

        /// <summary>
        /// Вызывается при разрушении объекта
        /// </summary>
        public void Dispose()
        {
            zipArchiveEntryStream.Seek(0, SeekOrigin.Begin);
            Document.Save(zipArchiveEntryStream);

            zipArchiveEntryStream.Close();
            zipArchive.Dispose();
        }

        /// <summary>
        /// Получает закладку
        /// </summary>
        /// <param name="name">Имя закладки</param>
        /// <returns></returns>
        public WordBookmark GetBookmark(string name)
        {
            var bookmark = new WordBookmark(this, Document, nsmgr, name);
            return bookmark;
        }

        /// <summary>
        /// Вставляет текст в таблицу по закладке
        /// </summary>
        /// <param name="bookmark"></param>
        /// <param name="text"></param>
        public void InsertTextToTable(WordBookmark bookmark, string text)
        {
            if (text == null) text = "";
            string uri = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";

            var lines = text.Split('\n').Reverse().ToList();
            if(lines.Count() > 1)
            {
                int index = lines.Count - 1;
                var l = lines[index];
                lines.RemoveAt(index);
                lines.Insert(0, l);
            }

            int line = 0;
            foreach (var s in lines)
            {
                // Блок с текстом
                XmlNode r = Document.CreateElement("w:r", uri);
                var rPr = r.AppendChild(Document.CreateElement("w:rPr", uri));
                var el = rPr.AppendChild(Document.CreateElement("w:sz", uri));
                el.Attributes.Append(Document.CreateAttribute("w:val", uri)).Value = "28";

                el = rPr.AppendChild(Document.CreateElement("w:szCs", uri));
                el.Attributes.Append(Document.CreateAttribute("w:val", uri)).Value = "28";

                el = r.AppendChild(Document.CreateElement("w:t", uri));
                el.InnerText = s;

                if (line == 0)
                {
                    bookmark.BookmarkStart.ParentNode.InsertAfter(r, bookmark.BookmarkStart);
                }
                else
                {
                    // Копируем узел
                    XmlNode paragraph = bookmark.BookmarkStart.ParentNode.Clone();

                    // Удаляем закладки
                    var node = paragraph.SelectSingleNode(".//w:bookmarkStart", nsmgr);
                    if (node != null) paragraph.RemoveChild(node);

                    node = paragraph.SelectSingleNode(".//w:bookmarkEnd", nsmgr);
                    if (node != null) paragraph.RemoveChild(node);

                    node = paragraph.SelectSingleNode(".//w:r", nsmgr);
                    if (node != null) paragraph.RemoveChild(node);

                    paragraph.AppendChild(r);
                    bookmark.BookmarkStart.ParentNode.ParentNode.InsertAfter(paragraph, bookmark.BookmarkStart.ParentNode);
                }

                line++;
            }
        }

        /// <summary>
        /// Ищет таблицу по вложенной закладке
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public WordTable SearchTable(string name)
        {
            var table = new WordTable(Document, nsmgr, name);
            if (table.IsValid) return table;
            return null;
        }

        /// <summary>
        /// Описывает таблицу в документе
        /// </summary>
        public class WordTable
        {
            private readonly XmlDocument doc;
            private readonly XmlNamespaceManager nsmgr;

            public XmlNode Table { get; }

            public WordTable(XmlDocument doc, XmlNamespaceManager nsmgr, string name)
            {
                this.doc = doc;
                this.nsmgr = nsmgr;

                var bookmark = doc.SelectSingleNode($"//w:bookmarkStart[@w:name=\"{name}\"]", nsmgr);
                ref XmlNode element = ref bookmark;
                while (element != null && element.Name != "w:tbl")
                {
                    element = bookmark.ParentNode;
                }

                if (element != null) Table = element;
            }

            /// <summary>
            /// Копирует строку
            /// </summary>
            /// <param name="index">Индекс строки</param>
            /// <param name="count">Сколько копий нужно</param>
            public void DuplicateRow(int index, int count)
            {
                var nodes = Table.SelectNodes("./w:tr", nsmgr);
                if (nodes.Count <= index) return;

                // Ищем максимальный идентификатор
                int id = 0;
                var bookmarks = Table.SelectNodes("//w:bookmarkStart/@w:id", nsmgr);
                foreach (XmlNode b in bookmarks)
                {
                    int p = 0;
                    if (int.TryParse(b.Value, out p) && id < p) id = p;
                }
                id++;

                for (int i = 0; i < count; i++)
                {
                    var clonedNode = nodes[index].CloneNode(true);

                    var bookmark = clonedNode.SelectSingleNode(".//w:bookmarkStart", nsmgr);
                    if (bookmark != null) bookmark.Attributes.GetNamedItem("w:id").Value = (id).ToString();

                    bookmark = clonedNode.SelectSingleNode(".//w:bookmarkEnd", nsmgr);
                    if (bookmark != null) bookmark.Attributes.GetNamedItem("w:id").Value = (id++).ToString();

                    Table.AppendChild(clonedNode);
                }
            }

            /// <summary>
            /// Устанавливает текст строки по закладке
            /// </summary>
            /// <param name="name">Имя закладки</param>
            /// <param name="index">Индекс строки с закладкой</param>
            /// <param name="text">Текст</param>
            public void SetTextRow(string name, int index, string text)
            {
                var nodes = Table.SelectNodes($".//w:bookmarkStart[@w:name=\"{name}\"]", nsmgr);
                if (nodes.Count <= index) return;

                var node = nodes[index];
                string uri = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";

                XmlNode r = doc.CreateElement("w:r", uri);
                var rPr = r.AppendChild(doc.CreateElement("w:rPr", uri));
                var el = rPr.AppendChild(doc.CreateElement("w:sz", uri));
                el.Attributes.Append(doc.CreateAttribute("w:val", uri)).Value = "28";

                el = rPr.AppendChild(doc.CreateElement("w:szCs", uri));
                el.Attributes.Append(doc.CreateAttribute("w:val", uri)).Value = "28";

                el = r.AppendChild(doc.CreateElement("w:t", uri));
                el.InnerText = text;

                node.ParentNode.InsertAfter(r, node);
            }

            /// <summary>
            /// Устанавливает текст строки по закладкам
            /// </summary>
            /// <param name="index">Индекс строки с закладками</param>
            /// <param name="list">Список закладок с текстами</param>
            public void SetTextRow(int index, Dictionary<string, string> list)
            {
                foreach (var l in list) SetTextRow(l.Key, index, l.Value);
            }

            /// <summary>
            /// Показывает, что таблица валидная
            /// </summary>
            public bool IsValid
            {
                get
                {
                    return Table != null;
                }
            }
        }

        /// <summary>
        /// Описывает закладку
        /// </summary>
        public class WordBookmark
        {
            private readonly WordTemplateFiller filler;
            public XmlNode BookmarkStart { get; }
            public XmlNode BookmarkEnd { get; }

            public WordBookmark(WordTemplateFiller filler, XmlDocument doc, XmlNamespaceManager nsmgr, string name)
            {
                this.filler = filler;
                BookmarkStart = doc.SelectSingleNode($"//w:bookmarkStart[@w:name=\"{name}\"]", nsmgr);
                if(BookmarkStart != null) BookmarkEnd = doc.SelectSingleNode($"//w:bookmarkEnd[@w:id=\"{BookmarkStart.Attributes.GetNamedItem("w:id").Value}\"]", nsmgr);
            }

            public void InsertTextAfter(string text)
            {
                if(IsValid) filler.InsertTextToTable(this, text);
            }

            /// <summary>
            /// Показывает, что закладка валидная
            /// </summary>
            public bool IsValid
            {
                get
                {
                    return BookmarkStart != null && BookmarkEnd != null;
                }
            }

            internal void InsertTextAfter(Func<string> p)
            {
                if (IsValid) filler.InsertTextToTable(this, (string)p.DynamicInvoke());
            }
        }
    }
}
