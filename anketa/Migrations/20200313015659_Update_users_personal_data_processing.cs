﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace anketa.Migrations
{
    public partial class Update_users_personal_data_processing : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "ConsentToNewsletter",
                table: "Users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "PersonalDataProcessing",
                table: "Users",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData("Users", "ConsentToNewsletter", null, "ConsentToNewsletter", true);
            migrationBuilder.UpdateData("Users", "ConsentToNewsletter", null, "ConsentToNewsletter", true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ConsentToNewsletter",
                table: "Users");

            migrationBuilder.DropColumn(
                name: "PersonalDataProcessing",
                table: "Users");
        }
    }
}
