﻿using anketa.Models.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Models.Application.Forms
{
    public class OrderForm
    {
        /// <summary>
        /// ID заказа
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// ID пользователя
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public User User { get; set; }

        /// <summary>
        /// Данные заказа
        /// </summary>
        [Display(Name = "Дополнительная информация о заказе")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Заполните информацию о заказе.")]
        public string OrderData { get; set; }

        /// <summary>
        /// Ид заказчика
        /// </summary>
        [Display(Name = "Заказчик")]
        [Required(ErrorMessage = "Укажите заказчика.")]
        public int OrdererId { get; set; }

        /// <summary>
        /// Информация о заказчике
        /// </summary>
        [Display(Name = "Дополнительная информация о заказчике")]
        public string OrdererInformation { get; set; }

        /// <summary>
        /// Ид плательщика
        /// </summary>
        [Display(Name = "Плательщик")]
        [Required(ErrorMessage = "Укажите плательщика.")]
        public int PayerId { get; set; }

        /// <summary>
        /// Информация о плательщике
        /// </summary>
        [Display(Name = "Дополнительная информация о плательщике")]
        public string PayerInformation { get; set; }

        /// <summary>
        /// Ид получателя товара
        /// </summary>
        [Display(Name = "Получатель товара")]
        [Required(ErrorMessage = "Укажите получателя товара.")]
        public int ProductReceiverId { get; set; }

        /// <summary>
        /// Ид адреса доставки товара
        /// </summary>
        [Display(Name = "Адрес доставки товара")]
        [Required(ErrorMessage = "Укажите адрес доставки товара.")]
        public int ProductReceiverAddressId { get; set; }

        /// <summary>
        /// Ид получателя документов
        /// </summary>
        [Display(Name = "Получатель документов")]
        [Required(ErrorMessage = "Укажите получателя документов.")]
        public int DocumentsReceiverId { get; set; }

        /// <summary>
        /// Ид адреса доставки документов
        /// </summary>
        [Display(Name = "Адрес доставки документов")]
        [Required(ErrorMessage = "Укажите адрес доставки товара.")]
        public int DocumentsReceiverAddressId { get; set; }

        /// <summary>
        /// Информация о получателе товара
        /// </summary>
        [Display(Name = "Дополнительная информация о получателе товара")]
        public string ProductReceiverInformation { get; set; }

        /// <summary>
        /// Информация о получателе документов
        /// </summary>
        [Display(Name = "Дополнительная информация о получателе документов")]
        public string DocumentsReceiverInformation { get; set; }

        /// <summary>
        /// Показывает, что получатель документов это заказчик
        /// </summary>
        [Display(Name = "Получатель документов совпадает с заказчиком")]
        public bool DocumentReceiverIsOrderer { get; set; }

        /// <summary>
        /// Показывает, что получатель товара это заказчик
        /// </summary>
        [Display(Name = "Получатель товара совпадает с заказчиком")]
        public bool ProductReceiverIsOrderer { get; set; }

    }
}
