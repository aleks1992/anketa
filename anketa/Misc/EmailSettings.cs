﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace anketa.Misc
{
    public class EmailSettings
    {
        /// <summary>
        /// Отправитель
        /// </summary>
        public string FromName { get; set; }

        /// <summary>
        /// Ящик отправителя
        /// </summary>
        public string FromEmail { get; set; }

        /// <summary>
        /// Хост
        /// </summary>
        public string SmtpHost { get; set; }

        /// <summary>
        /// Порт
        /// </summary>
        public int SmtpPort { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Использовать шифрование
        /// </summary>
        public bool UseSsl { get; set; }
    }
}
